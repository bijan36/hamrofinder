<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Mero google</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/coconut.css" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>






        <header>

            <div class="container">
                <div class="top-head">
                    <div class="row">
                        <div class="col-md-6"></div>
                        <div class="col-md-6">
                            <div class="right-nav">
                                <ul>
                                    <li id="loginToggle"><a href="" id="loginToggle">Login</a></li>
                                    <li id="registerToggle"><a href="">Register</a></li>
                                </ul>


                                <!--LOGIN BOX-->
                                <div id="loginBox" style="display: none;">
                                    <h3>Login</h3>
                                    <div class="row">

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="emailAddress">Email Address</label>
                                                <input type="email" class="form-control" value="">
                                            </div>
                                            <div class="form-group">
                                                <label for="passCode">Password</label>
                                                <input type="password" class="form-control" value="">
                                            </div>
                                            <div class="form-group">
                                                <button type="submit" class="blueButton">Sign In</button>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <hr/>
                                        </div>


                                        <div class="col-md-12">
                                            <h3>Not a memeber yet?</h3>

                                            <a href="" class="btn yelloButton">Register For Free</a>

                                            <p>
                                                When you registered, you will gain access to
                                            </p>

                                            Place you ad<br/>
                                            Save you ad<br/>
                                            Contact Advertise<br/>
                                            Email Address<br/>

                                        </div>
                                    </div>
                                </div>
                                <!--LOGIN BOX-->

                                <!--REGISTERED-->
                                <div id="registerBox" style="display: none;">
                                    <h3>Register</h3>
                                    <div class="row">

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="searchtype">Please select if you are looking for</label>
                                                <select name="searchtype" class="form-control">
                                                    <option value="student">STUDENT</option>
                                                    <option value="college">COLLEGE</option>
                                                </select>

                                            </div>
                                        </div>    



                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name">Name:</label>
                                                <input type="text" name="name" class="form-control" value="">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="address">Address:</label>
                                                <input type="text" name="address" class="form-control" value="">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="contact_no">Contact No.:</label>
                                                <input type="text" name="contact_no" class="form-control" value="">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="emailAddress">Email Address:</label>
                                                <input type="email" name="emailAddress" class="form-control" value="">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="passCode">Password:</label>
                                                <input type="password" name="password" class="form-control" value="">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="passCodeConfirm">Confirm Password:</label>
                                                <input type="password" name="passCodeConfirm" class="form-control" value="">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <button type="submit" class="blueButton">Sign Up</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--REGISTERED-->
                        </div>
                    </div>
                </div>
            
            
            
            <div class="head">
                <div class="row">
                    <div class="col-md-6">
                        <div class="logo">
                            <img src="img/logo.png" title="Mero Google">
                        </div>                                
                    </div>
                    <div class="col-md-6"></div>
                </div>

            </div>
            <div class="main-nav">
                <ul>
                    <li><a href="">Home</a></li>
                    <li><a href="">About Us</a></li>
                </ul>
            </div>
        </div>


    </header>








    <section>

        <div class="container">

            <div class="main white-bg standard-padding">
                <div class="row">



                    <div class="col-md-8">


                        <div class="blue-bg standard-padding round-border">


                            <div class="hero-text standard-padding text-center">
                                We help you find out various colleges/ students of your choice
                            </div>

                            <div class="text-center standard-padding">
                                <input type="radio" name="destination" value="nepal" checked> Nepal 
                                <input type="radio" name="destination" value="abroad"> Abroad 
                            </div>

                            <div class="hero-text-medium standard-padding text-center">
                                Start your search now
                            </div>

                            <div class="hero-text-medium text-center">
                                <input type="text" class="form-control input-lg text-center" name="search-key-word" placeholder="Area, Name of College, Course, Free">
                            </div>

                            <div class="text-center standard-padding">
                                <input type="radio" name="parameter" value="college_ad" checked> College Ad Search 
                                <input type="radio" name="parameter" value="student_ad"> Student Ad Search
                            </div>

                            <div class="text-center">
                                <button class="btn yelloButton center-block">Search Now</button>
                            </div>

                            <div class="hero-text-medium standard-padding text-center">
                                <a href="#">
                                    Advance Search
                                </a>
                            </div>

                        </div>


                    </div>



                    <div class="col-md-4">

                        <div class="ad-box-round-corner round-border">
                            <a href="#">
                                <img src="img/placead.png">
                            </a>
                        </div>

                        <div class="ad-box-round-corner round-border">
                            <a href="#">
                                <img src="img/placeadcollege.png">
                            </a>
                        </div>

                        <div class="ad-box-round-corner round-border">
                            <a href="#">
                                <img src="img/paidad.png">
                            </a>
                        </div>

                    </div>



                </div>
            </div>






        </div>

    </section>







    <section>
        <div class="container">
            <div class="white-bg standard-padding">

                <h2>Feature Students</h2>


                <div class="row">


                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="student-box round-border-with-border grey-bg">
                            <div class="row no-gutter">
                                <div class="col-md-3">
                                    <img src="img/student.png" class="img-responsive">
                                </div>
                                <div class="col-md-9">
                                    <table class="table">
                                        <tr>

                                            <td style="border-top: none;">
                                                <span class="bold-uppercase-blue">BISKSH BAHTTARI</span>
                                                <br/>
                                                <span class="text-uppercase">Chabahil, Kathmandu</span>
                                            </td>

                                            <td style="border-top: none;">
                                                <div class="row no-gutter">
                                                    <div class="col-md-4">
                                                        <button class="blueCompact-small">Save</button>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <button class="blueCompact-small">Interest: 50</button>
                                                    </div>
                                                </div>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td>Gender: <span class="bold-uppercase">Male</span></td>
                                            <td>Looking for: <span class="bold-uppercase">bhm</span></td>
                                        </tr>
                                        <tr>
                                            <td>Grade: <span class="bold-uppercase">60-90%</span></td>
                                            <td>Affiliated: <span class="bold-uppercase">tu</span></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">Looking for The course Fee: <span class="bold-uppercase">2,50,000 - 3,00,000</span></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">Admission within: <span class="bold-uppercase">10 Days</span></td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-12">
                                    <div class="student-box-message">
                                        Hello college, I am 26 years old matured student. Hello college, I am 26 years old matured student. Hello college, I am 26 years old matured student. Hello college.
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <button class="btn yellowCompact btn-block top-5">Show Interest</button>
                                </div>
                                <div class="col-md-4">
                                    <button class="btn yellowCompact btn-block top-5">Make Offer</button>
                                </div>
                                <div class="col-md-4">
                                    <button class="btn yellowCompact btn-block top-5">More Info</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="student-box round-border-with-border grey-bg">
                            <div class="row no-gutter">
                                <div class="col-md-3">
                                    <img src="img/student.png" class="img-responsive">
                                </div>
                                <div class="col-md-9">
                                    <table class="table">
                                        <tr>
                                            <td style="border-top: none;">
                                                <span class="bold-uppercase-blue">BISKSH BAHTTARI</span>
                                                <br/>
                                                <span class="text-uppercase">Chabahil, Kathmandu</span>
                                            </td>
                                            <td style="border-top: none;">
                                                <div class="row no-gutter">
                                                    <div class="col-md-4">
                                                        <button class="blueCompact-small">Save</button>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <button class="blueCompact-small">Interest: 50</button>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Gender: <span class="bold-uppercase">Male</span></td>
                                            <td>Looking for: <span class="bold-uppercase">bhm</span></td>
                                        </tr>
                                        <tr>
                                            <td>Grade: <span class="bold-uppercase">60-90%</span></td>
                                            <td>Affiliated: <span class="bold-uppercase">tu</span></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">Looking for The course Fee: <span class="bold-uppercase">2,50,000 - 3,00,000</span></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">Admission within: <span class="bold-uppercase">10 Days</span></td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-12">
                                    <div class="student-box-message">
                                        Hello college, I am 26 years old matured student. Hello college, I am 26 years old matured student. Hello college, I am 26 years old matured student. Hello college.
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <button class="btn yellowCompact btn-block top-5">Show Interest</button>
                                </div>
                                <div class="col-md-4">
                                    <button class="btn yellowCompact btn-block top-5">Make Offer</button>
                                </div>
                                <div class="col-md-4">
                                    <button class="btn yellowCompact btn-block top-5">More Info</button>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="student-box round-border-with-border grey-bg">
                            <div class="row no-gutter">
                                <div class="col-md-3">
                                    <img src="img/student.png" class="img-responsive">
                                </div>
                                <div class="col-md-9">
                                    <table class="table">
                                        <tr>
                                            <td style="border-top: none;">
                                                <span class="bold-uppercase-blue">BISKSH BAHTTARI</span>
                                                <br/>
                                                <span class="text-uppercase">Chabahil, Kathmandu</span>
                                            </td>
                                            <td style="border-top: none;">
                                                <div class="row no-gutter">
                                                    <div class="col-md-4">
                                                        <button class="blueCompact-small">Save</button>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <button class="blueCompact-small">Interest: 50</button>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Gender: <span class="bold-uppercase">Male</span></td>
                                            <td>Looking for: <span class="bold-uppercase">bhm</span></td>
                                        </tr>
                                        <tr>
                                            <td>Grade: <span class="bold-uppercase">60-90%</span></td>
                                            <td>Affiliated: <span class="bold-uppercase">tu</span></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">Looking for The course Fee: <span class="bold-uppercase">2,50,000 - 3,00,000</span></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">Admission within: <span class="bold-uppercase">10 Days</span></td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-12">
                                    <div class="student-box-message">
                                        Hello college, I am 26 years old matured student. Hello college, I am 26 years old matured student. Hello college, I am 26 years old matured student. Hello college.
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <button class="btn yellowCompact btn-block top-5">Show Interest</button>
                                </div>
                                <div class="col-md-4">
                                    <button class="btn yellowCompact btn-block top-5">Make Offer</button>
                                </div>
                                <div class="col-md-4">
                                    <button class="btn yellowCompact btn-block top-5">More Info</button>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>



            </div>
        </div>
    </section>










    <footer>

        <div class="container">

            <div class="row">

                <div class="col-md-9">


                    <div class="row">
                        <div class="col-md-3">
                            <div class="footer-list">
                                <h2>About Us</h2>
                                <ul>
                                    <li><a href="">Company Establishment</a></li>
                                    <li><a href="">Message from CEO</a></li>
                                    <li><a href="">News & Updates</a></li>
                                    <li><a href="">Photogallery</a></li>
                                    <li><a href="">Help</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="footer-list">
                                <h2>About Us</h2>
                                <ul>
                                    <li><a href="">Company Establishment</a></li>
                                    <li><a href="">Message from CEO</a></li>
                                    <li><a href="">News & Updates</a></li>
                                    <li><a href="">Photogallery</a></li>
                                    <li><a href="">Help</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="footer-list">
                                <h2>About Us</h2>
                                <ul>
                                    <li><a href="">Company Establishment</a></li>
                                    <li><a href="">Message from CEO</a></li>
                                    <li><a href="">News & Updates</a></li>
                                    <li><a href="">Photogallery</a></li>
                                    <li><a href="">Help</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="footer-list">
                                <h2>About Us</h2>
                                <ul>
                                    <li><a href="">Company Establishment</a></li>
                                    <li><a href="">Message from CEO</a></li>
                                    <li><a href="">News & Updates</a></li>
                                    <li><a href="">Photogallery</a></li>
                                    <li><a href="">Help</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="col-md-3">
                    <div class="ad-box-round-corner round-border facebook-banner">
                        <img src="img/facebook.png" class="img-responsive">
                    </div>
                </div>



                <div class="col-md-12">
                    <div class="footer-border"></div>
                </div>



                <div class="col-md-6">
                    <div class="footer-logo">
                        <img src="img/footer-logo.png" class="img-responsive">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="copy">
                        all rights reserved by ktmcollege.com | &copy <?php echo date('Y') ?> | <a href="http://coconutcreation.com" target="_balnk">Powered By: coocnutCreation</a> 
                    </div>
                </div>


            </div>

        </div>

    </footer>











    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
</body>
</html>