//USER REGISTER PROCESSER
$(document).ready(function () {
    //var baseURL = "http://localhost/hamrofinder/";
    var baseURL = "https://hamrofinder.com/";
    var request;
    $("#registeration-form").submit(function (event) {
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var $form = $(this);
        var $inputs = $form.find("input, select, button, textarea");
        var serializedData = $form.serialize();
        $inputs.prop("disabled", true);

        request = $.ajax({
            url: $(this).attr('action'),
            type: "post",
            data: serializedData
        });
        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('.showMsg').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
                $('#registeration-form')[0].reset();
            } else {
                $.LoadingOverlay("hide");
                $('.showMsg').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
        request.always(function () {
            $inputs.prop("disabled", false);
        });
        event.preventDefault();
    });

    //user login form
    $("#userLogin").submit(function (event) {

        if (request) {
            request.abort();
        }
        event.preventDefault();
        $.LoadingOverlay("show");
        var $form = $(this);
        var $inputs = $form.find("input, select, button, textarea");
        var serializedData = $form.serialize();
        $inputs.prop("disabled", true);

        request = $.ajax({
            url: $(this).attr('action'),
            type: "post",
            data: serializedData
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('.showMsg').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
                window.location.href = baseURL + "dashboard";
                return false;
            } else {
                $.LoadingOverlay("hide");
                $('.showMsg').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
                $('#userLogin')[0].reset();
                $inputs.prop("disabled", false);
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
            $inputs.prop("disabled", false);
        });

    });
    //RECOVER PASSWORD
    $("#userRecover").submit(function (event) {
        if (request) {
            request.abort();
        }
        event.preventDefault();
        $.LoadingOverlay("show");
        var $form = $(this);
        var $inputs = $form.find("input, select, button, textarea");
        var serializedData = $form.serialize();
        $inputs.prop("disabled", true);

        request = $.ajax({
            url: $(this).attr('action'),
            type: "post",
            data: serializedData
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                swal({
                    title: "Done!",
                    text: x.msg,
                    type: "success",
                    timer: 5000,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
                $('.showMsg').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
                $('.recover-box').hide();
                $('#userRecover')[0].reset();
                $inputs.prop("disabled", false);
            } else {
                $.LoadingOverlay("hide");
                $('.showMsg').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
                $('#userRecover')[0].reset();
                $inputs.prop("disabled", false);
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
            $inputs.prop("disabled", false);
        });

    });
    //RESET PASSWORD HERE
    $("#passReset").submit(function (event) {
        if (request) {
            request.abort();
        }
        event.preventDefault();
        $.LoadingOverlay("show");
        var $form = $(this);
        var $inputs = $form.find("input, select, button, textarea");
        var serializedData = $form.serialize();
        $inputs.prop("disabled", true);

        request = $.ajax({
            url: $(this).attr('action'),
            type: "post",
            data: serializedData
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                swal({
                    title: "Done!",
                    text: 'Congratulations! your password has been changed.',
                    type: "success",
                    timer: 5000,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
                $('.recover-box').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
                $('.hideAfter').hide();
            } else {
                $.LoadingOverlay("hide");
                $('.showMsg').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
                $('#passReset')[0].reset();
                $inputs.prop("disabled", false);
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
            $inputs.prop("disabled", false);
        });

    });

    //ADDING STUDETNS SUBJECT AND MARKS
    $(".addScoreMarks").click(function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var prefix = $(this).data('prefix');
        var subject = $('#' + prefix + '-subject').val();
        var score = $('#' + prefix + '-score').val();
        var id = $(this).attr('data-parentid');
        var level = $(this).attr('data-level');
        var formURL = baseURL + 'student_marks/add';
        var inputs = {subject: subject, score: score, id: id, level: level, prefix: prefix};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                swal({
                    title: "Done!",
                    text: x.msg,
                    type: "success",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
                $('#' + prefix + 'table tr:last').after(x.newsubject);
                $('#' + prefix + '-subject').val('');
                $('#' + prefix + '-score').val('');
            } else {
                $.LoadingOverlay("hide");
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
                $('.' + prefix + 'error').html(x.msg);
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });

    });

    //REMOVING MARKS
    $('body').delegate('.removeScore', 'click', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }

        $.LoadingOverlay("show");
        var id = $(this).data('id');
        var prefix = $(this).data('prefix');
        var formURL = 'student_marks/remove';
        var inputs = {id: id};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                swal({
                    title: "Done!",
                    text: x.msg,
                    type: "success",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
                $('.' + prefix + 'error').html('');
                var tableRow = $(event.target).closest('tr').children('td, th').css('background-color', '#fdb316');
                tableRow.fadeOut('slow',
                        function () {
                            tableRow.remove();
                        }
                );
            } else {
                $.LoadingOverlay("hide");
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
                $('.' + prefix + 'error').html(x.msg);
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });

    //RESEND VERIFICATION CODE
    $('body').delegate('.makeResend', 'click', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var formURL = 'user/resendverification';
        var inputs = {veryfy: true};
        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                swal({
                    title: "Done!",
                    text: x.msg,
                    type: "success",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });

                $('.showMsg').html(x.msg);

            } else {
                $.LoadingOverlay("hide");
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
                $('.' + prefix + 'error').html(x.msg);
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });

    //REMOVING STUDENT AD BY THEMSELVES
    $('body').delegate('.remove-student-ad', 'click', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }

        if (confirm('Are you sure you want to delete this Ad permanantly?')) {

            var id = $(this).data('id');
            //var prefix = $(this).data('prefix');
            var formURL = 'studentad/remove_student_ad';
            var inputs = {id: id};
            $.LoadingOverlay("show");
            request = $.ajax({
                url: formURL,
                type: "post",
                data: inputs,
                dataType: 'html',
            });

            request.done(function (res) {
                var x = JSON.parse(res);
                if (x.status == 'success') {
                    $.LoadingOverlay("hide");
                    swal({
                        title: "Done!",
                        text: x.msg,
                        type: "success",
                        timer: 3000,
                        html: true,
                        confirmButtonColor: "#fdb316",
                        showConfirmButton: true
                    });
                    var tableRow = $('.adbox' + id).css('background-color', '#fdb316');
                    tableRow.fadeOut('slow',
                            function () {
                                tableRow.remove();
                            }
                    );
                    //$('.adbox' + id).hide('slow');
                } else {
                    $.LoadingOverlay("hide");
                    swal({
                        title: "Not Done!",
                        text: x.msg,
                        type: "error",
                        timer: 3000,
                        html: true,
                        confirmButtonColor: "#fdb316",
                        showConfirmButton: true
                    });
                    $('.' + prefix + 'error').html(x.msg);
                }
            });
            request.fail(function (jqXHR, textStatus) {
                $.LoadingOverlay("hide");
                alert("Request failed: " + textStatus);
            });
        }
    });
    //REMOVING COLLEGE AD BY THEMSELVES
    $('body').delegate('.remove-college-ad', 'click', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var id = $(this).data('id');
        //var prefix = $(this).data('prefix');
        var formURL = 'collegead/remove_college_ad';
        var inputs = {id: id};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                swal({
                    title: "Done!",
                    text: x.msg,
                    type: "success",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
                var tableRow = $('.adbox' + id).css('background-color', '#fdb316');
                tableRow.fadeOut('slow',
                        function () {
                            tableRow.remove();
                        }
                );
                //$('.adbox' + id).hide('slow');
            } else {
                $.LoadingOverlay("hide");
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
                $('.' + prefix + 'error').html(x.msg);
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });
    //REMOVING INSTITUTE AD BY THEMSELVES
    $('body').delegate('.remove-institute-ad', 'click', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var id = $(this).data('id');
        //var prefix = $(this).data('prefix');
        var formURL = 'institutead/remove_college_ad';
        var inputs = {id: id};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                swal({
                    title: "Done!",
                    text: x.msg,
                    type: "success",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
                var tableRow = $('.adbox' + id).css('background-color', '#fdb316');
                tableRow.fadeOut('slow',
                        function () {
                            tableRow.remove();
                        }
                );
                //$('.adbox' + id).hide('slow');
            } else {
                $.LoadingOverlay("hide");
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
                $('.' + prefix + 'error').html(x.msg);
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });

    //STUDENT PROFILE EDIT FORM
    $("#userProfileEdit").submit(function (event) {
        event.preventDefault();

        if (request) {
            request.abort();
        }

        var form = $(this);
        var serializedData = form.serialize();
        var inputs = form.find("input, select, button, textarea");
        inputs.prop("disabled", true);
        $.LoadingOverlay("show");
        request = $.ajax({
            url: $(this).attr('action'),
            type: "post",
            data: serializedData,
            dataType: 'html'
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('.profileerror').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
                $('html, body').animate({
                    scrollTop: $(".profileerror").offset().top
                }, 1000);
                swal({
                    title: "Done!",
                    text: x.msg,
                    type: "success",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            } else {
                $.LoadingOverlay("hide");
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
                $('.profileerror').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
                $('html, body').animate({
                    scrollTop: $(".profileerror").offset().top
                }, 1000);
            }
        });

        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });

        request.always(function () {
            inputs.prop("disabled", false);
        });
    });


    //STUDENT AD SUBMITION HANDLER
    $("#studentAdAdding").submit(function (event) {
        event.preventDefault();

        if (request) {
            request.abort();
        }
        //$.LoadingOverlay("show");
        var form = $(this);
        var serializedData = form.serialize();
        var axN = $(this).attr('action');

        $('#check-confirm-password').modal({
            show: 'true'
        });
        //CONFRIM PASSWORD PROCESS START
        $("#con-pass-btn").click(function () {

            var conPass = $("#con-pass").val();
            var curData = {conPass: conPass};

            request = $.ajax({
                url: 'user/confirmByPass',
                type: "post",
                data: curData,
                dataType: 'html'
            });

            request.done(function (res) {
                var y = JSON.parse(res);
                //IF PASSWORD IS CORRENT PROCESS THE FORM OTHER WISE NOT
                if (y.status == 'success') {
                    //$.LoadingOverlay("hide");
                    request = $.ajax({
                        url: axN,
                        type: "post",
                        data: serializedData,
                        dataType: 'html'
                    });
                    request.done(function (resu) {
                        //$.LoadingOverlay("hide");
                        var x = JSON.parse(resu);
                        if (x.status == 'success') {
                            swal({
                                title: "Done!",
                                text: x.msg,
                                type: "success",
                                timer: 3000,
                                html: true,
                                confirmButtonColor: "#fdb316",
                                showConfirmButton: true
                            }, window.location.reload());

                            $('.studentadmsg').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
                            $('#studentAdAdding')[0].reset();
                            $('html, body').animate({
                                scrollTop: $(".studentadmsg").offset().top
                            }, 1000);
                        } else {
                            //$.LoadingOverlay("hide");
                            swal({
                                title: "Not Done!",
                                text: x.msg,
                                type: "error",
                                timer: 5000,
                                html: true,
                                confirmButtonColor: "#fdb316",
                                showConfirmButton: true
                            });
                            $('.studentadmsg').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
                            $('html, body').animate({
                                scrollTop: $(".studentadmsg").offset().top
                            }, 1000);
                        }
                    });
                    request.fail(function (jqXHR, textStatus) {
                        //$.LoadingOverlay("hide");
                        alert("Request failed: " + textStatus);
                    });
                    //CLOSE THE MODAL
                    $("#check-confirm-password").modal('hide')
                } else {
                    //$.LoadingOverlay("hide");
                    swal({
                        title: "Not Done!",
                        text: y.msg,
                        type: "error",
                        timer: 5000,
                        html: true,
                        confirmButtonColor: "#fdb316",
                        showConfirmButton: true
                    });

                    //CLOSE THE MODAL
                    $("#check-confirm-password").modal('hide');
                }
            });
            request.fail(function (jqXHR, textStatus) {
                //$.LoadingOverlay("hide");
                alert("Request failed: " + textStatus);
            });
        });
    });


    //COLLEGE AD SUBMISSION HANDLER
    $("#collegeAdAdding").submit(function (event) {
        event.preventDefault();

        if (request) {
            request.abort();
        }
        //$.LoadingOverlay("show");
        var form = $(this);
        var serializedData = form.serialize();
        var axN = $(this).attr('action');

        //LOAD THE PASSWORD MODAL
        $('#check-confirm-password-college').modal({
            show: 'true'
        });

        //SARTING CONFRIMTING PASSWORD
        $("#con-pass-btn-college").click(function () {

            var conPass = $("#con-pass").val();
            var curData = {conPass: conPass};

            $('#con-pass-btn-college').html('Checking! Please wait.');

            request = $.ajax({
                url: 'user/confirmByPass',
                type: "post",
                data: curData,
                dataType: 'html'
            });

            request.done(function (res) {

                var y = JSON.parse(res);
                //IF PASSWORD IS CORRENT PROCESS THE FORM OTHER WISE NOT
                if (y.status == 'success') {
                    //$.LoadingOverlay("hide");
                    request = $.ajax({
                        url: axN,
                        type: "post",
                        data: serializedData,
                        dataType: 'html'
                    });

                    request.done(function (res) {
                        var x = JSON.parse(res);
                        if (x.status == 'success') {
                            //$.LoadingOverlay("hide");
                            swal({
                                title: "Done!",
                                text: x.msg,
                                type: "success",
                                timer: 3000,
                                html: true,
                                confirmButtonColor: "#fdb316",
                                showConfirmButton: true
                            }, window.location.reload());
                            $('.collegeadmsg').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
                            $('#collegeAdAdding')[0].reset();
                            $('html, body').animate({
                                scrollTop: $(".collegeadmsg").offset().top
                            }, 1000);
                        } else {
                            //$.LoadingOverlay("hide");
                            swal({
                                title: "Not Done!",
                                text: x.msg,
                                type: "error",
                                timer: 3000,
                                html: true,
                                confirmButtonColor: "#fdb316",
                                showConfirmButton: true
                            });
                            $('.collegeadmsg').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
                            $('html, body').animate({
                                scrollTop: $(".collegeadmsg").offset().top
                            }, 1000);
                        }
                    });

                    request.fail(function (jqXHR, textStatus) {
                        //$.LoadingOverlay("hide");
                        alert("Request failed: " + textStatus);
                    });

                    //CLOSE THE MODAL
                    $("#check-confirm-password-college").modal('hide');

                } else {
                    //$.LoadingOverlay("hide");
                    swal({
                        title: "Not Done!",
                        text: y.msg,
                        type: "error",
                        timer: 5000,
                        html: true,
                        confirmButtonColor: "#fdb316",
                        showConfirmButton: true
                    });

                    //CLOSE THE MODAL
                    $("#check-confirm-password-college").modal('hide');
                }

            });
            request.fail(function (jqXHR, textStatus) {
                //$.LoadingOverlay("hide");
                alert("Request failed: " + textStatus);
            });
        });

    });
    //INSTITUTE AD SUBMISSION HANDLER
    $("#instituteAdAdding").submit(function (event) {
        event.preventDefault();

        if (request) {
            request.abort();
        }
        //$.LoadingOverlay("show");
        var form = $(this);
        var serializedData = form.serialize();
        var axN = $(this).attr('action');

        //LOAD THE PASSWORD MODAL
        $('#check-confirm-password-college').modal({
            show: 'true'
        });

        //SARTING CONFRIMTING PASSWORD
        $("#con-pass-btn-college").click(function () {
            var conPass = $("#con-pass").val();
            var curData = {conPass: conPass};

            $('#con-pass-btn-college').html('Checking! Please wait.');

            request = $.ajax({
                url: 'user/confirmByPass',
                type: "post",
                data: curData,
                dataType: 'html'
            });

            request.done(function (res) {
                var y = JSON.parse(res);
                //IF PASSWORD IS CORRENT PROCESS THE FORM OTHER WISE NOT
                if (y.status == 'success') {
                    //$.LoadingOverlay("hide");
                    request = $.ajax({
                        url: axN,
                        type: "post",
                        data: serializedData,
                        dataType: 'html'
                    });

                    request.done(function (res) {
                        var x = JSON.parse(res);
                        if (x.status == 'success') {
                            //$.LoadingOverlay("hide");
                            swal({
                                title: "Done!",
                                text: x.msg,
                                type: "success",
                                timer: 3000,
                                html: true,
                                confirmButtonColor: "#fdb316",
                                showConfirmButton: true
                            }, window.location.reload());
                            $('.collegeadmsg').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
                            $('#collegeAdAdding')[0].reset();
                            $('html, body').animate({
                                scrollTop: $(".collegeadmsg").offset().top
                            }, 1000);
                        } else {
                            //$.LoadingOverlay("hide");
                            swal({
                                title: "Not Done!",
                                text: x.msg,
                                type: "error",
                                timer: 3000,
                                html: true,
                                confirmButtonColor: "#fdb316",
                                showConfirmButton: true
                            });
                            $('.collegeadmsg').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
                            $('html, body').animate({
                                scrollTop: $(".collegeadmsg").offset().top
                            }, 1000);
                        }
                    });

                    request.fail(function (jqXHR, textStatus) {
                        //$.LoadingOverlay("hide");
                        alert("Request failed: " + textStatus);
                    });

                    //CLOSE THE MODAL
                    $("#check-confirm-password-college").modal('hide');

                } else {
                    //$.LoadingOverlay("hide");
                    swal({
                        title: "Not Done!",
                        text: y.msg,
                        type: "error",
                        timer: 5000,
                        html: true,
                        confirmButtonColor: "#fdb316",
                        showConfirmButton: true
                    });

                    //CLOSE THE MODAL
                    $("#check-confirm-password-college").modal('hide');
                }

            });
            request.fail(function (jqXHR, textStatus) {
                //$.LoadingOverlay("hide");
                alert("Request failed: " + textStatus);
            });
        });

    });

    //PROFILE UPDATE
    $('#userProfilePic').submit(function (e) {
        e.preventDefault();

        var postData = $(this).serialize();
        var formUrl = $(this).attr('action');
        $.LoadingOverlay("show");

        var request = $.ajax({
            url: formUrl,
            dataType: "html",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                swal({
                    title: "Done!",
                    text: x.msg,
                    type: "success",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
                $('.propic').html(x.img_src);
                $('#student_profile_pic').val('');
                $('.propic_error').html('');
                //window.location.reload();
            } else {
                $.LoadingOverlay("hide");
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
                $('.propic_error').html(x.msg);
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });
    //GALLERY IMAGES
    $('#galleryPic').submit(function (e) {
        e.preventDefault();

        var postData = $(this).serialize();
        var formUrl = $(this).attr('action');

        $('.galBtn').addClass('disabled');
        $('.galBtn').val('Submiting...');
        $.LoadingOverlay("show");

        var request = $.ajax({
            url: formUrl,
            dataType: "html",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                swal({
                    title: "Done!",
                    text: x.msg,
                    type: "success",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
                $('.gallery_img_div').append(x.img_src);
                $('#college_gallery').val('');
                $('.gal_pic_error').html('');
                $('.galBtn').removeClass('disabled');
                $('.galBtn').val('Add New Picture');
            } else {
                $.LoadingOverlay("hide");
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
                $('.gal_pic_error').html(x.msg);
                $('.galBtn').removeClass('disabled');
                $('.galBtn').val('Add New Picture');
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
            $('.galBtn').removeClass('disabled');
            $('.galBtn').val('Add New Picture');
        });
    });
    //GALLERY IMAGES DELETE
    $('body').delegate('.gal_del', 'click', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }

        var id = $(this).data('id');
        var prefix = $(this).data('prefix');
        var formURL = 'imagehub/del_gallery';
        var inputs = {id: id};
        $.LoadingOverlay("show");

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                swal({
                    title: "Done!",
                    text: x.msg,
                    type: "success",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
                $('.galind-' + id).css('display', 'none');

            } else {
                $.LoadingOverlay("hide");
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
                $('.' + prefix + 'error').html(x.msg);
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });




    //DOCUMENT UPLOAD HANDLER
    $('#docUploadFile').submit(function (e) {
        e.preventDefault();
        $.LoadingOverlay("show");

        var postData = $(this).serialize();
        var formUrl = $(this).attr('action');

        $('.galBtn').addClass('disabled');
        $('.docBtn').val('Uploading... (can take several mins)');

        var request = $.ajax({
            url: formUrl,
            dataType: "html",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                swal({
                    title: "Done!",
                    text: x.msg,
                    type: "success",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
                $('.doc_img_div').append(x.img_src);
                $('#docfiles').val('');
                $('.docError').html('');
                $('.docBtn').removeClass('disabled');
                $('.docBtn').val('Add New PDF');
            } else {
                $.LoadingOverlay("hide");
                swal({
                    title: "Unable to upload!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
                $('.docError').html(x.msg);
                $('.docBtn').removeClass('disabled');
                $('.docBtn').val('Add New PDF');
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
            $('.docBtn').removeClass('disabled');
            $('.docBtn').val('Add New PDF');
        });
    });


    //DOCUMENT DELETE
    $('body').delegate('.doc_del', 'click', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");

        var id = $(this).data('id');
        var formURL = 'documents/del_doc';
        var inputs = {id: id};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                swal({
                    title: "Done!",
                    text: x.msg,
                    type: "success",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
                $('.docholder-' + id).css('display', 'none');

            } else {
                $.LoadingOverlay("hide");
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
                $('.docError').html(x.msg);
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });


    //AREA FOR INSTI
    //ADDING PROGRAM OFFERED BY INSTITUTE
    $(".addProgramOfferedInstitute").click(function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var prefix = $(this).data('prefix');
        var offered_programe = $('#offered_programe').val();
        var program_title = $('#program_title').val();
        var program_title_alt = $('#program_title_alt').val();
        var affiliation = $('#affiliation').val();
        var affiliation_alt = $('#affiliation_alt').val();
        var program_duration = $('#program_duration').val();
        var class_held_at = $('#class_held_at').val();
        var admission_fee = $('#admission_fee').val();
        var total_course_fee = $('#total_course_fee').val();
        var other_fee = $('#other_fee').val();
        var fee_collect = $('#fee_collect').val();

        var formURL = 'institute_programe/add';
        var inputs = {
            offered_programe: offered_programe,
            program_title: program_title,
            program_title_alt: program_title_alt,
            affiliation: affiliation,
            affiliation_alt: affiliation_alt,
            program_duration: program_duration,
            class_held_at: class_held_at,
            admission_fee: admission_fee,
            total_course_fee: total_course_fee,
            other_fee: other_fee,
            fee_collect: fee_collect
        };

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                swal({
                    title: "Done!",
                    text: x.msg,
                    type: "success",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
                $('.listarea').append(x.newView);
                $('.programeerror').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
                $('html, body').animate({
                    scrollTop: $(".programeerror").offset().top
                }, 1000);
            } else {
                $.LoadingOverlay("hide");
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
                $('.programeerror').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
                $('html, body').animate({
                    scrollTop: $(".programeerror").offset().top
                }, 1000);
                //$('.programeerror').html(x.msg);
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });

    });

    //ADDING PROGRAM OFFERED BY COLLEGE
    $(".addProgramOffered").click(function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var prefix = $(this).data('prefix');
        var offered_programe = $('#offered_programe').val();
        var program_title = $('#program_title').val();
        var program_title_alt = $('#program_title_alt').val();
        var affiliation = $('#affiliation').val();
        var affiliation_alt = $('#affiliation_alt').val();
        var program_duration = $('#program_duration').val();
        var class_held_at = $('#class_held_at').val();
        var admission_fee = $('#admission_fee').val();
        var total_course_fee = $('#total_course_fee').val();
        var other_fee = $('#other_fee').val();
        var fee_collect = $('#fee_collect').val();

        var formURL = 'college_programe/add';
        var inputs = {
            offered_programe: offered_programe,
            program_title: program_title,
            program_title_alt: program_title_alt,
            affiliation: affiliation,
            affiliation_alt: affiliation_alt,
            program_duration: program_duration,
            class_held_at: class_held_at,
            admission_fee: admission_fee,
            total_course_fee: total_course_fee,
            other_fee: other_fee,
            fee_collect: fee_collect
        };

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                swal({
                    title: "Done!",
                    text: x.msg,
                    type: "success",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
                $('.listarea').append(x.newView);
                $('.programeerror').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
                $('html, body').animate({
                    scrollTop: $(".programeerror").offset().top
                }, 1000);
            } else {
                $.LoadingOverlay("hide");
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
                $('.programeerror').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
                $('html, body').animate({
                    scrollTop: $(".programeerror").offset().top
                }, 1000);
                //$('.programeerror').html(x.msg);
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });

    });



    //REMOVING PROGRAMMES OF COLLEGE PROFILE
    $('body').delegate('.removeOffer', 'click', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");

        var id = $(this).data('id');
        var parent = $(this).data('parent');
        var formURL = 'college_programe/remove';
        var inputs = {id: id, parent: parent};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                swal({
                    title: "Done!",
                    text: x.msg,
                    type: "success",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });

                //$('.unique-' + id).hide();
                var tableRow = $('.unique-' + id).css('background-color', '#fdb316');
                tableRow.fadeOut('slow',
                        function () {
                            tableRow.remove();
                        }
                );
            } else {
                $.LoadingOverlay("hide");
                swal({
                    title: "Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
                $('.removeerror').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');

            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });

    //REMOVING PROGRAMMES OF INSTITUE PROFILE
    $('body').delegate('.removeOfferIns', 'click', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");

        var id = $(this).data('id');
        var parent = $(this).data('parent');
        var formURL = 'institute_programe/remove';
        var inputs = {id: id, parent: parent};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                swal({
                    title: "Done!",
                    text: x.msg,
                    type: "success",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });

                //$('.unique-' + id).hide();
                var tableRow = $('.unique-' + id).css('background-color', '#fdb316');
                tableRow.fadeOut('slow',
                        function () {
                            tableRow.remove();
                        }
                );
            } else {
                $.LoadingOverlay("hide");
                swal({
                    title: "Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
                $('.removeerror').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');

            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });


    //SHOW INTEREST BTN FUNCTIONS
    $('body').delegate('.interest-btn', 'click', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");

        var id = $(this).data('id');
        var inputs = {id: id};

        var baseUrl = $(this).data('url');
        if (typeof (baseUrl) == 'undefined') {
            var baseUrl = '';
            var formURL = 'interest/interest_clicked';
        } else {
            var formURL = baseUrl + 'interest/interest_clicked';
        }

        $('.interest-btn').addClass('disabled');

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                swal({
                    title: "Done!",
                    text: x.msg,
                    type: "success",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });

                $('.interest-span' + id).html(x.total);
                var tableRow = $('.interest-span' + id).css('color', '#fdb316');
                tableRow.fadeIn('slow');
                $('.interest-btn').removeClass('disabled');
            } else {
                if (x.do == 'login') {
                    $.LoadingOverlay("hide");
                    swal({
                        title: "Login required!",
                        text: x.msg,
                        confirmButtonColor: "#fdb316",
                        type: "error",
                        html: true,
                        timer: 10000,
                        showConfirmButton: true
                    }, function () {
                        window.location.href = baseUrl + 'login';
                    });

                } else if (x.do == 'sametype') {
                    $.LoadingOverlay("hide");
                    swal({
                        title: "Not Done!",
                        text: x.msg,
                        type: "error",
                        timer: 10000,
                        html: true,
                        confirmButtonColor: "#fdb316",
                        showConfirmButton: true
                    });
                    $('.interest-btn').removeClass('disabled');
                } else {
                    $.LoadingOverlay("hide");
                    swal({
                        title: "Done!",
                        text: x.msg,
                        type: "success",
                        timer: 10000,
                        html: true,
                        confirmButtonColor: "#fdb316",
                        showConfirmButton: true
                    });
                    $('.interest-btn').removeClass('disabled');
                }
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            $('.interest-btn').removeClass('disabled');
            alert("Request failed: " + textStatus);
        });
    });



    //INTERESTED PERSON MODAL
    $('body').delegate('.who-intrested', 'click', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");

        var id = $(this).data('id');
        var baseUrl = $(this).data('url');

        if (typeof (baseUrl) == 'undefined') {
            var formURL = 'interest/who_intrested';
        } else {
            var formURL = baseUrl + 'interest/who_intrested';
        }

        var inputs = {id: id};

        $('.who-intrested').addClass('disabled');

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('.intersted-list').html(x.intersted_list);
                $('.toInterest').html(x.toInterest);
                $('.who-intrested').removeClass('disabled');
                $('#intersted-modal').modal('show');
            } else {
                $.LoadingOverlay("hide");
                swal({
                    title: "No one found!",
                    text: x.msg,
                    confirmButtonColor: "#fdb316",
                    type: "error",
                    html: true,
                    timer: 10000,
                    showConfirmButton: true
                });
                $('.who-intrested').removeClass('disabled');
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            $('.interest-btn').removeClass('disabled');
            alert("Request failed: " + textStatus);
        });
    });

    //SINGLE AD MODAL
    $('body').delegate('.which-ads', 'click', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");

        var id = $(this).data('id');
        var type = $(this).data('type');

        var baseUrl = $(this).data('url');
        if (typeof (baseUrl) == 'undefined') {
            if (type == 'College') {
                var formURL = window.location.origin + '/collegead/which_ads';
            }
            if (type == 'Student') {
                var formURL = window.location.origin + '/studentad/which_ads';
            }
            if (type == 'Institute') {
                var formURL = window.location.origin + '/institutead/which_ads';
            }
        } else {
            if (type == 'College') {
                var formURL = baseUrl + 'collegead/which_ads';
            }
            if (type == 'Student') {
                var formURL = baseUrl + 'studentad/which_ads';
            }
            if (type == 'Institute') {
                var formURL = baseUrl + 'institutead/which_ads';
            }
        }

        var inputs = {id: id};

        $('.which-ads').addClass('disabled');

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('.intersted-list-' + x.type).html(x.intersted_list);
                $('.which-ads').removeClass('disabled');
                $('#adURL-' + x.type).attr('href', x.adURL);
                $('#which-ad-modal-' + x.type).modal('show');

            } else {
                $.LoadingOverlay("hide");
                $('.which-ads').removeClass('disabled');
            }
        });

        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            $('.interest-btn').removeClass('disabled');
            alert("Request failed: " + textStatus);
        });
    });

    //MORE INF BTN FUNCTION
    $('body').delegate('.more-info-btn', 'click', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");

        var id = $(this).data('id');
        var baseUrl = $(this).data('url');

        if (typeof (baseUrl) == 'undefined') {
            var baseUrl = '';
            var formURL = 'user/validate_more_info';
        } else {
            var formURL = baseUrl + 'user/validate_more_info';
        }

        var inputs = {id: id};

        $('.more-info-btn').addClass('disabled');

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                var urls = x.controller + '/ads/' + x.adID;
                window.location.href = urls;
                $('.more-info-btn').removeClass('disabled');
            } else {
                $.LoadingOverlay("hide");
                if (x.con == 'login') {
                    swal({
                        title: "Login required!",
                        text: x.msg,
                        confirmButtonColor: "#fdb316",
                        type: "error",
                        html: true,
                        timer: 10000,
                        showConfirmButton: true
                    }, function () {
                        window.location.href = baseUrl + 'login';
                    });
                } else {
                    $.LoadingOverlay("hide");
                    swal({
                        title: "Invalid Operation!",
                        text: x.msg,
                        confirmButtonColor: "#fdb316",
                        type: "error",
                        html: true,
                        timer: 10000,
                        showConfirmButton: true
                    });
                    $('.more-info-btn').removeClass('disabled');
                }
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            $('.interest-btn').removeClass('disabled');
            alert("Request failed: " + textStatus);
        });
    });


    //SAVE AD
    $('body').delegate('.save-ad-btn', 'click', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");

        var id = $(this).data('id');
        var inputs = {id: id};

        var baseUrl = $(this).data('url');
        if (typeof (baseUrl) == 'undefined') {
            var baseUrl = '';
            var formURL = 'savedad/do_save';
        } else {
            var formURL = baseUrl + 'savedad/do_save';
        }


        $('.save-ad-btn').addClass('disabled');

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                swal({
                    title: "Done!",
                    text: x.msg,
                    type: "success",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });

                //$('.interest-span' + id).html(x.total);
                //var tableRow = $('.interest-span' + id).css('color', '#fdb316');
                //tableRow.fadeIn('slow');
                $('.save-ad-btn').removeClass('disabled');
            } else {
                $.LoadingOverlay("hide");
                if (x.do == 'login') {
                    swal({
                        title: "Login required!",
                        text: x.msg,
                        confirmButtonColor: "#fdb316",
                        type: "error",
                        html: true,
                        timer: 10000,
                        showConfirmButton: true
                    }, function () {
                        window.location.href = baseUrl + 'login';
                    });

                } else if (x.do == 'sametype') {
                    $.LoadingOverlay("hide");
                    swal({
                        title: "Not Done!",
                        text: x.msg,
                        type: "error",
                        timer: 10000,
                        html: true,
                        confirmButtonColor: "#fdb316",
                        showConfirmButton: true
                    });
                    $('.save-ad-btn').removeClass('disabled');
                } else {
                    $.LoadingOverlay("hide");
                    swal({
                        title: "Done!",
                        text: x.msg,
                        type: "success",
                        timer: 10000,
                        html: true,
                        confirmButtonColor: "#fdb316",
                        showConfirmButton: true
                    });
                    $('.save-ad-btn').removeClass('disabled');
                }
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            $('.save-ad-btn').removeClass('disabled');
            alert("Request failed: " + textStatus);
        });
    });


    //REMOVE SAVED AD
    $('body').delegate('.remove-saved-ad', 'click', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");

        if (confirm("Are you sure want to remove this saved ad from your list?")) {

            var id = $(this).data('id');
            var formURL = 'savedad/remove_ad';
            var inputs = {id: id};

            request = $.ajax({
                url: formURL,
                type: "post",
                data: inputs,
                dataType: 'html',
            });

            request.done(function (res) {
                var x = JSON.parse(res);
                if (x.status == 'success') {
                    $.LoadingOverlay("hide");
                    swal({
                        title: "Done!",
                        text: x.msg,
                        type: "success",
                        timer: 3000,
                        html: true,
                        confirmButtonColor: "#fdb316",
                        showConfirmButton: true
                    });
                    var tableRow = $('.rowid-' + id).css('background-color', '#fdb316');
                    tableRow.fadeOut('slow',
                            function () {
                                tableRow.remove();
                            }
                    );
                    //$('.adbox' + id).hide('slow');
                } else {
                    $.LoadingOverlay("hide");
                    swal({
                        title: "Not Done!",
                        text: x.msg,
                        type: "error",
                        timer: 3000,
                        html: true,
                        confirmButtonColor: "#fdb316",
                        showConfirmButton: true
                    });
                }
            });
            request.fail(function (jqXHR, textStatus) {
                $.LoadingOverlay("hide");
                alert("Request failed: " + textStatus);
            });
        }
    });

    //MAKE OFFER BTN HANDLER
    $('body').delegate('.offer_btn', 'click', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");

        var id = $(this).data('id');
        var baseUrl = $(this).data('url');

        if (typeof (baseUrl) == 'undefined') {
            var formURL = 'offer/validate_offer';
        } else {
            var formURL = baseUrl + 'offer/validate_offer';
        }

        var inputs = {id: id};

        $('.offer_btn').addClass('disabled');

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('.toInterest').html(x.toInterest);
                $('#adIDModalField').val(x.adID);
                $('.offer_btn').removeClass('disabled');
                $('#make-offer-modal').modal('show');
            } else {
                $.LoadingOverlay("hide");
                $('.offer_btn').removeClass('disabled');
                if (x.do == 'login') {
                    swal({
                        title: "Login required!",
                        text: x.msg,
                        confirmButtonColor: "#fdb316",
                        type: "error",
                        html: true,
                        timer: 10000,
                        showConfirmButton: true
                    }, function () {
                        window.location.href = 'login';
                    });
                } else {
                    $.LoadingOverlay("hide");
                    swal({
                        title: "Not Done!",
                        text: x.msg,
                        type: "error",
                        timer: 10000,
                        html: true,
                        confirmButtonColor: "#fdb316",
                        showConfirmButton: true
                    });
                }
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            $('.interest-btn').removeClass('disabled');
            alert("Request failed: " + textStatus);
        });
    });
    //MAKE OFFER MESSAGE AJAX
    $('body').delegate('#doMakeOffer', 'submit', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");

        var data = $(this).serialize();
        var baseUrl = $('#modalUrl').val();

        if (typeof (baseUrl) == 'undefined') {
            var formURL = 'offer/make_offer';
        } else {
            var formURL = baseUrl + 'offer/make_offer';
        }


        $('.submitOffer').addClass('disabled');
        $('.submitOffer').html('submitting...');

        request = $.ajax({
            url: formURL,
            type: "post",
            data: data,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('.submitOffer').removeClass('disabled');
                $('.submitOffer').html('Submit');
                $('#make-offer-modal').modal('hide');
                $('.offer-msg').val('');
                swal({
                    title: "Done",
                    text: "Your offer message has been sent to selected student. thank you for your offer.",
                    type: "success",
                    timer: 10000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            } else {
                $.LoadingOverlay("hide");
                //ERROR MESSAGE
                $('.submitOffer').removeClass('disabled');
                $('.submitOffer').html('Submit');
                swal({
                    title: "Not Done",
                    text: x.msg,
                    type: "error",
                    timer: 10000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            $('.submitOffer').removeClass('disabled');
            $('.submitOffer').html('Submit');
            alert("Request failed: " + textStatus);
        });
    });

    //MAKE APPLY NOW BTN
    $('body').delegate('.apply-now-btn', 'click', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var id = $(this).data('id');
        var baseUrl = $(this).data('url');

        if (typeof (baseUrl) == 'undefined') {
            var formURL = 'studentapply/validate_apply';
        } else {
            var formURL = baseUrl + 'studentapply/validate_apply';
        }
        var inputs = {id: id};
        $('.apply-now-btn').addClass('disabled');

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('.toApply').html(x.toApply);
                $('#adIDApplyField').val(x.adID);
                $('#adIDType').val(x.adType);
                $('.apply-now-btn').removeClass('disabled');
                $('#makeapply-modal').modal('show');
            } else {
                $.LoadingOverlay("hide");
                $('.apply-now-btn').removeClass('disabled');
                if (x.do == 'login') {
                    swal({
                        title: "Login required!",
                        text: x.msg,
                        confirmButtonColor: "#fdb316",
                        type: "error",
                        html: true,
                        timer: 10000,
                        showConfirmButton: true
                    }, function () {
                        window.location.href = 'login';
                    });
                } else {
                    $.LoadingOverlay("hide");
                    swal({
                        title: "Not Done!",
                        text: x.msg,
                        type: "error",
                        timer: 10000,
                        html: true,
                        confirmButtonColor: "#fdb316",
                        showConfirmButton: true
                    });
                }
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            $('.apply-now-btn').removeClass('disabled');
            alert("Request failed: " + textStatus);
        });
    });
    //MAKE APPLY NOW MESSAGE FUNCTION
    $('body').delegate('#doMakeApply', 'submit', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");

        var data = $(this).serialize();
        var baseUrl = $('#modalUrlApply').val();

        if (typeof (baseUrl) == 'undefined') {
            var formURL = 'studentapply/make_apply';
        } else {
            var formURL = baseUrl + 'studentapply/make_apply';
        }


        $('.submitApply').addClass('disabled');
        $('.submitApply').html('submitting...');

        request = $.ajax({
            url: formURL,
            type: "post",
            data: data,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('.submitApply').removeClass('disabled');
                $('.submitApply').html('Submit');
                $('#makeapply-modal').modal('hide');
                $('.apply-msg').val('');
                swal({
                    title: "Done",
                    text: "Your have successfully applied. thank you for your application.",
                    type: "success",
                    timer: 10000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            } else {
                $.LoadingOverlay("hide");
                //ERROR MESSAGE
                $('.submitApply').removeClass('disabled');
                $('.submitApply').html('Submit');
                swal({
                    title: "Not Done",
                    text: x.msg,
                    type: "error",
                    timer: 10000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            $('.submitOffer').removeClass('disabled');
            $('.submitOffer').html('Submit');
            alert("Request failed: " + textStatus);
        });
    });

    //SEARCH REQUEST ADVANCE SEARCHES
    $('body').delegate('.search_request', 'submit', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");

        //START LOADING CLASS
        //HIDE THE SWITCHER WHILE REQUISTIG
        $('.advSwitcher').css('display', 'none');
        $('.searchHead h1').contents().wrap('<a href="https://hamrofinder.com/search"></a>');

        var data = $(this).serialize();
        var baseUrl = $(this).data('url');
        var form = $(this);
        var inputs = form.find("input, select, button, textarea");
        inputs.prop("disabled", true);

        if (typeof (baseUrl) == 'undefined') {
            var formURL = 'search/advance_search';
        } else {
            var formURL = baseUrl + 'search/advance_search';
        }

        $('.blueButton').addClass('disabled');
        $('.blueButton').html('<i class="fa fa-spinner fa-pulse fa-fw"></i> submitting...');

        request = $.ajax({
            url: formURL,
            type: "post",
            data: data,
            dataType: 'html',
        });
        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('.adv-search').html(x.msg);
                $('.adv-search').html(x.newView);
            } else {
                //ERROR MESSAGE
                $.LoadingOverlay("hide");
                inputs.prop("disabled", false);
                $('.blueButton').removeClass('disabled');
                $('.blueButton').html('Submit');
                swal({
                    title: "Not Done",
                    text: x.msg,
                    type: "error",
                    timer: 10000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            $('.submitOffer').removeClass('disabled');
            $('.submitOffer').html('Submit');
            alert("Request failed: " + textStatus);
        });
    });

    //BROWSE FILTER REQUEST
    $('body').delegate('.browseRequest-location', 'change', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");

        var vals = $(this).val();
        var type = $(this).data('type');
        var method = $(this).data('method');
        var baseUrl = $(this).data('url');
        var level = $(this).data('level');

        if (method == 'Student') {
            $('.area-' + type).html('<span class="center-block"><img src="' + baseUrl + '_public/front/assets/img/loader.gif"> Loading...</span>');
        } else if (method == 'College') {
            $('.area-' + type + '-col').html('<span class="center-block"><img src="' + baseUrl + '_public/front/assets/img/loader.gif"> Loading...</span>');
        } else {
            $('.area-' + type + '-ins').html('<span class="center-block"><img src="' + baseUrl + '_public/front/assets/img/loader.gif"> Loading...</span>');
        }

        //LOADER HERE
        var data = {'type': type, 'val': vals, 'method': method, level: level};

        if (typeof (baseUrl) == 'undefined') {
            var formURL = 'search/browse_location';
        } else {
            var formURL = baseUrl + 'search/browse_location';
        }

        request = $.ajax({
            url: formURL,
            type: "post",
            data: data,
            dataType: 'html',
        });
        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                if (method == 'Student') {
                    $('.area-' + type).html(x.newView);
                } else if (method == "College") {
                    $('.area-' + type + '-col').html(x.newView);
                } else {
                    $('.area-' + type + '-ins').html(x.newView);
                }

            } else {
                //ERROR MESSAGE
                $.LoadingOverlay("hide");
                inputs.prop("disabled", false);
                $('.blueButton').removeClass('disabled');
                $('.blueButton').html('Submit');
                swal({
                    title: "Not Done",
                    text: x.msg,
                    type: "error",
                    timer: 10000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            $('.blueButton').removeClass('disabled');
            $('.blueButton').html('Submit');
            alert("Request failed: " + textStatus);
        });
    });


    //BROSER FILTER FOR AREA
    $('body').delegate('.browseRequest', 'change', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");

        var vals = $(this).val();
        var type = $(this).data('type');
        var method = $(this).data('method');
        var baseUrl = $(this).data('url');

        if (method == 'Student') {
            $('.area-' + type).html('<span class="center-block"><img src="' + baseUrl + '_public/front/assets/img/loader.gif"> Loading...</span>');
        } else if (method == 'College') {
            $('.area-' + type + '-col').html('<span class="center-block"><img src="' + baseUrl + '_public/front/assets/img/loader.gif"> Loading...</span>');
        } else {
            $('.area-' + type + '-ins').html('<span class="center-block"><img src="' + baseUrl + '_public/front/assets/img/loader.gif"> Loading...</span>');
        }

        //LOADER HERE
        var data = {'type': type, 'val': vals, 'method': method};

        if (typeof (baseUrl) == 'undefined') {
            var formURL = 'search/browse';
        } else {
            var formURL = baseUrl + 'search/browse';
        }

        request = $.ajax({
            url: formURL,
            type: "post",
            data: data,
            dataType: 'html',
        });
        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                if (method == 'Student') {
                    $('.area-' + type).html(x.newView);
                } else if (method == "College") {
                    $('.area-' + type + '-col').html(x.newView);
                } else {
                    $('.area-' + type + '-ins').html(x.newView);
                }

            } else {
                //ERROR MESSAGE
                $.LoadingOverlay("hide");
                inputs.prop("disabled", false);
                $('.blueButton').removeClass('disabled');
                $('.blueButton').html('Submit');
                swal({
                    title: "Not Done",
                    text: x.msg,
                    type: "error",
                    timer: 10000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            $('.blueButton').removeClass('disabled');
            $('.blueButton').html('Submit');
            alert("Request failed: " + textStatus);
        });
    });

    //VIDEO GALLERY
    $('body').delegate('#galleryVideo', 'submit', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");

        var data = $(this).serialize();

        $('.vidbtn').addClass('disabled');
        $('.vidbtn').val('submitting...');

        request = $.ajax({
            url: $(this).attr('action'),
            type: "post",
            data: data,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('.vidbtn').removeClass('disabled');
                $('.vidbtn').val('Add Video');
                $('#vid_code').val('');
                $('.gallery_vid_div').append(x.vid_frame);
                swal({
                    title: "Done",
                    text: "Video has been added successfully.",
                    type: "success",
                    timer: 10000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            } else {
                //ERROR MESSAGE
                $.LoadingOverlay("hide");
                $('.vidbtn').removeClass('disabled');
                $('.vidbtn').val('Add Video');
                swal({
                    title: "Not Done",
                    text: x.msg,
                    type: "error",
                    timer: 10000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            $('.vidbtn').removeClass('disabled');
            $('.vidbtn').val('Add Video');
            alert("Request failed: " + textStatus);
        });
    });
    //DELETE VIDEO GALLERY
    $('body').delegate('.vid_del', 'click', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var id = $(this).data('id');
        var formURL = 'videos/remove';
        var inputs = {id: id};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                swal({
                    title: "Done!",
                    text: x.msg,
                    type: "success",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
                $('.vidind-' + id).css('display', 'none');

            } else {
                $.LoadingOverlay("hide");
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
                $('.' + prefix + 'error').html(x.msg);
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });


    //CHAT CONVERSATION LOADER
    $('body').delegate('.showchatconversation', 'click', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }

        $('.singleConversationBox').removeClass('singleConversationBox-active');
        $(this).parents('div.singleConversationBox').addClass('singleConversationBox-active');

        $.LoadingOverlay("show");
        var id = $(this).data('id');
        var formURL = baseURL + 'chat/showconversation_details';
        var inputs = {id: id};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('.conversationDetails').html(x.message);
                $('.chattingHeading').html(x.messageHead);


                var objDiv = document.getElementById("chatDivResult");
                objDiv.scrollTop = objDiv.scrollHeight;


            } else {
                $.LoadingOverlay("hide");
                $('.conversationDetails').html('No conversation found! :)');
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });





    /*ADVANCE SEARCH PARMETERS FILTER*/
    $('body').delegate('.advSearchLebel', 'click', function (event) {
        event.preventDefault();

        $(this).removeClass('advSearchLebel');
        $(this).addClass('inactiveBtn');

        var totalString = '';
        $(".advSearchLebel").each(function () {
            var indStr = $(this).attr('data-kee');
            totalString += '.' + indStr;
        });


        $('.equal-item').hide('fast');
        $(totalString).show('fast');

        if (totalString === '') {
            $('.equal-item').show('fast');
        }
        //var allKey = $('#allParams').val();

        //alert(allKey);
        //return false;

        //var kee = $(this).attr('data-kee');
        //$('.' + $kee).toggle("slow");

        //$('.' + kee).hide('slow');

    });



    $('body').delegate('.inactiveBtn', 'click', function (event) {
        event.preventDefault();

        $(this).removeClass('inactiveBtn');
        $(this).addClass('advSearchLebel');

        var totalString = '';
        $(".advSearchLebel").each(function () {
            var indStr = $(this).attr('data-kee');
            totalString += '.' + indStr;
        });
        //alert(totalString);

        //var allKey = $('#allParams').val();
        //var allArray = allKey.split(',');

        //var kee = $(this).attr('data-kee');



        //REMOVING
        //var arr = ["jQuery", "JavaScript", "HTML", "Ajax", "Css"];
        //var itemtoRemove = "HTML";
        //arr.splice($.inArray(itemtoRemove, arr), 1);

        //$('.' + $kee).toggle("slow");
        $('.equal-item').hide('fast');
        $(totalString).show('fast');
    });


    //advance search tab
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var target = this.href.split('#');
        $('.nav a').filter('a[href="#' + target[1] + '"]').tab('show');
    });
    //MAKING REVERSE ACTIVE MENU FOR ADVANCE SEARCH
    $('#search1').click(function () {
        $('#headingTwoListWork').removeClass('current-menu active');
        $('#headingThreeListWork').removeClass('current-menu active');
        $('#headingOneListWork').addClass('current-menu active');
    });
    $('#search2').click(function () {
        $('#headingOneListWork').removeClass('current-menu active');
        $('#headingThreeListWork').removeClass('current-menu active');
        $('#headingTwoListWork').addClass('current-menu active');
    });
    $('#search3').click(function () {
        $('#headingOneListWork').removeClass('current-menu active');
        $('#headingTwoListWork').removeClass('current-menu active');
        $('#headingThreeListWork').addClass('current-menu active');
    });





    $(".expandBtnEvent").click(function (e) {
        e.preventDefault();
        var codes = $(this).attr('data-code');
        $("." + codes).css("height", "auto");
        $("." + codes + "-btn").hide();
    });



    //USER AD INSERT OPTION
    //ACTULLAY SEARCHING FOR
    $('body').delegate('#looking_for_type', 'change', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }

        $.LoadingOverlay("show");

        var valueSelected = this.value;

        var formURL = baseURL + 'settings/get_selectbox';
        var inputs = {searchingfor: valueSelected};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('#program-class-selector').html(x.resultSelect);
                $('#affiliation-certified').html(x.resultSelectAff);
            } else {
                $.LoadingOverlay("hide");
                $('#provienceDiv').html(x.selectProvince);
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });


    //ADDRESS CHAIN SELECT
    $('body').delegate('#country', 'change', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }

        $.LoadingOverlay("show");

        var valueSelected = this.value;

        var formURL = baseURL + 'address/on_change_country';
        var inputs = {country: valueSelected};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('#provienceDiv').html(x.resultSelect);
                $('#districtDiv').html(x.selectDistrict);
                $('#localLevelDiv').html(x.selectLocalLevel);
                $('#wardnoDiv').html(x.selectWordNo);
            } else {
                $.LoadingOverlay("hide");
                $('#provienceDiv').html(x.selectProvince);
                $('#districtDiv').html(x.selectDistrict);
                $('#localLevelDiv').html(x.selectLocalLevel);
                $('#wardnoDiv').html(x.selectWordNo);
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });


    //ON CHANVE PROVINCE
    $('body').delegate('#province', 'change', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var valueSelected = this.value;
        var formURL = baseURL + 'address/on_change_province';
        var inputs = {province: valueSelected};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('#districtDiv').html(x.selectDistrict);
                $('#localLevelDiv').html(x.selectLocalLevel);
                $('#wardnoDiv').html(x.selectWordNo);
            } else {
                $.LoadingOverlay("hide");
                $('#districtDiv').html(x.selectDistrict);
                $('#localLevelDiv').html(x.selectLocalLevel);
                $('#wardnoDiv').html(x.selectWordNo);
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });
    //ON CHANGE DISTRICT
    $('body').delegate('#district', 'change', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var valueSelected = this.value;
        var formURL = baseURL + 'address/on_change_district';
        var inputs = {district: valueSelected};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('#localLevelDiv').html(x.selectLocalLevel);
                $('#wardnoDiv').html(x.selectWordNo);
            } else {
                $.LoadingOverlay("hide");
                $('#localLevelDiv').html(x.selectLocalLevel);
                $('#wardnoDiv').html(x.selectWordNo);
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });
    //ON CHANGE LOCAL LEVEL
    $('body').delegate('#local_level', 'change', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var valueSelected = this.value;
        var formURL = baseURL + 'address/on_change_local_level';
        var inputs = {local_level: valueSelected};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('#wardnoDiv').html(x.selectWordNo);
            } else {
                $.LoadingOverlay("hide");
                $('#wardnoDiv').html(x.selectWordNo);
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });


    //FOR BROWSE FILTER DATA ONLY FOR STUDENT ==================================
    $('body').delegate('#browsecountry-student', 'change', function (event) {

        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var valueSelected = this.value;
        var formURL = baseURL + 'address/browse_change_country';
        var type = $(this).attr('data-type');
        var inputs = {country: valueSelected, type: type};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {

                $('#provienceDiv-' + type).html(x.resultSelect);
                $('#districtDiv-' + type).html(x.selectDistrict);
                $('#localLevelDiv-' + type).html(x.selectLocalLevel);
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);

                //NOW START SEARCH AJAX
                request = $.ajax({
                    url: baseURL + 'user/users_by_country',
                    type: "post",
                    data: inputs,
                    dataType: 'html',
                });

                request.done(function (res) {
                    var x = JSON.parse(res);
                    if (x.status == 'success') {
                        $.LoadingOverlay("hide");
                        $('.locations-' + type).html(x.newView);
                    } else {
                        $.LoadingOverlay("hide");
                        $('.locations-' + type).html('<p>NO data found! try again.</p>');
                    }
                });


            } else {
                $.LoadingOverlay("hide");
                $('#provienceDiv-' + type).html(x.selectProvince);
                $('#districtDiv-' + type).html(x.selectDistrict);
                $('#localLevelDiv-' + type).html(x.selectLocalLevel);
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });
    //ON BROWSE FILTER PROVINCE
    $('body').delegate('#browseprovince-student', 'change', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var valueSelected = this.value;
        var formURL = baseURL + 'address/browse_change_province';
        var type = $(this).attr('data-type');
        var inputs = {province: valueSelected, type: type};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('#districtDiv-' + type).html(x.selectDistrict);
                $('#localLevelDiv-' + type).html(x.selectLocalLevel);
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);


                //NOW START SEARCH AJAX
                var country = $('#browsecountry-' + type).val();

                var inputss = {country: country, province: valueSelected, type: type};
                request = $.ajax({
                    url: baseURL + 'user/users_by_country_province',
                    type: "post",
                    data: inputss,
                    dataType: 'html',
                });

                request.done(function (res) {
                    var x = JSON.parse(res);
                    if (x.status == 'success') {
                        $.LoadingOverlay("hide");
                        $('.locations-' + type).html(x.newView);
                    } else {
                        $.LoadingOverlay("hide");
                        $('.locations-' + type).html('<p>NO data found! try again.</p>');
                    }
                });



            } else {
                $.LoadingOverlay("hide");
                $('#districtDiv-' + type).html(x.selectDistrict);
                $('#localLevelDiv-' + type).html(x.selectLocalLevel);
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });
    //BROWSE DISTRICT
    $('body').delegate('#browsedistrict-student', 'change', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var valueSelected = this.value;
        var formURL = baseURL + 'address/browse_change_district';
        var type = $(this).attr('data-type');
        var inputs = {district: valueSelected, type: type};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('#localLevelDiv-' + type).html(x.selectLocalLevel);
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);

                //NOW START SEARCH AJAX
                var country = $('#browsecountry-' + type).val();
                var province = $('#browseprovince-' + type).val();
                var inputss = {country: country, province: province, district: valueSelected, type: type};

                request = $.ajax({
                    url: baseURL + 'user/users_by_country_province_dis',
                    type: "post",
                    data: inputss,
                    dataType: 'html',
                });

                request.done(function (res) {
                    var x = JSON.parse(res);
                    if (x.status == 'success') {
                        $.LoadingOverlay("hide");
                        $('.locations-' + type).html(x.newView);
                    } else {
                        $.LoadingOverlay("hide");
                        $('.locations-' + type).html('<p>NO data found! try again.</p>');
                    }
                });


            } else {
                $.LoadingOverlay("hide");
                $('#localLevelDiv-' + type).html(x.selectLocalLevel);
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });
    //BROWSE LOCAL LEVEL
    $('body').delegate('#browselocal_level-student', 'change', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var valueSelected = this.value;
        var formURL = baseURL + 'address/browse_change_local_level';
        var type = $(this).attr('data-type');
        var inputs = {local_level: valueSelected, type: type};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);

                //NOW START SEARCH AJAX
                var country = $('#browsecountry-' + type).val();
                var province = $('#browseprovince-' + type).val();
                var district = $('#browsedistrict-' + type).val();
                var inputss = {country: country, province: province, district: district, local_level: valueSelected, type: type};

                request = $.ajax({
                    url: baseURL + 'user/users_by_country_province_dis_lc',
                    type: "post",
                    data: inputss,
                    dataType: 'html',
                });

                request.done(function (res) {
                    var x = JSON.parse(res);
                    if (x.status == 'success') {
                        $.LoadingOverlay("hide");
                        $('.locations-' + type).html(x.newView);
                    } else {
                        $.LoadingOverlay("hide");
                        $('.locations-' + type).html('<p>NO data found! try again.</p>');
                    }
                });



            } else {
                $.LoadingOverlay("hide");
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });
    //BROWSE WARD NO
    $('body').delegate('#browseward_no-student', 'change', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var type = $(this).attr('data-type');
        var country = $('#browsecountry-' + type).val();
        var province = $('#browseprovince-' + type).val();
        var district = $('#browsedistrict-' + type).val();
        var local_level = $('#browselocal_level-' + type).val();
        var valueSelected = this.value;
        var formURL = baseURL + 'user/get_browse_address_by_filter';
        var inputs = {country: country, province: province, district: district, local_level: local_level, ward_no: valueSelected, type: type};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('#addressDiv-' + type).html(x.selectAddress);

                //NOW START SEARCH AJAX
                var country = $('#browsecountry-' + type).val();
                var province = $('#browseprovince-' + type).val();
                var district = $('#browsedistrict-' + type).val();
                var local_level = $('#browselocal_level-' + type).val();
                var inputss = {country: country, province: province, district: district, local_level: local_level, ward_no: valueSelected, type: type};

                request = $.ajax({
                    url: baseURL + 'user/users_by_country_province_dis_lc_wn',
                    type: "post",
                    data: inputss,
                    dataType: 'html',
                });

                request.done(function (res) {
                    var x = JSON.parse(res);
                    if (x.status == 'success') {
                        $.LoadingOverlay("hide");
                        $('.locations-' + type).html(x.newView);
                    } else {
                        $.LoadingOverlay("hide");
                        $('.locations-' + type).html('<p>NO data found! try again.</p>');
                    }
                });


            } else {
                $.LoadingOverlay("hide");
                $('#addressDiv-' + type).html(x.selectAddress);
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });
    //BROWSE ADDRESS
    $('body').delegate('#browseaddress-student', 'change', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var valueSelected = this.value;
        var type = $(this).attr('data-type');
        var country = $('#browsecountry-' + type).val();
        var province = $('#browseprovince-' + type).val();
        var district = $('#browsedistrict-' + type).val();
        var local_level = $('#browselocal_level-' + type).val();
        var ward_no = $('#browseward_no-' + type).val();
        var inputss = {country: country, province: province, district: district, local_level: local_level, ward_no: ward_no, address: valueSelected, type: type};

        request = $.ajax({
            url: baseURL + 'user/users_by_country_province_dis_lc_wn_address',
            type: "post",
            data: inputss,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('.locations-' + type).html(x.newView);
            } else {
                $.LoadingOverlay("hide");
                $('.locations-' + type).html('<p>NO data found! try again.</p>');
                swal({
                    title: "Not Done!",
                    text: 'No data found',
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });


    //FOR BROWSE FILTER DATA ONLY FOR COLLEGE ==================================
    $('body').delegate('#browsecountry-college', 'change', function (event) {

        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var valueSelected = this.value;
        var formURL = baseURL + 'address/browse_change_country';
        var type = $(this).attr('data-type');
        var inputs = {country: valueSelected, type: type};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {

                $('#provienceDiv-' + type).html(x.resultSelect);
                $('#districtDiv-' + type).html(x.selectDistrict);
                $('#localLevelDiv-' + type).html(x.selectLocalLevel);
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);

                //NOW START SEARCH AJAX
                request = $.ajax({
                    url: baseURL + 'user/users_by_country',
                    type: "post",
                    data: inputs,
                    dataType: 'html',
                });

                request.done(function (res) {
                    var x = JSON.parse(res);
                    if (x.status == 'success') {
                        $.LoadingOverlay("hide");
                        $('.locations-' + type).html(x.newView);
                    } else {
                        $.LoadingOverlay("hide");
                        $('.locations-' + type).html('<p>NO data found! try again.</p>');
                    }
                });


            } else {
                $.LoadingOverlay("hide");
                $('#provienceDiv-' + type).html(x.selectProvince);
                $('#districtDiv-' + type).html(x.selectDistrict);
                $('#localLevelDiv-' + type).html(x.selectLocalLevel);
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });
    //ON BROWSE FILTER PROVINCE
    $('body').delegate('#browseprovince-college', 'change', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var valueSelected = this.value;
        var formURL = baseURL + 'address/browse_change_province';
        var type = $(this).attr('data-type');
        var inputs = {province: valueSelected, type: type};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('#districtDiv-' + type).html(x.selectDistrict);
                $('#localLevelDiv-' + type).html(x.selectLocalLevel);
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);


                //NOW START SEARCH AJAX
                var country = $('#browsecountry-' + type).val();

                var inputss = {country: country, province: valueSelected, type: type};
                request = $.ajax({
                    url: baseURL + 'user/users_by_country_province',
                    type: "post",
                    data: inputss,
                    dataType: 'html',
                });

                request.done(function (res) {
                    var x = JSON.parse(res);
                    if (x.status == 'success') {
                        $.LoadingOverlay("hide");
                        $('.locations-' + type).html(x.newView);
                    } else {
                        $.LoadingOverlay("hide");
                        $('.locations-' + type).html('<p>NO data found! try again.</p>');
                    }
                });



            } else {
                $.LoadingOverlay("hide");
                $('#districtDiv-' + type).html(x.selectDistrict);
                $('#localLevelDiv-' + type).html(x.selectLocalLevel);
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });
    //BROWSE DISTRICT
    $('body').delegate('#browsedistrict-college', 'change', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var valueSelected = this.value;
        var formURL = baseURL + 'address/browse_change_district';
        var type = $(this).attr('data-type');
        var inputs = {district: valueSelected, type: type};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('#localLevelDiv-' + type).html(x.selectLocalLevel);
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);

                //NOW START SEARCH AJAX
                var country = $('#browsecountry-' + type).val();
                var province = $('#browseprovince-' + type).val();
                var inputss = {country: country, province: province, district: valueSelected, type: type};

                request = $.ajax({
                    url: baseURL + 'user/users_by_country_province_dis',
                    type: "post",
                    data: inputss,
                    dataType: 'html',
                });

                request.done(function (res) {
                    var x = JSON.parse(res);
                    if (x.status == 'success') {
                        $.LoadingOverlay("hide");
                        $('.locations-' + type).html(x.newView);
                    } else {
                        $.LoadingOverlay("hide");
                        $('.locations-' + type).html('<p>NO data found! try again.</p>');
                    }
                });


            } else {
                $.LoadingOverlay("hide");
                $('#localLevelDiv-' + type).html(x.selectLocalLevel);
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });
    //BROWSE LOCAL LEVEL
    $('body').delegate('#browselocal_level-college', 'change', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var valueSelected = this.value;
        var formURL = baseURL + 'address/browse_change_local_level';
        var type = $(this).attr('data-type');
        var inputs = {local_level: valueSelected, type: type};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);

                //NOW START SEARCH AJAX
                var country = $('#browsecountry-' + type).val();
                var province = $('#browseprovince-' + type).val();
                var district = $('#browsedistrict-' + type).val();
                var inputss = {country: country, province: province, district: district, local_level: valueSelected, type: type};

                request = $.ajax({
                    url: baseURL + 'user/users_by_country_province_dis_lc',
                    type: "post",
                    data: inputss,
                    dataType: 'html',
                });

                request.done(function (res) {
                    var x = JSON.parse(res);
                    if (x.status == 'success') {
                        $.LoadingOverlay("hide");
                        $('.locations-' + type).html(x.newView);
                    } else {
                        $.LoadingOverlay("hide");
                        $('.locations-' + type).html('<p>NO data found! try again.</p>');
                    }
                });



            } else {
                $.LoadingOverlay("hide");
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });
    //BROWSE WARD NO
    $('body').delegate('#browseward_no-college', 'change', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var type = $(this).attr('data-type');
        var country = $('#browsecountry-' + type).val();
        var province = $('#browseprovince-' + type).val();
        var district = $('#browsedistrict-' + type).val();
        var local_level = $('#browselocal_level-' + type).val();
        var valueSelected = this.value;
        var formURL = baseURL + 'user/get_browse_address_by_filter';
        var inputs = {country: country, province: province, district: district, local_level: local_level, ward_no: valueSelected, type: type};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('#addressDiv-' + type).html(x.selectAddress);

                //NOW START SEARCH AJAX
                var country = $('#browsecountry-' + type).val();
                var province = $('#browseprovince-' + type).val();
                var district = $('#browsedistrict-' + type).val();
                var local_level = $('#browselocal_level-' + type).val();
                var inputss = {country: country, province: province, district: district, local_level: local_level, ward_no: valueSelected, type: type};

                request = $.ajax({
                    url: baseURL + 'user/users_by_country_province_dis_lc_wn',
                    type: "post",
                    data: inputss,
                    dataType: 'html',
                });

                request.done(function (res) {
                    var x = JSON.parse(res);
                    if (x.status == 'success') {
                        $.LoadingOverlay("hide");
                        $('.locations-' + type).html(x.newView);
                    } else {
                        $.LoadingOverlay("hide");
                        $('.locations-' + type).html('<p>NO data found! try again.</p>');
                    }
                });


            } else {
                $.LoadingOverlay("hide");
                $('#addressDiv-' + type).html(x.selectAddress);
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });
    //BROWSE ADDRESS
    $('body').delegate('#browseaddress-college', 'change', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var valueSelected = this.value;
        var type = $(this).attr('data-type');
        var country = $('#browsecountry-' + type).val();
        var province = $('#browseprovince-' + type).val();
        var district = $('#browsedistrict-' + type).val();
        var local_level = $('#browselocal_level-' + type).val();
        var ward_no = $('#browseward_no-' + type).val();
        var inputss = {country: country, province: province, district: district, local_level: local_level, ward_no: ward_no, address: valueSelected, type: type};

        request = $.ajax({
            url: baseURL + 'user/users_by_country_province_dis_lc_wn_address',
            type: "post",
            data: inputss,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('.locations-' + type).html(x.newView);
            } else {
                $.LoadingOverlay("hide");
                $('.locations-' + type).html('<p>NO data found! try again.</p>');
                swal({
                    title: "Not Done!",
                    text: 'No data found',
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });


    //FOR BROWSE FILTER DATA ONLY FOR INSTITUTE ================================
    $('body').delegate('#browsecountry-institute', 'change', function (event) {

        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var valueSelected = this.value;
        var formURL = baseURL + 'address/browse_change_country';
        var type = $(this).attr('data-type');
        var inputs = {country: valueSelected, type: type};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {

                $('#provienceDiv-' + type).html(x.resultSelect);
                $('#districtDiv-' + type).html(x.selectDistrict);
                $('#localLevelDiv-' + type).html(x.selectLocalLevel);
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);

                //NOW START SEARCH AJAX
                request = $.ajax({
                    url: baseURL + 'user/users_by_country',
                    type: "post",
                    data: inputs,
                    dataType: 'html',
                });

                request.done(function (res) {
                    var x = JSON.parse(res);
                    if (x.status == 'success') {
                        $.LoadingOverlay("hide");
                        $('.locations-' + type).html(x.newView);
                    } else {
                        $.LoadingOverlay("hide");
                        $('.locations-' + type).html('<p>NO data found! try again.</p>');
                    }
                });


            } else {
                $.LoadingOverlay("hide");
                $('#provienceDiv-' + type).html(x.selectProvince);
                $('#districtDiv-' + type).html(x.selectDistrict);
                $('#localLevelDiv-' + type).html(x.selectLocalLevel);
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });
    //ON BROWSE FILTER PROVINCE
    $('body').delegate('#browseprovince-institute', 'change', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var valueSelected = this.value;
        var formURL = baseURL + 'address/browse_change_province';
        var type = $(this).attr('data-type');
        var inputs = {province: valueSelected, type: type};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('#districtDiv-' + type).html(x.selectDistrict);
                $('#localLevelDiv-' + type).html(x.selectLocalLevel);
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);


                //NOW START SEARCH AJAX
                var country = $('#browsecountry-' + type).val();

                var inputss = {country: country, province: valueSelected, type: type};
                request = $.ajax({
                    url: baseURL + 'user/users_by_country_province',
                    type: "post",
                    data: inputss,
                    dataType: 'html',
                });

                request.done(function (res) {
                    var x = JSON.parse(res);
                    if (x.status == 'success') {
                        $.LoadingOverlay("hide");
                        $('.locations-' + type).html(x.newView);
                    } else {
                        $.LoadingOverlay("hide");
                        $('.locations-' + type).html('<p>NO data found! try again.</p>');
                    }
                });



            } else {
                $.LoadingOverlay("hide");
                $('#districtDiv-' + type).html(x.selectDistrict);
                $('#localLevelDiv-' + type).html(x.selectLocalLevel);
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });
    //BROWSE DISTRICT
    $('body').delegate('#browsedistrict-institute', 'change', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var valueSelected = this.value;
        var formURL = baseURL + 'address/browse_change_district';
        var type = $(this).attr('data-type');
        var inputs = {district: valueSelected, type: type};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('#localLevelDiv-' + type).html(x.selectLocalLevel);
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);

                //NOW START SEARCH AJAX
                var country = $('#browsecountry-' + type).val();
                var province = $('#browseprovince-' + type).val();
                var inputss = {country: country, province: province, district: valueSelected, type: type};

                request = $.ajax({
                    url: baseURL + 'user/users_by_country_province_dis',
                    type: "post",
                    data: inputss,
                    dataType: 'html',
                });

                request.done(function (res) {
                    var x = JSON.parse(res);
                    if (x.status == 'success') {
                        $.LoadingOverlay("hide");
                        $('.locations-' + type).html(x.newView);
                    } else {
                        $.LoadingOverlay("hide");
                        $('.locations-' + type).html('<p>NO data found! try again.</p>');
                    }
                });


            } else {
                $.LoadingOverlay("hide");
                $('#localLevelDiv-' + type).html(x.selectLocalLevel);
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });
    //BROWSE LOCAL LEVEL
    $('body').delegate('#browselocal_level-institute', 'change', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var valueSelected = this.value;
        var formURL = baseURL + 'address/browse_change_local_level';
        var type = $(this).attr('data-type');
        var inputs = {local_level: valueSelected, type: type};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);

                //NOW START SEARCH AJAX
                var country = $('#browsecountry-' + type).val();
                var province = $('#browseprovince-' + type).val();
                var district = $('#browsedistrict-' + type).val();
                var inputss = {country: country, province: province, district: district, local_level: valueSelected, type: type};

                request = $.ajax({
                    url: baseURL + 'user/users_by_country_province_dis_lc',
                    type: "post",
                    data: inputss,
                    dataType: 'html',
                });

                request.done(function (res) {
                    var x = JSON.parse(res);
                    if (x.status == 'success') {
                        $.LoadingOverlay("hide");
                        $('.locations-' + type).html(x.newView);
                    } else {
                        $.LoadingOverlay("hide");
                        $('.locations-' + type).html('<p>NO data found! try again.</p>');
                    }
                });



            } else {
                $.LoadingOverlay("hide");
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });
    //BROWSE WARD NO
    $('body').delegate('#browseward_no-institute', 'change', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var type = $(this).attr('data-type');
        var country = $('#browsecountry-' + type).val();
        var province = $('#browseprovince-' + type).val();
        var district = $('#browsedistrict-' + type).val();
        var local_level = $('#browselocal_level-' + type).val();
        var valueSelected = this.value;
        var formURL = baseURL + 'user/get_browse_address_by_filter';
        var inputs = {country: country, province: province, district: district, local_level: local_level, ward_no: valueSelected, type: type};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('#addressDiv-' + type).html(x.selectAddress);

                //NOW START SEARCH AJAX
                var country = $('#browsecountry-' + type).val();
                var province = $('#browseprovince-' + type).val();
                var district = $('#browsedistrict-' + type).val();
                var local_level = $('#browselocal_level-' + type).val();
                var inputss = {country: country, province: province, district: district, local_level: local_level, ward_no: valueSelected, type: type};

                request = $.ajax({
                    url: baseURL + 'user/users_by_country_province_dis_lc_wn',
                    type: "post",
                    data: inputss,
                    dataType: 'html',
                });

                request.done(function (res) {
                    var x = JSON.parse(res);
                    if (x.status == 'success') {
                        $.LoadingOverlay("hide");
                        $('.locations-' + type).html(x.newView);
                    } else {
                        $.LoadingOverlay("hide");
                        $('.locations-' + type).html('<p>NO data found! try again.</p>');
                    }
                });


            } else {
                $.LoadingOverlay("hide");
                $('#addressDiv-' + type).html(x.selectAddress);
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });
    //BROWSE ADDRESS
    $('body').delegate('#browseaddress-institute', 'change', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var valueSelected = this.value;
        var type = $(this).attr('data-type');
        var country = $('#browsecountry-' + type).val();
        var province = $('#browseprovince-' + type).val();
        var district = $('#browsedistrict-' + type).val();
        var local_level = $('#browselocal_level-' + type).val();
        var ward_no = $('#browseward_no-' + type).val();
        var inputss = {country: country, province: province, district: district, local_level: local_level, ward_no: ward_no, address: valueSelected, type: type};

        request = $.ajax({
            url: baseURL + 'user/users_by_country_province_dis_lc_wn_address',
            type: "post",
            data: inputss,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('.locations-' + type).html(x.newView);
            } else {
                $.LoadingOverlay("hide");
                $('.locations-' + type).html('<p>NO data found! try again.</p>');
                swal({
                    title: "Not Done!",
                    text: 'No data found',
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });





    //FOR ADVANCE SEARCH ADDRESS FOR STUDENT-COUNTRY
    $('body').delegate('#advcountry-student', 'change', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var valueSelected = this.value;
        var formURL = baseURL + 'address/adv_change_country';
        var type = $(this).attr('data-type');
        var inputs = {country: valueSelected, type: type};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('#provienceDiv-' + type).html(x.resultSelect);
                $('#districtDiv-' + type).html(x.selectDistrict);
                $('#localLevelDiv-' + type).html(x.selectLocalLevel);
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);

            } else {
                $.LoadingOverlay("hide");
                $('#provienceDiv-' + type).html(x.selectProvince);
                $('#districtDiv-' + type).html(x.selectDistrict);
                $('#localLevelDiv-' + type).html(x.selectLocalLevel);
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });
//FOR ADVANCE SEARCH ADDRESS FOR STUDENT-PROVINCE
    $('body').delegate('#advprovince-student', 'change', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var valueSelected = this.value;
        var formURL = baseURL + 'address/adv_change_province';
        var type = $(this).attr('data-type');
        var inputs = {province: valueSelected, type: type};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('#districtDiv-' + type).html(x.selectDistrict);
                $('#localLevelDiv-' + type).html(x.selectLocalLevel);
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);

            } else {
                $.LoadingOverlay("hide");
                $('#districtDiv-' + type).html(x.selectDistrict);
                $('#localLevelDiv-' + type).html(x.selectLocalLevel);
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });
//FOR ADVANCE SEARCH ADDRESS FOR STUDENT-DISTRICT
    $('body').delegate('#advdistrict-student', 'change', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var valueSelected = this.value;
        var formURL = baseURL + 'address/adv_change_district';
        var type = $(this).attr('data-type');
        var inputs = {district: valueSelected, type: type};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('#localLevelDiv-' + type).html(x.selectLocalLevel);
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);

            } else {
                $.LoadingOverlay("hide");
                $('#localLevelDiv-' + type).html(x.selectLocalLevel);
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });
//FOR ADVANCE SEARCH ADDRESS FOR STUDENT-LOCALLEVEL
    $('body').delegate('#advlocal_level-student', 'change', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var valueSelected = this.value;
        var formURL = baseURL + 'address/adv_change_local_level';
        var type = $(this).attr('data-type');
        var inputs = {local_level: valueSelected, type: type};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);
            } else {
                $.LoadingOverlay("hide");
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });
//FOR ADVANCE SEARCH ADDRESS FOR STUDENT-WARDNO
    $('body').delegate('#advward_no-student', 'change', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var type = $(this).attr('data-type');
        var country = $('#advcountry-' + type).val();
        var province = $('#advprovince-' + type).val();
        var district = $('#advdistrict-' + type).val();
        var local_level = $('#advlocal_level-' + type).val();
        var valueSelected = this.value;
        var formURL = baseURL + 'user/get_adv_address_by_filter';
        var inputs = {country: country, province: province, district: district, local_level: local_level, ward_no: valueSelected, type: type};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('#addressDiv-' + type).html(x.selectAddress);
            } else {
                $.LoadingOverlay("hide");
                $('#addressDiv-' + type).html(x.selectAddress);
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });



//FOR ADVANCE SEARCH ADDRESS FOR COLLEGE-COUNTRY
    $('body').delegate('#advcountry-college', 'change', function (event) {

        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var valueSelected = this.value;
        var formURL = baseURL + 'address/adv_change_country';
        var type = $(this).attr('data-type');
        var inputs = {country: valueSelected, type: type};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('#provienceDiv-' + type).html(x.resultSelect);
                $('#districtDiv-' + type).html(x.selectDistrict);
                $('#localLevelDiv-' + type).html(x.selectLocalLevel);
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);
            } else {
                $.LoadingOverlay("hide");
                $('#provienceDiv-' + type).html(x.selectProvince);
                $('#districtDiv-' + type).html(x.selectDistrict);
                $('#localLevelDiv-' + type).html(x.selectLocalLevel);
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });
//FOR ADVANCE SEARCH ADDRESS FOR COLLEGE-PROVINCE
    $('body').delegate('#advprovince-college', 'change', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var valueSelected = this.value;
        var formURL = baseURL + 'address/adv_change_province';
        var type = $(this).attr('data-type');
        var inputs = {province: valueSelected, type: type};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('#districtDiv-' + type).html(x.selectDistrict);
                $('#localLevelDiv-' + type).html(x.selectLocalLevel);
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);

            } else {
                $.LoadingOverlay("hide");
                $('#districtDiv-' + type).html(x.selectDistrict);
                $('#localLevelDiv-' + type).html(x.selectLocalLevel);
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });
//FOR ADVANCE SEARCH ADDRESS FOR COLLEGE-DISTRICT
    $('body').delegate('#advdistrict-college', 'change', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var valueSelected = this.value;
        var formURL = baseURL + 'address/adv_change_district';
        var type = $(this).attr('data-type');
        var inputs = {district: valueSelected, type: type};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('#localLevelDiv-' + type).html(x.selectLocalLevel);
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);

            } else {
                $.LoadingOverlay("hide");
                $('#localLevelDiv-' + type).html(x.selectLocalLevel);
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });
//FOR ADVANCE SEARCH ADDRESS FOR COLLEGE-LOCALLEVEL
    $('body').delegate('#advlocal_level-college', 'change', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var valueSelected = this.value;
        var formURL = baseURL + 'address/adv_change_local_level';
        var type = $(this).attr('data-type');
        var inputs = {local_level: valueSelected, type: type};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);

            } else {
                $.LoadingOverlay("hide");
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });
//FOR ADVANCE SEARCH ADDRESS FOR COLLEGE-WARDNO
    $('body').delegate('#advward_no-college', 'change', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var type = $(this).attr('data-type');
        var country = $('#advcountry-' + type).val();
        var province = $('#advprovince-' + type).val();
        var district = $('#advdistrict-' + type).val();
        var local_level = $('#advlocal_level-' + type).val();
        var valueSelected = this.value;
        var formURL = baseURL + 'user/get_adv_address_by_filter';
        var inputs = {country: country, province: province, district: district, local_level: local_level, ward_no: valueSelected, type: type};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('#addressDiv-' + type).html(x.selectAddress);
            } else {
                $.LoadingOverlay("hide");
                $('#addressDiv-' + type).html(x.selectAddress);
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });


//FOR ADVANCE SEARCH ADDRESS FOR INSTITUTE-COUNTRY
    $('body').delegate('#advcountry-institute', 'change', function (event) {

        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var valueSelected = this.value;
        var formURL = baseURL + 'address/adv_change_country';
        var type = $(this).attr('data-type');
        var inputs = {country: valueSelected, type: type};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('#provienceDiv-' + type).html(x.resultSelect);
                $('#districtDiv-' + type).html(x.selectDistrict);
                $('#localLevelDiv-' + type).html(x.selectLocalLevel);
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);

            } else {
                $.LoadingOverlay("hide");
                $('#provienceDiv-' + type).html(x.selectProvince);
                $('#districtDiv-' + type).html(x.selectDistrict);
                $('#localLevelDiv-' + type).html(x.selectLocalLevel);
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });
//FOR ADVANCE SEARCH ADDRESS FOR INSTITUTE-PROVINCE
    $('body').delegate('#advprovince-institute', 'change', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var valueSelected = this.value;
        var formURL = baseURL + 'address/adv_change_province';
        var type = $(this).attr('data-type');
        var inputs = {province: valueSelected, type: type};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('#districtDiv-' + type).html(x.selectDistrict);
                $('#localLevelDiv-' + type).html(x.selectLocalLevel);
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);

            } else {
                $.LoadingOverlay("hide");
                $('#districtDiv-' + type).html(x.selectDistrict);
                $('#localLevelDiv-' + type).html(x.selectLocalLevel);
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });
//FOR ADVANCE SEARCH ADDRESS FOR INSTITUTE-DISTRICT
    $('body').delegate('#advdistrict-institute', 'change', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var valueSelected = this.value;
        var formURL = baseURL + 'address/adv_change_district';
        var type = $(this).attr('data-type');
        var inputs = {district: valueSelected, type: type};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('#localLevelDiv-' + type).html(x.selectLocalLevel);
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);
            } else {
                $.LoadingOverlay("hide");
                $('#localLevelDiv-' + type).html(x.selectLocalLevel);
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });
//FOR ADVANCE SEARCH ADDRESS FOR INSTITUTE-DISTRICT
    $('body').delegate('#advlocal_level-institute', 'change', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var valueSelected = this.value;
        var formURL = baseURL + 'address/adv_change_local_level';
        var type = $(this).attr('data-type');
        var inputs = {local_level: valueSelected, type: type};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);
            } else {
                $.LoadingOverlay("hide");
                $('#wardnoDiv-' + type).html(x.selectWordNo);
                $('#addressDiv-' + type).html(x.selectAddress);
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });
//FOR ADVANCE SEARCH ADDRESS FOR INSTITUTE-WARDNO
    $('body').delegate('#advward_no-institute', 'change', function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }
        $.LoadingOverlay("show");
        var type = $(this).attr('data-type');
        var country = $('#advcountry-' + type).val();
        var province = $('#advprovince-' + type).val();
        var district = $('#advdistrict-' + type).val();
        var local_level = $('#advlocal_level-' + type).val();
        var valueSelected = this.value;
        var formURL = baseURL + 'user/get_adv_address_by_filter';
        var inputs = {country: country, province: province, district: district, local_level: local_level, ward_no: valueSelected, type: type};

        request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $.LoadingOverlay("hide");
                $('#addressDiv-' + type).html(x.selectAddress);
            } else {
                $.LoadingOverlay("hide");
                $('#addressDiv-' + type).html(x.selectAddress);
                swal({
                    title: "Not Done!",
                    text: x.msg,
                    type: "error",
                    timer: 3000,
                    html: true,
                    confirmButtonColor: "#fdb316",
                    showConfirmButton: true
                });
            }
        });
        request.fail(function (jqXHR, textStatus) {
            $.LoadingOverlay("hide");
            alert("Request failed: " + textStatus);
        });
    });




    //STOPPING THE AUTOPLAY
    $('.carousel').carousel({
        pause: true,
        interval: false
    });

});





$('#accordion .panel-collapse').on('shown.bs.collapse', function () {
    $(this).prev().find(".glyphicon").removeClass("glyphicon-chevron-right").addClass("glyphicon-chevron-down");
});

//The reverse of the above on hidden event:

$('#accordion .panel-collapse').on('hidden.bs.collapse', function () {
    $(this).prev().find(".glyphicon").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-right");
});



$('#accordion-inner .panel-collapse').on('shown.bs.collapse', function () {
    $(this).prev().find(".glyphicon").removeClass("glyphicon-chevron-right").addClass("glyphicon-chevron-down");
});

//The reverse of the above on hidden event:

$('#accordion-inner .panel-collapse').on('hidden.bs.collapse', function () {
    $(this).prev().find(".glyphicon").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-right");
});


//user ad place form here
$('#test-not-appeared').change(function () {
    if (this.checked) {
        $('#test-not-appeared-box').fadeOut('show');
    } else {
        $('#test-not-appeared-box').fadeIn('show');
    }
});

//CHANGE LABEL FOR CHANGING LOOKING FOR OPTION IN REGISER FORM
function twisterlebal(sel) {
    if (sel.value == 'college') {
        $('#label_name').html('College Name:');
        $('#label_address').html('College Address:');
    } else if (sel.value == 'institute') {
        $('#label_name').html('Institution Name:');
        $('#label_address').html('Institution Address:');
    } else {
        $('#label_name').html('Name:');
        $('#label_address').html('Address:');
    }
}


