$(document).ready(function () {
    var baseURL = 'http://localhost/hamrofinder/';
    //var baseURL = 'https://hamrofinder.com/';
    //LOGOUT FUNCTION
    $(document).on('click', 'a[name=logOut]', function (e) {
        e.preventDefault();
        var postData = {"logout": "yes"};
        var formUrl = $(this).attr('data-url');
        $(this).find('.ajax-overlay').css('display', 'block');
        var request = $.ajax({
            url: formUrl,
            method: "POST",
            data: postData,
            dataType: "html"
        });
        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                window.location.href = 'appslogin';
            } else {
                alert('somthing wrong');
                window.location.href = 'appslogin';
            }
        });
        request.fail(function (jqXHR, textStatus) {
            alert("Request Fail: " + textStatus);
        });
    });

    //ADDING NEW IMAGE
    $('#addImages').submit(function (e) {
        e.preventDefault();
        var postData = $(this).serialize();
        var formUrl = $(this).attr('action');
        //styling css while ajaxing
        $(this).find('.ajax-overlay').css('display', 'block');
        $('#submitBtn').addClass('disabled');
        $('#submitBtn').html('<i class="fa fa-spinner fa-spin"></i> Submiting...');
        var request = $.ajax({
            url: formUrl,
            dataType: "html",
            type: "POST", // Type of request to be send, called as method
            data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false, // The content type used when sending data to the server.
            cache: false, // To unable request pages to be cached
            processData: false,
        });
        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                //styling css while ajax finish
                $('.ajax-overlay').css('display', 'none');
                $('#submitBtn').removeClass('disabled');
                $('#submitBtn').html('Submit');
                $('.showMsg').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
                window.location.reload();
            } else {
                //styling css while ajax finish
                $('.ajax-overlay').css('display', 'none');
                $('#submitBtn').removeClass('disabled');
                //putting values
                $('#submitBtn').html('Submit');
                $('.showMsg').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
            }
        });
        request.fail(function (jqXHR, textStatus) {
            alert("Request failed: " + textStatus);
        });
    });

    //SETINGS AJAX 
    $('#settingUpdateForm').submit(function (e) {
        e.preventDefault();
        var postData = $(this).serialize();
        var formUrl = $(this).attr('action');
        $(this).find('.ajax-overlay').css('display', 'block');
        var request = $.ajax({
            url: formUrl,
            method: "POST",
            data: postData,
            dataType: "html"
        });
        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $('.ajax-overlay').css('display', 'none');
                $('.showMsg').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
            } else {
                $('.ajax-overlay').css('display', 'none');
                $('.showMsg').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
            }
        });
        request.fail(function (jqXHR, textStatus) {
            alert("Request Fail: " + textStatus);
        });
    });

    //AJAX UPDATE METHOD IN WEBSITE BACKEND
    $('.commonUpdate').submit(function (e) {
        e.preventDefault();
        var postData = $(this).serialize();
        var formUrl = $(this).attr('action');
        $(this).find('.ajax-overlay').css('display', 'block');
        var request = $.ajax({
            url: formUrl,
            method: "POST",
            data: postData,
            dataType: "html"
        });
        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $('.ajax-overlay').css('display', 'none');
                $('.showMsg').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
            } else {
                $('.ajax-overlay').css('display', 'none');
                $('.showMsg').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
            }
        });
        request.fail(function (jqXHR, textStatus) {
            alert("Request Fail: " + textStatus);
        });
    });
    //UPDATE REFRESH
    $('.commonUpdateRefresh').submit(function (e) {
        e.preventDefault();
        var postData = $(this).serialize();
        var formUrl = $(this).attr('action');
        $(this).find('.ajax-overlay').css('display', 'block');
        var request = $.ajax({
            url: formUrl,
            method: "POST",
            data: postData,
            dataType: "html"
        });
        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $('.ajax-overlay').css('display', 'none');
                $('.showMsg').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
                window.location.reload();
            } else {
                $('.ajax-overlay').css('display', 'none');
                $('.showMsg').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
                alert(x.msg);
                window.location.reload();
            }
        });
        request.fail(function (jqXHR, textStatus) {
            alert("Request Fail: " + textStatus);
        });
    });

    //NEWS UPDATES
    $('.newsInsert').submit(function (e) {
        e.preventDefault();
        var postData = $(this).serialize();
        var formUrl = $(this).attr('action');
        var baseURL = $('#base').val();
        $(this).find('.ajax-overlay').css('display', 'block');
        var request = $.ajax({
            url: formUrl,
            method: "POST",
            data: postData,
            dataType: "html"
        });
        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                e;
                $('.ajax-overlay').css('display', 'none');
                $('.showMsg').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
                window.location.href = baseURL + 'appsnews/edit/' + x.ID;
            } else {
                $('.ajax-overlay').css('display', 'none');
                $('.showMsg').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
            }
        });
        request.fail(function (jqXHR, textStatus) {
            alert("Request Fail: " + textStatus);
        });
    });
    //NEWS UPDATES UPDATES
    $('.newsUpdateRefresh').submit(function (e) {
        e.preventDefault();
        var postData = $(this).serialize();
        var formUrl = $(this).attr('action');
        var baseURL = $('#base').val();
        $(this).find('.ajax-overlay').css('display', 'block');
        var request = $.ajax({
            url: formUrl,
            method: "POST",
            data: postData,
            dataType: "html"
        });
        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $('.ajax-overlay').css('display', 'none');
                $('.showMsg').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
                window.location.href = baseURL + 'appsnews/edit/' + x.ID;
            } else {
                $('.ajax-overlay').css('display', 'none');
                $('.showMsg').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
            }
        });
        request.fail(function (jqXHR, textStatus) {
            alert("Request Fail: " + textStatus);
        });
    });
    //inserting
    $('.admininsertevent').submit(function (e) {
        e.preventDefault();
        var postData = $(this).serialize();
        var formUrl = $(this).attr('action');
        $(this).find('.ajax-overlay').css('display', 'block');
        var request = $.ajax({
            url: formUrl,
            method: "POST",
            data: postData,
            dataType: "html"
        });
        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $('.ajax-overlay').css('display', 'none');
                $('.showMsg').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
                window.location.href = baseURL + 'appsevents/edit/' + x.ID;
            } else {
                $('.ajax-overlay').css('display', 'none');
                $('.showMsg').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
            }
        });
        request.fail(function (jqXHR, textStatus) {
            alert("Request Fail: " + textStatus);
        });
    });
    //INSERTING ADDRESS
    $('.admininsertcountry').submit(function (e) {
        e.preventDefault();
        var postData = $(this).serialize();
        var formUrl = $(this).attr('action');
        $(this).find('.ajax-overlay').css('display', 'block');
        var request = $.ajax({
            url: formUrl,
            method: "POST",
            data: postData,
            dataType: "html"
        });
        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $('.ajax-overlay').css('display', 'none');
                $('.showMsg').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
                window.location.href = baseURL + 'appsaddress/edit/' + x.ID;
            } else {
                $('.ajax-overlay').css('display', 'none');
                $('.showMsg').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
            }
        });
        request.fail(function (jqXHR, textStatus) {
            alert("Request Fail: " + textStatus);
        });
    });

    //ADDRESS UPDATE MODAL
    $('.editmdladdress').click(function () {
        var title = $(this).attr('data-title');
        var id = $(this).attr('data-id');
        $('#addressTitle').val(title);
        $('#addressID').val(id)
    });

    //UPDATING
    $('.adminupdate').submit(function (e) {
        e.preventDefault();
        var postData = $(this).serialize();
        var formUrl = $(this).attr('action');
        $(this).find('.ajax-overlay').css('display', 'block');
        var request = $.ajax({
            url: formUrl,
            method: "POST",
            data: postData,
            dataType: "html"
        });
        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $('.ajax-overlay').css('display', 'none');
                $('.showMsg').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
                window.location.reload();
            } else {
                $('.ajax-overlay').css('display', 'none');
                $('.showMsg').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
                window.location.reload();
            }
        });
        request.fail(function (jqXHR, textStatus) {
            alert("Request Fail: " + textStatus);
        });
    });

    //ADDING GALLERY IMAGE
    $('.adminupdategallery').submit(function (e) {
        e.preventDefault();
        var postData = $(this).serialize();
        var formUrl = $(this).attr('action');
        $(this).find('.ajax-overlay').css('display', 'block');
        var request = $.ajax({
            url: formUrl,
            method: "POST",
            data: postData,
            dataType: "html"
        });
        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $('.ajax-overlay').css('display', 'none');
                $('.showMsg').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
                window.location.reload();
            } else {
                $('.ajax-overlay').css('display', 'none');
                $('.showMsg').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
            }
        });
        request.fail(function (jqXHR, textStatus) {
            alert("Request Fail: " + textStatus);
        });
    });
    
    
    //UPDATE ADDRESS
    $('body').delegate('#updateAddressTitle', 'click', function (e) {
        e.preventDefault();
        var id = $('#addressID').val();
        var title = $('#addressTitle').val();
        var formURL = baseURL + 'appsaddress/update';
        var inputs = {id: id, title: title};
        var request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });
        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $('.showMsg').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
            } else {
                $('.showMsg').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
            }
        });
        request.fail(function (jqXHR, textStatus) {
            alert("Request failed: " + textStatus);
        });
    });


    //GALLERY IMAGES DELETE
    $('body').delegate('.remove-btn-img', 'click', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var userID = $(this).data('user');
        var formURL = baseURL + 'appsimages/del_gallery_adm';
        var inputs = {id: id, userID: userID};
        var request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });
        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $('.galimgsingle-' + id).css('display', 'none');
                $('.showMsg').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
            } else {
                $('.showMsg').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
            }
        });
        request.fail(function (jqXHR, textStatus) {
            alert("Request failed: " + textStatus);
        });
    });
    //REMOVE VIDEO BY ADMIN
    $('body').delegate('.vid_del_adm', 'click', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var userID = $(this).data('user');
        var formURL = baseURL + 'appsvideos/remove';
        var inputs = {id: id, userID: userID};
        var request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });
        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $('.vidindadm-' + id).css('display', 'none');
                $('.showMsg').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
            } else {
                $('.showMsg').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
            }
        });
        request.fail(function (jqXHR, textStatus) {
            alert("Request failed: " + textStatus);
        });
    });
    //REMOVING DOCUMENTS 
    $('body').delegate('.doc_del_adm', 'click', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var userID = $(this).data('user');
        var formURL = baseURL + 'appsdocument/del_doc';
        var inputs = {id: id, userID: userID};
        var request = $.ajax({
            url: formURL,
            type: "post",
            data: inputs,
            dataType: 'html',
        });
        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $('.docholderadm-' + id).css('display', 'none');
                $('.showMsg').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
            } else {
                $('.showMsg').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
            }
        });
        request.fail(function (jqXHR, textStatus) {
            alert("Request failed: " + textStatus);
        });
    });

    //adding provience
    $('#provinceAdd').click(function (e) {
        e.preventDefault();
        var html = '<div class="well">';
        html += '<label>Province Name:</label><input type="text" name="province[]" class="form-control">';
        html += "</div>";
        $('#provinceDiv').append(html);
    });
    //adding district
    $('.districtAdd').click(function (e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        var html = '<div class="well container-' + id + '">';
        html += '<label>District Name:</label><input type="text" name="districts-' + id + '[]" class="form-control">';
        html += "</div>";
        $('.well-' + id).append(html);
    });

    //SIDEBAR
    $('body').delegate('#textbtn', 'click', function (event) {
        event.preventDefault();
        $('#mainWiget').append('<div class="wigetBox"><label>Text to be display(can use HTML):</label><textarea class="form-control" name="fields[]"></textarea></div>');
    });
    $('body').delegate('#imgbtn', 'click', function (event) {
        event.preventDefault();
        $('#mainWiget').append('<div class="wigetBox"><label>Image Banner URL:</label><p>Please select image from <a href="http://kanekhusi.com/appsimages" target="_blank">Image Library</a><br/>Give image absolute URL:eg http://example.com/img/image.jpg</p><input type="text" class="form-control" name="fields[]"><label>Link URL:</label><input type="text" class="form-control" name="fields[]" value="#"></div>');
    });
    $('body').delegate('#galleryimgbtn', 'click', function (event) {
        event.preventDefault();
        $('#mainWiget').append('<div class="wigetBox"><label>Add New Photo</label><p>Please select image from <a href="http://kanekhusi.com/appsimages" target="_blank">Image Library</a><br/>Give image absolute URL:eg http://example.com/img/image.jpg</p><input type="text" class="form-control" name="url"><br/><label>Display Photo Thumbnail Size:</label><br/><input type="radio" name="type" value="Big"> Big <input type="radio"  name="type" value="Small" checked> Small</div>');
        $('.adder').hide();
    });






});



