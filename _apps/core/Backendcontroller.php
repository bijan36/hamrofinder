<?php

defined('BASEPATH') OR exit('No direct script access allowed');

Class Backendcontroller extends MX_Controller {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Kathmandu");
        $this->load->helper('back_helper');
        Modules::run('appslogin/is_admin_login');
    }

}
