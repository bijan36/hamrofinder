<?php

defined('BASEPATH') OR exit('No direct script access allowed');

Class Frontendcontroller extends MX_Controller {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Kathmandu");
        $this->load->helper('front_helper');
    }

}
