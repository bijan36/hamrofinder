<?php

//HELPER FOR FRONT END
function get_template($x = NULL) {
    if ($x) {
        return 'back/templates/' . $x;
    } else {
        return 'back/templates/login';
    }
}

function load_ajax($x) {
    if ($x) {
        return 'back/ajax/' . $x;
    }
}

//GET PUBLIC URL
function get_assist($x = NULL) {
    if ($x) {
        return base_url() . '_public/back/' . $x . '/';
    } else {
        return base_url() . '_public/back/';
    }
}

//GETTING SESSION DATA
function sessionData($dataName) {
    $CI = & get_instance();
    return $CI->session->userdata($dataName) ? $CI->session->userdata($dataName) : 'unknown';
}

function justRedirect($controllers = null, $mood = null, $message = null) {
    $con = $controllers ? $controllers : 'appsdashboard';
    $mod = $mood ? $mood : 'msge';
    $msg = $message ? $message : ADMINMSG;
    $CI = & get_instance();
    $CI->session->set_flashdata($mod, $msg);
    redirect($con);
    exit();
}

function slugify($text) {
    // replace non letter or digits by -
    $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
    // trim
    $text = trim($text, '-');
    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    // lowercase
    $text = strtolower($text);
    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);
    if (empty($text)) {
        return 'n-a';
    }
    return $text;
}

//MAKING TITLE OF THE IAMGE
function make_image_title($filename) {
    $file = preg_replace('/[^A-Za-z0-9\-]/', '', $filename);
    $file = preg_replace('/\\.[^.\\s]{3,4}$/', '', $file);
    $file = str_replace('_', ' ', $file);
    $file = str_replace('-', ' ', $file);
    $file = str_replace('jpg', '', $file);
    $file = str_replace('JPG', '', $file);
    $file = str_replace('JPEG', '', $file);
    $file = str_replace('jpeg', '', $file);
    $file = str_replace('png', '', $file);
    $file = str_replace('PNG', '', $file);
    $file = str_replace('gif', '', $file);
    $file = str_replace('GIF', '', $file);
    $file = str_replace('pdf', '', $file);
    $file = str_replace('PDF', '', $file);
    $file = preg_replace('!\s+!', ' ', $file);
    $file = preg_replace('/\d/', '', $file); //remove numbeers
    return $file . ' ' . COMPANYNAME;
}

function make_image_filename($filename) {
    $filename = preg_replace('/[^A-Za-z\-]/', '_', $filename);
    $filename = str_replace('-', '_', $filename);
    $filename = preg_replace('/_+/', '_', $filename);
    return $filename;
}

//COLORED STATUS
function status_color($status) {
    if (isset($status)) {
        if ($status == ACTIVE) {
            return "<span class='text-success'><i class='fa fa-check-square-o' aria-hidden='true'></i> " . ACTIVE . "</span>";
        } elseif ($status == INACTIVE) {
            return "<span class='text-danger'><i class='fa fa-times' aria-hidden='true'></i>" . INACTIVE . "</span>";
        } elseif ($status == 'Banned') {
            return "<span class='text-danger'><i class='fa fa-times' aria-hidden='true'></i>Banned</span>";
        } elseif ($status == 'Expired') {
            return "<span class='text-danger'><i class='fa fa-times' aria-hidden='true'></i>Expired</span>";
        } else {
            return "<span class='text-warning'><i class='fa fa-asterisk' aria-hidden='true'></i>Unknown</span>";
        }
    } else {
        return "<span class='text-warning'><i class='fa fa-asterisk' aria-hidden='true'></i>Unknown</span>";
    }
}

//TODAY DATE
function today_date(){
    return date('Y-m-d');
}

//AD STATUS
function ad_status_color($status, $end_date) {
    if (isset($status) && isset($end_date)) {
        if (today_date() > $end_date) {
            return "<span class='text-danger'><i class='fa fa-times' aria-hidden='true'></i>Expired</span>";
        } else {
            if ($status == ACTIVE) {
                return "<span class='text-success'><i class='fa fa-check-square-o' aria-hidden='true'></i> " . ACTIVE . "</span>";
            } elseif ($status == INACTIVE) {
                return "<span class='text-danger'><i class='fa fa-times' aria-hidden='true'></i>" . INACTIVE . "</span>";
            } elseif ($status == 'Deleted') {
                return "<span class='text-danger'><i class='fa fa-times' aria-hidden='true'></i>Deleted</span>";
            } else {
                return "<span class='text-warning'><i class='fa fa-asterisk' aria-hidden='true'></i>Unknown</span>";
            }
        }
    } else {
        return "<span class='text-warning'><i class='fa fa-asterisk' aria-hidden='true'></i>Unknown</span>";
    }
}

//filtering date only for front end
function makeReadableDate($datetime) {
    if ($datetime == 0) {
        return "--";
    } else {
        $date = strtotime($datetime);
        return date('d-m-Y', $date);
    }
}

function makedatadate($string) {
    $parts = explode('-', $string);
    return $parts[2] . '-' . $parts[1] . '-' . $parts[0];
}

function makevisibledate($string) {
    $parts = explode('-', $string);
    return $parts[2] . '-' . $parts[1] . '-' . $parts[0];
}

function current_date() {
    return date('Y-m-d');
}

//VERIFY START DATE
function verify_ad_start_date($string) {
    $parts = explode('-', $string);
    if ($parts[0] == '0') {
        return date('d-m-Y');
    } else {
        return $parts[2] . '-' . $parts[1] . '-' . $parts[0];
    }
}

function verify_ad_end_date($string, $string1 = null) {
    $parts = explode('-', $string);
    if ($parts[0] == '0') {
        return date('d-m-Y', strtotime('+2 months'));
    } else {
        return $parts[2] . '-' . $parts[1] . '-' . $parts[0];
    }
}

function make_responsive_image($image_name, $size, $extra_class = null) {
    $img = image(base_url() . 'media/' . $image_name, $size);
    $eclass = $extra_class ? $extra_class : '';
    return '<img src="' . $img . '" class="img-responsive ' . $eclass . '">';
}

function user_level_select($level) {
    if (!empty($level)) {
        $allLevel = array('Registered', 'Basic', 'Privileged', 'Premium');

        $boxes = '<select class="form-control" name="level">';
        if (is_array($allLevel)) {
            foreach ($allLevel as $part):
                if ($part == $level) {
                    $boxes .= '<option value="' . $part . '" selected>' . $part . '</option>';
                } else {
                    $boxes .= '<option value="' . $part . '">' . $part . '</option>';
                }

            endforeach;
        }
        $boxes .= '</select>';
        return $boxes;
    }
}

function is_serialized($data, $strict = true) {
    // if it isn't a string, it isn't serialized.
    if (!is_string($data)) {
        return false;
    }
    $data = trim($data);
    if ('N;' == $data) {
        return true;
    }
    if (strlen($data) < 4) {
        return false;
    }
    if (':' !== $data[1]) {
        return false;
    }
    if ($strict) {
        $lastc = substr($data, -1);
        if (';' !== $lastc && '}' !== $lastc) {
            return false;
        }
    } else {
        $semicolon = strpos($data, ';');
        $brace = strpos($data, '}');
        // Either ; or } must exist.
        if (false === $semicolon && false === $brace)
            return false;
        // But neither must be in the first X characters.
        if (false !== $semicolon && $semicolon < 3)
            return false;
        if (false !== $brace && $brace < 4)
            return false;
    }
    $token = $data[0];
    switch ($token) {
        case 's' :
            if ($strict) {
                if ('"' !== substr($data, -2, 1)) {
                    return false;
                }
            } elseif (false === strpos($data, '"')) {
                return false;
            }
        // or else fall through
        case 'a' :
        case 'O' :
            return (bool) preg_match("/^{$token}:[0-9]+:/s", $data);
        case 'b' :
        case 'i' :
        case 'd' :
            $end = $strict ? '$' : '';
            return (bool) preg_match("/^{$token}:[0-9.E-]+;$end/", $data);
    }
    return false;
}

function makeMoney($x) {
    if ($x) {
        return number_format($x, 2, '.', '');
    } else {
        return '0.00';
    }
}

?>