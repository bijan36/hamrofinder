<?php

//HELPER FOR FRONT END
function get_template($x = NULL) {
    if ($x) {
        return 'front/templates/' . $x;
    } else {
        return 'front/templates/default';
    }
}

//GET PUBLIC URL
function get_assist($x = NULL) {
    if ($x) {
        return base_url() . '_public/front/assets/' . $x;
    } else {
        return base_url() . '_public/front/assets/';
    }
}

function load_ajax($x) {
    if ($x) {
        return 'front/ajax/' . $x;
    }
}

function is_set_value($id) {
    if (!$id) {
        redirect('site');
        exit();
    }
}

function get_placeholder_img($ext) {
    $sampleFile = 'file.png';
    if (isset($ext)) {
        $ext = strtolower($ext);
        if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif') {
            $sampleFile = 'jpg.png';
        } elseif ($ext == 'pdf') {
            $sampleFile = 'pdf.png';
        } elseif ($ext == 'doc' || $ext == 'docx') {
            $sampleFile = 'doc.png';
        } elseif ($ext == 'xls' || $ext == 'xlsx') {
            $sampleFile = 'xls.png';
        } else {
            
        }
    }
    return $sampleFile;
}

function makeArrayID($complex) {
    if (!empty($complex)) {
        $ids = array();
        foreach ($complex as $complex1):
            $ids[] = $complex1->ID;
        endforeach;
        return $ids;
    }else {
        return array();
    }
}

function makeArrayParentID($complex) {
    if (!empty($complex)) {
        $ids = array();
        foreach ($complex as $complex1):
            $ids[] = $complex1->parent_ID;
        endforeach;
        return $ids;
    }else {
        return array();
    }
}

function make_image_title($filename) {
    $file = preg_replace('/\\.[^.\\s]{3,4}$/', '', $filename);
    //$file = preg_replace('/[^A-Za-z\-]/', '', $file);
    //$file = preg_replace('/[^a-z]+/i', '', $filename);
    $file = str_replace('_', ' ', $file);
    $file = str_replace('-', ' ', $file);
    $file = str_replace('jpg', '', $file);
    $file = str_replace('JPG', '', $file);
    $file = str_replace('JPEG', '', $file);
    $file = str_replace('jpeg', '', $file);
    $file = str_replace('png', '', $file);
    $file = str_replace('PNG', '', $file);
    $file = str_replace('gif', '', $file);
    $file = str_replace('GIF', '', $file);
    $file = str_replace('pdf', '', $file);
    $file = str_replace('PDF', '', $file);
    $file = preg_replace('!\s+!', ' ', $file);
    $file = preg_replace('/\d/', '', $file); //remove numbeers
    return $file . ' ' . COMPANYNAME;
}

function make_image_title_no_company($filename) {
    $file = preg_replace('/\\.[^.\\s]{3,4}$/', '', $filename);
    //$file = preg_replace('/[^A-Za-z\-]/', '', $file);
    //$file = preg_replace('/[^a-z]+/i', '', $filename);
    $file = str_replace('_', ' ', $file);
    $file = str_replace('-', ' ', $file);
    $file = str_replace('jpg', '', $file);
    $file = str_replace('JPG', '', $file);
    $file = str_replace('JPEG', '', $file);
    $file = str_replace('jpeg', '', $file);
    $file = str_replace('png', '', $file);
    $file = str_replace('PNG', '', $file);
    $file = str_replace('gif', '', $file);
    $file = str_replace('GIF', '', $file);
    $file = str_replace('pdf', '', $file);
    $file = str_replace('PDF', '', $file);
    $file = preg_replace('!\s+!', ' ', $file);
    $file = preg_replace('/\d/', '', $file); //remove numbeers
    return $file;
}

function make_image_filename($filename) {
    $file = preg_replace('/\\.[^.\\s]{3,4}$/', '', $filename);
    $file = str_replace('-', '_', $file);
    return $file;
}

//CHECK THE ADVERTISE CAN BE SHOWN OR NOT
function can_show_ad($adID, $userID) {
    if (isset($adID) && isset($userID)) {
        //CHECK THE USER VALID
        //$CI =& get_instance();
        $userRow = Modules::run('user/get_row', $userID);
        if ($userRow) {
            if ($userRow->status == ACTIVE) {
                //IF STUDENT SEARCH IN STUDENT AD TABLE
                if ($userRow->type == 'Student') {
                    $studentAD = Modules::run('studentad/get_row', $adID);
                    if ($studentAD && $studentAD->parent_ID == $userID) {
                        if ($studentAD->status == ACTIVE) {
                            $today = today_date();
                            if ($today <= $studentAD->ad_end_date && $today >= $studentAD->ad_start_date) {
                                //FINALLY VALID
                                return true;
                            } else {
                                return false;
                            }
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                    //IF STUDENT SEARCH IN STUDENT AD TABLE
                } elseif ($userRow->type == 'College') {
                    $collegeAD = Modules::run('collegead/get_row', $adID);
                    if ($collegeAD && $collegeAD->parent_ID == $userID) {
                        if ($collegeAD->status == ACTIVE) {
                            $today = today_date();
                            if ($today <= $collegeAD->ad_end_date && $today >= $collegeAD->ad_start_date) {
                                //FINALLY VALID
                                return true;
                            } else {
                                return false;
                            }
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                } elseif ($userRow->type == 'Institute') {
                    $collegeAD = Modules::run('institutead/get_row', $adID);
                    if ($collegeAD && $collegeAD->parent_ID == $userID) {
                        if ($collegeAD->status == ACTIVE) {
                            $today = today_date();
                            if ($today <= $collegeAD->ad_end_date && $today >= $collegeAD->ad_start_date) {
                                //FINALLY VALID
                                return true;
                            } else {
                                return false;
                            }
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    } else {
        return false;
    }
}

//GETTING URS OF USERS
function profile_url($userID) {
    if (isset($userID)) {

        $userRow = Modules::run('user/get_row', $userID);
        if ($userRow) {
            if ($userRow->type == 'Student') {
                return site_url('user/show/' . $userRow->ID);
            } elseif ($userRow->type == 'College') {
                return site_url('user/show/' . $userRow->ID);
            } elseif ($userRow->type == 'Institute') {
                return site_url('user/show/' . $userRow->ID);
            } else {
                return site_url('notfound');
            }
        } else {
            return site_url('notfound');
        }
    } else {
        return site_url('notfound');
    }
}

//AD URL 
function get_ads_permalink($id, $userID) {
    if (isset($id) && isset($userID)) {
        if (can_show_ad($id, $userID)) {
            $userRow = Modules::run('user/get_row', $userID);
            if ($userRow->type == 'Student') {
                return site_url('studentad/ads/' . $id);
            } elseif ($userRow->type == 'College') {
                return site_url('collegead/ads/' . $id);
            } elseif ($userRow->type == 'Institute') {
                return site_url('institutead/ads/' . $id);
            } else {
                return site_url('notfound');
            }
        } else {
            return site_url('notfound');
        }
    } else {
        return site_url('notfound');
    }
}

//GET SLECT BOX
function get_select_box($slug, $selected = null) {
    $output = '<option value="">Select One</option>';
    if ($slug) {
        $CI = & get_instance();
        $rows = Modules::run('settings/get_value', $slug);
        if ($rows) {
            $rows = explode(',', $rows);
            sort($rows);
            foreach ($rows as $ro):
                if ($selected) {
                    $sel = $selected == trim($ro) ? 'selected' : '';
                    $output .= '<option value="' . trim($ro) . '"' . $sel . '>' . trim($ro) . '</option>';
                } else {
                    $output .= '<option value="' . trim($ro) . '">' . trim($ro) . '</option>';
                }
            endforeach;
        } else {
            $output .= '<option value="0">Not Found!</option>';
        }
    } else {
        $output .= '<option value="0">Not Found!</option>';
    }
    return $output;
}

function get_check_box($slug, $name, $chekedArray = null) {
    $output = '';
    if ($slug) {
        $CI = & get_instance();
        $rows = Modules::run('settings/get_value', $slug);
        if ($rows) {
            $rows = explode(',', $rows);
            foreach ($rows as $ro):
                $ro = trim($ro);
                if (!empty($chekedArray)) {
                    $chk = in_array($ro, $chekedArray) ? 'checked' : '';
                    $output .= '<div class="myCheck"><input type="checkbox" name="' . $name . '[]" value="' . trim($ro) . '" ' . $chk . '> ' . $ro . "</div>";
                } else {
                    $output .= '<div class="myCheck"><input type="checkbox" name="' . $name . '[]" value="' . trim($ro) . '"> ' . $ro . "</div>";
                }
            endforeach;
        } else {
            $output .= '';
        }
    } else {
        $output .= '';
    }
    return $output;
}

//GET TOTAL INTEREST
function total_interest($adID, $type) {
    if (isset($adID)) {
        $totalRow = Modules::run('interest/get_total_by_ad', $adID, $type);
        if ($totalRow) {
            return $totalRow;
        } else {
            return '0';
        }
    } else {
        return '0';
    }
}

//GETTING ADMIN NOTIFICATION EMAIL
function admin_notification_email() {
    $emailValue = Modules::run('emailsettings/get_value', 'admin-email-for-notification');
    if ($emailValue) {
        return $emailValue;
    } else {
        return SYSTEMEMAIL;
    }
}

function admin_notification_email_name() {
    $emailName = Modules::run('emailsettings/get_value', 'admin-name-of-notification-email');
    if ($emailName) {
        return $emailName;
    } else {
        return COMPANYNAME;
    }
}

//LIMITED WORDS
function get_limited_words($content, $numbers) {
    if (strlen($content) > $numbers) {
        $pos = strpos($content, ' ', $numbers);
        return substr($content, 0, $pos) . '...';
    } else {
        return $content;
    }
}

function justRedirect($controllers = null, $mood = null, $message = null) {
    $con = $controllers ? $controllers : 'site';
    $mod = $mood ? $mood : 'msge';
    $msg = $message ? $message : '';

    $ci = &get_instance();
    $ci->session->set_flashdata($mod, $msg);
    redirect($con);
    exit();
}

//GETTING COUNTRY LIST
function country_list($name, $selected = null) {
    $CI = & get_instance();
    $countries = $CI->config->item('country_list');
    if ($selected && !empty($selected)) {
        return form_dropdown($name, $countries, $selected, "class='form-control'");
    } else {
        return form_dropdown($name, $countries, '', 'class="form-control"');
    }
}

function country_name($string) {
    $CI = & get_instance();
    $countries = $CI->config->item('country_list');
    return $countries[$string];
}

//CHECKING USER LOGIN
function is_user_login() {
    $CI = & get_instance();
    if ($CI->session->userdata('userLogin')) {
        return true;
    } else {
        return false;
    }
}

function is_user_student() {
    $CI = & get_instance();
    if ($CI->session->userdata('userType')) {
        if ($CI->session->userdata('userType') == 'Student') {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function is_user_student_able() {
    $CI = & get_instance();
    if ($CI->session->userdata('userType')) {
        if ($CI->session->userdata('userType') == 'Student') {
            $userRow = Modules::run('user/get_row', $CI->session->userdata('userID'));
            if ($userRow) {
                if ($userRow->status == ACTIVE) {
                    $data = array('status' => 'success');
                    return $data;
                } else {
                    $data = array('status' => 'error', 'msg' => 'Your account is ' . $userRow->status . ' so that you are unable to update it.');
                    return $data;
                }
            } else {
                $data = array('status' => 'error', 'msg' => 'You are unable to update your profile');
                return $data;
            }
        } else {
            $data = array('status' => 'error', 'msg' => 'You are unable to update your profile');
            return $data;
        }
    } else {
        $data = array('status' => 'error', 'msg' => 'You are unable to update your profile');
        return $data;
    }
}

//LOGIN STUDENT ID
function get_student_ID() {
    $CI = & get_instance();
    if ($CI->session->userdata('userType')) {
        if ($CI->session->userdata('userType') == 'Student') {
            if ($CI->session->userdata('userID')) {
                return $CI->session->userdata('userID');
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    } else {
        return 0;
    }
}

function is_user_college() {
    $CI = & get_instance();
    if ($CI->session->userdata('userType')) {
        if ($CI->session->userdata('userType') == 'College') {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function is_user_college_able() {
    $CI = & get_instance();
    if ($CI->session->userdata('userType')) {
        if ($CI->session->userdata('userType') == 'College') {
            $userRow = Modules::run('user/get_row', $CI->session->userdata('userID'));
            if ($userRow) {
                if ($userRow->status == ACTIVE) {
                    $data = array('status' => 'success');
                    return $data;
                } else {
                    $data = array('status' => 'error', 'msg' => 'Your account is ' . $userRow->status . ' so that you are unable to update it.');
                    return $data;
                }
            } else {
                $data = array('status' => 'error', 'msg' => 'You are unable to update your profile');
                return $data;
            }
        } else {
            $data = array('status' => 'error', 'msg' => 'You are unable to update your profile');
            return $data;
        }
    } else {
        $data = array('status' => 'error', 'msg' => 'You are unable to update your profile');
        return $data;
    }
}

function is_user_institute() {
    $CI = & get_instance();
    if ($CI->session->userdata('userType')) {
        if ($CI->session->userdata('userType') == 'Institute') {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function is_user_institute_able() {
    $CI = & get_instance();
    if ($CI->session->userdata('userType')) {
        if ($CI->session->userdata('userType') == 'Institute') {
            $userRow = Modules::run('user/get_row', $CI->session->userdata('userID'));
            if ($userRow) {
                if ($userRow->status == ACTIVE) {
                    $data = array('status' => 'success');
                    return $data;
                } else {
                    $data = array('status' => 'error', 'msg' => 'Your account is ' . $userRow->status . ' so that you are unable to update it.');
                    return $data;
                }
            } else {
                $data = array('status' => 'error', 'msg' => 'You are unable to update your profile');
                return $data;
            }
        } else {
            $data = array('status' => 'error', 'msg' => 'You are unable to update your profile');
            return $data;
        }
    } else {
        $data = array('status' => 'error', 'msg' => 'You are unable to update your profile');
        return $data;
    }
}

//IS VALID USER PROFILE
function is_valid_profile($userID) {
    $userRow = Modules::run('user/get_row', $userID);
    if ($userRow) {
        if ($userRow->status == ACTIVE) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

//LOGIN COLLEGE ID
function get_college_ID() {
    $CI = & get_instance();
    if ($CI->session->userdata('userType')) {
        if ($CI->session->userdata('userType') == 'College') {
            if ($CI->session->userdata('userID')) {
                return $CI->session->userdata('userID');
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    } else {
        return 0;
    }
}

//GET STUDENT ID
function get_user_ID() {
    $CI = & get_instance();
    if ($CI->session->userdata('userID')) {
        return $CI->session->userdata('userID');
    } else {
        return 0;
    }
}

//LOGIN INSTITUTE ID
function get_institute_ID() {
    $CI = & get_instance();
    if ($CI->session->userdata('userType')) {
        if ($CI->session->userdata('userType') == 'Institute') {
            if ($CI->session->userdata('userID')) {
                return $CI->session->userdata('userID');
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    } else {
        return 0;
    }
}

function page_parmalink($slug) {
    if (isset($slug) && !empty($slug)) {
        return site_url('page/' . $slug);
    } else {
        return site_url('#');
    }
}

function pro_parmalink($slug) {
    if (isset($slug) && !empty($slug)) {
        return site_url('product/' . $slug);
    } else {
        return site_url('#');
    }
}

function get_unknown_link() {
    return '<a href="#">Unknown</a>';
}

function is_serialized($data, $strict = true) {
    // if it isn't a string, it isn't serialized.
    if (!is_string($data)) {
        return false;
    }
    $data = trim($data);
    if ('N;' == $data) {
        return true;
    }
    if (strlen($data) < 4) {
        return false;
    }
    if (':' !== $data[1]) {
        return false;
    }
    if ($strict) {
        $lastc = substr($data, -1);
        if (';' !== $lastc && '}' !== $lastc) {
            return false;
        }
    } else {
        $semicolon = strpos($data, ';');
        $brace = strpos($data, '}');
        // Either ; or } must exist.
        if (false === $semicolon && false === $brace)
            return false;
        // But neither must be in the first X characters.
        if (false !== $semicolon && $semicolon < 3)
            return false;
        if (false !== $brace && $brace < 4)
            return false;
    }
    $token = $data[0];
    switch ($token) {
        case 's' :
            if ($strict) {
                if ('"' !== substr($data, -2, 1)) {
                    return false;
                }
            } elseif (false === strpos($data, '"')) {
                return false;
            }
        // or else fall through
        case 'a' :
        case 'O' :
            return (bool) preg_match("/^{$token}:[0-9]+:/s", $data);
        case 'b' :
        case 'i' :
        case 'd' :
            $end = $strict ? '$' : '';
            return (bool) preg_match("/^{$token}:[0-9.E-]+;$end/", $data);
    }
    return false;
}

//make readable date
function make_date($timestamp) {
    return date('d-m-Y', strtotime($timestamp));
}

function convert_date($string) {
    $parts = explode('-', $string);
    return $parts[2] . '-' . $parts[1] . '-' . $parts[0];
}

function make_date_short($timestamp) {
    return date('M d, y', strtotime($timestamp));
}

function get_full_date($timestamp) {
    return date('M jS Y, l', strtotime($timestamp));
}

function get_day($date) {
    return date('d', strtotime($date));
}

function get_month($date) {
    return date('m', strtotime($date));
}

function get_year($date) {
    return date('Y', strtotime($date));
}

function get_month_year($date) {
    return date('M/Y', strtotime($date));
}

function get_date_time() {
    return date('Y-m-d H:i:s');
}

function today_date() {
    return date('Y-m-d');
}

function oneYearDate() {
    return date('Y-m-d', strtotime('+1 year'));
}

function makeFancyDate($dateTime) {
    $old_date_timestamp = strtotime($dateTime);
    $new_date = date('F j, Y, g:i a', $old_date_timestamp);
    return $new_date;
}

function unique_hash($index, $pro_ID) {
    return $index . '-' . date('Ymd') . random_string('alnum', '4') . $pro_ID;
}

function get_hash() {
    return md5(random_string('alnum', 16));
}

function get_captcha() {
    return "6LfZ6QYUAAAAAG2-DyDQwKjVshonx21A_cyR-WZC";
}

function get_captcha_secrect() {
    return "6LfZ6QYUAAAAAGEvZgDhDQKCtfhpc2tSsi8gyPn2";
}

function positive_narration($term) {
    if ($term) {
        if ($term == 'interest') {
            $nerration = 'Can show interest to students.';
        } elseif ($term == 'offer') {
            $nerration = 'Can make offer to students.';
        } elseif ($term == 'save') {
            $nerration = 'Can save ad.';
        } elseif ($term == 'profile') {
            $nerration = 'Can view student profile.';
        } elseif ($term == 'chat') {
            $nerration = 'Can live chat.';
        } else {
            $nerration = '--';
        }
    } else {
        $nerration = '--';
    }
    return $nerration;
}

function negative_narration($term) {
    if ($term) {
        if ($term == 'interest') {
            $nerration = 'Can not show interest to students.';
        } elseif ($term == 'offer') {
            $nerration = 'Can not make offer to students.';
        } elseif ($term == 'save') {
            $nerration = 'Can not save ad.';
        } elseif ($term == 'profile') {
            $nerration = 'Can not view student profile.';
        } elseif ($term == 'chat') {
            $nerration = 'Can not live chat.';
        } else {
            $nerration = '--';
        }
    } else {
        $nerration = '--';
    }
    return $nerration;
}

function makeMoney($x) {
    if ($x) {
        return number_format($x, 2, '.', '');
    } else {
        return '0.00';
    }
}

?>