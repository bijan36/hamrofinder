<?php
$ctrl = 'appsstudent';
$term = 'Student';
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Student <i class="fa fa-angle-double-right fa-fw"></i> <?php echo $alldata->name; ?></h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo site_url('appsdashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>


<section class="content">


    <div class="row">
        <div class="col-md-12" style="margin-bottom: 15px;">
            <a href="<?php echo site_url($ctrl) ?>" class="btn btn-primary">Back to list</a>
        </div>
    </div>

    <!-- Content Header (Page header) -->
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><?php echo $term; ?> Edit</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            <?php if (!empty($alldata)): ?>

                <div class="row">
                    <div class="col-md-12 showMsg">
                        <?php $this->load->view('back/inc/notification'); ?>
                    </div>
                </div>



                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#general_tab" data-toggle="tab">General Info</a></li>
                        <li><a href="#status_tab" data-toggle="tab">Status</a></li>
                        <li><a href="#plans_tab" data-toggle="tab">Plans</a></li>
                        <li><a href="#advertise_tab" data-toggle="tab">Advertise</a></li>
                        <li><a href="#saved_tab" data-toggle="tab">Saved Ad</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="general_tab">
                            <?php echo Modules::run('appsuser/get_student_details', $alldata->ID); ?>
                        </div>


                        <div class="tab-pane" id="status_tab">
                            <?php echo form_open('appsstudent/status_setting', array('class' => 'adminupdate')); ?>
                            <div class="well">
                                <div class="row">
                                    <div class="col-md-12">
                                        <p class="small">
                                            <strong>Active:</strong> user valid ads display in website.<br/> 
                                            <strong>Inactive:</strong> user valid and invalid both ads not display in website. User can not access the login and system notification to user that inactivate by website admin once you update.<br/> 
                                            <strong>Banned:</strong> user valid and invalid both ads not display in website. User can not access the login and system notification to user that banned by website admin once you update.<br/> 
                                            <strong>Expired:</strong> ------<br/> 
                                        </p>
                                        <div class="form-group">
                                            <label for="status">User Current Status:</label><br/>
                                            <?php
                                            $rd1 = $alldata->status == 'Active' ? 'checked' : '';
                                            $rd2 = $alldata->status == 'Inactive' ? 'checked' : '';
                                            $rd3 = $alldata->status == 'Banned' ? 'checked' : '';
                                            $rd4 = $alldata->status == 'Expired' ? 'checked' : '';
                                            ?>
                                            <input type="radio" name="status" value="Active" <?php echo $rd1; ?>> Active
                                            <input type="radio" name="status" value="Inactive" <?php echo $rd2; ?>> Inactive
                                            <input type="radio" name="status" value="Banned" <?php echo $rd3; ?>> Banned
                                            <input type="radio" name="status" value="Expired" <?php echo $rd4; ?>> Expired          
                                        </div>
                                        <div class="form-group">
                                            <input type="hidden" name="id" value="<?php echo $alldata->ID ?>">
                                            <input type="submit" name="approve" class="btn btn-primary" value="Update">
                                        </div>
                                    </div>
                                </div>
                                <div class="ajax-overlay"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>
                            </div>
                            <?php echo form_close() ?>
                        </div>


                        <div class="tab-pane" id="plans_tab">
                            <?php echo form_open('appsstudent/level_setting', array('class' => 'adminupdate')); ?>
                            <div class="well">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="status">Current User Level:</label><br/>
                                            <?php //echo user_level_select($alldata->level); ?>
                                            <span style="font-size: 20px;"><?php echo $alldata->level; ?></span>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <input type="hidden" name="id" value="<?php echo $alldata->ID ?>">
                                        <div class="form-group">
                                            <!--<input type="submit" name="approve" class="btn btn-primary" value="Update">-->
                                        </div>
                                    </div>
                                </div>
                                <div class="ajax-overlay"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>
                            </div>
                            <?php echo form_close() ?>
                        </div>


                        <div class="tab-pane" id="advertise_tab">
                            <?php
                            $all_related_ads = Modules::run('appsstudentad/get_rows_by_parent', $alldata->ID);
                            if ($all_related_ads) {
                                ?>
                                <table class="table table-bordered table-condensed">
                                    <tr>
                                        <th>SN</th>
                                        <th>AD ID</th>
                                        <th>Details</th>
                                        <th>Status</th>
                                        <th>Events</th>
                                        <th>View / Edit</th>
                                    </tr>
                                    <?php
                                    $sn = 1;
                                    foreach ($all_related_ads as $ad):
                                        ?>
                                        <tr>
                                            <td><?php echo $sn++; ?></td>
                                            <td><a href="<?php echo site_url('appsstudentad/edit/' . $ad->ID) ?>" target="_blank" data-toggle="tooltip" data-placement="top" title="View in details"><?php echo $ad->ID; ?></a></td>
                                            <td>
                                                <small>
                                                    <table>
                                                        <tr>
                                                            <td>Looking For</td>
                                                            <td>:<strong><?php echo $ad->looking_for; ?></strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Affiliation</td>
                                                            <td>:<?php echo $ad->affiliation; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Admission Within</td>
                                                            <td>:<?php echo $ad->admission_within; ?> Days</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Achieved Grade</td>
                                                            <td>:<?php echo $ad->achieved_grade; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Tuition Fee Range</td>
                                                            <td>:<?php echo $ad->tution_fee_range; ?></td>
                                                        </tr>
                                                    </table>
                                                </small>
                                            </td>
                                            <td><?php echo status_color($ad->status); ?></td>
                                            <td>
                                                <small>
                                                    <table>
                                                        <tr>
                                                            <td>Submitted</td>
                                                            <td>:<strong><?php echo makeReadableDate($ad->ad_submit_date); ?></strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Start</td>
                                                            <td>:<?php echo makeReadableDate($ad->ad_start_date); ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Ends</td>
                                                            <td>:<?php echo makeReadableDate($ad->ad_end_date); ?></td>
                                                        </tr>
                                                    </table>
                                                </small>
                                            </td>
                                            <td>
                                                <a href="<?php echo site_url('appsstudentad/edit/' . $ad->ID) ?>" target="_blank"><i class="fa fa-eye"></i> View / Edit</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </table>
                                <?php
                            }else {
                                ?>
                                <p>No ads by user yet! :)</p>
                                <?php
                            }
                            ?>
                        </div>


                        <div class="tab-pane" id="saved_tab">
                            <?php
                            $interestRow = Modules::run('appssavedad/get_saved_row_by_student', $alldata->ID);
                            if ($interestRow) {
                                ?>
                                <table class="table table-condensed">
                                    <tr>
                                        <th style="width: 40px;">SN</th>
                                        <th>College/Institute Name</th>
                                        <th>Looking For</th>
                                        <th>Fee Range(Lacks)</th>
                                    </tr>

                                    <?php
                                    $sn = 1;
                                    foreach ($interestRow as $irow):
                                        $colAdRow = Modules::run('appscollegead/get_row', $irow->ad_ID);
                                        if ($colAdRow) {
                                            $colUserRow = Modules::run('appsuser/get_college_row', $colAdRow->parent_ID);
                                            if ($colUserRow) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $sn++; ?></td>
                                                    <td>
                                                        <a href="<?php echo site_url('appscollege/edit/' . $colUserRow->ID); ?>" target="_blank">
                                                            <?php echo $colUserRow->name; ?>
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <a href="<?php echo site_url('appscollegead/edit/' . $colAdRow->ID); ?>" target="_blank">
                                                            <?php echo $colAdRow->looking_for; ?>
                                                        </a>
                                                    </td>
                                                    <td><?php echo $colAdRow->tuition_fee_range; ?></td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                    endforeach;
                                    ?>

                                </table>
                                <?php
                            }
                            ?>
                        </div>


                    </div>
                </div>


            <?php endif; ?>
        </div>
    </div>