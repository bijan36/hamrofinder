<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Appsemail extends Backendcontroller {

    /**
     * @ Author: Suraj Bajracharya
     * @ Author URL : www.indesignmedia.net
     * @see http://indesignmedia.net
     */
    public function __construct() {
        parent::__construct();
    }

    //DEFAULT FUNCTION
    public function index() {
        echo 'Not good place to go';
    }

    //ADDING DATA
    public function addrandomly($data) {
        $this->usersmeta->insert($data);
    }

    //Do email function
    public function doEmail($array) {

        $this->load->library('email');

        $from = isset($array['from']) ? $array['from'] : SYSTEMEMAIL;
        $fromName = isset($array['fromName']) ? $array['fromName'] : COMPANYNAME;
        $to = isset($array['to']) ? $array['to'] : SYSTEMEMAIL;
        $toName = isset($array['toName']) ? $array['toName'] : COMPANYNAME;
        $replyTo = isset($array['replyTo']) ? $array['replyTo'] : SYSTEMEMAIL;
        $replyToName = isset($array['replyToName']) ? $array['replyToName'] : COMPANYNAME;
        $subject = isset($array['subject']) ? $array['subject'] : 'Email from ' . COMPANYNAME;
        $message = isset($array['message']) ? $array['message'] : '';


        //$data['from'] = $from;
        //$data['fromName'] = $fromName;
        //$data['to'] = $to;
        //$data['toName'] = $toName;
        //$data['replyTo'] = $replyTo;
        //$data['replyToName'] = $replyToName;
        //$data['subject'] = $subject;
        $data['message'] = $message;
        $data['others'] = $array['others'];


        $message_body = $this->template->load(get_template('email'), 'appsemail/'.$array['template'], $data, true);


        $config['charset'] = 'iso-8859-1';
        $config['mailtype'] = 'html';
        $config['wordwrap'] = TRUE;

        
        $this->email->initialize($config);


        $this->email->from($from, $fromName);
        $this->email->to($to, $toName);
        $this->email->reply_to($replyTo, $replyToName);
        $this->email->subject($subject);

        $this->email->message($message_body);

        $this->email->send();
        $this->email->clear(TRUE);
        return true;
    }

}
