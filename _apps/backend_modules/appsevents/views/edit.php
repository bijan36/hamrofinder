<?php
$ctrl = 'appsevents';
$term = 'Events';
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><?php echo $term; ?> <i class="fa fa-angle-double-right"></i> Edit</a></h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo site_url('appsdashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>

<section class="content">
    <div class="row" style="margin-bottom: 15px;">
        <div class="col-md-12">
            <a href="<?php echo site_url($ctrl) ?>" class="btn btn-primary">Back to list</a>
        </div>
        <div class="col-md-12 showMsg">
            <?php $this->load->view('back/inc/notification'); ?>
        </div>
    </div>



    <?php echo form_open($ctrl . '/update', array('name' => 'updateCms', 'class' => 'commonUpdate')); ?>
    <div class="row">
        <div class="col-md-9">
            <!-- Content Header (Page header) -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Add <?php echo $term; ?></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group input-group-lg">
                                        <label for="title">Event Title: *</label>
                                        <input type="text" class="form-control" name="title" value="<?php echo $row->title ? $row->title : ''; ?>">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="title">Content</label>
                                        <textarea class="form-control myEditor" name="content"><?php echo $row->description ? $row->description : ''; ?></textarea>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Operation</h3>
                </div>
                <div class="box-body">
                    <p>To save or update click button below:</p>
                    <input type="submit" name="addAction" class="btn btn-primary btn-block" value="Update">
                    <input type="hidden" name="id" value="<?php echo $row->ID; ?>">
                </div>
            </div>

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Event Info</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label>Venue: *</label>
                        <input type="text" name="venue" class="form-control" placeholder="eg: New Baneshwor" value="<?php echo $row->venue ? $row->venue : ''; ?>">
                    </div>
                    <div class="form-group">
                        <label>Event Date: *</label>
                        <input type="text" name="event_date" class="form-control" placeholder="eg: 2017-05-21" value="<?php echo $row->event_date ? $row->event_date : ''; ?>">
                    </div>
                    <div class="form-group">
                        <label>Event Time (24Hours Format): *</label>
                        <input type="text" name="event_time" class="form-control" placeholder="eg: 10:30" value="<?php echo $row->event_time ? $row->event_time : ''; ?>">
                    </div>
                    <div class="form-group">
                        <label>Total Seats: *</label>
                        <input type="number" name="total_sets" class="form-control" value="<?php echo $row->total_sets ? $row->total_sets : ''; ?>">
                    </div>
                    <div class="form-group">
                        <label>Price(NPR):</label>
                        <input type="text" name="price" class="form-control" placeholder="eg: 500" value="<?php echo $row->price ? $row->price : ''; ?>">
                    </div>
                </div>
            </div>

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Booking Dates:</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label>Booking Open Date:</label>
                        <input type="text" name="booking_open_date" class="form-control" placeholder="eg: 2017-01-25" value="<?php echo $row->booking_open_date ? $row->booking_open_date : ''; ?>">
                    </div>
                    <div class="form-group">
                        <label>Booking Close Date:</label>
                        <input type="text" name="booking_close_date" class="form-control" placeholder="eg: 2017-05-20" value="<?php echo $row->booking_close_date ? $row->booking_close_date : ''; ?>">
                    </div>
                </div>
            </div>
            
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Event Banner:</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label>Event Banner Image</label><p>Please select image from <a href="<?php echo base_url('appsimages'); ?>" target="_blank">Image Library</a><br/>Give image absolute URL:eg http://example.com/img/image.jpg for better looks: 730px X 430px</p>
                        <input type="text" name="event_banner" class="form-control" value="<?php echo $row->event_banner ? $row->event_banner : ''; ?>">
                    </div>
                </div>
            </div>

        </div>
    </div>
    <?php echo form_close(); ?>

