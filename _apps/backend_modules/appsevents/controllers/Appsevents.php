<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Appsevents extends Backendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('Appsevents_m', 'bevent');
    }

    //STUDENT LIST
    public function index() {
        $data['alldata'] = $this->bevent->get_all();
        $this->template->load(get_template('default'), 'list', $data);
    }

    //GET ALL
    public function get_all() {
        $rows = $this->bevent->get_all();
        return $rows ? $rows : false;
    }

//    //ADD
    public function add() {
        $this->template->load(get_template('default'), 'add');
    }

    //SAVE
    public function save() {

        $this->form_validation->set_rules('title', 'Page title', 'required');
        $this->form_validation->set_rules('venue', 'Venue', 'required');
        $this->form_validation->set_rules('event_date', 'Event Date', 'required');
        $this->form_validation->set_rules('event_time', 'Event Time', 'required');
        $this->form_validation->set_rules('total_sets', 'Total Sets for booking', 'required|is_natural');
        $this->form_validation->set_rules('price', 'Price', 'required');
        $this->form_validation->set_rules('booking_open_date', 'Booking start date', 'required');
        $this->form_validation->set_rules('booking_close_date', 'Booking close date', 'required');
        $this->form_validation->set_rules('event_banner', 'Event Banner', 'required');

        if ($this->form_validation->run() == TRUE) {

            $dataUpdate = array(
                'title' => $this->input->post('title'),
                'description' => $this->input->post('content'),
                'venue' => $this->input->post('venue'),
                'event_date' => $this->input->post('event_date'),
                'event_time' => $this->input->post('event_time'),
                'total_sets' => $this->input->post('total_sets'),
                'price' => $this->input->post('price'),
                'booking_open_date' => $this->input->post('booking_open_date'),
                'booking_close_date' => $this->input->post('booking_close_date'),
                'event_banner' => $this->input->post('event_banner'),
            );

            if ($this->bevent->insert($dataUpdate)) {
                $data = array(
                    'status' => 'success',
                    'msg' => 'Event has been added',
                    'ID' => $this->bevent->get_last_id(),
                );
                echo json_encode($data);
                exit();
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => 'Unable to add event'
                );
                echo json_encode($data);
                exit();
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => validation_errors()
            );
            echo json_encode($data);
            exit();
        }
    }

    //ADD
    public function edit($id) {
        $row = $this->bevent->get($id);
        if ($row) {
            $data['row'] = $row;
            $this->template->load(get_template('default'), 'edit', $data);
        } else {
            justRedirect();
        }
    }

    //UPDATEING PAGE
    public function update() {

        $this->form_validation->set_rules('title', 'Page title', 'required');
        $this->form_validation->set_rules('venue', 'Venue', 'required');
        $this->form_validation->set_rules('event_date', 'Event Date', 'required');
        $this->form_validation->set_rules('event_time', 'Event Time', 'required');
        $this->form_validation->set_rules('total_sets', 'Total Sets for booking', 'required|is_natural');
        $this->form_validation->set_rules('price', 'Price', 'required');
        $this->form_validation->set_rules('booking_open_date', 'Booking start date', 'required');
        $this->form_validation->set_rules('booking_close_date', 'Booking close date', 'required');
        $this->form_validation->set_rules('event_banner', 'Event Banner', 'required');

        if ($this->form_validation->run() == TRUE) {

            $dataUpdate = array(
                'title' => $this->input->post('title'),
                'description' => $this->input->post('content'),
                'venue' => $this->input->post('venue'),
                'event_date' => $this->input->post('event_date'),
                'event_time' => $this->input->post('event_time'),
                'total_sets' => $this->input->post('total_sets'),
                'price' => $this->input->post('price'),
                'booking_open_date' => $this->input->post('booking_open_date'),
                'booking_close_date' => $this->input->post('booking_close_date'),
                'event_banner' => $this->input->post('event_banner'),
            );

            $ID = $this->input->post('id');
            if ($this->bevent->update($ID, $dataUpdate)) {
                $data = array(
                    'status' => 'success',
                    'msg' => 'Page has been updated'
                );
                echo json_encode($data);
                exit();
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => 'Unable to update page'
                );
                echo json_encode($data);
                exit();
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => validation_errors()
            );
            echo json_encode($data);
            exit();
        }
    }

}
