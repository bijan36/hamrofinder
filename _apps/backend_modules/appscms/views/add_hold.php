<?php
$ctrl = 'appscms';
$term = 'Pages';
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><?php echo $term; ?> <i class="fa fa-angle-double-right"></i> Add</a></h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo site_url('appsdashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>


<section class="content">

    <div class="row" style="margin-bottom: 15px;">
        <div class="col-md-12">
            <a href="<?php echo site_url('appscms') ?>" class="btn btn-primary">Back to list</a>
        </div>
        <div class="col-md-12 showMsg">
            <?php $this->load->view('back/inc/notification'); ?>
        </div>
    </div>





    <div class="row">
        <div class="col-md-9">
            <!-- Content Header (Page header) -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Add <?php echo $term; ?></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group input-group-lg">
                                        <label for="title">Title</label>
                                        <input type="text" class="form-control" name="title">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="title">Content</label>
                                        <textarea class="form-control myEditor" name="content"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="excerpt">Short Content</label>
                                        <textarea class="form-control" name="excerpt"></textarea>
                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <hr/>
                                </div>


                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="seo_title">SEO Title</label>
                                        <input type="text" class="form-control" name="seo_title">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="seo_description">SEO Description</label>
                                        <textarea class="form-control" name="seo_description"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="seo_keywords">SEO Keywords</label>
                                        <textarea class="form-control" name="seo_keywords"></textarea>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Operation</h3>
                </div>
                <div class="box-body">
                    <p>
                        To save or update click button below:
                    </p>
                    <input type="submit" name="addAction" class="btn btn-primary btn-block" value="Update">
                </div>
            </div>
        </div>
    </div>













