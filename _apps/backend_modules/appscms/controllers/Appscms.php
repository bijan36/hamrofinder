<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Appscms extends Backendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('appscms_m', 'bpage');
    }

    //STUDENT LIST
    public function index() {
        $data['alldata'] = $this->bpage->get_all();
        $this->template->load(get_template('default'), 'list', $data);
    }

    //GET ALL
    public function get_all() {
        $rows = $this->bpage->get_all();
        return $rows ? $rows : false;
    }

//    //ADD
//    public function add(){
//        $this->template->load(get_template('default'), 'add');
//    }
//    
    //ADD
    public function edit($id) {
        $row = $this->bpage->get($id);
        if ($row) {
            $data['row'] = $row;
            $this->template->load(get_template('default'), 'edit', $data);
        } else {
            justRedirect();
        }
    }

    //UPDATEING PAGE
    public function update() {

        $this->form_validation->set_rules('title', 'Page title', 'required');
        $this->form_validation->set_rules('slug', 'Page slug url', 'required');
        $this->form_validation->set_rules('content', 'Page content', 'required');
        $this->form_validation->set_rules('template', 'Template', 'required');
        $this->form_validation->set_rules('sidebar', 'Sidebar', 'required');
        $this->form_validation->set_rules('id', 'Update referance', 'required');

        if ($this->form_validation->run() == TRUE) {

            $dataUpdate = array(
                'title' => $this->input->post('title'),
                'slug' => $this->input->post('slug'),
                'content' => $this->input->post('content'),
                'template' => $this->input->post('template'),
                'sidebar' => $this->input->post('sidebar'),
                'seo_title' => $this->input->post('seo_title'),
                'seo_description' => $this->input->post('seo_description'),
                'seo_keywords' => $this->input->post('seo_keywords')
            );

            $ID = $this->input->post('id');
            if ($this->bpage->update($ID, $dataUpdate)) {
                $data = array(
                    'status' => 'success',
                    'msg' => 'Page has been updated'
                );
                echo json_encode($data);
                exit();
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => 'Unable to update page'
                );
                echo json_encode($data);
                exit();
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => validation_errors()
            );
            echo json_encode($data);
            exit();
        }
    }

}
