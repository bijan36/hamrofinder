<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Appsuser extends Backendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('Appsuser_m', 'buser');
    }

    //USER DASHBOARD
    public function index() {
        echo 'good';
    }

    public function get_pagination_students($perPage, $currentQty) {
        $userRow = $this->buser->order_by('ID', 'DESC')->limit($perPage, $currentQty)->get_many_by(array('type' => 'Student'));
        return $userRow ? $userRow : false;
    }

    //GET STUDENT
    public function get_all_students() {
        $userRow = $this->buser->order_by('ID', 'DESC')->get_many_by(array('type' => 'Student'));
        return $userRow ? $userRow : false;
    }
    
    
    public function get_pagination_colleges($perPage, $currentQty) {
        $userRow = $this->buser->order_by('ID', 'DESC')->limit($perPage, $currentQty)->get_many_by(array('type' => 'College'));
        return $userRow ? $userRow : false;
    }

    public function get_all_colleges() {
        $userRow = $this->buser->order_by('ID', 'DESC')->get_many_by(array('type' => 'College'));
        return $userRow ? $userRow : 'no';
    }
    
    public function get_pagination_institute($perPage, $currentQty) {
        $userRow = $this->buser->order_by('ID', 'DESC')->limit($perPage, $currentQty)->get_many_by(array('type' => 'Institute'));
        return $userRow ? $userRow : false;
    }

    public function get_all_institute() {
        $userRow = $this->buser->order_by('ID', 'DESC')->get_many_by(array('type' => 'Institute'));
        return $userRow ? $userRow : 'no';
    }

    //GET ONE STUDENT ROW
    public function get_student_row($ID) {
        $userRow = $this->buser->get_by(array('ID' => $ID, 'type' => 'Student'));
        return $userRow ? $userRow : false;
    }

    public function get_college_row($ID) {
        $userRow = $this->buser->get_by(array('ID' => $ID, 'type' => 'College'));
        return $userRow ? $userRow : false;
    }

    public function get_institute_row($ID) {
        $userRow = $this->buser->get_by(array('ID' => $ID, 'type' => 'Institute'));
        return $userRow ? $userRow : false;
    }

    public function get_new_students() {
        $rows = $this->buser->order_by('ID', 'DESC')->get_many_by(array('type' => 'Student', 'status' => INACTIVE));
        return $rows ? $rows : false;
    }
    public function get_new_students_count() {
        $rows = $this->buser->order_by('ID', 'DESC')->count_by(array('type' => 'Student', 'status' => INACTIVE));
        return $rows ? $rows : 0;
    }

    public function get_new_colleges() {
        $rows = $this->buser->order_by('ID', 'DESC')->get_many_by(array('type' => 'College', 'status' => INACTIVE));
        return $rows ? $rows : false;
    }
    public function get_new_colleges_count() {
        $rows = $this->buser->order_by('ID', 'DESC')->count_by(array('type' => 'College', 'status' => INACTIVE));
        return $rows ? $rows : 0;
    }

    public function get_new_institute() {
        $rows = $this->buser->order_by('ID', 'DESC')->get_many_by(array('type' => 'Institute', 'status' => INACTIVE));
        return $rows ? $rows : false;
    }
    public function get_new_institute_count() {
        $rows = $this->buser->order_by('ID', 'DESC')->count_by(array('type' => 'Institute', 'status' => INACTIVE));
        return $rows ? $rows : 0;
    }

    public function total_students() {
        $total = $this->buser->count_by(array('type' => 'Student'));
        return $total;
    }

    public function total_college() {
        $total = $this->buser->count_by(array('type' => 'College'));
        return $total;
    }

    public function total_institute() {
        $total = $this->buser->count_by(array('type' => 'Institute'));
        return $total;
    }

    //STUDENT SHORT DETAILS
    public function get_student_short_details($ID) {
        $userRow = $this->buser->get($ID);
        $output = '';
        if ($userRow->type == 'Student') {
            unset($userRow->passcode);
            unset($userRow->code);

            $output .= '<div class="row">';
            $output .= '<div class="col-md-3">';
            $output .= Modules::run('appsimages/get_img_size', $userRow->profile_picture, 'thumb50x50');
            $output .= '</div>';
            $output .= '<div class="col-md-9">';
            $output .= '<small><table>';
            
            $country = !empty($userRow->country) ? ', ' . $userRow->country : '';
            $province = !empty($userRow->province) ? ', ' . $userRow->province : '';
            $district = !empty($userRow->district) ? ', ' . $userRow->district : '';
            $local_level = !empty($userRow->local_level) ? ', ' . $userRow->local_level : '';
            $ward_no = !empty($userRow->ward_no) ? ' - ' . $userRow->ward_no : '';
            $address = !empty($userRow->address) ? $userRow->address : '';

            $address = $address . $local_level . $ward_no . $district . $province . $country;
            
            
            foreach ($userRow as $k => $v) {
                if ($k == 'name') {
                    $output .= '<tr>';
                    $output .= '<td><strong>' . $v . '</strong></td>';
                    $output .= '</tr>';
                } elseif ($k == 'address') {
                    $output .= '<tr>';
                    $output .= '<td>' . $address . '</td>';
                    $output .= '</tr>';
                } elseif ($k == 'contact') {
                    $output .= '<tr>';
                    $output .= '<td>' . $v . '</td>';
                    $output .= '</tr>';
                } elseif ($k == 'email') {
                    $output .= '<tr>';
                    $output .= '<td>' . $v . '</td>';
                    $output .= '</tr>';
                }
            }
            $output .= '</table></small>';
            $output .= '</div>';
            $output .= '</div>';
        }
        return $output;
    }

    public function get_name_contact($ID) {
        $userRow = $this->buser->get($ID);
        $output = '';
        if ($userRow->type == 'Student') {
            unset($userRow->passcode);
            unset($userRow->code);

            $output .= '<div class="row">';
            $output .= '<div class="col-md-2">';
            $output .= Modules::run('appsimages/get_img_size', $userRow->profile_picture, 'thumb50x50');
            $output .= '</div>';
            $output .= '<div class="col-md-10">';
            $output .= '<small><table>';
            foreach ($userRow as $k => $v) {
                if ($k == 'name') {
                    $output .= '<tr>';
                    $output .= '<td><strong>' . $v . '</strong></td>';
                    $output .= '</tr>';
                } elseif ($k == 'contact') {
                    $output .= '<tr>';
                    $output .= '<td>' . $v . '</td>';
                    $output .= '</tr>';
                }
            }
            $output .= '</table></small>';
            $output .= '</div>';
            $output .= '</div>';
        }
        return $output;
    }

    //STUDENT SHORT DETAILS
    public function get_college_short_details($ID) {
        $userRow = $this->buser->get($ID);
        $output = '';
        if ($userRow->type == 'College') {
            unset($userRow->passcode);
            unset($userRow->code);

            $output .= '<div class="row">';
            $output .= '<div class="col-md-2">';
            $output .= Modules::run('appsimages/get_img_size', $userRow->profile_picture, 'thumb50x50');
            $output .= '</div>';
            $output .= '<div class="col-md-10">';
            $output .= '<small><table>';
            
            $country = !empty($userRow->country) ? ', ' . $userRow->country : '';
            $province = !empty($userRow->province) ? ', ' . $userRow->province : '';
            $district = !empty($userRow->district) ? ', ' . $userRow->district : '';
            $local_level = !empty($userRow->local_level) ? ', ' . $userRow->local_level : '';
            $ward_no = !empty($userRow->ward_no) ? ' - ' . $userRow->ward_no : '';
            $address = !empty($userRow->address) ? $userRow->address : '';

            $address = $address . $local_level . $ward_no . $district . $province . $country;
            
            foreach ($userRow as $k => $v) {
                if ($k == 'name') {
                    $output .= '<tr>';
                    $output .= '<td><strong>' . $v . '</strong></td>';
                    $output .= '</tr>';
                } elseif ($k == 'address') {
                    $output .= '<tr>';
                    $output .= '<td>' . $address . '</td>';
                    $output .= '</tr>';
                } elseif ($k == 'contact') {
                    $output .= '<tr>';
                    $output .= '<td>' . $v . '</td>';
                    $output .= '</tr>';
                } elseif ($k == 'email') {
                    $output .= '<tr>';
                    $output .= '<td>' . $v . '</td>';
                    $output .= '</tr>';
                }
            }
            $output .= '</table></small>';
            $output .= '</div>';
            $output .= '</div>';
        }
        return $output;
    }

    public function get_institute_short_details($ID) {
        $userRow = $this->buser->get($ID);
        $output = '';
        if ($userRow->type == 'Institute') {
            unset($userRow->passcode);
            unset($userRow->code);

            $output .= '<div class="row">';
            $output .= '<div class="col-md-3">';
            $output .= Modules::run('appsimages/get_img_size', $userRow->profile_picture, 'thumb50x50');
            $output .= '</div>';
            $output .= '<div class="col-md-9">';
            $output .= '<small><table>';
            
            $country = !empty($userRow->country) ? ', ' . $userRow->country : '';
            $province = !empty($userRow->province) ? ', ' . $userRow->province : '';
            $district = !empty($userRow->district) ? ', ' . $userRow->district : '';
            $local_level = !empty($userRow->local_level) ? ', ' . $userRow->local_level : '';
            $ward_no = !empty($userRow->ward_no) ? ' - ' . $userRow->ward_no : '';
            $address = !empty($userRow->address) ? $userRow->address : '';

            $address = $address . $local_level . $ward_no . $district . $province . $country;
            
            foreach ($userRow as $k => $v) {
                if ($k == 'name') {
                    $output .= '<tr>';
                    $output .= '<td><strong>' . $v . '</strong></td>';
                    $output .= '</tr>';
                } elseif ($k == 'address') {
                    $output .= '<tr>';
                    $output .= '<td>' . $address . '</td>';
                    $output .= '</tr>';
                } elseif ($k == 'contact') {
                    $output .= '<tr>';
                    $output .= '<td>' . $v . '</td>';
                    $output .= '</tr>';
                } elseif ($k == 'email') {
                    $output .= '<tr>';
                    $output .= '<td>' . $v . '</td>';
                    $output .= '</tr>';
                }
            }
            $output .= '</table></small>';
            $output .= '</div>';
            $output .= '</div>';
        }
        return $output;
    }

    public function get_name_contact_college($ID) {
        $userRow = $this->buser->get($ID);
        $output = '';
        if ($userRow->type == 'College') {
            unset($userRow->passcode);
            unset($userRow->code);

            $output .= '<div class="row">';
            $output .= '<div class="col-md-3">';
            $output .= Modules::run('appsimages/get_img_size', $userRow->profile_picture, 'thumb50x50');
            $output .= '</div>';
            $output .= '<div class="col-md-9">';
            $output .= '<small><table>';
            foreach ($userRow as $k => $v) {
                if ($k == 'name') {
                    $output .= '<tr>';
                    $output .= '<td><strong>' . $v . '</strong></td>';
                    $output .= '</tr>';
                } elseif ($k == 'contact') {
                    $output .= '<tr>';
                    $output .= '<td>' . $v . '</td>';
                    $output .= '</tr>';
                }
            }
            $output .= '</table></small>';
            $output .= '</div>';
            $output .= '</div>';
        }
        return $output;
    }

    public function get_name_contact_institute($ID) {
        $userRow = $this->buser->get($ID);
        $output = '';
        if ($userRow->type == 'Institute') {
            unset($userRow->passcode);
            unset($userRow->code);

            $output .= '<div class="row">';
            $output .= '<div class="col-md-3">';
            $output .= Modules::run('appsimages/get_img_size', $userRow->profile_picture, 'thumb50x50');
            $output .= '</div>';
            $output .= '<div class="col-md-9">';
            $output .= '<small><table>';
            foreach ($userRow as $k => $v) {
                if ($k == 'name') {
                    $output .= '<tr>';
                    $output .= '<td><strong>' . $v . '</strong></td>';
                    $output .= '</tr>';
                } elseif ($k == 'contact') {
                    $output .= '<tr>';
                    $output .= '<td>' . $v . '</td>';
                    $output .= '</tr>';
                }
            }
            $output .= '</table></small>';
            $output .= '</div>';
            $output .= '</div>';
        }
        return $output;
    }

    //GET USER DETIALS 
    public function get_student_details($ID) {
        $userRow = $this->buser->get($ID);
        $output = '';
        if ($userRow->type == 'Student') {
            unset($userRow->passcode);
            unset($userRow->code);

            $output .= '<table class="table table-bordered table-condensed">';
            foreach ($userRow as $k => $v) {
                if ($k == 'profile_picture') {
                    $proImg = Modules::run('appsimages/get_img_size', $v, 'thumb50x50');
                    $output .= '<tr>';
                    $output .= '<td style="width:150px;"><strong>' . strtoupper(str_replace('_', ' ', $k)) . '</strong></td>';
                    $output .= '<td>' . $proImg . '</td>';
                    $output .= '</tr>';
                } elseif ($k == 'status') {
                    $output .= '<tr>';
                    $output .= '<td style="width:150px;"><strong>' . strtoupper(str_replace('_', ' ', $k)) . '</strong></td>';
                    $output .= '<td>' . status_color($v) . '</td>';
                    $output .= '</tr>';
                } else {
                    $output .= '<tr>';
                    $output .= '<td style="width:150px;"><strong>' . strtoupper(str_replace('_', ' ', $k)) . '</strong></td>';
                    $output .= '<td>' . $v . '</td>';
                    $output .= '</tr>';
                }
            }
            $output .= '</table>';
        }
        return $output;
    }

    //GET USER DETIALS 
    public function get_college_details($ID) {
        $userRow = $this->buser->get($ID);
        $output = '';
        if ($userRow->type == 'College') {
            unset($userRow->passcode);
            unset($userRow->code);

            $output .= '<table class="table table-bordered table-condensed">';
            foreach ($userRow as $k => $v) {

                $allPic = '';
                if ($k == 'gallery_picture') {
                    if (!empty($v)) {
                        $pics = explode(',', $v);
                        foreach ($pics as $pic):
                            $allPic .= '<div class="thumb_inline">' . Modules::run('appsimages/get_img_size', $pic, 'thumb50x50') . '</div>';
                        endforeach;
                    }
                }

                if ($k == 'profile_picture') {
                    $proImg = Modules::run('appsimages/get_img_size', $v, 'thumb50x50');
                    $output .= '<tr>';
                    $output .= '<td><strong>' . strtoupper(str_replace("_", " ", $k)) . '</strong></td>';
                    $output .= '<td>' . $proImg . '</td>';
                    $output .= '</tr>';
                } elseif ($k == 'status') {
                    $output .= '<tr>';
                    $output .= '<td style="width:150px;"><strong>' . strtoupper(str_replace('_', ' ', $k)) . '</strong></td>';
                    $output .= '<td>' . status_color($v) . '</td>';
                    $output .= '</tr>';
                } elseif ($k == 'gallery_picture') {
                    $output .= '<tr>';
                    $output .= '<td style="width:150px;"><strong>' . strtoupper(str_replace('_', ' ', $k)) . '</strong></td>';
                    $output .= '<td>' . $allPic . '</td>';
                    $output .= '</tr>';
                } else {
                    $output .= '<tr>';
                    $output .= '<td><strong>' . strtoupper(str_replace("_", " ", $k)) . '</strong></td>';
                    $output .= '<td>' . $v . '</td>';
                    $output .= '</tr>';
                }
            }
            $output .= '</table>';
        }
        return $output;
    }

    public function get_institute_details($ID) {
        $userRow = $this->buser->get($ID);
        $output = '';
        if ($userRow->type == 'Institute') {
            unset($userRow->passcode);
            unset($userRow->code);

            $output .= '<table class="table table-bordered table-condensed">';
            foreach ($userRow as $k => $v) {

                $allPic = '';
                if ($k == 'gallery_picture') {
                    if (!empty($v)) {
                        $pics = explode(',', $v);
                        foreach ($pics as $pic):
                            $allPic .= '<div class="thumb_inline">' . Modules::run('appsimages/get_img_size', $pic, 'thumb50x50') . '</div>';
                        endforeach;
                    }
                }

                if ($k == 'profile_picture') {
                    $proImg = Modules::run('appsimages/get_img_size', $v, 'thumb50x50');
                    $output .= '<tr>';
                    $output .= '<td><strong>' . strtoupper(str_replace("_", " ", $k)) . '</strong></td>';
                    $output .= '<td>' . $proImg . '</td>';
                    $output .= '</tr>';
                } elseif ($k == 'status') {
                    $output .= '<tr>';
                    $output .= '<td style="width:150px;"><strong>' . strtoupper(str_replace('_', ' ', $k)) . '</strong></td>';
                    $output .= '<td>' . status_color($v) . '</td>';
                    $output .= '</tr>';
                } elseif ($k == 'gallery_picture') {
                    $output .= '<tr>';
                    $output .= '<td style="width:150px;"><strong>' . strtoupper(str_replace('_', ' ', $k)) . '</strong></td>';
                    $output .= '<td>' . $allPic . '</td>';
                    $output .= '</tr>';
                } else {
                    $output .= '<tr>';
                    $output .= '<td><strong>' . strtoupper(str_replace("_", " ", $k)) . '</strong></td>';
                    $output .= '<td>' . $v . '</td>';
                    $output .= '</tr>';
                }
            }
            $output .= '</table>';
        }
        return $output;
    }

    //update settings
    public function setting_update($array, $id) {
        if (isset($array) && isset($id)) {
            if (is_array($array)) {
                if ($this->buser->update($id, $array)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    //GET SAFE DATA BY USER ID
    public function get_row($ID) {
        $userRow = $this->buser->get($ID);
        return $userRow ? $userRow : false;
    }

    //GET SAFE DATA BY USER PARENT
    public function get_row_by_parent($parent_ID) {
        $userRow = $this->buser->get_by(array('parent_ID' => $parent_ID));
        return $userRow ? $userRow : false;
    }

    //REMOVE GALLEY IMAGE
    public function deletegalleyimage($ids, $userID) {
        if ($this->buser->update($userID, array('gallery_picture' => $ids))) {
            return true;
        } else {
            return false;
        }
    }
    
    
    //NEW STUDENT LIST
    public function new_students_list(){
        $this->template->load(get_template('default'), 'appsstudentad/new_list');
    }
    //NEW STUDENT LIST
    public function new_college_list(){
        $this->template->load(get_template('default'), 'appscollegead/new_list');
    }
    //NEW STUDENT LIST
    public function new_institute_list(){
        $this->template->load(get_template('default'), 'appsinstitutead/new_list');
    }

}
