<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Welcome to admin dashboard </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo site_url('appsdashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>


<section class="content">


    <div class="row">

        <div class="col-md-4">
            <div class="box">
                <div class="box-body">
                    <!-- Apply any bg-* class to to the info-box to color it -->
                    <div class="info-box bg-red">
                        <span class="info-box-icon"><i class="fa fa-graduation-cap"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Students</span>
                            <span class="info-box-number"><?php echo Modules::run('appsuser/total_students') ?></span>
                            <!-- The progress section is optional -->
                            <div class="progress">
                                <div class="progress-bar" style="width: 65%"></div>
                            </div>
                        </div><!-- /.info-box-content -->
                    </div><!-- /.info-box -->

                    <div class="list-group">
                        <div class="list-group-item">Active Ads: <?php echo Modules::run('appsstudentad/total_active_ads'); ?></div>
                        <div class="list-group-item">Inactive Ads: <?php echo Modules::run('appsstudentad/total_inactive_ads'); ?></div>
                        <div class="list-group-item"><a href="<?php echo site_url('appsstudentad/waiting_list'); ?>">Waiting for approval: <?php echo Modules::run('appsstudentad/get_new_ads_count'); ?></a></div>
                        <div class="list-group-item"><a href="<?php echo site_url('appsuser/new_students_list'); ?>">New registered: <?php echo Modules::run('appsuser/get_new_students_count'); ?></a></div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="box">
                <div class="box-body">
                    <!-- Apply any bg-* class to to the info-box to color it -->
                    <div class="info-box bg-green">
                        <span class="info-box-icon"><i class="fa fa-graduation-cap"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Colleges</span>
                            <span class="info-box-number"><?php echo Modules::run('appsuser/total_college') ?></span>
                            <!-- The progress section is optional -->
                            <div class="progress">
                                <div class="progress-bar" style="width: 65%"></div>
                            </div>
                        </div><!-- /.info-box-content -->
                    </div><!-- /.info-box -->

                    <div class="list-group">
                        <div class="list-group-item">Active Ads: <?php echo Modules::run('appscollegead/total_active_ads'); ?></div>
                        <div class="list-group-item">Inactive Ads: <?php echo Modules::run('appscollegead/total_inactive_ads'); ?></div>
                        <div class="list-group-item"><a href="<?php echo site_url('appscollegead/waiting_list'); ?>">Waiting for approval: <?php echo Modules::run('appscollegead/get_new_ads_count'); ?></a></div>
                        <div class="list-group-item"><a href="<?php echo site_url('appsuser/new_college_list'); ?>">New registered: <?php echo Modules::run('appsuser/get_new_colleges_count'); ?></a></div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="box">
                <div class="box-body">
                    <!-- Apply any bg-* class to to the info-box to color it -->
                    <div class="info-box bg-purple">
                        <span class="info-box-icon"><i class="fa fa-graduation-cap"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Institute</span>
                            <span class="info-box-number"><?php echo Modules::run('appsuser/total_institute') ?></span>
                            <!-- The progress section is optional -->
                            <div class="progress">
                                <div class="progress-bar" style="width: 65%"></div>
                            </div>
                        </div><!-- /.info-box-content -->
                    </div><!-- /.info-box -->

                    <div class="list-group">
                        <div class="list-group-item">Active Ads: <?php echo Modules::run('appsinstitutead/total_active_ads'); ?></div>
                        <div class="list-group-item">Inactive Ads: <?php echo Modules::run('appsinstitutead/total_inactive_ads'); ?></div>
                        <div class="list-group-item"><a href="<?php echo site_url('appsinstitutead/waiting_list'); ?>">Waiting for approval: <?php echo Modules::run('appsinstitutead/get_new_ads_count'); ?></a></div>
                        <div class="list-group-item"><a href="<?php echo site_url('appsuser/new_institute_list'); ?>">New registered: <?php echo Modules::run('appsuser/get_new_institute_count'); ?></a></div>
                    </div>

                </div>
            </div>
        </div>
    </div>







