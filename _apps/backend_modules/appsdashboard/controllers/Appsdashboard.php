<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Appsdashboard extends Backendcontroller {

    /**
     * @ Author: Suraj Bajracharya
     * @ Author URL : www.indesignmedia.net
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
    }

    /*     * *************************************************************
     * @ APPS LOGIN VALIDATION
     * *********************************************************** */

    public function index() {
        $this->template->load(get_template('default'), 'dashboard');
    }

}
