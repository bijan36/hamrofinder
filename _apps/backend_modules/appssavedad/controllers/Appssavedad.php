<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Appssavedad extends Backendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('Appssavedad_m', 'bsavedad');
    }

    //USER DASHBOARD
    public function index() {
        redirect('appsdashboard');
        exit();
    }

    public function get_saved_row_by_student($student_ID) {
        $rows = $this->bsavedad->order_by('ID', 'DESC')->get_many_by(array('interest_ID' => $student_ID));
        return $rows ? $rows : false;
    }

    //GETTING TOTAL BY AD
    public function get_total_by_ad($adID, $type) {
        return $this->bsavedad->count_by(array('ad_ID' => $adID, 'type' => $type));
    }

    //GET ROWS BY USER
    public function get_rows_by_user($userID) {
        $rows = $this->bsavedad->get_many_by(array('interest_ID' => $userID));
        return $rows ? $rows : false;
    }

}
