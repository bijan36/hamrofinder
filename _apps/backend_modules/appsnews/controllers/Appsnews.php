<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Appsnews extends Backendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('appsnews_m', 'bnews');
    }

    //STUDENT LIST
    public function index() {
        $data['alldata'] = $this->bnews->order_by('position', 'DESC')->get_all();
        $this->template->load(get_template('default'), 'list', $data);
    }

    //GET ALL
    public function get_all() {
        $rows = $this->bnews->get_all();
        return $rows ? $rows : false;
    }

    //ADD
    public function add() {
        $this->template->load(get_template('default'), 'add');
    }

    //ADDING
    public function adding() {

        $this->form_validation->set_rules('title', 'Page title', 'required');
        $this->form_validation->set_rules('content', 'Page content', 'required');
        $this->form_validation->set_rules('template', 'Template', 'required');
        $this->form_validation->set_rules('sidebar', 'Sidebar', 'required');

        if ($this->form_validation->run() == TRUE) {

            $slug = $this->bnews->return_slug($this->input->post('title'));
            $position = $this->bnews->get_position();


            $dataUpdate = array(
                'title' => $this->input->post('title'),
                'slug' => $slug,
                'content' => $this->input->post('content'),
                'excerpt' => $this->input->post('excerpt'),
                'template' => $this->input->post('template'),
                'sidebar' => $this->input->post('sidebar'),
                'feature_image' => $this->input->post('feature_image'),
                'gallery1' => $this->input->post('gallery1'),
                'gallery2' => $this->input->post('gallery2'),
                'gallery3' => $this->input->post('gallery3'),
                'gallery4' => $this->input->post('gallery4'),
                'gallery5' => $this->input->post('gallery5'),
                'gallery6' => $this->input->post('gallery6'),
                'position' => $position,
                'seo_title' => $this->input->post('seo_title'),
                'seo_description' => $this->input->post('seo_description'),
                'seo_keywords' => $this->input->post('seo_keywords'),
                'ipaddress'=>$this->input->ip_address()
            );


            if ($this->bnews->insert($dataUpdate)) {
                $latestID = $this->bnews->get_last_id();
                $data = array(
                    'status' => 'success',
                    'msg' => 'News & Updates upblished',
                    'ID' => $latestID
                );
                echo json_encode($data);
                exit();
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => 'Unable to update news & updates'
                );
                echo json_encode($data);
                exit();
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => validation_errors()
            );
            echo json_encode($data);
            exit();
        }
    }

    public function edit($id) {
        $row = $this->bnews->get($id);
        if ($row) {
            $data['row'] = $row;
            $this->template->load(get_template('default'), 'edit', $data);
        } else {
            justRedirect();
        }
    }

    //UPDATEING PAGE
    public function update() {

        $this->form_validation->set_rules('title', 'Page title', 'required');
        $this->form_validation->set_rules('slug', 'Page slug url', 'required');
        $this->form_validation->set_rules('content', 'Page content', 'required');
        $this->form_validation->set_rules('template', 'Template', 'required');
        $this->form_validation->set_rules('sidebar', 'Sidebar', 'required');
        $this->form_validation->set_rules('id', 'Update referance', 'required');

        if ($this->form_validation->run() == TRUE) {

            $ID = $this->input->post('id');
            $slug = $this->bnews->return_slug($this->input->post('slug'), $ID);

            $dataUpdate = array(
                'title' => $this->input->post('title'),
                'slug' => $slug,
                'content' => $this->input->post('content'),
                'excerpt' => $this->input->post('excerpt'),
                'template' => $this->input->post('template'),
                'sidebar' => $this->input->post('sidebar'),
                'feature_image' => $this->input->post('feature_image'),
                'gallery1' => $this->input->post('gallery1'),
                'gallery2' => $this->input->post('gallery2'),
                'gallery3' => $this->input->post('gallery3'),
                'gallery4' => $this->input->post('gallery4'),
                'gallery5' => $this->input->post('gallery5'),
                'gallery6' => $this->input->post('gallery6'),
                'seo_title' => $this->input->post('seo_title'),
                'seo_description' => $this->input->post('seo_description'),
                'seo_keywords' => $this->input->post('seo_keywords'),
                'ipaddress'=>$this->input->ip_address()
            );

            if ($this->bnews->update($ID, $dataUpdate)) {
                $data = array(
                    'status' => 'success',
                    'msg' => 'News and updates updated successfully',
                    'ID' => $ID
                );
                echo json_encode($data);
                exit();
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => 'Unable to update news and updates'
                );
                echo json_encode($data);
                exit();
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => validation_errors()
            );
            echo json_encode($data);
            exit();
        }
    }

    public function position($updown, $ID) {
        if ($updown == 'up') {
            $row = $this->bnews->get($ID);
            if ($row) {
                $newPos = $row->position + 1;
                $this->bnews->update($ID, array('position' => $newPos));
            }
        } else {
            $row = $this->bnews->get($ID);
            if ($row) {
                $newPos = $row->position - 1;
                $this->bnews->update($ID, array('position' => $newPos));
            }
        }
        redirect('appsnews');
        exit();
    }

    //CHANGE STATUS
    public function status($ID) {
        $row = $this->bnews->get($ID);
        if ($row) {
            if ($row->status == ACTIVE) {
                $this->bnews->update($ID, array('status' => INACTIVE));
                justRedirect('appsnews', 'msgs', 'Successfully inactivated.');
            } else {
                $this->bnews->update($ID, array('status' => ACTIVE));
                justRedirect('appsnews', 'msgs', 'Successfully activated');
            }
        } else {
            justRedirect('appsnews', 'msge', 'Unable to modify');
        }
    }

    //delete
    public function delete($ID) {
        $row = $this->bnews->get($ID);
        if ($row) {
            if ($this->bnews->delete($ID)) {
                justRedirect('appsnews', 'msgs', 'Successfully deleted.');
            } else {
                justRedirect('appsnews', 'msge', 'Unable to delete right now.');
            }
        } else {
            justRedirect('appsnews', 'msge', 'Unable to delete');
        }
    }

}
