<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Appsnews_m extends MY_Model {

    /**
     * @ Author: Suraj Bajracharya
     * @ Author URL : www.indesignmedia.net
     * @see http://indesignmedia.net
     */
    protected $_table = SYSTEMNEWS;

    public function __construct() {
        parent::__construct();
        $this->_database = $this->db;
        $this->primary_key = 'ID';
    }

    public function index() {
        
    }

    //RETURNING SLUG
    public function return_slug($slug, $id = null) {
        if ($id) {
            //UPDATING
            $row = $this->get($id);
            if ($row) {

                if ($row->slug == slugify($slug)) {
                    return slugify($slug);
                } else {
                    return slugify($slug);
                }
            } else {
                return "no-slug-" . random_string('alnum', 8);
            }
        } else {
            //INSERTING
            $row = $this->get_by(array('slug' => slugify($slug)));
            if ($row) {
                return $row->slug . '-' . random_string('alnum', 8);
            } else {
                return slugify($slug);
            }
        }
    }

    //GETING NEW POSITON
    public function get_position() {
        $total = $this->count_all();
        $total = $total + 1;
        return $total;
    }
    
    public function get_last_id() {
        $this->db->select_max('ID');
        $result = $this->db->get(SYSTEMNEWS)->result_array();
        return $result[0]['ID'];
    }

}
