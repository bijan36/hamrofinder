<?php
$ctrl = 'appsnews';
$term = 'News & Updates';
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><?php echo $term; ?> <i class="fa fa-angle-double-right"></i> Edit</a></h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo site_url('appsdashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>

<section class="content">
    <div class="row" style="margin-bottom: 15px;">
        <div class="col-md-12">
            <a href="<?php echo site_url('appsnews') ?>" class="btn btn-primary">Back to list</a>
            <a href="<?php echo site_url($ctrl . '/add') ?>" class="btn btn-primary">Add News & Updates</a>
            <a href="<?php echo site_url('news/' . $row->slug) ?>" class="btn btn-primary" target="_blank">View</a>
        </div>
        <div class="col-md-12 showMsg">
            <?php $this->load->view('back/inc/notification'); ?>
        </div>
    </div>
    <?php echo form_open($ctrl . '/update', array('name' => 'updateCms', 'class' => 'newsUpdateRefresh')); ?>
    <div class="row">
        <div class="col-md-7">
            <!-- Content Header (Page header) -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Edit <?php echo $term; ?></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group input-group-lg">
                                        <label for="title">Title</label>
                                        <input type="text" class="form-control" value="<?php echo $row->title; ?>" name="title">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="slug">Slug</label>
                                        <p>No space allowed (eg sample-working-page)</p>
                                        <input type="text" class="form-control" value="<?php echo $row->slug; ?>" name="slug">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="title">Content</label>
                                        <textarea class="form-control myEditor" name="content"><?php echo $row->content; ?></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="excerpt">Short Content</label>
                                        <textarea class="form-control" name="excerpt"><?php echo $row->excerpt; ?></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <hr/>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="seo_title">SEO Title</label>
                                        <input type="text" class="form-control" value="<?php echo $row->seo_title; ?>" name="seo_title">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="seo_description">SEO Description</label>
                                        <textarea class="form-control" name="seo_description"><?php echo $row->seo_description; ?></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="seo_keywords">SEO Keywords</label>
                                        <textarea class="form-control" name="seo_keywords"><?php echo $row->seo_keywords; ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Operation</h3>
                </div>
                <div class="box-body">
                    <p>
                        To save or update click button below:
                    </p>
                    <input type="hidden" name="id" value="<?php echo $row->ID; ?>">
                    <input type="hidden" name="base" id="base" value="<?php echo base_url(); ?>">
                    <input type="submit" name="addAction" class="btn btn-primary btn-block" value="Update">
                </div>
            </div>
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Set Feature Image</h3>
                </div>
                <div class="box-body">
                    <div class="sidebar-modules">
                        <?php
                        if ($row->feature_image) {
                            ?>
                            <img src="<?php echo $row->feature_image; ?>" class="img-responsive">
                            <?php
                        }
                        ?>
                        <label>Add New Photo</label><p>Please select image from <a href="http://localhost/merogoogle/appsimages" target="_blank">Image Library</a><br/>Give image absolute URL:eg http://example.com/img/image.jpg</p>
                        <input type="text" name="feature_image" class="form-control" value="<?php echo $row->feature_image; ?>">
                    </div>
                </div>
            </div>
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Set Gallery Image</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <p>Please select image from <a href="http://localhost/merogoogle/appsimages" target="_blank">Image Library</a></p>
                        </div>

                        <?php
                        for ($i = 1; $i <= 6; $i++):
                            $res = 'gallery' . $i;
                            ?>
                            <div class="col-md-4">
                                <?php
                                if ($row->$res) {
                                    ?>
                                    <img src="<?php echo $row->$res; ?>" class="img-responsive">
                                    <?php
                                }
                                ?>
                                <label>Image <?php echo $i; ?></label>
                                <input type="text" name="gallery<?php echo $i; ?>" class="form-control" value="<?php echo $row->$res; ?>">
                            </div>
                            <?php if ($i == 3) { ?>
                                <div class="clearfix"></div>
                            <?php } ?>

                        <?php endfor; ?>

                    </div>


                </div>
            </div>
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Set Page Layout for this page</h3>
                </div>
                <div class="box-body">
                    <div class="sidebar-modules">
                        <?php
                        $template = $row->template;
                        $one = $template == 'Full Width' ? 'checked' : '';
                        $two = $template == 'Right Sidebar' ? 'checked' : '';
                        $three = $template == 'Left Sidebar' ? 'checked' : '';
                        ?>
                        <small>
                            <li style="list-style: none; border-bottom: 1px solid #f0f0f0;">
                                <input type="radio" name="template" value="Full Width" <?php echo $one; ?>>&nbsp;Full Width
                            </li>
                            <li style="list-style: none; border-bottom: 1px solid #f0f0f0;">
                                <input type="radio" name="template" value="Right Sidebar" <?php echo $two; ?>>&nbsp;Right Sidebar
                            </li>
                            <li style="list-style: none; border-bottom: 1px solid #f0f0f0;">
                                <input type="radio" name="template" value="Left Sidebar" <?php echo $three; ?>>&nbsp;Left Sidebar
                            </li>
                        </small>
                    </div>
                </div>
            </div>
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Set Sidebar for this page</h3>
                </div>
                <div class="box-body">
                    <div class="sidebar-modules">
                        <small>
                            <?php
                            $allSlider = Modules::run('appssidebar/get_all');
                            foreach ($allSlider as $sidebar):
                                ?>
                                <li style="list-style: none; border-bottom: 1px solid #f0f0f0;">
                                    <input type="radio" name="sidebar" value="<?php echo $sidebar->ID ?>" <?php echo ($sidebar->ID == $row->sidebar) ? 'checked' : ''; ?>>&nbsp;<a href="<?php echo site_url('appssidebar/edit/' . $sidebar->ID) ?>" target="_blank"><?php echo $sidebar->name; ?></a><br/>
                                </li>
                                <?php
                            endforeach;
                            ?>
                        </small>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo form_close(); ?>