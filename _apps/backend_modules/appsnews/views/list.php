<?php
$ctrl = 'appsnews';
$term = 'News & Updates';
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><?php echo $term; ?></h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo site_url('appsdashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>


<section class="content">

    <div class="row" style="margin-bottom: 15px;">
        <div class="col-md-12">
            <a href="<?php echo site_url($ctrl . '/add') ?>" class="btn btn-primary">Add News & Updates</a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 showMsg">
            <?php echo $this->load->view('back/inc/notification'); ?>
        </div>
    </div>

    <!-- Content Header (Page header) -->
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><?php echo $term; ?> List</h3>
        </div><!-- /.box-header -->
        <div class="box-body">



            <?php
            if (!empty($alldata)) {
                ?>
                <table id="example1" class="table table-condensed table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Status</th>
                            <th>Position</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sn = 1;
                        foreach ($alldata as $row):
                            ?>
                            <tr>
                                <td><?php echo $sn++; ?></td>
                                <td><?php echo $row->ID; ?></td>
                                <td><?php echo $row->title; ?></td>
                                <td>
                                    <?php echo $row->status; ?><br/>
                                    <?php
                                    $oppo = $row->status == ACTIVE ? INACTIVE : ACTIVE;
                                    ?>
                                    <a href="<?php echo site_url('appsnews/status/' . $row->ID) ?>">Click to <?php echo $oppo; ?></a>
                                </td>
                                <td>
                                    <div class="btn-group" role="group">
                                        <a href="<?php echo site_url('appsnews/position/up/' . $row->ID) ?>" class="btn btn-default btn-xs"><i class="fa fa-chevron-up"></i></a>
                                        <a href="<?php echo site_url('appsnews/position/down/' . $row->ID) ?>" class="btn btn-default" btn-xs><i class="fa fa-chevron-down"></i></a>
                                    </div>
                                </td>
                                <td><a href="<?php echo site_url($ctrl . '/edit/' . $row->ID) ?>"><i class="fa fa-edit"></i> Edit</a></td>
                                <td><a href="<?php echo site_url($ctrl . '/delete/' . $row->ID) ?>" onclick="return confirm('Are you sure want to delete it permanently?')"><span class="text-danger"><i class="fa fa-times"></i> Delete</span></a></td>
                            </tr>
                            <?php
                        endforeach;
                        ?>
                    </tbody>
                </table>
                <?php
            }else {
                echo "<tr><td colspan='6'>No new & updates found!</td></tr>";
            }
            ?>
        </div>
    </div>