<?php
$ctrl = 'appsnews';
$term = 'News & Updates';
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><?php echo $term; ?> <i class="fa fa-angle-double-right"></i> Add</a></h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo site_url('appsdashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>


<section class="content">

    <div class="row" style="margin-bottom: 15px;">
        <div class="col-md-12">
            <a href="<?php echo site_url($ctrl) ?>" class="btn btn-primary">Back to list</a>
        </div>
        <div class="col-md-12 showMsg">
            <?php $this->load->view('back/inc/notification'); ?>
        </div>
    </div>




    <?php echo form_open($ctrl . '/adding', array('name' => 'newsupdate', 'class' => 'newsInsert')); ?>
    <div class="row">
        <div class="col-md-7">
            <!-- Content Header (Page header) -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Add <?php echo $term; ?></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group input-group-lg">
                                        <label for="title">Title</label>
                                        <input type="text" class="form-control" name="title">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="title">Content</label>
                                        <textarea class="form-control myEditor" name="content"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="excerpt">Short Content</label>
                                        <p>150 character are best in look</p>
                                        <textarea class="form-control" name="excerpt"></textarea>
                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <hr/>
                                </div>


                                <div class="col-md-12">
                                    <p>The SEO section will help your content to index in search engines like Google, Bing, Ask. So please be serious to fill out the section below with actual information and phrase that describe to above content.</p>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="seo_title">SEO Title</label>
                                        <p>Should be 65 character</p>
                                        <input type="text" class="form-control" name="seo_title">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="seo_description">SEO Description</label>
                                        <p>Should be 165 character</p>
                                        <textarea class="form-control" name="seo_description"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="seo_keywords">SEO Keywords</label>
                                        <p>Should be separated by comma(,)</p>
                                        <textarea class="form-control" name="seo_keywords"></textarea>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Operation</h3>
                </div>
                <div class="box-body">
                    <p>
                        To save or update click button below:
                    </p>
                    <input type="hidden" name="base" id="base" value="<?php echo base_url(); ?>">
                    <input type="submit" name="addAction" class="btn btn-primary btn-block" value="Update">
                </div>
            </div>
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Set Feature Image</h3>
                </div>
                <div class="box-body">
                    <div class="sidebar-modules">
                        <label>Add New Photo</label><p>Please select image from <a href="<?php echo base_url('appsimages');?>" target="_blank">Image Library</a><br/>Give image absolute URL:eg http://example.com/img/image.jpg</p>
                        <input type="text" name="feature_image" class="form-control">
                    </div>
                </div>
            </div>
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Set Gallery Image</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <p>Please select image from <a href="<?php echo base_url('appsimages');?>" target="_blank">Image Library</a></p>
                        </div>
                        
                        <?php
                        for ($i = 1; $i <= 6; $i++):
                            ?>
                            <div class="col-md-4">
                                <label>Image <?php echo $i;?></label>
                                <input type="text" name="gallery<?php echo $i;?>" class="form-control">
                            </div>
                        <?php endfor; ?>
                        
                      
                    </div>


                </div>
            </div>
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Set News Page Layout</h3>
                </div>
                <div class="box-body">
                    <div class="sidebar-modules">
                        <small>
                            <li style="list-style: none; border-bottom: 1px solid #f0f0f0;">
                                <input type="radio" name="template" value="Full Width">&nbsp;Full Width
                            </li>
                            <li style="list-style: none; border-bottom: 1px solid #f0f0f0;">
                                <input type="radio" name="template" value="Right Sidebar" checked>&nbsp;Right Sidebar
                            </li>
                            <li style="list-style: none; border-bottom: 1px solid #f0f0f0;">
                                <input type="radio" name="template" value="Left Sidebar">&nbsp;Left Sidebar
                            </li>
                        </small>
                    </div>
                </div>
            </div>
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Set Sidebar for this page</h3>
                </div>
                <div class="box-body">
                    <div class="sidebar-modules">
                        <small>
                            <?php
                            $allSlider = Modules::run('appssidebar/get_all');
                            foreach ($allSlider as $sidebar):
                                ?>
                                <li style="list-style: none; border-bottom: 1px solid #f0f0f0;">
                                    <input type="radio" name="sidebar" value="<?php echo $sidebar->ID ?>" <?php echo ($sidebar->ID == '30') ? 'checked' : ''; ?>>&nbsp;<a href="<?php echo site_url('appssidebar/edit/' . $sidebar->ID) ?>" target="_blank"><?php echo $sidebar->name; ?></a><br/>
                                </li>
                                <?php
                            endforeach;
                            ?>
                        </small>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo form_close(); ?>













