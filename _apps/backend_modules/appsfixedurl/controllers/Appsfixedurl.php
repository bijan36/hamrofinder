<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Appsfixedurl extends Backendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
    }

    //STUDENT LIST
    public function index() {
        $this->template->load(get_template('default'), 'list');
    }

}
