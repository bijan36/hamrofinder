<?php
$ctrl = 'appsfixedurl';
$term = 'Fixed Urls';
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Application Fixed URLs </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo site_url('appsdashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>


<section class="content">


    <!-- Content Header (Page header) -->
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><?php echo $term; ?> List</h3>
        </div><!-- /.box-header -->
        <div class="box-body">

            <div class="row">
                <div class="col-md-12 showMsg">
                    <?php $this->load->view('back/inc/notification'); ?>
                </div>
            </div>


            <table id="example1" class="table table-condensed table-bordered table-striped">
                <thead>
                    <tr>
                        <th>SN</th>
                        <th>URLs</th>
                        <th>Details</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td><?php echo site_url('search'); ?></td>
                        <td>Advance Search</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td><?php echo site_url('browse'); ?></td>
                        <td>Data Browse</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td><?php echo site_url('content/about-us'); ?></td>
                        <td>About company details</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td><?php echo site_url('content/contact-us'); ?></td>
                        <td>Company contact/mailing address</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td><?php echo site_url('dashboard'); ?></td>
                        <td>User dashboard</td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td><?php echo site_url('user/show/USER_UNIQUE_ID'); ?></td>
                        <td>User profile page</td>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td><?php echo site_url('studentad/ads/AD_UNIQUE_ID'); ?></td>
                        <td>Student ad details page</td>
                    </tr>
                    <tr>
                        <td>8</td>
                        <td><?php echo site_url('collegead/ads/AD_UNIQUE_ID'); ?></td>
                        <td>College ad details page</td>
                    </tr>
                    <tr>
                        <td>9</td>
                        <td><?php echo site_url('news'); ?></td>
                        <td>College news & event archive page</td>
                    </tr>
                    <tr>
                        <td>10</td>
                        <td><?php echo site_url('news/NEWS_UNIQUE_ID'); ?></td>
                        <td>College news & event details page</td>
                    </tr>
                    <tr>
                        <td>11</td>
                        <td><?php echo site_url('content/message-from-ceo'); ?></td>
                        <td>Message from CEO</td>
                    </tr>
                    <tr>
                        <td>12</td>
                        <td><?php echo site_url('content/help'); ?></td>
                        <td>Help page</td>
                    </tr>
                    <tr>
                        <td>13</td>
                        <td><?php echo site_url('content/faq'); ?></td>
                        <td>Frequently asked questions page</td>
                    </tr>
                    <tr>
                        <td>14</td>
                        <td><?php echo site_url('content/instruction'); ?></td>
                        <td>Website operation instruction page for users</td>
                    </tr>
                    <tr>
                        <td>15</td>
                        <td><?php echo site_url('content/terms-condition'); ?></td>
                        <td>Website terms & condition page</td>
                    </tr>
                    <tr>
                        <td>16</td>
                        <td><?php echo site_url('login'); ?></td>
                        <td>Session login page</td>
                    </tr>
                    <tr>
                        <td>17</td>
                        <td><?php echo site_url('register'); ?></td>
                        <td>User registration page</td>
                    </tr>
                    <tr>
                        <td>18</td>
                        <td><?php echo site_url('logout'); ?></td>
                        <td>Session logout page</td>
                    </tr>
                </tbody>

            </table>
        </div>
    </div>
