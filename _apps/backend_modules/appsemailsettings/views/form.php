<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Settings <i class="fa fa-angle-right fa-fw"></i> <?php echo $row->kee ?></h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo site_url('appsdashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>


<section class="content">


    <div class="box">
        <div class="box-body">
            <?php $this->load->view('back/inc/notification'); ?>
            <div class="row">
                <div class="col-md-12 showMsg"></div>
            </div>
            <!-- Content Header (Page header) -->

            <?php
            if ($row && !empty($row)) {
                ?>
                <?php if ($row->description) { ?>
                    <div class="well">
                        <?php echo $row->description; ?>
                    </div>
                <?php } ?>


                <?php echo form_open('appsemailsettings/update', array('id' => 'settingUpdateForm')) ?>

                <?php
                if ($row->type == 'single') {
                    ?>
                    <div class="form-group">
                        <label for="val"><?php echo $row->kee ?></label>
                        <input type="text" name="vals" class="form-control" value="<?php echo $row->val ?>">
                    </div>
                    <?php
                } else {
                    ?>
                    <div class="form-group">                        
                        <label for="val"><?php echo $row->kee ?></label>
                        <textarea name="vals" class="form-control"><?php echo $row->val ?></textarea>
                    </div>
                    <?php
                }
                ?>

                <input type="hidden" name="ids" value="<?php echo $row->ID ?>">
                <div class="form-group">
                    <button type="submit" name="submitForm" class="btn btn-primary">Update</button>
                </div>
                <?php echo form_close() ?>


                <?php
            } else {
                echo 'Sorry nothing to adjust here';
            }
            ?>
        </div>
    </div>
   
