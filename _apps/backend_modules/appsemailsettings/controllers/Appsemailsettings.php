<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Appsemailsettings extends Backendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('Appsemailsettings_m', 'besettings');
    }

    //USER DASHBOARD
    public function index() {
        $data['alldata'] = $this->besettings->get_all();
        $this->template->load(get_template('default'), 'list', $data);
    }

    public function formhandler($slug) {
        $data['row'] = $this->besettings->get_by(array('slug' => $slug));
        $this->template->load(get_template('default'), 'form', $data);
    }

    //GETTING ALL THE OPTIONS
    public function get_all_settings() {
        return $this->besettings->get_all();
    }

    //UPDATEING
    public function update() {
        $ids = $this->input->post('ids');
        $vals = $this->input->post('vals');

        if ($this->besettings->update($ids, array('val' => $vals))) {
            $data = array(
                'status' => 'success',
                'msg' => 'Settings updated successfully',
            );
            echo json_encode($data);
            exit();
        } else {
            $data = array(
                'status' => 'error',
                'msg' => 'Unable to save setting try again later',
            );
            echo json_encode($data);
            exit();
        }
    }

}
