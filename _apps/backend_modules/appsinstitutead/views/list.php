<?php
$ctrl = 'appsinstitutead';
$term = 'Institute Ad';
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Institute Advertise </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo site_url('appsdashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>


<section class="content">

    <!-- Content Header (Page header) -->
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><?php echo $term; ?> List</h3>
        </div><!-- /.box-header -->
        <div class="box-body">

            <div class="row">
                <div class="col-md-12 showMsg">
                    <?php $this->load->view('back/inc/notification'); ?>
                </div>
            </div>

            <table id="justtable" class="table table-condensed table-bordered table-striped">
                <thead>
                    <tr>
                        <th>SN</th>
                        <th>AD ID</th>
                        <th>User Details</th>
                        <th>Ad Details</th>
                        <th>Grade/Fee Range</th>
                        <th>Events</th>
                        <th>Status</th>
                        <th>Edit</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sn = 1;
                    foreach ($alldata as $data):
                        ?>
                        <tr>
                            <td><?php echo $sn++; ?></td>
                            <td><?php echo $data->ID; ?></td>
                            <td>
                                <?php echo Modules::run('appsuser/get_institute_short_details', $data->parent_ID); ?>
                            </td>
                            <td>
                                <small>
                                    <table>
                                        <tr>
                                            <td>Looking for</td>
                                            <td>:<strong><?php echo $data->looking_for; ?></strong></td>
                                        </tr>
                                        <tr>
                                            <td>Certified By</td>
                                            <td>:<strong><?php echo $data->affiliation; ?></strong></td>
                                        </tr>
                                        <tr>
                                            <td>Class Duration</td>
                                            <td>:<strong><?php echo $data->class_duration; ?></strong></td>
                                        </tr>
                                    </table>
                                </small>
                            </td>
                            <td>
                                <small>
                                    <table>
                                        <tr>
                                            <td>Scholarships Upto (%)</td>
                                            <td>:<strong><?php echo $data->grade_accepted; ?></strong></td>
                                        </tr>
                                        <tr>
                                            <td>Total Fee</td>
                                            <td>:<strong><?php echo $data->tuition_fee_range; ?></strong></td>
                                        </tr>
                                    </table>                                    
                                </small>
                            </td>

                            <td>
                                <small>
                                    <table>
                                        <tr>
                                            <td>Submit</td>
                                            <td>:<?php echo makeReadableDate($data->ad_submit_date); ?></td>
                                        </tr>
                                        <tr>
                                            <td>Start</td>
                                            <td>:<?php echo makeReadableDate($data->ad_start_date); ?></td>
                                        </tr>
                                        <tr>
                                            <td>End</td>
                                            <td>:<?php echo makeReadableDate($data->ad_end_date); ?></td>
                                        </tr>
                                    </table>
                                </small>
                            </td>
                            <td>

                                <?php
                                if ($data->ad_end_date < current_date() && $data->ad_end_date != 0) {
                                    echo "<span class='text-danger'><i class='fa fa-times' aria-hidden='true'></i>Expired</span>";
                                } else {
                                    echo status_color($data->status);
                                }
                                ?>
                                <br/>
                                <small>Feature:<?php echo $data->feature; ?></small>

                            </td>
                            <td>
                                <a href="<?php echo site_url($ctrl . '/edit/' . $data->ID) ?>"><i class="fa fa-edit"></i> Edit</a>
                            </td>
                        </tr>
                        <?php
                    endforeach;
                    ?>
                </tbody>

            </table>
            <div>
                <?php echo $pagination; ?>
            </div>
        </div>
    </div>
