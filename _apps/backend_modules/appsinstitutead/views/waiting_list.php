<?php
$ctrl = 'appsinstitutead';
$term = 'Institute Ad';
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Institute Advertise</h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo site_url('appsdashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>


<section class="content">
    <div class="row">
        <div class="col-md-12" style="margin-bottom: 15px;">
            <a href="<?php echo site_url('appsinstitute') ?>" class="btn btn-primary">Back to Institute list</a>
            <a href="<?php echo site_url($ctrl) ?>" class="btn btn-primary">Back to Institute ads list</a>
        </div>
    </div>
    <!-- Content Header (Page header) -->
    <div class="box">
       
        

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">New institute ad waiting for review</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <?php
                    //search for new institute ads
                    $newColAd = Modules::run('appsinstitutead/get_new_ads');
                    if ($newColAd) {
                        ?>
                        <table id="only20_4" class="table table-condensed table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>AD ID</th>
                                    <th>User Details</th>
                                    <th>Events</th>
                                    <th>Edit</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $sn = 1;
                                foreach ($newColAd as $row):
                                    $data['row'] = $row;
                                    $data['sn'] = $sn++;
                                    $this->load->view('appsinstitutead/loop', $data);
                                endforeach;
                                ?>
                            </tbody>

                        </table>
                        <?php
                    }else {
                        echo "No waiting data found!";
                    }
                    ?>
                </div>
            </div>


        
    </div>
</section>