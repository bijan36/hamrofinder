<?php
$ctrl = 'appsinstitute';
$term = 'Institute Ad';
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Newly registered institute</h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo site_url('appsdashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>


<section class="content">
    <div class="row">
        <div class="col-md-12" style="margin-bottom: 15px;">
            <a href="<?php echo site_url('appsinstitute') ?>" class="btn btn-primary">Back to Institute list</a>
            <a href="<?php echo site_url('appsinstitutead') ?>" class="btn btn-primary">Back to Institute ads list</a>
        </div>
    </div>
    <!-- Content Header (Page header) -->
    <!--NEW STUDENT USER REGISTER-->


        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Newly registered institute</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <!--NEW INSTITUTE USER REGISTER-->
                <?php
                $newStu = Modules::run('appsuser/get_new_institute');
                if ($newStu) {
                    ?>
                    <table id="only20_5" class="table table-condensed table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>SN</th>
                                <th>ID</th>
                                <th>User Details</th>
                                <th>View</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $sn = 1;
                            foreach ($newStu as $row):
                                $data['row'] = $row;
                                $data['sn'] = $sn++;
                                $this->load->view('appsinstitute/loop', $data);
                            endforeach;
                            ?>
                        </tbody>

                    </table>
                    <?php
                }else {
                    echo "No new data found!";
                }
                ?>
            </div>
        </div>

    <!--NEW STUDENT USER REGISTER-->
</section>










