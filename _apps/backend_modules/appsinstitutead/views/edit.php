<?php
$ctrl = 'appsinstitutead';
$term = 'Institute Ad';
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><?php echo $term; ?></h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo site_url('appsdashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>


<section class="content">

    <div class="row">
        <div class="col-md-12" style="margin-bottom: 15px;">
            <a href="<?php echo site_url('appsinstitute') ?>" class="btn btn-primary">Back to institute list</a>
            <a href="<?php echo site_url($ctrl) ?>" class="btn btn-primary">Back to institute ads list</a>
        </div>
    </div>

    <!-- Content Header (Page header) -->
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><?php echo $term; ?> Edit</h3>
        </div><!-- /.box-header -->
        <div class="box-body">

            <div class="row">
                <div class="col-md-12 showMsg">
                    <?php $this->load->view('back/inc/notification'); ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-5">
                    <h4>USER DETAILS</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <?php echo Modules::run('appsuser/get_institute_details', $alldata->parent_ID); ?>
                        </div>
                    </div>
                </div>


                <div class="col-md-7">
                    <h4>ADVERTISE SETTINGS</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <?php echo form_open($ctrl . '/feature_setting', array('class' => 'adminupdate')); ?>
                            <div class="well">
                                <div class="row">
                                    <div class="col-md-12">
                                        <p>
                                            Feature ads are display in front page feature section and scroll automatically one after another.
                                        </p>
                                        <div class="form-group">
                                            <label for="feature">Is this feature advertise?:</label><br/>
                                            <?php
                                            $chk1 = $alldata->feature == 'Yes' ? 'checked' : '';
                                            $chk2 = $alldata->feature == 'No' ? 'checked' : '';
                                            ?>
                                            <input type="radio" name="feature" value="Yes" <?php echo $chk1; ?>> Yes
                                            <input type="radio" name="feature" value="No" <?php echo $chk2; ?>> No
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <input type="hidden" name="id" value="<?php echo $alldata->ID ?>">
                                        <input type="submit" name="approve" class="btn btn-primary" value="Update">
                                    </div>
                                </div>
                                <div class="ajax-overlay"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>
                            </div>
                            <?php echo form_close() ?>
                        </div>

                        <div class="col-md-12">
                            <div class="well">
                                <div class="row">

                                    <?php echo form_open($ctrl . '/approve'); ?>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="looking_for">Looking For:</label>
                                            <input type="text" name="looking_for" class="form-control" value="<?php echo $alldata->looking_for; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="affiliation">Certified By:</label>
                                            <input type="text" name="affiliation" class="form-control" value="<?php echo $alldata->affiliation; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="class_duration">Class Duration:</label>
                                            <input type="text" name="class_duration" class="form-control" value="<?php echo $alldata->class_duration; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="grade_accepted">Scholarships Upto (%):</label>
                                            <input type="text" name="grade_accepted" class="form-control" value="<?php echo $alldata->grade_accepted; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="tuition_fee_range">Total Fee (NRs. in 000):</label>
                                            <input type="text" name="tuition_fee_range" class="form-control" value="<?php echo $alldata->tuition_fee_range; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="ad_start_date">Ad Start:</label>
                                            <?php $stDate = verify_ad_start_date($alldata->ad_start_date); ?>
                                            <input type="text" name="ad_start_date" class="form-control datepicker" value="<?php echo $stDate; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="ad_end_date">Ad Ends:</label>
                                            <?php $enDate = verify_ad_end_date($alldata->ad_end_date, $stDate); ?>
                                            <input type="text" name="ad_end_date" class="form-control datepicker" value="<?php echo $enDate; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="message">Message:</label>
                                            <textarea name="message" class="form-control"><?php echo $alldata->message; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="status">Status:</label>
                                            <?php
                                            $rd1 = $alldata->status == 'Active' ? 'checked' : '';
                                            $rd2 = $alldata->status == 'Inactive' ? 'checked' : '';
                                            ?>
                                            <input type="radio" name="status" value="Active" <?php echo $rd1; ?>> Active
                                            <input type="radio" name="status" value="Inactive" <?php echo $rd2; ?>> Inactive

                                        </div>
                                    </div>

                                    <?php
                                    if ($alldata->ad_end_date < current_date()) {
                                        ?>
                                        <div class="col-md-12">
                                            <div class="alert alert-danger">
                                                <?php
                                                if ($alldata->ad_start_date == 0 && $alldata->ad_start_date == 0) {
                                                    echo 'Ads is waiting for approval.';
                                                } else {
                                                    ?>
                                                    This advertise is already expired on <?php echo $enDate; ?>.
                                                    <?php
                                                }
                                                ?>

                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>


                                    <div class="col-md-12">
                                        <input type="hidden" name="id" value="<?php echo $alldata->ID ?>">
                                        <input type="submit" name="approve" class="btn btn-primary" value="Submit">
                                    </div>




                                    <?php echo form_close() ?>
                                </div>
                                <div class="ajax-overlay"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>





        </div>
    </div>
