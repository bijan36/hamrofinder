<?php
$ctrl = 'appsinstitute_ad_pricing';
$term = 'Institute Ads Pricing';
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><?php echo $term; ?> <i class="fa fa-angle-double-right fa-fw"></i> <?php echo $alldata->per_year_fee; ?></h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo site_url('appsdashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>


<section class="content">
    <div class="row">
        <div class="col-md-12" style="margin-bottom: 15px;">
            <a href="<?php echo site_url($ctrl) ?>" class="btn btn-primary">Back to list</a>
        </div>
    </div>
    <?php if (!empty($alldata)): ?>
        <?php echo form_open($ctrl . '/update', array('class' => 'commonUpdate')); ?>
        <div class="row">
            <div class="col-md-8">
                <!-- Content Header (Page header) -->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo $term; ?> Edit</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12 showMsg">
                                <?php $this->load->view('back/inc/notification'); ?>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Per Year Fee(NRP)</label>
                                    <input type="text" name="per_year_fee" class="form-control" value="<?php echo $alldata->per_year_fee; ?>">
                                </div>
                            </div>


                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Number of allowed ads:</label>
                                    <input type="text" name="number_ads" class="form-control" value="<?php echo $alldata->number_ads; ?>">
                                </div>
                            </div>


                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Display Position:</label>
                                    <input type="number" name="position" class="form-control" value="<?php echo $alldata->position; ?>">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Operation</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <p>
                            To save or update click button below:
                        </p>
                        <input type="hidden" name="id" value="<?php echo $alldata->ID; ?>">
                        <input type="submit" name="addAction" class="btn btn-primary btn-block" value="Update">
                    </div>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
    <?php endif; ?>

