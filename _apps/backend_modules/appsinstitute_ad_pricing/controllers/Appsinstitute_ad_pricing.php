<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Appsinstitute_ad_pricing extends Backendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('Appsinstitute_ad_pricing_m', 'binsadprice');
    }

    //STUDENT LIST
    public function index() {
        $data['alldata'] = $this->binsadprice->order_by('position', 'ASC')->get_all();
        $this->template->load(get_template('default'), 'list', $data);
    }

    public function edit($ID) {
        $data['alldata'] = $this->binsadprice->get($ID);
        $this->template->load(get_template('default'), 'edit', $data);
    }

    public function update() {

        $this->form_validation->set_rules('id', 'Parmeter', 'required|is_natural_no_zero');
        $this->form_validation->set_rules('per_year_fee', 'Per year fee', 'required|numeric');
        $this->form_validation->set_rules('number_ads', 'Number of ads', 'required|is_natural');
        $this->form_validation->set_rules('position', 'Position', 'required|is_natural');

        if ($this->form_validation->run() == TRUE) {

            $ID = $this->input->post('id');
            

            $updateArrray = array(
                'per_year_fee' => $this->input->post('per_year_fee'),
                'number_ads' => $this->input->post('number_ads'),
                'position' => $this->input->post('position')
            );


            if ($this->binsadprice->update($ID, $updateArrray)) {
                $data = array(
                    'status' => 'success',
                    'msg' => 'Settings updated successfully'
                );
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => validation_errors()
                );
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => validation_errors()
            );
        }

        echo json_encode($data);
        exit();
    }

}
