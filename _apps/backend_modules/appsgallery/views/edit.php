<?php
$ctrl = 'appsgallery';
$term = 'Gallery';
$cat = urldecode($this->uri->segment(3));
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><?php echo $term; ?> <i class="fa fa-angle-double-right"></i> Edit</a></h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo site_url('appsdashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>


<section class="content">
    <div class="row" style="margin-bottom: 15px;">
        <div class="col-md-12">
            <a href="<?php echo site_url('appsgallery') ?>" class="btn btn-primary">Back to list</a>
            <a href="<?php echo site_url('photogallery/' . $cat) ?>" target="_blank" class="btn btn-primary">View Gallery</a>
        </div>
        <div class="col-md-12 showMsg">
            <?php $this->load->view('back/inc/notification'); ?>
        </div>
    </div>


    <div class="row">

        <?php echo form_open($ctrl . '/addimage', array('class' => 'adminupdategallery')); ?>
        <div class="col-md-8">
            <!-- Content Header (Page header) -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><span style="font-size: 18px;"><?php echo $cat; ?></span></h3>
                </div><!-- /.box-header -->
                <div class="box-body">

                    <table class="table">
                        <thead>
                            <tr>
                                <td>SN</td>
                                <td>Photos</td>
                                <td>Thumb Size</td>
                                <td>Position</td>
                                <td>Remove</td>
                            </tr>
                        </thead>
                        <?php
                        $allGalleryImages = Modules::run('appsgallery/get_all', $cat);

                        if (!empty($allGalleryImages)) {
                            $sn = 1;
                            foreach ($allGalleryImages as $img):
                                ?>
                                <tr>
                                    <td><?php echo $sn++; ?></td>
                                    <td><img src="<?php echo $img->url ?>" style="height: 60px"></td>
                                    <td><?php echo $img->type; ?></td>
                                    <td>
                                        <div class="btn-group" role="group">
                                            <a href="<?php echo site_url('appsgallery/position/' . $img->Category . '/up/' . $img->ID) ?>" class="btn btn-default btn-xs"><i class="fa fa-chevron-up"></i></a>
                                            <a href="<?php echo site_url('appsgallery/position/' . $img->Category . '/down/' . $img->ID) ?>" class="btn btn-default" btn-xs><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                    </td>
                                    <td><a href="<?php echo site_url('appsgallery/remove/' . $img->Category . '/' . $img->ID) ?>" onclick="return confirm('Are you sure want to remove it from image gallery?')" class="btn btn-danger btn-xs"><i class="fa fa-times-circle-o"></i></a></td>
                                </tr>
                                <?php
                            endforeach;
                        }
                        ?>
                    </table>
                    <div id="mainWiget">
                    </div>
                </div>
            </div>
            <div class="box adder">
                <div class="box-header">
                    <h3 class="box-title">Add Gallery Photo</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <button type="submit" id="galleryimgbtn" class="btn btn-primary" value="Submit">Add Photo</button>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Save Gallery</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <button type="submit" name="saveBtn" class="btn btn-primary" value="Submit">Save</button>
                </div>
            </div>
        </div>
        <input type="hidden" name="Cateogry" value="<?php echo $cat ?>">
        <?php echo form_close(); ?>
    </div>