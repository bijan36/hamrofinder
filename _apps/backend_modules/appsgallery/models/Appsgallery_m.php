<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Appsgallery_m extends MY_Model {

    /**
     * @ Author: Suraj Bajracharya
     * @ Author URL : www.indesignmedia.net
     * @see http://indesignmedia.net
     */
    protected $_table = SYSTEMGALLERY;

    public function __construct() {
        parent::__construct();
        $this->_database = $this->db;
        $this->primary_key = 'ID';
    }

    public function index() {
        
    }

    public function get_new_position($cat) {
        $totoal = $this->count_by(array('Category' => $cat));
        return $totoal + 1;
    }

}
