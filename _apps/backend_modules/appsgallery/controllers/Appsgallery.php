<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Appsgallery extends Backendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('appsgallery_m', 'bgal');
    }

    //STUDENT LIST
    public function index() {
        $data['alldata'] = Modules::run('appssettings/get_all_settings', 'Gallery');
        $this->template->load(get_template('default'), 'list', $data);
    }

    //GET ALL
    public function get_all($category) {
        $rows = $this->bgal->order_by('position','DESC')->get_many_by(array('Category' => $category));
        return $rows ? $rows : false;
    }

    //ADD
    public function edit($category) {
        $row = $this->bgal->get_by(array('Category' => $category));
        $data['row'] = $row;
        $this->template->load(get_template('default'), 'edit', $data);
    }

    //UPDATING GALLERY IMAGE
    public function addimage() {
        $url = $this->input->post('url');
        $type = $this->input->post('type');
        $Cateogry = $this->input->post('Cateogry');
        if (!empty($url)) {
            $position = $this->bgal->get_new_position($Cateogry);
            $updateData = array(
                'Category' => $Cateogry,
                'url' => $url,
                'position' => $position,
                'type' => $type
            );

            if ($this->bgal->insert($updateData)) {
                $data = array(
                    'status' => 'success',
                    'msg' => 'Photo has been updated'
                );
                echo json_encode($data);
                exit();
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => $updateData
                );
                echo json_encode($data);
                exit();
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => 'Please fill out the image absolute url'
            );
            echo json_encode($data);
            exit();
        }
    }
    
    //REMOVE IAMGE
    public function remove($cat,$ID){
        $row = $this->bgal->get($ID);
        if($row){
            $this->bgal->delete($ID);
        }
        redirect('appsgallery/edit/'.$cat);
        exit();
    }

    //POSITON
    public function position($cat, $updown, $ID) {
        if ($updown == 'up') {
            $row = $this->bgal->get($ID);
            if ($row) {
                $newPos = $row->position + 1;
                $this->bgal->update($ID, array('position' => $newPos));
            }
        } else {
            $row = $this->bgal->get($ID);
            if ($row) {
                $newPos = $row->position - 1;
                $this->bgal->update($ID, array('position' => $newPos));
            }
        }
        redirect('appsgallery/edit/'.$cat);
        exit();
    }

}
