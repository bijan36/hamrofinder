<?php
$ctrl = 'appssidebar';
$term = 'Sidebar';
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><?php echo $term; ?> <i class="fa fa-angle-double-right"></i> Edit</a></h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo site_url('appsdashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>


<section class="content">
    <div class="row" style="margin-bottom: 15px;">
        <div class="col-md-12">
            <a href="<?php echo site_url('appssidebar') ?>" class="btn btn-primary">Back to list</a>
        </div>
        <div class="col-md-12 showMsg">
            <?php $this->load->view('back/inc/notification'); ?>
        </div>
    </div>


    <div class="row">
        <div class="col-md-4">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Image Banner</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <button type="submit" id="imgbtn" class="btn btn-primary" value="Submit">Add Image Banner</button>
                </div>
            </div>
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Text Box</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <button type="submit" id="textbtn" class="btn btn-primary" value="Submit">Add Text Area</button>
                </div>
            </div>
        </div>
        <?php echo form_open($ctrl . '/update', array('class' => 'adminupdate')); ?>
        <div class="col-md-4">
            <!-- Content Header (Page header) -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><span style="font-size: 18px;"><?php echo $row->name; ?></span></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div id="mainWiget">
                        <?php
                        $sidebarContent = $this->bsidebars->get($row->ID);
                        if (!empty($sidebarContent->content)) {
                            $parts = explode('[sep]', $sidebarContent->content);
                        }

                        if (!empty($parts)) {
                            foreach ($parts as $part):
                                $fistLetter = substr($part, 0, 4);
                                if ($fistLetter == 'http') {
                                    $anotherParts = explode('[url]',$part);
                                    echo '<img src="' . $anotherParts[0] . '" class="img-responsive">';
                                    echo '<div style="margin-bottom:15px; padding:15px; border:1px solid #f0f0f0;"><label>Image Banner URL:</label><p>Plese select image from <a href="'.site_url("appsimages").'" target="_blank">Image Library</a><br/>Give image absolute URL:eg http://example.com/img/image.jpg</p><input type="text" class="form-control" name="fields[]" value="' . $anotherParts[0] . '"><label>Image Banner URL:</label><input type="text" class="form-control" name="fields[]" value="' . $anotherParts[1] . '"></div>';
                                } else {
                                    echo '<div style="margin-bottom:15px; padding:15px; border:1px solid #f0f0f0;"><label>Text to be display(can use HTML):</label><textarea class="form-control" name="fields[]">' . $part . '</textarea></div>';
                                }
                            endforeach;
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Save Sidebar</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <button type="submit" name="saveBtn" class="btn btn-primary" value="Submit">Save</button>
                </div>
            </div>
        </div>
        <input type="hidden" name="sidebarID" value="<?php echo $row->ID ?>">
        <?php echo form_close(); ?>
    </div>