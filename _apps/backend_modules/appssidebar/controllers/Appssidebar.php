<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Appssidebar extends Backendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('appssidebar_m', 'bsidebars');
    }

    //STUDENT LIST
    public function index() {
        $data['alldata'] = $this->bsidebars->get_all();
        $this->template->load(get_template('default'), 'list', $data);
    }

    //GET ALL
    public function get_all() {
        $rows = $this->bsidebars->get_all();
        return $rows ? $rows : false;
    } 
    //ADD
    public function edit($id) {
        $row = $this->bsidebars->get($id);
        if ($row) {
            $data['row'] = $row;
            $this->template->load(get_template('default'), 'edit', $data);
        } else {
            justRedirect();
        }
    }

    //UPDATING SIDEBAR
    public function update() {
        $fields = $this->input->post('fields[]');
        $ID = $this->input->post('sidebarID');
        if (!empty($fields)) {
            $string = '';
            $sep = "";
            $image = false;
            foreach ($fields as $field):
                if (!empty($field)) {
                    $fistLetter = substr($field, 0, 4);
                    if (!$image) {
                        if ($fistLetter == 'http') {
                            //its image
                            $image = true;
                            $string .=$sep . $field;
                        } else {
                            //its content
                            $string .=$sep . $field;
                        }
                    } else {
                        $field = $field ? $field : '#';
                        $string .='[url]' . $field;
                        $image = false;
                    }
                    $sep = "[sep]";
                }
            endforeach;
            if ($this->bsidebars->update($ID, array('content' => $string))) {
                $data = array(
                    'status' => 'success',
                    'msg' => 'Sidebar saved.'
                );
                echo json_encode($data);
                exit();
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => 'Unable to save data.'
                );
                echo json_encode($data);
                exit();
            }
        } else {

            if ($this->bsidebars->update($ID, array('content' => ''))) {
                $data = array(
                    'status' => 'success',
                    'msg' => 'An empty value was saved for this sidebar.'
                );
                echo json_encode($data);
                exit();
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => 'Unable to save data.'
                );
                echo json_encode($data);
                exit();
            }
        }
    }

}
