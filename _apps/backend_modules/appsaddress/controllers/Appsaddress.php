<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Appsaddress extends Backendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('Appsaddress_m', 'baddress');
    }

    //STUDENT LIST
    public function index() {
        $data['alldata'] = $this->baddress->order_by('position', 'ASC')->get_many_by(array('parent_ID' => 0));
        $this->template->load(get_template('default'), 'list', $data);
    }

    //WORK FOR THE PROVINCES
    public function province($parent_ID) {
        $data['alldata'] = $this->baddress->order_by('position', 'ASC')->get_many_by(array('parent_ID' => $parent_ID, 'level' => 'Province'));
        $data['parentRow'] = $this->baddress->get($parent_ID);
        $this->template->load(get_template('default'), 'list_province', $data);
    }

    public function district($parent_ID1, $parent_ID) {
        $data['alldata'] = $this->baddress->order_by('position', 'ASC')->get_many_by(array('parent_ID' => $parent_ID, 'level' => 'District'));
        $data['parentRow1'] = $this->baddress->get($parent_ID1);
        $data['parentRow'] = $this->baddress->get($parent_ID);
        $this->template->load(get_template('default'), 'list_district', $data);
    }

    public function locallevel($parent_ID1, $parent_ID2, $parent_ID) {
        $data['alldata'] = $this->baddress->order_by('position', 'ASC')->get_many_by(array('parent_ID' => $parent_ID, 'level' => 'Local Level'));
        $data['parentRow1'] = $this->baddress->get($parent_ID1);
        $data['parentRow2'] = $this->baddress->get($parent_ID2);
        $data['parentRow'] = $this->baddress->get($parent_ID);
        $this->template->load(get_template('default'), 'list_locallevel', $data);
    }

    public function wardno($parent_ID1, $parent_ID2, $parent_ID3, $parent_ID) {
        $data['alldata'] = $this->baddress->order_by('position', 'ASC')->get_many_by(array('parent_ID' => $parent_ID, 'level' => 'Ward No'));
        $data['parentRow1'] = $this->baddress->get($parent_ID1);
        $data['parentRow2'] = $this->baddress->get($parent_ID2);
        $data['parentRow3'] = $this->baddress->get($parent_ID3);
        $data['parentRow'] = $this->baddress->get($parent_ID);
        $this->template->load(get_template('default'), 'list_wardno', $data);
    }

    public function get_select_box($parentID, $index) {
        $rows = $this->baddress->order_by('position', 'ASC')->get_many_by(array('parent_ID' => $parentID, 'level' => $index));
        $output = '<seclect class="form-control">';
        foreach ($rows as $row):
            $output .= '<option value="' . $row->ID . '">' . $row->name . '</option>';
        endforeach;
        $output .= '</select>';
        return $output;
    }

    //GET ALL
    public function get_all() {
        $rows = $this->baddress->get_all();
        return $rows ? $rows : false;
    }

//    //ADD
    public function add() {
        $this->template->load(get_template('default'), 'add');
    }

    //SAVE
    public function save() {
        $this->form_validation->set_rules('name', 'Country Name', 'required');
        if ($this->form_validation->run() == TRUE) {
            $dataUpdate = array(
                'name' => $this->input->post('name'),
                'parent_ID' => 0,
                'level' => 'country'
            );
            if ($this->baddress->insert($dataUpdate)) {
                $data = array(
                    'status' => 'success',
                    'msg' => 'Country has been added',
                    'ID' => $this->baddress->get_last_id(),
                );
                echo json_encode($data);
                exit();
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => 'Unable to add event'
                );
                echo json_encode($data);
                exit();
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => validation_errors()
            );
            echo json_encode($data);
            exit();
        }
    }

    //ADD
    public function edit($id) {
        $row = $this->baddress->get($id);
        if ($row) {
            $data['row'] = $row;
            $this->template->load(get_template('default'), 'edit', $data);
        } else {
            justRedirect();
        }
    }

    //UPDTE
    public function update() {
        $ID = $this->input->post('id');
        $title = $this->input->post('name');
        if ($this->baddress->update($ID, array('name' => $title))) {
            $data = array(
                'status' => 'success',
                'msg' => 'Updated successfully'
            );
            echo json_encode($data);
            exit();
        } else {

            $data = array(
                'status' => 'error',
                'msg' => 'Unable to update'
            );
            echo json_encode($data);
            exit();
        }
    }

    //UPDATEING DDRESS
    public function addaddress() {

        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('parent_ID', 'Parent ID', 'required');
        $this->form_validation->set_rules('level', 'Level', 'required');

        if ($this->form_validation->run() == TRUE) {

            $parent_ID = $this->input->post('parent_ID');
            $level = $this->input->post('level');
            $name = $this->input->post('name');

            //FIND DUBLICATE
            $row = $this->baddress->get_by(array('parent_ID' => $parent_ID, 'name' => $name, 'level' => $level));

            if ($row) {
                $data = array(
                    'status' => 'error',
                    'msg' => 'The data you are trying to add is already exist. Dublicate entry is not allowed.'
                );
                echo json_encode($data);
                exit();
            } else {

                $position = $this->baddress->count_by(array('parent_ID' => $parent_ID, 'level' => $level));

                $dataUpdate = array(
                    'name' => $name,
                    'level' => $level,
                    'parent_ID' => $parent_ID,
                    'position' => $position + 1,
                );

                if ($this->baddress->insert($dataUpdate)) {
                    $data = array(
                        'status' => 'success',
                        'msg' => 'Updated successfully'
                    );
                    echo json_encode($data);
                    exit();
                } else {
                    $data = array(
                        'status' => 'error',
                        'msg' => 'Unable to update page'
                    );
                    echo json_encode($data);
                    exit();
                }
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => validation_errors()
            );
            echo json_encode($data);
            exit();
        }
    }

    //REMOVE
    public function remove($ID, $parent, $parameter) {
        $this->baddress->delete($ID);
        $this->session->set_flashdata(array('msgs' => 'Removed successfully'));
        redirect('appsaddress/' . $parameter . '/' . $parent);
    }

    public function removeextended($ID, $parent1, $parent, $parameter) {
        $this->baddress->delete($ID);
        $this->session->set_flashdata(array('msgs' => 'Removed successfully'));
        redirect('appsaddress/' . $parameter . '/' . $parent1 . '/' . $parent);
    }

    public function remove1($ID, $parent1, $parent2, $parent, $parameter) {
        $this->baddress->delete($ID);
        $this->session->set_flashdata(array('msgs' => 'Removed successfully'));
        redirect('appsaddress/' . $parameter . '/' . $parent1 . '/' . $parent2 . '/' . $parent);
    }

    public function remove2($ID, $parent1, $parent2, $parent3, $parent, $parameter) {
        $this->baddress->delete($ID);
        $this->session->set_flashdata(array('msgs' => 'Removed successfully'));
        redirect('appsaddress/' . $parameter . '/' . $parent1 . '/' . $parent2 . '/' . $parent3 . '/' . $parent);
    }

    public function adjust($do, $ID, $parent, $parameter) {
        $row = $this->baddress->get($ID);
        $newPos = $row->position;
        if ($do == 'down') {
            $newPos = $newPos + 1;
            $this->baddress->update($ID, array('position' => $newPos));
            $this->session->set_flashdata(array('msgs' => 'Successfully done'));
            redirect('appsaddress/' . $parameter . '/' . $parent);
        } else {
            if ($newPos > 1) {
                $newPos = $newPos - 1;
            }
            $this->baddress->update($ID, array('position' => $newPos));
            $this->session->set_flashdata(array('msgs' => 'Successfully done'));
            redirect('appsaddress/' . $parameter . '/' . $parent);
        }
    }

    public function adjust1($do, $ID, $parent1, $parent, $parameter) {
        $row = $this->baddress->get($ID);
        $newPos = $row->position;
        if ($do == 'down') {
            $newPos = $newPos + 1;
            $this->baddress->update($ID, array('position' => $newPos));
            $this->session->set_flashdata(array('msgs' => 'Successfully done'));
            redirect('appsaddress/' . $parameter . '/' . $parent1 . '/' . $parent);
        } else {
            if ($newPos > 1) {
                $newPos = $newPos - 1;
            }
            $this->baddress->update($ID, array('position' => $newPos));
            $this->session->set_flashdata(array('msgs' => 'Successfully done'));
            redirect('appsaddress/' . $parameter . '/' . $parent1 . '/' . $parent);
        }
    }

    public function adjust2($do, $ID, $parent1, $parent2, $parent, $parameter) {
        $row = $this->baddress->get($ID);
        $newPos = $row->position;
        if ($do == 'down') {
            $newPos = $newPos + 1;
            $this->baddress->update($ID, array('position' => $newPos));
            $this->session->set_flashdata(array('msgs' => 'Successfully done'));
            redirect('appsaddress/' . $parameter . '/' . $parent1 . '/' . $parent2 . '/' . $parent);
        } else {
            if ($newPos > 1) {
                $newPos = $newPos - 1;
            }
            $this->baddress->update($ID, array('position' => $newPos));
            $this->session->set_flashdata(array('msgs' => 'Successfully done'));
            redirect('appsaddress/' . $parameter . '/' . $parent1 . '/' . $parent2 . '/' . $parent);
        }
    }

    public function adjust3($do, $ID, $parent1, $parent2, $parent3, $parent, $parameter) {
        $row = $this->baddress->get($ID);
        $newPos = $row->position;
        if ($do == 'down') {
            $newPos = $newPos + 1;
            $this->baddress->update($ID, array('position' => $newPos));
            $this->session->set_flashdata(array('msgs' => 'Successfully done'));
            redirect('appsaddress/' . $parameter . '/' . $parent1 . '/' . $parent2 . '/' . $parent3 . '/' . $parent);
        } else {
            if ($newPos > 1) {
                $newPos = $newPos - 1;
            }
            $this->baddress->update($ID, array('position' => $newPos));
            $this->session->set_flashdata(array('msgs' => 'Successfully done'));
            redirect('appsaddress/' . $parameter . '/' . $parent1 . '/' . $parent2 . '/' . $parent3 . '/' . $parent);
        }
    }

    public function get_provinces($parent_ID) {
        $rows = $this->baddress->order_by('position', 'ASC')->get_many_by(array('parent_ID' => $parent_ID, 'level' => 'Province'));
        return $rows ? $rows : false;
    }

    public function get_districts($parent_ID) {
        $rows = $this->baddress->order_by('position', 'ASC')->get_many_by(array('parent_ID' => $parent_ID, 'level' => 'District'));
        return $rows ? $rows : false;
    }

}
