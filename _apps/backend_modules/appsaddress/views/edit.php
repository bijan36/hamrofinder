<?php
$ctrl = 'appsaddress';
$term = 'Address';
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><?php echo $term; ?> <i class="fa fa-angle-double-right"></i> Edit</a></h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo site_url('appsdashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>

<section class="content">
    <div class="row" style="margin-bottom: 15px;">
        <div class="col-md-12">
            <a href="<?php echo site_url('appsaddress') ?>" class="btn btn-primary">Back to list</a>
        </div>
        <div class="col-md-12 showMsg">
            <?php $this->load->view('back/inc/notification'); ?>
        </div>
    </div>
    <?php echo form_open($ctrl . '/update', array('name' => 'updateCms', 'class' => 'commonUpdateRefresh')); ?>
    <div class="row">
        <div class="col-md-9">
            <!-- Content Header (Page header) -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Edit <?php echo $term; ?></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group input-group-lg">
                                        <label for="name">Country</label>
                                        <input type="text" class="form-control" value="<?php echo $row->name; ?>" name="name">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Operation</h3>
                </div>
                <div class="box-body">
                    <p>
                        To save or update click button below:
                    </p>
                    <input type="hidden" name="id" value="<?php echo $row->ID; ?>">
                    <input type="submit" name="addAction" class="btn btn-primary btn-block" value="Update">
                </div>
            </div>

        </div>
    </div>
    <?php echo form_close(); ?>
</section>