<?php
$ctrl = 'appsaddress';
$term = 'Address';
$url = 'locallevel';
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><?php echo $term; ?>
        <a href="<?php echo site_url('appsaddress/province/'. $parentRow1->ID) ?>">
        <i class='fa fa-angle-right fa-fw'></i><?php echo $parentRow1->name; ?>
        </a>
        <a href="<?php echo site_url('appsaddress/district/' . $parentRow1->ID . '/' . $parentRow2->ID) ?>">
        <i class='fa fa-angle-right fa-fw'></i><?php echo $parentRow2->name; ?>
        </a>
        <i class='fa fa-angle-right fa-fw'></i><?php echo $parentRow->name; ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo site_url('appsdashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>


<section class="content">
    <div class="row">
        <div class="col-md-8">
            <!-- Content Header (Page header) -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Local Level List</h3>
                </div><!-- /.box-header -->
                <div class="box-body">

                    <div class="row">
                        <div class="col-md-12 showMsg">
                            <?php $this->load->view('back/inc/notification'); ?>
                        </div>
                    </div>

                    <?php
                    if (!empty($alldata)) {
                        ?>
                        <table id="example1" class="table table-condensed table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>ID</th>
                                    <th>Local Level</th>
                                    <th>Add/Edit Ward</th>
                                    <th>Position</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $sn = 1;
                                foreach ($alldata as $row):
                                    $parent_ID = $parentRow->ID;
                                    ?>
                                    <tr>
                                        <td><?php echo $sn++; ?></td>
                                        <td><?php echo $row->ID; ?></td>
                                        <td><?php echo $row->name; ?></td>
                                        <td><a href="<?php echo site_url($ctrl . '/wardno/' . $parentRow1->ID . '/'. $parentRow2->ID . '/' . $parent_ID . '/' . $row->ID) ?>"><i class="fa fa-edit"></i> Add/Edit Ward</a></td>
                                        <td>
                                            <div class="btn-group" role="group">
                                                <a href="<?php echo site_url($ctrl . '/adjust2/up/' . $row->ID . '/' . $parentRow1->ID . '/' . $parentRow2->ID . '/' . $parent_ID . '/' . $url) ?>" class="btn btn-primary btn-sm"><i class="fa fa-angle-up fa-fw"></i></a>
                                                <a href="<?php echo site_url($ctrl . '/adjust2/down/' . $row->ID . '/' . $parentRow1->ID . '/' . $parentRow2->ID . '/' . $parent_ID . '/' . $url) ?>" class="btn btn-primary btn-sm"><i class="fa fa-angle-down fa-fw"></i></a>
                                            </div>

                                        </td>
                                        <td><a href="#" data-toggle="modal" data-id='<?php echo $row->ID ?>' data-title='<?php echo $row->name; ?>' data-target="#editmdladdress" class="editmdladdress"><i class="fa fa-edit"></i> Edit</a></td>
                                        <td><a href="<?php echo site_url($ctrl . '/remove1/' . $row->ID . '/' . $parentRow1->ID . '/' . $parentRow2->ID . '/' . $parent_ID . '/' . $url) ?>" onclick="return(confirm('Are you sure want to remove all the data associated with it and itself permanently?'))"><i class="fa fa-times"></i> Delete</a></td>
                                    </tr>
                                    <?php
                                endforeach;
                                ?>
                            </tbody>
                        </table>
                        <?php
                    }else {
                        echo "<tr><td colspan='6'>No local level found!</td></tr>";
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Add Local Level</h3>
                </div>
                <div class="box-body">
                    <?php echo form_open($ctrl . '/addaddress', array('class' => 'commonUpdateRefresh')); ?>
                    <div class="form-group">
                        <label>Add New Local Level.</label>
                        <input type="text" name="name" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="parent_ID" id="parentID" value="<?php echo $parentRow->ID; ?>">
                        <input type="hidden" name="level" id="level" value="Local Level">
                        <input type="submit" name="submit" class="btn btn-primary btn-block" value="Add">
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>






    <div class="modal fade" id="editmdladdress" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Modal title</h4>
                </div>
                <div class="modal-body">
                    <div class="showMsg"></div>
                    <input type="text" id="addressTitle" class="form-control">
                    <input type="hidden" id="addressID">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="updateAddressTitle">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
