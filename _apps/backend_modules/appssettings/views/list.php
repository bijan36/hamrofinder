<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Settings </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo site_url('appsdashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>


<section class="content">
        <div class="box">
            <?php $this->load->view('back/inc/notification'); ?>
            <div class="row">
                <div class="col-md-12 showMsg"></div>
            </div>
            <!-- Content Header (Page header) -->
            <table class="table table-bordered table-condensed">
                <tr>
                    <th>SN</th>
                    <th>Setting Name</th>
                    <th>Setting Value</th>
                    <th>Action</th>
                </tr>
                <?php
                $sn = 1;
                foreach ($alldata as $set):
                    ?>
                    <tr>
                        <td><?php echo $sn++; ?></td>
                        <td><?php echo $set->kee; ?></td>
                        <td><?php echo $set->val; ?></td>
                        <td>
                            <a href="<?php echo site_url('settings/' . $set->slug) ?>">Edit</a>
                        </td>
                    </tr>
                    <?php
                endforeach;
                ?>
            </table>
        </div>