<?php
$ctrl = 'appscollege';
$term = 'College';
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>College <i class="fa fa-angle-double-right fa-fw"></i> <?php echo $alldata->name; ?></h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo site_url('appsdashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>


<section class="content">
    <div class="row">
        <div class="col-md-12" style="margin-bottom: 15px;">
            <a href="<?php echo site_url($ctrl) ?>" class="btn btn-primary">Back to list</a>
        </div>
    </div>
    <!-- Content Header (Page header) -->

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Setting for <span class="text-danger"><?php echo $alldata->name; ?></span></h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            <?php if (!empty($alldata)): ?>

                <div class="row">
                    <div class="col-md-12 showMsg">
                        <?php $this->load->view('back/inc/notification'); ?>
                    </div>
                </div>
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#general_tab" data-toggle="tab">General Info</a></li>
                        <li><a href="#status_tab" data-toggle="tab">Status</a></li>
                        <li><a href="#plans_tab" data-toggle="tab">Plans</a></li>
                        <li><a href="#advertise_tab" data-toggle="tab">Advertise</a></li>
                        <li><a href="#imagegallery_tab" data-toggle="tab">Image Gallery</a></li>
                        <li><a href="#videogallery_tab" data-toggle="tab">Video Gallery</a></li>
                        <li><a href="#files_tab" data-toggle="tab">Download Files</a></li>
                        <li><a href="#payment_history" data-toggle="tab">Payments</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="general_tab">
                            <?php echo Modules::run('appsuser/get_college_details', $alldata->ID); ?>
                        </div><!-- /.tab-pane -->
                        <div class="tab-pane" id="status_tab">
                            <?php echo form_open('appscollege/status_setting', array('class' => 'adminupdate')); ?>
                            <div class="well">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="status">User's current account status:</label><br/>
                                            <?php
                                            $rd1 = $alldata->status == 'Active' ? 'checked' : '';
                                            $rd2 = $alldata->status == 'Inactive' ? 'checked' : '';
                                            $rd3 = $alldata->status == 'Banned' ? 'checked' : '';
                                            $rd4 = $alldata->status == 'Expired' ? 'checked' : '';
                                            ?>
                                            <input type="radio" name="status" value="Active" <?php echo $rd1; ?>> <strong>Active</strong> (user valid ads display in website.)<br/>
                                            <input type="radio" name="status" value="Inactive" <?php echo $rd2; ?>> <strong>Inactive</strong> (user valid and invalid both ads not display in website. User can not access the login and system notification)<br/>
                                            <input type="radio" name="status" value="Banned" <?php echo $rd3; ?>> <strong>Banned</strong> (user valid and invalid both ads not display in website. User can not access the login and system notification)<br/>
                                            <input type="radio" name="status" value="Expired" <?php echo $rd4; ?>> <strong>Expired</strong> (user valid and invalid both ads not display in website. User can not access the login and system notification)<br/>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <input type="hidden" name="id" value="<?php echo $alldata->ID ?>">
                                        <input type="submit" name="approve" class="btn btn-primary" value="Update">
                                    </div>
                                </div>
                            </div>
                            <?php echo form_close() ?>
                        </div><!-- /.tab-pane -->
                        <div class="tab-pane" id="plans_tab">
                            <?php echo form_open('appsstudent/level_setting', array('class' => 'adminupdate')); ?>
                            <div class="well">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="status">Current User Level:</label>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <?php echo user_level_select($alldata->level); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <input type="hidden" name="id" value="<?php echo $alldata->ID ?>">
                                        <input type="submit" name="approve" class="btn btn-primary" value="Update">
                                    </div>
                                </div>
                            </div>
                            <?php echo form_close() ?>
                        </div><!-- /.tab-pane -->
                        <div class="tab-pane" id="advertise_tab">
                            <?php
                            $all_related_ads = Modules::run('appscollegead/get_rows_by_parent', $alldata->ID);
                            if ($all_related_ads) {
                                ?>
                                <table class="table table-bordered table-condensed">
                                    <tr>
                                        <th>SN</th>
                                        <th>AD ID</th>
                                        <th>Details</th>
                                        <th>Status</th>
                                        <th>Events</th>
                                        <th>View / Edit</th>
                                    </tr>
                                    <?php
                                    $sn = 1;
                                    foreach ($all_related_ads as $ad):
                                        ?>
                                        <tr>
                                            <td><?php echo $sn++; ?></td>
                                            <td><?php echo $ad->ID; ?></td>
                                            <td>
                                                <small>
                                                    <table>
                                                        <tr>
                                                            <td>Looking For</td>
                                                            <td>:<strong><?php echo $ad->looking_for; ?></strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Affiliation</td>
                                                            <td>:<?php echo $ad->affiliation; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Admission Within</td>
                                                            <td>:<?php echo $ad->admission_within; ?> Days</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Grade Accepted</td>
                                                            <td>:<?php echo $ad->grade_accepted; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Tuition Fee Range</td>
                                                            <td>:<?php echo $ad->tuition_fee_range; ?></td>
                                                        </tr>
                                                    </table>
                                                </small>
                                            </td>
                                            <td><?php echo status_color($ad->status); ?></td>
                                            <td>
                                                <small>
                                                    <table>
                                                        <tr>
                                                            <td>Submitted</td>
                                                            <td>:<strong><?php echo makeReadableDate($ad->ad_submit_date); ?></strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Start</td>
                                                            <td>:<?php echo $ad->ad_start_date; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Ends</td>
                                                            <td>:<?php echo $ad->ad_end_date; ?></td>
                                                        </tr>
                                                    </table>
                                                </small>
                                            </td>
                                            <td>
                                                <a href="<?php echo site_url('appscollegead/edit/' . $ad->ID) ?>" target="_blank"><i class="fa fa-eye"></i> View / Edit</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </table>
                                <?php
                            }else {
                                echo 'No advertise by user yet :)';
                            }
                            ?>
                        </div>
                        <div class="tab-pane" id="imagegallery_tab">
                            <?php
                            $galPic = $alldata->gallery_picture;
                            if ($galPic && !empty($galPic)) {
                                $pics = explode(',', $galPic);
                                foreach ($pics as $pic):
                                    ?>
                                    <div class="thumb_inline editable-thumb galimgsingle-<?php echo $pic; ?>">
                                        <?php echo Modules::run('appsimages/get_img_size_linked', $pic, 'thumb100x100'); ?>
                                        <div class="remove-btn-img" data-id="<?php echo $pic; ?>" data-user="<?php echo $alldata->ID; ?>"><i class="fa fa-times fa-fw"></i></div>
                                    </div>
                                    <?php
                                endforeach;
                            }else {
                                echo 'No gallery picture';
                            }
                            ?>
                        </div>
                        <div class="tab-pane" id="videogallery_tab">
                            <div class="row">
                                <?php
                                $allVideos = Modules::run('appsvideos/get_videos', $alldata->ID);
                                if ($allVideos) {
                                    foreach ($allVideos as $av):
                                        ?>
                                        <div class="col-md-2 vid-cover-adm vidindadm-<?php echo $av->ID; ?>">
                                            <iframe width="200" height="120" src="https://www.youtube.com/embed/<?php echo $av->video_code; ?>"></iframe>
                                            <a href="#" class="vid_del_adm" data-id="<?php echo $av->ID; ?>" data-user="<?php echo $alldata->ID; ?>"><i class="fa fa-times"></i></a>
                                        </div>
                                        <?php
                                    endforeach;
                                } else {
                                    echo 'No videos';
                                }
                                ?>
                            </div>
                        </div>
                        <div class="tab-pane" id="files_tab">
                            <div class="row">
                                <?php
                                $allFiles = Modules::run('appsdocument/get_docs', $alldata->ID);
                                if ($allFiles) {
                                    foreach ($allFiles as $dc):
                                        ?>
                                        <div class="col-md-1  docholderadm-<?php echo $dc->ID; ?>">
                                            <div class="doc_img_cover">
                                                <a href="<?php echo base_url() ?>documents/<?php echo $dc->filename ?>" target="_blank" title="<?php echo $dc->title; ?>">
                                                    <img src="<?php echo base_url(); ?>documents/sample.jpg"  class="img-responsive">
                                                    <div class="docname"><?php echo $dc->title; ?></div>
                                                </a>
                                                <a href="#" class="doc_del_adm" data-id="<?php echo $dc->ID; ?>" data-user="<?php echo $alldata->ID; ?>">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <?php
                                    endforeach;
                                } else {
                                    echo 'No videos';
                                }
                                ?>
                            </div>
                        </div>
                        <div class="tab-pane" id="payment_history">
                            <div class="row">
                                <?php
                                $pRows = Modules::run('appsipn/get_all_the_payment_by', $alldata->ID);
                                if ($pRows) {
                                    ?>
                                    <table class="table">
                                        <tr>
                                            <th>SN</th>
                                            <th>Unique ID</th>
                                            <th>Ref. ID</th>
                                            <th>Payment Date</th>
                                            <th>Payment For</th>
                                            <th>Amount</th>
                                            <th>Start</th>
                                            <th>Expire</th>
                                            <th>Payment Status</th>
                                            <th>IP Address</th>
                                        </tr>
                                        <?php
                                        $pSn = 1;
                                        $ttol = 0;
                                        foreach ($pRows as $py):
                                            $ttol = $ttol + $py->price;
                                            ?>
                                            <tr>
                                                <td><?php echo $pSn++; ?></td>
                                                <td><?php echo $py->unique_ID; ?></td>
                                                <td><?php echo $py->ref_ID; ?></td>
                                                <td><?php echo $py->created_at; ?></td>
                                                <td>

                                                    <strong><?php echo $py->title; ?></strong><br/>
                                                    <?php
                                                    if ($py->privileged) {
                                                        if (!empty($py->privileged)) {
                                                            if (is_serialized($py->privileged)) {
                                                                $pData = unserialize($py->privileged);
                                                                foreach ($pData as $kk => $vv):
                                                                    echo $kk . ': ' . $vv . '<br/>';
                                                                endforeach;
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                </td>

                                                <td><?php echo $py->price; ?></td>
                                                <td><?php echo makedatadate($py->start_date); ?></td>
                                                <td><?php echo makedatadate($py->end_date); ?></td>
                                                <td><strong><?php echo $py->payment_status; ?></strong></td>
                                                <td><?php echo $py->ip_address; ?></td>
                                            </tr>
                                            <?php
                                        endforeach;
                                        ?>
                                        <tr>
                                            <td colspan="5" class="text-right"><strong>Total:</strong></td>
                                            <td colspan="3"><strong><?php echo makeMoney($ttol); ?></strong></td>
                                        </tr>

                                    </table>
                                    <?php
                                } else {
                                    echo 'No Payments found yet!';
                                }
                                ?>
                            </div>
                        </div>
                    </div><!-- /.tab-content -->
                </div><!-- nav-tabs-custom -->
            <?php endif; ?>
        </div>
    </div>
