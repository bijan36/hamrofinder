<?php
$ctrl = 'appscollege';
$term = 'College';
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>College </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo site_url('appsdashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>


<section class="content">


    <!-- Content Header (Page header) -->
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><?php echo $term; ?> List</h3>
        </div><!-- /.box-header -->
        <div class="box-body">

            <div class="row">
                <div class="col-md-12 showMsg">
                    <?php $this->load->view('back/inc/notification'); ?>
                </div>
            </div>


            <table id="justtable" class="table table-condensed table-bordered table-striped">
                <thead>
                    <tr>
                        <th>SN</th>
                        <th>ID</th>
                        <th>User Details</th>
                        <th>Level/Status</th>
                        <th>Ads Details</th>
                        <th>Events</th>
                        <th>Edit</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sn = 1;
                    foreach ($alldata as $data):
                        ?>
                        <tr>
                            <td><?php echo $sn++; ?></td>
                            <td><?php echo $data->ID; ?></td>
                            <td>
                                <?php echo Modules::run('appsuser/get_college_short_details', $data->ID); ?>
                            </td>
                            <td>
                                <small>
                                    <table>
                                        <tr>
                                            <td>Level</td>
                                            <td>:<strong><?php echo $data->level; ?></strong></td>
                                        </tr>
                                        <tr>
                                            <td>Status</td>
                                            <td>:<strong><?php echo status_color($data->status); ?></strong></td>
                                        </tr>
                                        <?php if ($data->status == ACTIVE) { ?>
                                            <tr>
                                                <td>Login As</td>
                                                <td>:
                                                    <a href="<?php echo site_url("user/admlogin/" . $data->code . "/" . $data->type . "/" . $data->ID) ?>" target="_blank">
                                                        Login
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </table>
                                </small>
                            </td>
                            <td>
                                <small>
                                    <table>
                                        <tr>
                                            <td>Total Ads</td>
                                            <td>:<strong><a  href="<?php echo site_url($ctrl . '/edit/' . $data->ID) ?>" data-toggle="tooltip" data-placement="top" title="Click to view ads"><?php echo Modules::run('appscollegead/total_ads_by_user', $data->ID); ?></a></strong></td>
                                        </tr>
                                    </table>
                                </small>
                            </td>
                            <td>
                                <small>
                                    <table>
                                        <tr>
                                            <td>Join Date</td>
                                            <td>:<?php echo makeReadableDate($data->join_date); ?></td>
                                        </tr>
                                        <tr>
                                            <td>Last Login</td>
                                            <td>:<?php echo makeReadableDate($data->last_login); ?></td>
                                        </tr>
                                        <tr>
                                            <td>Last IP</td>
                                            <td>:<?php echo $data->last_IP; ?></td>
                                        </tr>
                                    </table>
                                </small>
                            </td>
                            <td>
                                <a href="<?php echo site_url($ctrl . '/edit/' . $data->ID) ?>"><i class="fa fa-edit"></i> Edit</a>
                            </td>
                        </tr>
                        <?php
                    endforeach;
                    ?>
                </tbody>

            </table>
            <div>
                <?php echo $pagination; ?>
            </div>
        </div>
    </div>
