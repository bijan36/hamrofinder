<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Appsstudentad_m extends MY_Model {

    /**
     * @ Author: Suraj Bajracharya
     * @ Author URL : www.indesignmedia.net
     * @see http://indesignmedia.net
     */
    protected $_table = SYSTEMSTUDENTAD;

    public function __construct() {
        parent::__construct();
        $this->_database = $this->db;
        $this->primary_key = 'ID';
    }

    public function index() {
        echo "No good stuff";
    }
    
    public function get_last_id() {
        $this->db->select_max('ID');
        $result = $this->db->get(SYSTEMSTUDENTAD)->result_array();
        return $result[0]['ID'];
    }
    

}
