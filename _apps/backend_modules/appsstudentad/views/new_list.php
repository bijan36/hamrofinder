<?php
$ctrl = 'appsstudent';
$term = 'Student Ad';
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Newly registered students</h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo site_url('appsdashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>


<section class="content">
    <div class="row">
        <div class="col-md-12" style="margin-bottom: 15px;">
            <a href="<?php echo site_url('appsstudent') ?>" class="btn btn-primary">Back to Student list</a>
            <a href="<?php echo site_url('appsstudentad') ?>" class="btn btn-primary">Back to Student ads list</a>
        </div>
    </div>
    <!-- Content Header (Page header) -->
    <!--NEW STUDENT USER REGISTER-->

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Newly registered students</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            <?php
            $newStu = Modules::run('appsuser/get_new_students');
            if ($newStu) {
                ?>
                <table id="only20_1" class="table table-condensed table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>ID</th>
                            <th>User Details</th>
                            <th>View</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sn = 1;
                        foreach ($newStu as $row):
                            $data['row'] = $row;
                            $data['sn'] = $sn++;
                            $this->load->view('appsstudent/loop', $data);
                        endforeach;
                        ?>
                    </tbody>

                </table>
                <?php
            }else{
                echo "No new student registered found!";
            }
            ?>
        </div>
    </div>
    <!--NEW STUDENT USER REGISTER-->
</section>










