<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Appsimages extends Backendcontroller {

    /**
     * @ Author: Suraj Bajracharya
     * @ Author URL : www.indesignmedia.net
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     *
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('appsimages_m', 'images');
    }

    /*
     * **************************************************************
     * @ PRODUCT CATEGORY HANDLER
     * ************************************************************* 
     */

    public function index() {
        
        $config['base_url'] = base_url('appsimages');
        $config['total_rows'] = $this->images->count_all();
        $config['per_page'] = ADMINPERPAGEIMAGES;
        $config["uri_segment"] = 2;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);

        //config for bootstrap pagination class integration
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $data['pagination'] = $this->pagination->create_links();
        
        
        $data['allimages'] = $this->images->limit($config["per_page"], $data['page'])->order_by('ID','DESC')->get_all();
        
        //$data['allimages'] = $this->images->order_by('title', 'ASC')->get_all();
        $this->template->load(get_template('default'), 'list', $data);
    }

    public function get_row($id) {
        $row = $this->images->get($id);
        return $row ? $row : false;
    }

    public function get_image_by_id($id) {
        return $this->images->get_feature_image($id);
    }
    //BY ID AND SIZE
    public function get_feature_image($id,$size) {
        return $this->images->get_feature_image_size($id,$size);
    }
    //BY ID AND SIZE
    public function get_img_size($id,$size) {
        return $this->images->get_feature_image_size($id,$size);
    }

    public function get_img_size_linked($id,$size) {
        return $this->images->get_feature_image_size_linked($id,$size);
    }

    //GETTING ALL THE IMAGE
    public function get_all() {
        $rows = $this->images->order_by('ID', 'DESC')->get_all();
        return $rows ? $rows : false;
    }

    //ADDING IMAEG TO LIBRARY
    public function add() {
        if ($this->input->post('addtolib') == 'yes') {
            $imgName = $_FILES['system_file']['name']; 
            if (!empty($imgName)) {
                $data = $this->images->do_upload('system_file');
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => 'Not uploaded'
                );
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => 'Not allowed'
            );
        }
        echo json_encode($data);
        exit();
    }
    //ADDING BY AJAX
    public function ajaxadd() {
        if ($this->input->post('addtolib') == 'yes') {
            $imgName = $_FILES['system_file']['name']; 
            if (!empty($imgName)) {
                $class = $this->input->post('classname');
                $datas = $this->images->do_upload_ajax('system_file',$class);
                //print_r($data);
            } else {
                $datas = array(
                    'status' => 'error',
                    'msg' => 'Not uploaded'
                );
            }
        } else {
            $datas = array(
                'status' => 'error',
                'msg' => 'Not allowed'
            );
        }
        echo json_encode($datas);
        exit();
    }
    
    //SEARCH FURN
    public function searchimg(){
        $keyword = $this->input->post('keyword');        
        
        $rows = $this->images->serchids($keyword);
        if($rows){
            
            $datas['results'] = $rows;
            
            $datas = array(
                'status' => 'success',
                'msg' => 'good',
                'html' =>$this->load->view('modal/image_replacer_search',$datas, true),
                'count' =>count($rows)
            );
        }else{
            $datas = array(
                'status' => 'error',
                'msg' => 'Not found',
                'html' =>'Not matched'
            );
        }
        echo json_encode($datas);
        exit();
        
    }

    //DELETING 
    public function delete($id) {
        $row = $this->images->get($id);
        if ($row) {
            //echo base_url().'media/'.$row->fname;
            if (file_exists('./media/' . $row->fname)) {
                unlink('./media/' . $row->fname);
                $this->images->delete($id);
            } else {
                $this->images->delete($id);
            }
            justRedirect('appsimages', 'msgs', 'Image removed successfully');
        } else {
            justRedirect('appsimages', 'msge', 'No such images found!');
        }
    }

    //AJAX USE
    public function get_img_back() {
        $id = $this->input->post('id');
        if ($row = $this->images->get($id)) {
            if (file_exists('./media/' . $row->fname)) {
                $imgUrl = base_url() . 'media/' . $row->fname;
                $imgs = "<img src='" . $imgUrl . "' class='img-responsive'>";
                $data = array(
                    'imgurl' => $imgs
                );
            } else {
                $imgUrl = base_url() . 'media/sample.jpg';
                $imgs = "<img src='" . $imgUrl . "'  class='img-responsive'>";
                $data = array(
                    'imgurl' => $imgs
                );
            }
        } else {
            $imgUrl = base_url() . 'media/sample.jpg';
            $imgs = "<img src='" . $imgUrl . "'  class='img-responsive'>";
            $data = array(
                'imgurl' => $imgs
            );
        }
        echo json_encode($data);
        exit();
    }
    public function get_img_back_with_box() {
        $id = $this->input->post('id');
        if ($row = $this->images->get($id)) {
            if (file_exists('./media/' . $row->fname)) {
                $imgUrl = base_url() . 'media/' . $row->fname;
                $imgs = "<div class='col-sm-4 thumb-remove-holder'><div class='remover-icon'><i class='fa fa-times fa-2x'></i></div><img src='" . $imgUrl . "' class='img-responsive'><input type='hidden' name='gallery_image[]' value='".$id."'></div>";
                $data = array(
                    'imgurl' => $imgs
                );
            } else {
                $imgUrl = base_url() . 'media/sample.jpg';
                $imgs = "<div class='col-sm-4 thumb-remove-holder'><img src='" . $imgUrl . "' class='img-responsive'><input type='hidden' name='gallery_image[]' value='0'></div>";
                $data = array(
                    'imgurl' => $imgs
                );
            }
        } else {
            $imgUrl = base_url() . 'media/sample.jpg';
            $imgs = "<div class='col-sm-4 thumb-remove-holder'><img src='" . $imgUrl . "' class='img-responsive'><input type='hidden' name='gallery_image[]' value='0'></div>";
            $data = array(
                'imgurl' => $imgs
            );
        }
        echo json_encode($data);
        exit();
    }

    //removing the gallery image of college
    public function del_gallery_adm() {
        $galID = $this->input->post('id');
        $userID = $this->input->post('userID');

        $userRow = Modules::run('appsuser/get_row', $userID);

        if (!empty($userRow->gallery_picture)) {
            if (strpos($userRow->gallery_picture, ',') !== false) {
                //more than one value
                $idArray = explode(',', $userRow->gallery_picture);
                $newIds = array();
                foreach ($idArray as $gp):
                    if ($gp != $galID) {
                        $newIds[] = $gp;
                    }
                endforeach;

                if (count($newIds) == 0) {
                    $finalIds = '';
                } elseif (count($newIds) == 1) {
                    $finalIds = $newIds[0];
                } else {
                    $comma = '';
                    $finalIds = '';
                    foreach ($newIds as $nid):
                        $finalIds .= $comma . $nid;
                        $comma = ',';
                    endforeach;
                }
                if (Modules::run('appsuser/deletegalleyimage', $finalIds, $userID)) {
                    $datas = array(
                        'status' => 'success',
                        'msg' => 'Change has been made'
                    );
                    //lets delete the image from database permanently
                    $this->images->delete($galID);
                    echo json_encode($datas);
                    exit();
                } else {
                    $datas = array(
                        'status' => 'error',
                        'msg' => 'Unable to update gallery'
                    );
                    echo json_encode($datas);
                    exit();
                }
            } else {
                //just one value
                $finalIds = '';
                if (Modules::run('appsuser/deletegalleyimage', $finalIds, $userID)) {
                    $datas = array(
                        'status' => 'success',
                        'msg' => 'Change has been made'
                    );
                    //lets delete the image from database permanently
                    $this->images->delete($galID);
                    echo json_encode($datas);
                    exit();
                } else {
                    $datas = array(
                        'status' => 'error',
                        'msg' => 'Unable to update gallery'
                    );
                    echo json_encode($datas);
                    exit();
                }
            }
        } else {
            $datas = array(
                'status' => 'error',
                'msg' => 'No image found to be deleted'
            );
            echo json_encode($datas);
            exit();
        }
    }

}
