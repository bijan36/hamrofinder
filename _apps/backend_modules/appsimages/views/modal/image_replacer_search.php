<?php
if (isset($results) && !empty($results)) {
    foreach($results as $result):
    ?>
    <div class="col-xs-1">
        <div class='img-box-separate' data-id="<?php echo $result->ID; ?>">
            <img src="<?php echo image(base_url() . 'media/' . $result->fname,"thumb150x150") ?>" class="img-responsive"/>
            <div class="row">
                <div class="col-xs-8">
                    <div class="img-box-title">
                        <?php echo $result->title; ?>
                    </div>
                </div>
                <div class="col-xs-4" style="text-align: right">
                    <a href="<?php echo base_url() ?>media/<?php echo $result->fname; ?>" target="_blank" title="View Orginal Image"><i class="fa fa-eye fa-fw"></i></a>
                </div>
            </div>
        </div>
    </div>

    <?php
    endforeach;
}
?>