<?php
if (isset($fileNameTo)) {
    ?>
    <div class="col-xs-1">
        <div class='img-box-separate<?php echo $class;?>' data-id="<?php echo $fileID; ?>">
            <img src="<?php echo base_url() . 'media/' . $fileNameTo; ?>" class="img-responsive"/>

            <div class="row">
                <div class="col-xs-8">
                    <div class="img-box-title">
                        <?php echo $fileNameTo; ?>
                    </div>
                </div>
                <div class="col-xs-4" style="text-align: right">
                    <a href="<?php echo base_url() ?>media/<?php echo $fileNameTo; ?>" target="_blank" title="View Orginal Image"><i class="fa fa-eye fa-fw"></i></a>
                </div>
            </div>
        </div>
    </div>
    <?php
}
?>