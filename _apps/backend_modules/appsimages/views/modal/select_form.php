<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <?php
            $allimages = Modules::run('appsimages/get_all');
            ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="img-upload-container">
                                    <p>
                                        <strong>Upload New Image to image library</strong>
                                    </p>
                                    <?php echo form_open_multipart('appsimages/ajaxadd', array('id' => 'addImagesInstance')); ?>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <input type='file' id="feature_image" name='system_file'/>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <input type="hidden" name="addtolib" value="yes">
                                                <input type='submit' name='addnewimage' class='btn btn-primary' value="Upload Image Now" />
                                            </div>
                                        </div>
                                    </div>
                                    <?php echo form_close(); ?>
                                    <div class="showMsg-mdl"></div>
                                </div>
                            </div>
                        </div>


                        <div class="library-cover">
                            <div class="row no-gutter">
                                <div class="col-md-12">
                                    <input type="text" name="ajaxsearch" class="form-control" id="ajaxsearch" placeholder="Search Images">
                                </div>
                            </div>
                            <div class="row no-gutter images-list" style="max-height: 300px; overflow-y: scroll;">
                                <?php
                                if (isset($allimages) && !empty($allimages)) {
                                    foreach ($allimages as $img):
                                        ?>
                                        <div class="col-xs-1">
                                            <div class='img-box-separate' data-id="<?php echo $img->ID; ?>">
                                                <img src="<?php echo image(base_url() . 'media/' . $img->fname, "thumb150x150"); ?>" class="img-responsive" title="<?php echo $img->title; ?>"/>
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <div class="img-box-title">
                                                            <?php echo $img->title; ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-4" style="text-align: right">
                                                        <a href="<?php echo base_url() ?>media/<?php echo $img->fname; ?>" target="_blank" title="View Orginal Image"><i class="fa fa-eye fa-fw"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    endforeach;
                                }
                                ?>
                            </div>
                        </div>


                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Save</button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).on('click', '.img-box-separate', function () {

        var id = $(this).attr('data-id');
        //passing id to hidden field
        $('#select_image').val(id);
        $('.toggle-btn').html("<div class='remove_feature'><a href='#' id='remove_feature'>Remove Image</a></div>");
        $("div").removeClass("select-box");
        $(this).addClass('select-box');
        //getting image

        //updating selected image to div
        var urlTogetImg = '<?php echo base_url() ?>' + 'appsimages/get_img_back'
        var request = $.ajax({
            url: urlTogetImg,
            method: "POST",
            data: {"id": id},
            dataType: "html"
        });

        request.done(function (res) {
            var x = JSON.parse(res);
            $('#showImg').html(x.imgurl);
        });
    });
</script>
<script type="text/javascript">
    //UPLOADING NEW IMAGE IN MODAL
    //ADDING NEW IMAGE
    $('doucment').ready(function () {
        $('#addImagesInstance').submit(function (e) {
            e.preventDefault();

            var postData = $(this).serialize();
            var formUrl = $(this).attr('action');

            var request = $.ajax({
                url: formUrl,
                dataType: "html",
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
            });

            request.done(function (res) {
                var x = JSON.parse(res);
                if (x.status == 'success') {

                    $('.showMsg-mdl').html(x.msg);
                    $('.images-list').prepend(x.img_list);
                } else {

                    $('.overlay').css('display', 'none');
                    $('#submitBtn').removeClass('disabled');

                    $('#submitBtn').html('Submit');
                    $('.showMsg-mdl').html('Unable to add');
                }
            });
            request.fail(function (jqXHR, textStatus) {
                alert("Request failed: " + textStatus);
            });
        });
    });
</script>
<script>
    $('#ajaxsearch').keyup(function () {
        var keyword = $('#ajaxsearch').val();
        var searchUrl = '<?php echo base_url() ?>' + 'appsimages/searchimg'
        var request = $.ajax({
            url: searchUrl,
            dataType: "html",
            type: "POST",
            data: {"keyword": keyword}
        });
        request.done(function (res) {
            var x = JSON.parse(res);
            if (x.status == 'success') {
                $('.showMsg-mdl').html(x.count + ' image found.');
                $('.images-list').html(x.html);
            } else {
                //styling css while ajax finish
                $('.overlay').css('display', 'none');
                $('#submitBtn').removeClass('disabled');
                //putting values
                $('#submitBtn').html('Submit');
                $('.showMsg-mdl').html('Unable to add');
            }
        });
        request.fail(function (jqXHR, textStatus) {
            alert("Request failed: " + textStatus);
        });
    });
</script>
