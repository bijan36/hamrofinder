<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Image Library</h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo site_url('appsdashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>


<section class="content">
    <?php $this->load->view('back/inc/notification'); ?>
    <div class="row">
        <div class="col-md-12 showMsg"></div>
    </div>
    <!-- Content Header (Page header) -->


    <div class="row">
        <div class="col-md-12">

            <div class="row">
                <div class="col-md-12">
                    <div class="img-upload-container">
                        <p>
                            <strong>Upload New Image to image library</strong>
                        </p>
                        <?php echo form_open_multipart('appsimages/add', array('id' => 'addImages')); ?>
                        <div class="form-group" style="text-align: center;">
                            <input type='file' id="feature_image" name='system_file' class="center-block" />
                        </div>
                        <div class="form-group">
                            <input type="hidden" name="addtolib" value="yes">
                            <input type='submit' name='addnewimage' class='btn btn-default btn-lg' value="Add Now" />
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>


            <div class="library-cover">
                <div class="row no-gutter">
                    <?php
                    if (isset($allimages)) {
                        foreach ($allimages as $img):
                            ?>
                            <div class="col-xs-1">
                                <div class='img-box'>
                                    <img src="<?php echo image(base_url() . 'media/' . $img->fname, "thumb100x100"); ?>" class="img-responsive"/>
                                    <div class="row">
                                        <div class="col-xs-8">
                                            <div class="img-box-title">
                                                <?php echo $img->title; ?>
                                            </div>
                                        </div>
                                        <div class="col-xs-4" style="text-align: right">
                                            <a href="<?php echo base_url() ?>media/<?php echo $img->fname; ?>" target="_blank" title="View Orginal Image"><i class="fa fa-eye fa-fw"></i></a>
                                            <a href="<?php echo base_url() ?>appsimages/delete/<?php echo $img->ID; ?>" title="Remove" onclick="return confirm('Are you sure want to delete it?')"><i class="fa fa-times fa-fw"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        endforeach;
                    }
                    ?>
                </div>
                <div>
                    <?php echo $pagination; ?>
                </div>
            </div>


        </div>
    </div>
    <?php
    //$this->load->view(load_ajax('image-ajax')); ?>