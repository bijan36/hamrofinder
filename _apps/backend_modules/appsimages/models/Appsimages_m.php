<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Appsimages_m extends MY_Model {

    /**
     * @ Author: Suraj Bajracharya
     * @ Author URL : www.indesignmedia.net
     * @see http://indesignmedia.net
     */
    protected $_table = SYSTEMIMAGES;

    public function __construct() {
        parent::__construct();
        $this->_database = $this->db;
        $this->primary_key = 'ID';
    }

    public function index() {
        echo "No good stuff";
    }

    /*
     * **************************************************************
     * @ TAKING CARE ABOUT PRODUCT CATEGORY
     * ************************************************************* 
     */

    public function do_upload($filename) {
        //$filename = make_image_filename($filename);
        $newname = make_image_filename($_FILES["system_file"]['name']);
        $config = array(
            'upload_path' => "./media/",
            'allowed_types' => "gif|jpg|png|jpeg|pdf",
            'overwrite' => FALSE,
            'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
            'max_height' => IMGMAXH,
            'max_width' => IMGMAXW,
            'file_name' => $newname
                //'file_name'
        );
        //$new_name = time().$_FILES["userfiles"]['name'];
        //$config['file_name'] = $new_name;
        $this->load->library('upload', $config);
        if ($this->upload->do_upload($filename)) {
            $data = array('upload_data' => $this->upload->data());

            //MAKING TITLE FROM THE FILE NAME
            $imgTitle = make_image_title($data['upload_data']['file_name']);

            //LETS SAVE THE FILE

            if ($this->insert(array('title' => $imgTitle, 'fname' => $data['upload_data']['file_name']))) {
                $returnData = array(
                    'status' => 'success',
                    'msg' => 'Image has been uploaded',
                    'img_id' => $this->get_last_id(),
                    'image_name' => $data['upload_data']['file_name']
                );
            } else {
                $returnData = array(
                    'status' => 'error',
                    'msg' => 'Unable to upload image'
                );
            }
        } else {
            $returnData = array(
                'status' => 'error',
                'msg' => $this->upload->display_errors()
            );
        }
        return $returnData;
    }

    public function do_upload_ajax($filename, $class = null) {
        $config = array(
            'upload_path' => "./media/",
            'allowed_types' => "gif|jpg|png|jpeg|pdf",
            'overwrite' => FALSE,
            'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
            'max_height' => IMGMAXH,
            'max_width' => IMGMAXW
        );
        $this->load->library('upload', $config);
        if ($this->upload->do_upload($filename)) {

            $data = array(
                'upload_data' => $this->upload->data()
            );

            //MAKING TITLE FROM THE FILE NAME
            $imgTitle = make_image_title($data['upload_data']['file_name']);

            //LETS SAVE THE FILE

            if ($this->insert(array('title' => $imgTitle, 'fname' => $data['upload_data']['file_name']))) {


                $dataview['fileNameTo'] = $data['upload_data']['file_name'];
                $dataview['fileID'] = $this->get_last_id();
                $dataview['class'] = $class ? $class : '';


                $returnData = array(
                    'status' => 'success',
                    'msg' => 'Image has been uploaded',
                    'img_id' => $this->get_last_id(),
                    'image_name' => $data['upload_data']['file_name'],
                    'img_list' => $this->load->view('modal/image_replacer', $dataview, true)
                );
            } else {
                $returnData = array(
                    'status' => 'error',
                    'msg' => 'Unable to upload image'
                );
            }
        } else {
            $returnData = array(
                'status' => 'error',
                'msg' => $this->upload->display_errors()
            );
        }
        return $returnData;
    }

    public function serchids($key) {
        $this->db->from(SYSTEMIMAGES);
        $this->db->order_by("ID", "desc");
        $this->db->like("title", $key);
        $query = $this->db->get();
        //$query = $this->db->select('*')->from(SYSTEMIMAGES)->order_by('ID', 'DESC')->like("title", $key)->get();
        return $query->result();
    }

    //GET LAT ID
    public function get_last_id() {
        $this->db->select_max('ID');
        $result = $this->db->get(SYSTEMIMAGES)->result_array();
        return $result[0]['ID'];
    }

    //get feature image
    public function get_feature_image($id) {
        if (isset($id)) {
            $row = $this->get($id);
            if ($row) {
                if (file_exists('./media/' . $row->fname)) {
                    $fileName = $row->fname;
                } else {
                    $fileName = 'sample.jpg';
                }
            } else {
                $fileName = 'sample.jpg';
            }
        } else {
            $fileName = 'sample.jpg';
        }
        return $fileName;
    }

    //size specific
    public function get_feature_image_size($id, $size) {
        $title = COMPANYNAME;
        if (isset($id)) {
            $row = $this->get($id);
            if ($row) {
                if (file_exists('./media/' . $row->fname)) {
                    $fileName = $row->fname;
                    $title = $row->title;
                } else {
                    $fileName = 'sample.jpg';
                }
            } else {
                $fileName = 'sample.jpg';
            }
        } else {
            $fileName = 'sample.jpg';
        }
        $img = image(base_url() . 'media/' . $fileName, $size);
        return '<img src="' . $img . '" alt="' . $row->title . '" title="' . $row->title . '" class="img-responsive">';
    }

    public function get_feature_image_size_linked($id, $size) {
        $title = COMPANYNAME;
        if (isset($id)) {
            $row = $this->get($id);
            if ($row) {
                if (file_exists('./media/' . $row->fname)) {
                    $fileName = $row->fname;
                    $title = $row->title;
                } else {
                    $fileName = 'sample.jpg';
                }
            } else {
                $fileName = 'sample.jpg';
            }
        } else {
            $fileName = 'sample.jpg';
        }
        $img = image(base_url() . 'media/' . $fileName, $size);
        $imgOri = base_url() . 'media/' . $fileName;
        return '<a href="' . $imgOri . '" target="_blank"><img src="' . $img . '" alt="' . $row->title . '" title="' . $row->title . '" class="img-responsive"></a>';
    }

    //size specific
    public function get_img_size($id, $size) {
        $title = COMPANYNAME;
        if (isset($id)) {
            $row = $this->get($id);
            if ($row) {
                if (file_exists('./media/' . $row->fname)) {
                    $fileName = $row->fname;
                    $title = $row->title;
                } else {
                    $fileName = 'sample.jpg';
                }
            } else {
                $fileName = 'sample.jpg';
            }
        } else {
            $fileName = 'sample.jpg';
        }
        $img = image(base_url() . 'media/' . $fileName, $size);
        return '<img src="' . $img . '" alt="' . $row->title . '" title="' . $row->title . '" class="img-responsive">';
    }

}
