<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Appsinstitute extends Backendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('Appsinstitute_m', 'binstitute');
    }

    //STUDENT LIST
    public function index() {
        
        $config['base_url'] = base_url('appsinstitute');
        $config['total_rows'] = Modules::run('appsuser/total_institute');
        $config['per_page'] = ADMINPERPAGE;
        $config["uri_segment"] = 2;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);

        //config for bootstrap pagination class integration
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $data['pagination'] = $this->pagination->create_links();
        
        
        $data['alldata'] = Modules::run('appsuser/get_pagination_institute', $config["per_page"], $data['page']);
        $this->template->load(get_template('default'), 'list', $data);
    }

    //EDIT
    public function edit($id) {
        if (isset($id)) {
            $data['alldata'] = Modules::run('appsuser/get_institute_row', $id);
            $this->template->load(get_template('default'), 'edit', $data);
        } else {
            justRedirect();
        }
    }

    

    public function status_setting() {
        $new_feature = $this->input->post('status');
        $userID = $this->input->post('id');
        $dataArray = array('status' => $new_feature);
        $userRow = Modules::run('appsuser/get_row', $userID);
        if ($userRow) {
            if (Modules::run('appsuser/setting_update', $dataArray, $userID)) {

                //LETS NOTIFY USER
                $data['to'] = $userRow->email;
                $data['toName'] = $userRow->name;
                $data['subject'] = 'User status has been updated.';
                $data['template'] = 'user_status_changed_by_admin';
                $data['others'] = array(
                    'userfullname' => $userRow->name,
                    'emailaddress' => $userRow->email,
                    'status' => $new_feature
                );
                Modules::run('appsemail/doEmail', $data);
                //LETS NOTIFY USER

                $data = array(
                    'status' => 'success',
                    'msg' => 'Update successfully'
                );
                echo json_encode($data);
                exit();
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => 'Unable to update'
                );
                echo json_encode($data);
                exit();
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => 'No such user found!'
            );
            echo json_encode($data);
            exit();
        }
    }

    public function level_setting() {
        $new_feature = $this->input->post('level');
        $id = $this->input->post('id');
        $userRow = Modules::run('appsuser/get_row', $id);
        if ($userRow) {
            $dataArray = array('level' => $new_feature);
            if (Modules::run('appsuser/setting_update', $dataArray, $id)) {
                //LETS NOTIFY USER
                $data['to'] = $userRow->email;
                $data['toName'] = $userRow->name;
                $data['subject'] = 'User level has been updated.';
                $data['template'] = 'user_level_changed_by_admin';
                $data['others'] = array(
                    'userfullname' => $userRow->name,
                    'emailaddress' => $userRow->email,
                    'level' => $new_feature
                );
                Modules::run('appsemail/doEmail', $data);
                //LETS NOTIFY USER
                $data = array(
                    'status' => 'success',
                    'msg' => 'Update successfully'
                );
                echo json_encode($data);
                exit();
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => 'Unable to update'
                );
                echo json_encode($data);
                exit();
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => 'Unable to update'
            );
            echo json_encode($data);
            exit();
        }
    }

    //EDIT
    public function approve() {
        $id = $this->input->post('id');

        if ($adRow = $this->binstitute->get($id)) {

            $this->form_validation->set_rules('looking_for', 'Looking For', 'required');
            $this->form_validation->set_rules('affiliation', 'Affiliation', 'required');
            $this->form_validation->set_rules('admission_within', 'Admission within', 'required');
            $this->form_validation->set_rules('achieved_grade', 'Achieved Grade', 'required');
            $this->form_validation->set_rules('tuition_fee_range', 'Tuition Fee Range', 'required');
            $this->form_validation->set_rules('ad_start_date', 'Ad Start Date', 'required');
            $this->form_validation->set_rules('ad_end_date', 'Ad End Date', 'required');
            $this->form_validation->set_rules('message', 'Message', 'required');
            $this->form_validation->set_rules('status', 'Status', 'required');

            if ($this->form_validation->run() == TRUE) {

                $data = array(
                    'looking_for' => $this->input->post('looking_for'),
                    'affiliation' => $this->input->post('affiliation'),
                    'admission_within' => $this->input->post('admission_within'),
                    'achieved_grade' => $this->input->post('achieved_grade'),
                    'tuition_fee_range' => $this->input->post('tuition_fee_range'),
                    'ad_start_date' => $this->input->post('ad_start_date'),
                    'ad_end_date' => $this->input->post('ad_end_date'),
                    'message' => $this->input->post('message'),
                    'status' => $this->input->post('status')
                );

                if ($this->binstitute->update($id, $data)) {
                    $this->session->set_flashdata(array('msgs' => 'Updated successfully'));
                    redirect('appsstudentad/edit/' . $id);
                    exit();
                } else {
                    $this->session->set_flashdata(array('msge' => 'Unable to update'));
                    redirect('appsstudentad/edit/' . $id);
                    exit();
                }
            } else {
                $this->session->set_flashdata(array('msge' => 'Fill out the form and try again'));
                redirect('appsstudentad/edit/' . $id);
                exit();
            }
        } else {
            $this->session->set_flashdata(array('msge' => 'Fill out the form and try again'));
            redirect('appsstudentad/edit/' . $id);
            exit();
        }
    }

    //GET SAFE DATA BY USER ID
    public function get_row($ID) {
        $userRow = $this->binstitute->get($ID);
        return $userRow ? $userRow : false;
    }

    //GET SAFE DATA BY USER PARENT
    public function get_row_by_parent($parent_ID) {
        $userRow = $this->binstitute->get_by(array('parent_ID' => $parent_ID));
        return $userRow ? $userRow : false;
    }

}
