<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Appsinstitute_m extends MY_Model {

    /**
     * @ Author: Suraj Bajracharya
     * @ Author URL : www.indesignmedia.net
     * @see http://indesignmedia.net
     */
    protected $_table = SYSTEMINSTITUTE;

    public function __construct() {
        parent::__construct();
        $this->_database = $this->db;
        $this->primary_key = 'ID';
    }

    public function index() {
        echo "No good stuff";
    }
    
    public function get_last_id() {
        $this->db->select_max('ID');
        $result = $this->db->get(SYSTEMINSTITUTE)->result_array();
        return $result[0]['ID'];
    }
    

}
