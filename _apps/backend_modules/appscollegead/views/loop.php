<tr>
    <td><?php echo $sn; ?></td>
    <td><?php echo $row->ID; ?></td>
    <td>
        <?php echo Modules::run('appsuser/get_name_contact_college', $row->parent_ID); ?>
    </td>
    <td>
        <small>
            <table>
                <tr>
                    <td>Submit</td>
                    <td>:<?php echo makeReadableDate($row->ad_submit_date); ?></td>
                </tr>
            </table>
        </small>
    </td>
    <td>
        <a href="<?php echo site_url('appscollegead/edit/' . $row->ID) ?>"><i class="fa fa-edit"></i> Edit</a>
    </td>
</tr>