<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Appscollegead extends Backendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('Appscollegead_m', 'bcollegead');
    }

    //USER DASHBOARD
    public function index() {
        $config['base_url'] = base_url('appscollegead');
        $config['total_rows'] = $this->bcollegead->count_all();
        $config['per_page'] = ADMINPERPAGE;
        $config["uri_segment"] = 2;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);

        //config for bootstrap pagination class integration
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $data['pagination'] = $this->pagination->create_links();


        $data['alldata'] = $this->bcollegead->limit($config["per_page"], $data['page'])->order_by('ID', 'DESC')->get_all();
        $this->template->load(get_template('default'), 'list', $data);
    }

    //EDIT
    public function edit($id) {
        if (isset($id)) {
            $data['alldata'] = $this->bcollegead->get($id);
            $this->template->load(get_template('default'), 'edit', $data);
        } else {
            justRedirect();
        }
    }

    //SOME IMPORTANT SETTINGS
    public function feature_setting() {
        $new_feature = $this->input->post('feature');
        $adID = $this->input->post('id');

        //getting ad row
        $adRow = $this->bcollegead->get($adID);

        if ($adRow) {
            //get user row
            $userRow = Modules::run('appsuser/get_college_row', $adRow->parent_ID);
            if ($userRow) {

                $dataArray = array('feature' => $new_feature);
                if ($this->bcollegead->update($adID, $dataArray)) {

                    $sub = '';
                    if ($new_feature == 'Yes') {
                        $sub .= 'Congratulation! Your ad has been turned to feature ad by ' . COMPANYNAME;
                    } else {
                        $sub .= 'Your ad has been remvoved from feature ad list by ' . COMPANYNAME;
                    }

                    //LETS NOTIFY USER
                    $data['to'] = $userRow->email;
                    $data['toName'] = $userRow->name;
                    $data['subject'] = $sub;
                    $data['template'] = 'ad_feature_changed_by_admin';
                    $data['others'] = array(
                        'userfullname' => $userRow->name,
                        'emailaddress' => $userRow->email,
                        'feature' => $sub
                    );
                    Modules::run('appsemail/doEmail', $data);
                    //LETS NOTIFY USER

                    $data = array(
                        'status' => 'success',
                        'msg' => 'Update successfully'
                    );
                    echo json_encode($data);
                    exit();
                } else {
                    $data = array(
                        'status' => 'error',
                        'msg' => 'Unable to update'
                    );
                    echo json_encode($data);
                    exit();
                }
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => 'No such student for update'
                );
                echo json_encode($data);
                exit();
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => 'No such ad found for update'
            );
            echo json_encode($data);
            exit();
        }
    }

    //EDIT
    public function approve() {
        $id = $this->input->post('id');

        if ($adRow = $this->bcollegead->get($id)) {

            $this->form_validation->set_rules('looking_for', 'Looking For', 'required');
            $this->form_validation->set_rules('affiliation', 'Affiliation', 'required');
            $this->form_validation->set_rules('admission_within', 'Admission within', 'required');
            $this->form_validation->set_rules('grade_accepted', 'Grade accepted', 'required');
            $this->form_validation->set_rules('tuition_fee_range', 'Tuition Fee Range', 'required');
            $this->form_validation->set_rules('ad_start_date', 'Ad Start Date', 'required');
            $this->form_validation->set_rules('ad_end_date', 'Ad End Date', 'required');
            //$this->form_validation->set_rules('message', 'Message', 'required');
            $this->form_validation->set_rules('status', 'Status', 'required');

            if ($this->form_validation->run() == TRUE) {



                $data = array(
                    'looking_for' => $this->input->post('looking_for'),
                    'affiliation' => $this->input->post('affiliation'),
                    'admission_within' => $this->input->post('admission_within'),
                    'grade_accepted' => $this->input->post('grade_accepted'),
                    'tuition_fee_range' => $this->input->post('tuition_fee_range'),
                    'ad_start_date' => makedatadate($this->input->post('ad_start_date')),
                    'ad_end_date' => makedatadate($this->input->post('ad_end_date')),
                    'message' => $this->input->post('message'),
                    'status' => $this->input->post('status')
                );

                if ($this->bcollegead->update($id, $data)) {
                    $this->session->set_flashdata(array('msgs' => 'Updated successfully'));
                    redirect('appscollegead/edit/' . $id);
                    exit();
                } else {
                    $this->session->set_flashdata(array('msge' => 'Unable to update'));
                    redirect('appscollegead/edit/' . $id);
                    exit();
                }
            } else {
                $this->session->set_flashdata(array('msge' => 'Fill out the form and try again'));
                redirect('appscollegead/edit/' . $id);
                exit();
            }
        } else {
            $this->session->set_flashdata(array('msge' => 'Fill out the form and try again'));
            redirect('appscollegead/edit/' . $id);
            exit();
        }
    }

    //TOTAL AD BY USER
    public function total_ads_by_user($ID) {
        $rows = $this->bcollegead->count_by(array('parent_ID' => $ID));
        return $rows;
    }

    //GET NEW UNAPROVED ADS
    public function get_new_ads() {
        $rows = $this->bcollegead->get_many_by(array('ad_start_date' => '000-00-00', 'ad_end_date' => '000-00-00'));
        return $rows ? $rows : false;
    }
    
    //GET NEW UNAPROVED ADS
    public function get_new_ads_count() {
        $rows = $this->bcollegead->count_by(array('ad_start_date' => '000-00-00', 'ad_end_date' => '000-00-00'));
        return $rows ? $rows : 0;
    }

    //GET SAFE DATA BY USER ID
    public function get_row($ID) {
        $userRow = $this->bcollegead->get($ID);
        return $userRow ? $userRow : false;
    }

    //GET SAFE DATA BY USER PARENT
    public function get_row_by_parent($parent_ID) {
        $userRow = $this->bcollegead->get_by(array('parent_ID' => $parent_ID));
        return $userRow ? $userRow : false;
    }

    //GET SAFE DATA BY USER PARENT
    public function get_rows_by_parent($parent_ID) {
        $userRow = $this->bcollegead->get_many_by(array('parent_ID' => $parent_ID));
        return $userRow ? $userRow : false;
    }

    //TOTAL ACTIVE ADS
    public function total_active_ads() {
        $total = $this->bcollegead->count_by(array('status' => ACTIVE));
        return $total;
    }
    //TOTAL ACTIVE ADS
    public function total_inactive_ads() {
        $total = $this->bcollegead->count_by(array('status!=' => ACTIVE));
        return $total;
    }
    
     //WAITING LIST
    public function waiting_list(){
        $this->template->load(get_template('default'), 'waiting_list');
    }

}
