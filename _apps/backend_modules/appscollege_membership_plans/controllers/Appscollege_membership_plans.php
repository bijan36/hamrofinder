<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Appscollege_membership_plans extends Backendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('Appscollege_membership_plans_m', 'bmembership');
    }

    //STUDENT LIST
    public function index() {
        $data['alldata'] = $this->bmembership->order_by('position', 'ASC')->get_all();
        $this->template->load(get_template('default'), 'list', $data);
    }

    public function edit($ID) {
        $data['alldata'] = $this->bmembership->get($ID);
        $this->template->load(get_template('default'), 'edit', $data);
    }

    public function update() {

        $this->form_validation->set_rules('interest', 'Interest', 'required');
        $this->form_validation->set_rules('offer', 'Offer', 'required');
        $this->form_validation->set_rules('save', 'Save', 'required');
        $this->form_validation->set_rules('profile', 'Profile', 'required');
        $this->form_validation->set_rules('chat', 'Chat', 'required');
        $this->form_validation->set_rules('id', 'Parmeter', 'required|is_natural_no_zero');
        $this->form_validation->set_rules('per_year_fee', 'Per year fee', 'required|numeric');
        $this->form_validation->set_rules('free_ad', 'Free Ad', 'required|is_natural');
        $this->form_validation->set_rules('position', 'Position', 'required|is_natural');

        if ($this->form_validation->run() == TRUE) {

            $ID = $this->input->post('id');
            $rules = array(
                'interest' => $this->input->post('interest'),
                'offer' => $this->input->post('offer'),
                'save' => $this->input->post('save'),
                'profile' => $this->input->post('profile'),
                'chat' => $this->input->post('chat')
            );

            $updateArrray = array(
                'per_year_fee' => $this->input->post('per_year_fee'),
                'free_ad' => $this->input->post('free_ad'),
                'privileged' => serialize($rules),
                'position' => $this->input->post('position')
            );


            if ($this->bmembership->update($ID, $updateArrray)) {
                $data = array(
                    'status' => 'success',
                    'msg' => 'Settings updated successfully'
                );
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => validation_errors()
                );
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => validation_errors()
            );
        }

        echo json_encode($data);
        exit();
    }

}
