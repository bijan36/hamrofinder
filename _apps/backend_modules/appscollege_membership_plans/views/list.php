<?php
$ctrl = 'appscollege_membership_plans';
$term = 'College Membership Type';
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><?php echo $term; ?></h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo site_url('appsdashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>


<section class="content">


    <!-- Content Header (Page header) -->
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><?php echo $term; ?> List</h3>
        </div><!-- /.box-header -->
        <div class="box-body">

            <div class="row">
                <div class="col-md-12 showMsg">
                    <?php $this->load->view('back/inc/notification'); ?>
                </div>
            </div>


            <table id="example1" class="table table-condensed table-bordered table-striped">
                <thead>
                    <tr>
                        <th>SN</th>
                        <th>ID</th>
                        <th>Membership</th>
                        <th>Privileged</th>
                        <th>Fee</th>
                        <th>Free Ad</th>
                        <th>Edit</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sn = 1;
                    foreach ($alldata as $data):
                        ?>
                        <tr>
                            <td><?php echo $sn++; ?></td>
                            <td><?php echo $data->ID; ?></td>
                            <td><?php echo $data->title; ?></td>
                            <td>
                                <?php
                                if (is_serialized($data->privileged)) {
                                    $pres = unserialize($data->privileged);
                                    foreach ($pres as $k => $pre):
                                        echo $k . ':' . $pre . ', ';
                                    endforeach;
                                }
                                ?>
                            </td>
                            <td><?php echo $data->per_year_fee; ?></td>
                            <td><?php echo $data->free_ad; ?></td>
                            <td>
                                <a href="<?php echo site_url($ctrl . '/edit/' . $data->ID) ?>"><i class="fa fa-edit"></i> Edit</a>
                            </td>
                        </tr>
                        <?php
                    endforeach;
                    ?>
                </tbody>

            </table>
        </div>
    </div>
