<?php
$ctrl = 'appscollege_membership_plans';
$term = 'College Membership Type';
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><?php echo $term; ?> <i class="fa fa-angle-double-right fa-fw"></i> <?php echo $alldata->title; ?></h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo site_url('appsdashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>


<section class="content">
    <div class="row">
        <div class="col-md-12" style="margin-bottom: 15px;">
            <a href="<?php echo site_url($ctrl) ?>" class="btn btn-primary">Back to list</a>
        </div>
    </div>
    <?php if (!empty($alldata)): ?>
        <?php echo form_open($ctrl.'/update', array('class' => 'commonUpdate')); ?>
        <div class="row">
            <div class="col-md-8">
                <!-- Content Header (Page header) -->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo $term; ?> Edit</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12 showMsg">
                                <?php $this->load->view('back/inc/notification'); ?>
                            </div>
                            <div class="col-md-12">
                                <h4>PLANS: <?php echo $alldata->title; ?></h4>
                                <div class="row">
                                    <div class="col-md-12">
                                        <strong>Set rules for this plan:</strong><br/><br/>
                                        <?php
                                        $prev = unserialize($alldata->privileged);
                                        ?>
                                        <table class="table table-bordered">
                                            <tr>
                                                <th>Privileged Terms</th>
                                                <th>Conditions</th>
                                            </tr>
                                            <tr>
                                                <td>Can Show Interest to student ad</td>
                                                <td>
                                                    <?php $chk1 = $prev['interest'] == 'Yes' ? 'checked' : ''; ?>
                                                    <?php $chk2 = $prev['interest'] == 'No' ? 'checked' : ''; ?>
                                                    <input type="radio" name="interest" value="Yes" <?php echo $chk1; ?>>Yes
                                                    <input type="radio" name="interest" value="No" <?php echo $chk2; ?>>No
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Make Offer to student</td>
                                                <td>
                                                    <?php $chk1 = $prev['offer'] == 'Yes' ? 'checked' : ''; ?>
                                                    <?php $chk2 = $prev['offer'] == 'No' ? 'checked' : ''; ?>
                                                    <input type="radio" name="offer" value="Yes" <?php echo $chk1; ?>>Yes
                                                    <input type="radio" name="offer" value="No" <?php echo $chk2; ?>>No
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Save ad of student</td>
                                                <td>
                                                    <?php $chk1 = $prev['save'] == 'Yes' ? 'checked' : ''; ?>
                                                    <?php $chk2 = $prev['save'] == 'No' ? 'checked' : ''; ?>
                                                    <input type="radio" name="save" value="Yes" <?php echo $chk1; ?>>Yes
                                                    <input type="radio" name="save" value="No" <?php echo $chk2; ?>>No
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>view student profile</td>
                                                <td>
                                                    <?php $chk1 = $prev['profile'] == 'Yes' ? 'checked' : ''; ?>
                                                    <?php $chk2 = $prev['profile'] == 'No' ? 'checked' : ''; ?>
                                                    <input type="radio" name="profile" value="Yes" <?php echo $chk1; ?>>Yes
                                                    <input type="radio" name="profile" value="No" <?php echo $chk2; ?>>No
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Can live chat with students</td>
                                                <td>
                                                    <?php $chk1 = $prev['chat'] == 'Yes' ? 'checked' : ''; ?>
                                                    <?php $chk2 = $prev['chat'] == 'No' ? 'checked' : ''; ?>
                                                    <input type="radio" name="chat" value="Yes" <?php echo $chk1; ?>>Yes
                                                    <input type="radio" name="chat" value="No" <?php echo $chk2; ?>>No
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Plan Position:</td>
                                                <td>
                                                    <input type="number" name="position" class="form-control" value="<?php echo $alldata->position; ?>">
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Operation</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <p>
                            To save or update click button below:
                        </p>
                        <input type="hidden" name="id" value="<?php echo $alldata->ID; ?>">
                        <input type="submit" name="addAction" class="btn btn-primary btn-block" value="Update">
                    </div>
                </div>

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Per Year Fee(NRP)</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <p>
                            The annual charge for the current plan
                        </p>
                        <input type="text" name="per_year_fee" class="form-control" value="<?php echo $alldata->per_year_fee; ?>">
                    </div>
                </div>
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Free Ads</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <p>
                            Initial Free Ads for this plan
                        </p>
                        <input type="text" name="free_ad" class="form-control" value="<?php echo $alldata->free_ad; ?>">
                    </div>
                </div>

            </div>
        </div>
        <?php echo form_close(); ?>
    <?php endif; ?>

