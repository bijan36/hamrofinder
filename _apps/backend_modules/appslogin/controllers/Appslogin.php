<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Appslogin extends MX_Controller {

    /**
     * @ Author: Suraj Bajracharya
     * @ Author URL : www.indesignmedia.net
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('appslogin_m', 'appsMdl');
    }

    public function index() {
        $this->template->load('back/templates/login', 'login');
    }

    /*     * *************************************************************
     * @ APPS LOGIN VALIDATION
     * *********************************************************** */

    public function validate() {

        if ($this->input->post('username') && $this->input->post('password')) {

            $validation = array(
                array(
                    'field' => 'username',
                    'lebel' => 'Username',
                    'rules' => 'required|valid_email|max_length[25]'
                ),
                array(
                    'field' => 'password',
                    'lebel' => 'Password',
                    'rules' => 'required|exact_length[10]'
                ),
            );
            

            $this->form_validation->set_rules($validation);
            if ($this->form_validation->run() == TRUE) {

                $dataArray = array(
                    'emailAddress' => $this->input->post('username'),
                    'code' => $this->password_encrypt($this->input->post('password')),
                    'status' => ACTIVE
                );

                //CHECK WITH MODEL
                $this->appsMdl->validateMe($dataArray);
            } else {
                $data = array('status' => 'fail', 'msg' => 'Invalid Credentials 1');
                echo json_encode($data);
                exit();
            }
        } else {
            $data = array('status' => 'fail', 'msg' => 'Missing Fields');
            echo json_encode($data);
            exit();
        }
    }

    /*     * *************************************************************
     * @ CHECKING ADMIN LOGIN EVERY TIME
     * *********************************************************** */

    public function is_admin_login() {
        if ($this->session->userdata('adminLogged') != TRUE) {
            $argus = array('adminID', 'adminEmail', 'adminLogged', 'adminName');
            $this->session->unset_userdata($argus);
            $this->session->set_flashdata('msgs', 'Successfully loged out');
            redirect('appslogin');
            exit();
        }
    }

    public function logout() {

        $argus = array('adminID', 'adminEmail', 'adminLogged', 'adminName');
        $this->session->unset_userdata($argus);

        $data = array('status' => 'success', 'msg' => 'Successfully logout');
        echo json_encode($data);
        exit();
        //$this->session->set_flashdata('msgs', 'Successfully loged out');
        //redirect('appslogin');
        //exit();
    }

    private function get_crypt() {
        $st = $this->config->item('encryption_key');
        return substr($st, 5, 14);
    }

    private function password_encrypt($password) {
        $getTo = $password . $this->get_crypt();
        $hash = hash('sha512', $getTo);
        return $hash;
    }

}
