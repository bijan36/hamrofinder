<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Appslogin_m extends MY_Model {

    /**
     * @ Author: Suraj Bajracharya
     * @ Author URL : www.indesignmedia.net
     * @see http://indesignmedia.net
     */
    protected $_table = SYSTEMCONTROL;

    public function __construct() {
        parent::__construct();
        $this->_database = $this->db;
        $this->primary_key = 'stystem_id';
    }

    /*     * ********************************************
     * @ form validation
     * ******************************************* */

    public function validateMe($data) {
        if (isset($data) && is_array($data)) {

            $row = $this->get_by($data);            
            
            if ($row) {

                $sessArray = array(
                    'adminID' => $row->system_id,
                    'adminName' => $row->username,
                    'adminEmail' => $row->emailAddress,
                    'adminLogged' => TRUE
                );

                $this->session->set_userdata($sessArray);
                $data = array('status' => 'success', 'msg' => 'Success pleae wait a while.');
                echo json_encode($data);
                exit();
            } else {
                $data = array('status' => 'fail', 'msg' => 'Invalid Credentials 3');
                echo json_encode($data);
                exit();
            }
        } else {
            $data = array('status' => 'fail', 'msg' => 'Invalid Credentials 4');
            echo json_encode($data);
            exit();
        }
    }

}
