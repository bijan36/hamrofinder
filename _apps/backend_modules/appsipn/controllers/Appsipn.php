<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Appsipn extends Backendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('appsipn_m', 'bipn');
    }

    //STUDENT LIST
    public function index() {
        $data['alldata'] = $this->bipn->get_all();
        $this->template->load(get_template('default'), 'list', $data);
    }

    //GET ALL
    public function get_all() {
        $rows = $this->bipn->get_all();
        return $rows ? $rows : false;
    }
    
    public function get_all_the_payment_by($parentID){
        if (isset($parentID)) {
            $row = $this->bipn->order_by('ID', 'DESC')->get_many_by(array('parent_ID' => $parentID));
            return $row ? $row : false;
        } else {
            return false;
        }
    }

}
