<?php
$ctrl = 'appscms';
$term = 'Pages';
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><?php echo $term; ?></h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo site_url('appsdashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>


<section class="content">



    <!-- Content Header (Page header) -->
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><?php echo $term; ?> List</h3>
        </div><!-- /.box-header -->
        <div class="box-body">

            <div class="row">
                <div class="col-md-12 showMsg">
                    <?php $this->load->view('back/inc/notification'); ?>
                </div>
            </div>

            <?php
            if (!empty($alldata)) {
                ?>
                <table id="example1" class="table table-condensed table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sn = 1;
                        foreach ($alldata as $row):
                            ?>
                            <tr>
                                <td><?php echo $sn++; ?></td>
                                <td><?php echo $row->ID; ?></td>
                                <td><?php echo $row->title; ?></td>
                                <td><?php echo $row->status; ?></td>
                                <td><a href="<?php echo site_url($ctrl . '/edit/' . $row->ID) ?>"><i class="fa fa-edit"></i> Edit</a></td>
                            </tr>
                            <?php
                        endforeach;
                        ?>
                    </tbody>
                </table>
                <?php
            }else {
                echo "<tr><td colspan='6'>No pages found!</td></tr>";
            }
            ?>
        </div>
    </div>