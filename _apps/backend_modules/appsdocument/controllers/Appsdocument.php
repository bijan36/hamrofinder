<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Appsdocument extends Backendcontroller {

    /**
     * @ Author: Suraj Bajracharya
     * @ Author URL : www.indesignmedia.net
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     *
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('appsdocument_m', 'bdoc');
    }

    /*
     * **************************************************************
     * @ IMAGE CONTROLLERS
     * ************************************************************* 
     */

    public function index() {
        redirect('appsdashboard');
        exit();
    }
    
    public function get_all(){
        return $this->bdoc->get_all();
    }

    //GET THE RELATED DOCUMENTS
    public function get_docs($parent_ID) {
        $docs = $this->bdoc->get_many_by(array('parent_ID' => $parent_ID));
        return $docs ? $docs : false;
    }

    public function del_doc() {
        $docID = $this->input->post('id');
        $userID = $this->input->post('userID');

        $docRow = $this->bdoc->get_by(array('ID' => $docID, 'parent_ID' => $userID));
        if ($docRow) {
            if (is_file('./documents/' . $docRow->filename)) {
                unlink('./documents/' . $docRow->filename);
            }

            if ($this->bdoc->delete($docID)) {
                $datas = array(
                    'status' => 'success',
                    'msg' => 'Doument has been removed'
                );
            } else {
                $datas = array(
                    'status' => 'error',
                    'msg' => 'Unable to remove document'
                );
            }
        } else {
            $datas = array(
                'status' => 'error',
                'msg' => 'Your are not authorise to remove this document'
            );
        }
        echo json_encode($datas);
        exit();
    }
}
