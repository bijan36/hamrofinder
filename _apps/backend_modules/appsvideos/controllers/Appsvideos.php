<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Appsvideos extends Backendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('Appsvideos_m', 'bvideo');
    }

    //USER DASHBOARD
    public function index() {
        redirect('appsdashboard');
        exit();
    }

    public function remove() {
        $id = $this->input->post('id');
        $userID = $this->input->post('userID');
        if (!empty($id)) {
            $row = $this->bvideo->get($id);
            if ($row) {
                if ($row->parent_ID == $userID) {
                    if ($this->bvideo->delete($id)) {
                        $data = array(
                            'status' => 'success',
                            'msg' => 'Video has been removed successfully',
                        );
                        echo json_encode($data);
                        exit();
                    } else {
                        $data = array(
                            'status' => 'error',
                            'msg' => 'Unable to remove it!'
                        );
                        echo json_encode($data);
                        exit();
                    }
                } else {
                    $data = array(
                        'status' => 'error',
                        'msg' => 'You are not authorised to remove it!'
                    );
                    echo json_encode($data);
                    exit();
                }
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => 'Nothing found to remove!!'
                );
                echo json_encode($data);
                exit();
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => 'Unable to remove it'
            );
            echo json_encode($data);
            exit();
        }
    }

    //GET SAFE DATA BY USER ID
    public function get_row($ID) {
        $userRow = $this->bvideo->get($ID);
        return $userRow ? $userRow : false;
    }

    public function get_videos($parent_ID) {
        $userRow = $this->bvideo->get_many_by(array('parent_ID' => $parent_ID));
        return $userRow ? $userRow : false;
    }

    //GET SAFE DATA BY USER PARENT
    public function get_by_parent($parent_ID) {
        $userRows = $this->bvideo->get_many_by(array('parent_ID' => $parent_ID, 'level' => $level));
        return $userRows ? $userRows : false;
    }

}
