<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>HamroFinder.com</title>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo get_assist('img/icon.png') ?>">

        <!-- Bootstrap -->
        <link href="<?php echo get_assist('css/bootstrap.min.css') ?>" rel="stylesheet">
        <link href="<?php echo get_assist('css/datepicker.css') ?>" rel="stylesheet">
        <link href="<?php echo get_assist('css/coconut.css') ?>" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="<?php echo get_assist('fontawesome/css/font-awesome.min.css'); ?>">
        <script src="<?php echo get_assist('dist/sweetalert.min.js') ?>"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo get_assist('dist/sweetalert.css') ?>">
        <link rel="stylesheet" href="<?php //echo get_assist('css/datatables/dataTables.bootstrap.css')                              ?>">
        <link rel="stylesheet" href="<?php echo get_assist('css/hover.css') ?>">
        <link rel="stylesheet" href="<?php echo get_assist('css/animate.css') ?>">
        <link rel="stylesheet" href="<?php echo get_assist('colorbox/colorbox.css') ?>" />


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

        <script src="<?php echo get_assist('js/jquery.min.js'); ?>"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo get_assist('js/bootstrap.min.js') ?>"></script>
        <script src="<?php echo get_assist('js/bootstrap-datepicker.js') ?>"></script>
        <script src="<?php echo get_assist('js/main.js') ?>"></script>
        <script src="<?php echo get_assist('js/wow.min.js') ?>"></script>

        <script src="<?php echo get_assist('js/jquery.timeago.js') ?>" type="text/javascript"></script>
        <script src="<?php echo get_assist('datatables/jquery.dataTables.min.js') ?>"></script>
        <script src="<?php echo get_assist('datatables/dataTables.bootstrap.min.js') ?>"></script>


        <script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay.min.js"></script>
        <script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay_progress.min.js"></script>
        <script src="<?php echo get_assist('colorbox/jquery.colorbox-min.js') ?>"></script>
        <script>
            $(document).ready(function () {
                //Examples of how to assign the Colorbox event to elements
                $(".group1").colorbox({rel: 'group1'});
                $(".youtube").colorbox({iframe: true, innerWidth: 640, innerHeight: 390});
            });
        </script>



        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- Go to www.addthis.com/dashboard to customize your tools -->
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-50603f93666db950"></script>


        <link type="text/css" rel="stylesheet" media="all" href="<?php echo get_assist('css/chat/chat.css'); ?>" />
        <link type="text/css" rel="stylesheet" media="all" href="<?php echo get_assist('css/chat/screen.css'); ?>" />
        <script type="text/javascript" src="<?php echo get_assist('js/chat/chat.js'); ?>"></script>




    </head>
    <body>

        <header>
            <div class="container">
                <div class="top-head">
                    <div class="row">
                        <div class="col-md-12">
                            <img src="<?php echo get_assist('img/art.png') ?>" class="img-responsive">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">

                            <div class="footer-social" style="margin-top:3px;">
                                <div class="row no-gutter">
                                    <?php
                                    $facebookurl = Modules::run('settings/get_value', 'facebok-url');
                                    if ($facebookurl) {
                                        ?>
                                        <div class="col-md-2"><div class="social-box-top"><a href="<?php echo $facebookurl ?>" title="Facebook" target="_blank"><i class="fa fa-facebook-f fa-fw"></i></a></div></div>
                                        <?php
                                    }
                                    ?>
                                    <?php
                                    $twitterurl = Modules::run('settings/get_value', 'twitter-url');
                                    if ($twitterurl) {
                                        ?>
                                        <div class="col-md-2"><div class="social-box-top"><a href="<?php echo $twitterurl ?>" title="Twitter" target="_blank"><i class="fa fa-twitter fa-fw"></i></a></div></div>
                                        <?php
                                    }
                                    ?>
                                    <?php
                                    $googleplus = Modules::run('settings/get_value', 'google-plus-url');
                                    if ($googleplus) {
                                        ?>
                                        <div class="col-md-2"><div class="social-box-top"><a href="<?php echo $googleplus ?>" title="Google Plus" target="_blank"><i class="fa fa-google-plus fa-fw"></i></a></div></div>
                                        <?php
                                    }
                                    ?>
                                    <?php
                                    $linkedin = Modules::run('settings/get_value', 'linked-in-url');
                                    if ($linkedin) {
                                        ?>
                                        <div class="col-md-2"><div class="social-box-top"><a href="<?php echo $linkedin ?>" title="Linked In" target="_blank"><i class="fa fa-linkedin fa-fw"></i></a></div></div>
                                        <?php
                                    }
                                    ?>
                                    </ul>
                                </div>
                            </div>



                        </div>
                        <div class="col-md-9">
                            <div class="right-nav">
                                <ul>

                                    <?php
                                    if (is_user_login()) {
                                        ?>
                                        <li><a href="<?php echo site_url('user/show/' . $this->session->userdata('userID')) ?>" title="View Profile">Welcome! <?php echo $this->session->userdata('userName'); ?></a></li>
                                        <li><a href="<?php echo site_url('dashboard') ?>" title="My Dashboard"><i class="fa fa-dashboard fa-fw"></i> My Dashboard</a></li>
                                        <?php
                                        if (is_user_college() || is_user_institute()) {
                                            ?>
                                            <li><a href="<?php echo site_url('myplans') ?>" title="My Plans">My Plans</a></li>
                                            <?php
                                        }
                                        ?>
                                        <li><a href="<?php echo site_url('logout'); ?>"><i class="fa fa-sign-out fa-fw"></i> Sign Out</a></li>
                                        <li><a href="<?php echo site_url('chat') ?>" title="Chat Conversation"><i class="fa fa-comments fa-fw" aria-hidden="true"></i></a></li>
                                    <?php } else { ?>
                                        <li id="loginToggle"><a href="<?php echo site_url('login'); ?>" id="loginToggle"><i class="fa fa-sign-in fa-fw"></i> Login</a></li>
                                        <li id="registerToggle"><a href="<?php echo site_url('register'); ?>"><i class="fa fa-user-plus fa-fw"></i>Register</a></li>
                                    <?php } ?>
                                </ul>
                            </div>
                            <!--REGISTERED-->
                        </div>
                    </div>
                </div>
                <div class="head">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="logo">
                                <a href="<?php echo base_url(); ?>" title="Hamro Finder">
                                    <img src="<?php echo get_assist('img/logo.png') ?>" class="img-responsive" title="Hamro Finder">
                                </a>
                            </div>                                
                        </div>
                        <div class="col-md-6">
                        </div>
                    </div>
                </div>
                <div class="main-nav">
                    <ul>
                        <li><a href="<?php echo site_url(); ?>">Home</a></li>
                        <li><a href="<?php echo site_url('search'); ?>" title="Advanced Search">Advanced Search</a></li>
                        <li><a href="<?php echo site_url('browse'); ?>" title="Browse">Browse</a></li>
                        <li><a href="<?php echo site_url('dashboard'); ?>" title="Place you Ad">Place Ad</a></li>
                        <li><a href="<?php echo site_url('payment-plans'); ?>" title="Payment Plans">Payment Plans</a></li>
                        <li><a href="<?php echo site_url('event-plans'); ?>" title="Payment Plans">Upcoming Events</a></li>
                    </ul>


                    <ul class="pull-right">
                        <li><a href="<?php echo site_url('content/faq'); ?>" title="FAQ">FAQ</a></li>
                        <li><a href="<?php echo site_url('news'); ?>" title="News & Updates">News & Updates</a></li>
                        <li><a href="<?php echo site_url('content/help'); ?>">Help</a></li>

                    </ul>

                </div>
            </div>
        </header>