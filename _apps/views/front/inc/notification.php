<?php
if ($this->session->flashdata('msge')) {
    ?>
    <div class="alert alert-danger alert-dismissable" style="margin-top:10px;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo $this->session->flashdata('msge') ?>
    </div>
    <?php
}
?>
<?php
if ($this->session->flashdata('msgs')) {
    ?>
    <div class="alert alert-success alert-dismissable"  style="margin-top:10px;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo $this->session->flashdata('msgs') ?>
    </div>
    <?php
}
?>
<?php
if ($this->session->flashdata('msgw')) {
    ?>
    <div class="alert alert-warning alert-dismissable"  style="margin-top:10px;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo $this->session->flashdata('msgw') ?>
    </div>
    <?php
}
?>