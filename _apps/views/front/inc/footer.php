<section>
    <div class="container">
        <div class="helpline">
            Instant Help Line: <?php echo INSTANTHELPLINE; ?>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <footer>
            <div class="row">
                <div class="col-md-11">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="footer-list">
                                <h2>About Us</h2>
                                <ul>
                                    <li><a href="<?php echo site_url('content/company-establishment') ?>">Company Establishment</a></li>
                                    <li><a href="<?php echo site_url('content/message-from-ceo') ?>">Message from CEO</a></li>
                                    <li><a href="<?php echo site_url('news') ?>">News & Updates</a></li>
                                    <li><a href="<?php echo site_url('photogallery') ?>">Photogallery</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="footer-list">
                                <h2>Search & Ad</h2>
                                <ul>
                                    <li><a href="<?php echo site_url('browse') ?>">Place your Ad</a></li>
                                    <li><a href="<?php echo site_url('site') ?>">Basic Search</a></li>
                                    <li><a href="<?php echo site_url('search') ?>">Advance Search</a></li>
                                    <li><a href="<?php echo site_url('browse') ?>">Student/College Browse</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="footer-list">
                                <h2>Quick Links</h2>
                                <ul>
                                    <li><a href="<?php echo site_url('content/about-us') ?>" title="About Us">About Us</a></li>
                                    <li><a href="<?php echo site_url('content/terms-and-conditions') ?>">User Terms & Conditions</a></li>
                                    <li><a href="<?php echo site_url('content/privacy-policy') ?>">Privacy Policy</a></li>
                                    <li><a href="<?php echo site_url('content/faq') ?>">FAQ's</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="footer-list">
                                <h2>Contact Us</h2>
                                <p>
                                    <?php echo Modules::run('settings/get_value', 'website-contact-address'); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-1">

                    <div class="footer-list">
                        <h2>Find Us</h2>
                        <div class="footer-social">
                            <div class="row no-gutter">
                                <?php
                                $facebookurl = Modules::run('settings/get_value', 'facebok-url');
                                if ($facebookurl) {
                                    ?>
                                    <div class="col-md-12"><div class="social-box wow flipInX"><a href="<?php echo $facebookurl ?>" title="Facebook" target="_blank"><i class="fa fa-facebook-f fa-fw"></i></a></div></div>
                                    <?php
                                }
                                ?>
                                <?php
                                $twitterurl = Modules::run('settings/get_value', 'twitter-url');
                                if ($twitterurl) {
                                    ?>
                                    <div class="col-md-12"><div class="social-box wow flipInX"><a href="<?php echo $twitterurl ?>" title="Twitter" target="_blank"><i class="fa fa-twitter fa-fw"></i></a></div></div>
                                    <?php
                                }
                                ?>
                                <?php
                                $googleplus = Modules::run('settings/get_value', 'google-plus-url');
                                if ($googleplus) {
                                    ?>
                                    <div class="col-md-12"><div class="social-box wow flipInX"><a href="<?php echo $googleplus ?>" title="Google Plus" target="_blank"><i class="fa fa-google-plus fa-fw"></i></a></div></div>
                                    <?php
                                }
                                ?>
                                <?php
                                $linkedin = Modules::run('settings/get_value', 'linked-in-url');
                                if ($linkedin) {
                                    ?>
                                    <div class="col-md-12"><div class="social-box wow flipInX"><a href="<?php echo $linkedin ?>" title="Linked In" target="_blank"><i class="fa fa-linkedin  fa-fw"></i></a></div></div>
                                    <?php
                                }
                                ?>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </footer>
    </div>
</section>



<section>
    <div class="container">
        <div  class="footer-copy">
            <div class="row">
                <div class="col-md-4">
                    <div class="footer-logo">
                        <img src="<?php echo get_assist('img/footer-logo.png') ?>" class="img-responsive">
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="copy">
                        all rights reserved by <?php echo COMPANYNAME; ?> | &copy <?php echo date('Y') ?> | Powered by: <a href="https://coconutcreation.com" target="_blnak" title="Coconut Creation Pvt. Ltd.">Coconut Creation</a> 
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom-style">
            <div class="row">
                <div class="col-md-12">
                    <img src="<?php echo get_assist('img/art.png') ?>" class="img-responsive">
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $('.datepicker').datepicker({
        format: "dd-mm-yyyy",
        autoclose: true,
    }).on('changeDate', function (ev) {
        $(this).datepicker('hide');
    });
    $('[data-toggle="tooltip"]').tooltip();
    $('.student-menu li').click(function () {
        $('.student-menu li').removeClass('current-menu');
        $(this).addClass('current-menu');
    });
    $('.panel-heading').click(function () {
        var id = $(this).attr('id');
        $('.student-menu li').removeClass('current-menu');
        $('#' + id + 'List').addClass('current-menu');
        //$('#' + id + 'List').stop(true,false).removeAttr('style').addClass('current-menu', {duration:5000});
    });
    jQuery(document).ready(function () {
        jQuery("time.timeago").timeago();
    });</script>
<script>
    $('#dt1').DataTable({
        "iDisplayLength": 100,
        "order": [[0, "asc"]]
    });
    $('#dt2').DataTable({
        "iDisplayLength": 100,
        "order": [[1, "asc"]]
    });</script>
<script>
    var style = $('.content-area img').attr('style');
    if (style == 'float: left;') {
        $('.content-area img').css('margin-right', '20px');
        $('.content-area img').css('margin-bottom', '20px');
    }
    if (style == 'float: right;') {
        $('.content-area img').css('margin-left', '20px');
        $('.content-area img').css('margin-bottom', '20px');
    }
</script>
<script>
    new WOW().init();</script>
<?php $this->load->view('user/modal/who_intersted'); ?>
<?php $this->load->view('user/modal/make_offer'); ?>
<?php $this->load->view('user/modal/make_apply'); ?>
<?php $this->load->view('collegead/modal/college_ad'); ?>
<?php $this->load->view('studentad/modal/student_ad'); ?>
<?php $this->load->view('institutead/modal/institute_ad'); ?>
</body>
</html>