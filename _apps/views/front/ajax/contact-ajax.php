<script>
    $(document).ready(function () {
        //ADDING UPDATING CATEGORY
        $('#contctForm').submit(function (e) {
            e.preventDefault();
            var postData = $(this).serialize();
            var formUrl = $(this).attr('action');
            $('.overlay').css('display','block');
            var request = $.ajax({
                url: formUrl,
                method: "POST",
                data: postData,
                dataType: "html"
            });
            request.done(function (res) {
                var x = JSON.parse(res);
                if (x.status == 'success') {
                    $('.showMsg').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
                    $('.overlay').css('display','none');
                    $('#contctForm').each(function () {
                        this.reset();
                    });
                } else {
                    $('.overlay').css('display','none');
                    $('.showMsg').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
                }
            });
            request.fail(function (jqXHR, textStatus) {
                alert("Request failed: " + textStatus);
            });
        });
    });
</script>
