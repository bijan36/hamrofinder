<?php $this->load->view('front/inc/header'); ?>
<div class="adv-search">
    <section>
        <div class="container">
            <div class="main white-bg standard-padding margin-bottom-5">
                <div class="row">
                     <div class="col-md-4">
                        <?php
                        $sidebar = $content->sidebar;
                        echo Modules::run('sidebar/get_vertical_sidebar', $sidebar); 
                        ?>
                    </div>
                    <div class="col-md-8 content-area">
                        <?php echo $contents; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php $this->load->view('front/inc/footer'); ?>