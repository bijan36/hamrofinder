<?php $this->load->view('front/inc/header'); ?>
<div class="adv-search">
    <section>
        <div class="container">
            <div class="main white-bg standard-padding margin-bottom-5">
                <div class="row">
                    <div class="col-md-12 content-area">
                        <?php echo $contents; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php $this->load->view('front/inc/footer'); ?>