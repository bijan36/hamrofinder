<?php $this->load->view('front/inc/header'); ?>
<div class="adv-search">
    <section>
        <div class="container">
            <div class="main white-bg standard-padding margin-bottom-5">
                <div class="row">
                    <div class="col-md-8 content-area">
                        <?php echo $contents; ?>
                    </div>
                    <div class="col-md-4">
                        <?php
                        $newss = Modules::run('news/get_latest', 20);
                        if ($newss) {
                            ?>
                            <div class="ad-box-round-corner standard-padding-little-height round-border normal-list">
                                <h4>Latest News & Updates</h4>
                                <ul>
                                    <?php
                                    foreach ($newss as $singlenews):
                                        if ($singlenews->slug != $row->slug) {
                                            ?>
                                            <li><a href="<?php echo site_url('news/' . $singlenews->slug); ?>" title="<?php echo $singlenews->title; ?>"><?php echo $singlenews->title; ?></a></li>
                                            <?php
                                        }
                                    endforeach;
                                    ?>
                                </ul>
                            </div>
                            <?php
                        }
                        ?>

                        <?php
                        echo Modules::run('sidebar/get_vertical_sidebar', '30');
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php $this->load->view('front/inc/footer'); ?>