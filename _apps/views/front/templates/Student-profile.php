<?php $this->load->view('front/inc/header');?>
<section>
    <div class="container">
        <div class="main white-bg standard-padding margin-bottom-5">
            <div class="row">
                <div class="col-md-8">
                    <?php echo $contents; ?>
                </div>
                <div class="col-md-4" style="padding-top: 51px;">
                    <?php echo $this->load->view('front/sidebar/student-profile-vertical-sidebar'); ?>
                </div>
                <div class="col-md-12" style="margin-top: 25px;">
                    <?php echo Modules::run('sidebar/get_horizontal_sidebar', 22); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view('front/inc/footer');?>