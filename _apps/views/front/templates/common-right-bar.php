<?php $this->load->view('front/inc/header'); ?>
<div class="adv-search">
    <section>
        <div class="container">
            <div class="main white-bg standard-padding margin-bottom-5">
                <div class="row">
                    <div class="col-md-8">
                        <?php echo $contents; ?>
                    </div>
                    <div class="col-md-4">
                        <?php echo $this->load->view('front/sidebar/common-sidebar'); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php $this->load->view('front/inc/footer'); ?>