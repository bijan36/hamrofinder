<?php $this->load->view('front/inc/header'); ?>
<section>
    <div class="container">
        <div class="student-menu white-bg standard-padding-little-height">
            <div class="row">
                <div class="col-md-3">
                    <h1>Payment Plans <i class="fa fa-angle-double-right fa-fw"></i></h1>
                </div>
                <div class="col-md-9">
                    <ul>
                        <li id="headingOneList"><a role="button"  href="<?php echo site_url('payment-plans'); ?>" >Membership Plans</a></li>
                        <li id="headingFourList"><a role="button" href="<?php echo site_url('ad-plans'); ?>" >Purchased Ads</a></li>
                        <li id="headingFourList" class="current-menu"><a role="button"  href="<?php echo site_url('event-plans'); ?>" >Events & Activities</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="main white-bg standard-padding margin-bottom-5">
            <div class="row">
                <div class="col-md-12">
                    <?php echo $contents; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view('front/inc/footer'); ?>