<?php
$this->load->view('front/inc/header');
?>
<section>
    <div class="container">
        <div class="main white-bg standard-padding margin-bottom-5">
            <?php echo $contents; ?>
        </div>
    </div>
</section>
<?php
$this->load->view('front/inc/footer');
?>