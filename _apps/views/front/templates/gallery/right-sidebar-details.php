<?php $this->load->view('front/inc/header'); ?>
<div class="adv-search">
    <section>
        <div class="container">
            <div class="main white-bg standard-padding margin-bottom-5">
                <div class="row">
                    <div class="col-md-8 content-area">
                        <?php echo $contents; ?>
                    </div>
                    <div class="col-md-4">
                        <?php
                        $allgallery = Modules::run('settings/get_value', 'gallery-category');
                        if ($allgallery) {
                            $galleries = explode(',', $allgallery);
                            $params = urldecode($this->uri->segment(2));
                            ?>
                            <div class="ad-box-round-corner standard-padding-little-height round-border normal-list">
                                <h4>Other Galleries</h4>
                                <ul>
                                    <?php
                                    foreach ($galleries as $gal):
                                        
                                        if ($gal != $params) {
                                            ?>
                                            <li><a href="<?php echo site_url('photogallery/' . $gal); ?>" title="<?php echo $gal; ?>"><?php echo $gal; ?></a></li>
                                            <?php
                                        }
                                    endforeach;
                                    ?>
                                </ul>
                            </div>
                            <?php
                        }
                        ?>

                        <?php
                        echo Modules::run('sidebar/get_vertical_sidebar', '24');
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php $this->load->view('front/inc/footer'); ?>