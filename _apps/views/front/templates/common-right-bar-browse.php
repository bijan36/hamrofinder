<?php $this->load->view('front/inc/header'); ?>
<section>
    <div class="container">
        <div class="student-menu white-bg standard-padding-little-height">
            <div class="row">
                <div class="col-md-2">
                    <h1>Browse <i class="fa fa-angle-double-right fa-fw"></i></h1>
                </div>
                <div class="col-md-10">
                    <ul>
                        <li id="headingOneListWork" class="current-menu active"><a role="button" data-toggle="tab" href="#studentBrowse">Student</a></li>
                        <li id="headingTwoListWork"><a role="button" data-toggle="tab" href="#collegeBrowse">College</a></li>
                        <li id="headingThreeListWork"><a role="button" data-toggle="tab" href="#instituteBrowse">Institute</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="main white-bg standard-padding  margin-bottom-5">
            <div class="row">
                <div class="col-md-12">
                    <?php echo $contents; ?>
                </div>

<!--
                <div class="col-md-4">
                    <?php //echo $this->load->view('front/sidebar/browse-sidebar'); ?>
                </div>
    -->            
                
            </div>
        </div>
    </div>
</section>
<?php $this->load->view('front/inc/footer'); ?>