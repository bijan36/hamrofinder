<?php $this->load->view('front/inc/header'); ?>
<section>
    <div class="container">
        <div class="student-menu white-bg standard-padding-little-height">
            <div class="row">
                <div class="col-md-3 searchHead">
                    <h1>Advanced Search <i class="fa fa-angle-double-right fa-fw"></i></h1>
                </div>
                <div class="col-md-9 advSwitcher">
                    <ul>
                        <li id="headingOneListWork" class="current-menu active"><a role="button" data-toggle="tab" href="#advanceStudent">Student</a></li>
                        <li id="headingTwoListWork"><a role="button" data-toggle="tab" href="#advanceCollege">College</a></li>
                        <li id="headingThreeListWork"><a role="button" data-toggle="tab" href="#advanceInstitute">Institute</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="main white-bg standard-padding margin-bottom-5">
            <?php echo $contents; ?>                
        </div>
    </div>
</section>
<?php $this->load->view('front/inc/footer'); ?>