<?php
$this->load->view('front/inc/header');
?>

<section>
    <div class="container">
        <div class="main white-bg standard-padding margin-bottom-5">
            <div class="row">
                <div class="col-md-12">
                    <?php echo $contents; ?>
                </div>
            </div>
        </div>
    </div>
</section>


<?php
$this->load->view('front/inc/footer');
?>