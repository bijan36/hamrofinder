<?php $this->load->view('front/inc/header');?>
<section>
    <div class="container">
        <div class="student-menu white-bg standard-padding-little-height">
            <div class="row">
                <div class="col-md-2">
                   <h1>Dashboard <i class="fa fa-angle-double-right fa-fw"></i></h1>
                </div>
                <div class="col-md-10">
                    <ul>
                        <li id="headingOneList"><a role="button" data-toggle="collapse" href="#myAd" aria-expanded="false" aria-controls="collapseOne" data-parent="#accordion">My Ad</a></li>
                        <li id="headingTwoList"><a role="button" data-toggle="collapse" href="#savedAd" aria-expanded="false" aria-controls="collapseOne" data-parent="#accordion">Saved Ad</a></li>
                        <li id="headingThreeList"><a role="button" data-toggle="collapse" href="#placeAd" aria-expanded="false" aria-controls="collapseOne" data-parent="#accordion">Place Your Ad</a></li>
                        <li id="headingFourList"><a role="button" data-toggle="collapse" href="#profileInfo" aria-expanded="false" aria-controls="collapseOne" data-parent="#accordion">Add/Edit Profile Info</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="main white-bg standard-padding margin-bottom-5">
            <div class="row">
                <div class="col-md-8">
                    <?php echo $contents; ?>
                </div>
                <div class="col-md-4">
                    <?php echo $this->load->view('front/sidebar/college-profile-vertical-sidebar'); ?>
                    <?php //echo $this->load->view('front/sidebar/college-sidebar'); ?>
                </div>
                <div class="col-md-12" style="margin-top: 25px;">
                    <?php echo Modules::run('sidebar/get_horizontal_sidebar', 20); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view('front/inc/footer');?>