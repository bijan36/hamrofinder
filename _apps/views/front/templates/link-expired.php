
        <div class="row">
            <div class="col-md-12">
                <h1 class="border-heading" style="text-align: center; font-size: 25px;"><i class="fa fa-exclamation-triangle fa-fw text-danger"></i> We can't Find the page you're looking for</h1>
                <div class="error-details" style="min-height: 500px; text-align: center;">
                    <img src="<?php echo get_assist('img/broken-links.png') ?>">
                    <p style="text-align: center; font-size: 20px; font-weight: 300;">
                        The page you are looking for is removed or expired. Please try again later.
                    </p>
                    <a class="blue-btn btn" href="<?php echo site_url(); ?>"><i class="fa fa-caret-left"></i> Go Back To Home</a>
                </div> 
            </div>
        </div>
    