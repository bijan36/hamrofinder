<?php
$this->load->view('front/inc/header');
?>
<div class="container error-info">
    <div class="main white-bg standard-padding margin-bottom-5">


        <div class="row">
            <div class="col-md-12">
                <h1 class="border-heading" style="text-align: center; font-size: 25px;"><i class="fa fa-exclamation-triangle fa-fw text-danger"></i> Invalid Operation!</h1>
                <div class="error-details" style="min-height: 500px; text-align: center;">
                    <img src="<?php echo get_assist('img/security.png') ?>">
                    <p style="padding-top:40px">
                        <a class="blue-btn btn" href="<?php echo site_url(); ?>"><i class="fa fa-caret-left"></i> Go Back To Home</a>
                    </p>
                </div> 
            </div>
        </div>

    </div>
</div>
<?php
$this->load->view('front/inc/footer');
?>