<?php
//ISSUED AD

$self = false;
$self = ($this->session->userdata('userID') == $userRow->ID) ? true : false;

$myadrows = Modules::run('studentad/get_rows_by_parent', $userRow->ID);
if ($myadrows) {
    ?>
    <div class="ad-box-round-corner standard-padding-little-height round-border normal-list">
        <h4><?php echo $self ? 'Your Published Ads' : 'Published Ads'; ?></h4>

        <?php if (count($myadrows) > 10) { ?>
            <div class="scrolDiv">
            <?php } ?>

            <ul>
                <?php
                foreach ($myadrows as $row):
                    if ($row->status == 'Active') {
                        if (today_date() < $row->ad_end_date) {
                            ?>
                            <li>
                                <span class="which-ads" data-id="<?php echo $row->ID ?>-Student" data-type="Student" data-url="<?php echo base_url(); ?>">
                                    Looking for: <strong><?php echo $row->looking_for; ?> </strong>
                                </span>
                                <?php
                                $totalInterest = total_interest($row->ID, 'Student');
                                if ($totalInterest > 0) {
                                    ?>
                                    <small>
                                        <div class="red-batch who-intrested"  data-toggle="tooltip" data-placement="top" title="Total interest <?php echo $totalInterest; ?>" data-id="<?php echo $row->ID . '-Student'; ?>" data-url="<?php echo base_url(); ?>"><?php echo $totalInterest; ?></div>
                                    </small>
                                    <?php
                                }
                                ?>
                            </li>
                            <?php
                        }
                    }
                endforeach;
                ?>
            </ul>
            <?php if (count($myadrows) > 10) { ?>
            </div>
        <?php } ?>
    </div>
    <?php
}
?>

<?php
//INTERSTED COLLEGE
$interst_rows = Modules::run('interest/get_rows_by_user', $userRow->ID);
if ($interst_rows) {
    ?>
    <div class="ad-box-round-corner standard-padding-little-height round-border normal-list">
        <h4><?php echo $self ? 'Interest You Have Shown' : 'Interested On'; ?></h4>
        <?php if (count($interst_rows) > 10) { ?>
            <div class="scrolDiv">
            <?php } ?>
            <ul>
                <?php
                foreach ($interst_rows as $row):
                    $adRow = Modules::run('collegead/get_row', $row->ad_ID);
                    if ($adRow) {
                        if ($adRow->status == ACTIVE) {
                            ?>
                            <li>
                                <span class="which-ads" data-id="<?php echo $adRow->ID ?>-College" data-type="College" data-url="<?php echo base_url(); ?>">
                                    <strong><?php echo $adRow->looking_for; ?> </strong>
                                    <em>
                                        <small>
                                            by <?php echo Modules::run('user/get_name', $adRow->parent_ID); ?>
                                        </small>
                                    </em>
                                </span>



                                <?php
                                $totalInterest = total_interest($adRow->ID, 'College');
                                if ($totalInterest > 0) {
                                    ?>
                                    <small>
                                        <div class="red-batch who-intrested"  data-toggle="tooltip" data-placement="top" title="Total interest <?php echo $totalInterest; ?>" data-id="<?php echo $adRow->ID . '-College'; ?>" data-url="<?php echo base_url(); ?>"><?php echo $totalInterest; ?></div>
                                    </small>
                                    <?php
                                }
                                ?>

                            </li>
                            <?php
                        }
                    }
                endforeach;
                ?>
            </ul>
            <?php if (count($interst_rows) > 10) { ?>
            </div>
        <?php } ?>
    </div>

    <?php
}
?>




<?php
//COLLEGE YOU HAVE APPLIED
$apply_rows = Modules::run('studentapply/get_rows_by_student_ID', $userRow->ID);
if ($apply_rows) {
    ?>
    <div class="ad-box-round-corner standard-padding-little-height round-border normal-list">
        <h4><?php echo $self ? 'College You Have Applied' : 'College Applied'; ?></h4>
        <?php if (count($apply_rows) > 10) { ?>
            <div class="scrolDiv">
            <?php } ?>
            <ul>
                <?php
                foreach ($apply_rows as $row):
                    $adRow = Modules::run('collegead/get_row', $row->ad_ID);
                    if ($adRow) {
                        if ($adRow->status == ACTIVE) {
                            ?>
                            <li>
                                <span class="which-ads" data-id="<?php echo $adRow->ID ?>-College" data-type="College" data-url="<?php echo base_url(); ?>">
                                    <strong><?php echo $adRow->looking_for; ?> </strong>
                                    <em>
                                        <small>
                                            by <?php echo Modules::run('user/get_name', $adRow->parent_ID); ?>
                                        </small>
                                    </em>
                                </span>
                            </li>
                            <?php
                        }
                    }
                endforeach;
                ?>
            </ul>
            <?php if (count($apply_rows) > 10) { ?>
            </div>
        <?php } ?>
    </div>

    <?php
}
?>



<?php
//OFFER MADE TO YOU AD
$offer_rows = Modules::run('offer/get_offers_rows_by_student', $userRow->ID);
//echo '<pre>';
//print_r($offer_rows);
//echo '</pre>';
if ($offer_rows) {
    ?>
    <div class="ad-box-round-corner standard-padding-little-height round-border normal-list">
        <h4><?php echo $self ? 'Offer Made To Your Ad' : 'Offer Made To Ad'; ?></h4>
        <?php if (count($offer_rows) > 10) { ?>
            <div class="scrolDiv">
            <?php } ?>
            <ul>
                <?php
                foreach ($offer_rows as $row):
                    $adRow = Modules::run('studentad/get_row', $row->ad_ID);
                    if ($adRow) {
                        if ($adRow->status == ACTIVE) {
                            ?>
                            <li>
                                <span class="which-ads" data-id="<?php echo $adRow->ID ?>-Student" data-type="Student" data-url="<?php echo base_url(); ?>">
                                    <strong><?php echo $adRow->looking_for; ?> </strong>
                                    <em>
                                        <small>
                                            by <?php echo Modules::run('user/get_name', $row->college_ID); ?>
                                        </small>
                                    </em>
                                </span>
                            </li>
                            <?php
                        }
                    }
                endforeach;
                ?>
            </ul>
            <?php if (count($offer_rows) > 10) { ?>
            </div>
        <?php } ?>
    </div>

    <?php
}
?>

<?php echo Modules::run('sidebar/get_vertical_sidebar', 21); ?>


