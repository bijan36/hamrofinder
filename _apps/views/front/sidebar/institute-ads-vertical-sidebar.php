<?php
$meta = Modules::run('institute/get_row_by_parent', $userRow->ID);
?>
<div class="grey-bg-half-border standard-padding round-border">
    <div class="row">
        <div class="col-md-12">
            <div class="profile-pic">
                <?php
                $proImgID = $userRow->profile_picture ? $userRow->profile_picture : '0';
                $profile_image = Modules::run('imagehub/get_circle_image', $proImgID, 'thumb150x150');
                echo $profile_image;
                ?>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <h5 class="text-center"><strong><?php echo $userRow->name; ?></strong></h5>
                    <span class="bold-uppercase-highlight center-block" style="margin-top: 10px;"><?php echo $userRow->type; ?></span>
                </div>
                <div class="col-md-12">
                    <table class="table">

                        <?php if (!empty($userRow->contact)) { ?>
                            <tr>
                                <td width="80px">Mobile No.:</td>
                                <td><?php echo $userRow->contact; ?></td>
                            </tr>
                        <?php } ?>

                        <?php if (!empty($meta->phone_no)) { ?>
                            <tr>
                                <td>Phone No.:</td>
                                <td><?php echo $meta->phone_no; ?></td>
                            </tr>
                        <?php } ?>
                        <?php if (!empty($userRow->email)) { ?>
                            <tr>
                                <td>Email:</td>
                                <td><?php echo $userRow->email; ?></td>
                            </tr>
                        <?php } ?>


                        <?php if (!empty($userRow->address)) { ?>
                            <tr>
                                <td>Address:</td>
                                <td><?php echo Modules::run('user/get_full_address', $userRow->ID); ?></td>
                            </tr>
                        <?php } ?>

                        <?php if (!empty($meta->website)) { ?>
                            <tr>
                                <td>Website:</td>
                                <td><?php echo $meta->website; ?></td>
                            </tr>
                        <?php } ?>

                        <?php if (!empty($meta->year_established)) { ?>
                            <tr>
                                <td>ESTD:</td>
                                <td><?php echo $meta->year_established; ?></td>
                            </tr>
                        <?php } ?>
                        <?php if (!empty($meta->area_occupied)) { ?>
                            <tr>
                                <td>Area:</td>
                                <td><?php echo $meta->area_occupied; ?></td>
                            </tr>
                        <?php } ?>
                        <?php if (!empty($meta->average_student_per_class)) { ?>
                            <tr>
                                <td>Average Student/Class:</td>
                                <td><?php echo $meta->average_student_per_class; ?></td>
                            </tr>
                        <?php } ?>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <?php
    if (!empty($meta->facilities_in_college)) {
        ?>
        <div class="col-md-12">
            <div class="grey-bg standard-padding round-border" style="margin-top: 15px;">
                <div class="row">
                    <div class="col-md-12">
                        <h4>Facilities in institute:</h4>
                    </div>
                    <div class="col-md-12">
                        <table class="table">
                            <?php
                            $params = explode(',', $meta->facilities_in_college);
                            foreach ($params as $prm):
                                ?>
                                <tr>
                                    <td><i class="fa fa-check-square-o fa-fw"></i> <?php echo $prm; ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
    <?php
    if (!empty($meta->secuirty_issues)) {
        ?>
        <div class="col-md-12">
            <div class="grey-bg standard-padding round-border" style="margin-top: 15px;">
                <div class="row">
                    <div class="col-md-12">
                        <h4>Security Issue:</h4>
                    </div>
                    <div class="col-md-12">
                        <table class="table">
                            <?php
                            $params = explode(',', $meta->secuirty_issues);
                            foreach ($params as $prm):
                                ?>
                                <tr>
                                    <td><i class="fa fa-check-square-o fa-fw"></i> <?php echo $prm; ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
    <div class="clearfix"></div>
    <?php
    if (!empty($meta->additional_features)) {
        ?>
        <div class="col-md-12">
            <div class="grey-bg standard-padding round-border" style="margin-top: 15px;">
                <div class="row">
                    <div class="col-md-12">
                        <h4>Additional Features:</h4>
                    </div>
                    <div class="col-md-12">
                        <table class="table">
                            <?php
                            $params = explode(',', $meta->additional_features);
                            foreach ($params as $prm):
                                ?>
                                <tr>
                                    <td><i class="fa fa-check-square-o fa-fw"></i> <?php echo $prm; ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
</div>
<?php
$allVid = Modules::run('videos/get_videos', $userRow->ID);
if ($allVid) {
    ?>
    <div class="ad-box-round-corner standard-padding-little-height round-border normal-list">
        <h4>Feature Videos</h4>
        <div class="row">
            <?php
            foreach ($allVid as $vid):
                $this->load->view('videos/video_thumb_sidebar', array('video_code' => $vid->video_code));
            endforeach;
            ?>
        </div>
    </div>
    <?php
}
?>
<?php echo Modules::run('sidebar/get_vertical_sidebar', 15); ?>


