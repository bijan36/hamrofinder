<?php
$meta = Modules::run('student/get_row_by_parent', $userRow->ID);
?>
<div class="grey-bg-half-border standard-padding round-border">
    <div class="row">
        <div class="col-md-12">
            <div class="profile-pic">
                <?php
                $proImgID = $userRow->profile_picture ? $userRow->profile_picture : '0';
                $profile_image = Modules::run('imagehub/get_circle_image', $proImgID, 'thumb150x150');
                echo $profile_image;
                ?>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row">

                <div class="col-md-12">
                    <h5 class="text-center"><strong><?php echo $userRow->name; ?></strong></h5>
                    <span class="bold-uppercase-highlight center-block" style="margin-top: 5px; margin-bottom: 5px;"><?php echo $userRow->type; ?></span>


                </div>

                <div class="col-md-12">
                    <table class="table">
                        <?php if (!empty($userRow->contact)) { ?>
                            <?php if (!empty($meta->gender)) { ?>
                                <tr>
                                    <td style="width: 90px">Gender:</td>
                                    <td><?php echo $meta->gender; ?></td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td>Mobile No.:</td>
                                <td><?php echo $userRow->contact; ?></td>
                            </tr>
                        <?php } ?>

                        <?php if (!empty($meta->phone_no)) { ?>
                            <tr>
                                <td>Phone No.:</td>
                                <td><?php echo $meta->phone_no; ?></td>
                            </tr>
                        <?php } ?>
                        <?php if (!empty($userRow->email)) { ?>
                            <tr>
                                <td>Email:</td>
                                <td><?php echo $userRow->email; ?></td>
                            </tr>
                        <?php } ?>
                        <?php if (!empty($userRow->address)) { ?>
                            <tr>
                                <td>Address:</td>
                                <td><?php echo Modules::run('user/get_full_address', $userRow->ID); ?></td>
                            </tr>
                        <?php } ?>


                        <?php if (!empty($meta->intersted_sports)) { ?>
                            <tr>
                                <td>Sports:</td>
                                <td><?php echo $meta->intersted_sports; ?></td>
                            </tr>
                        <?php } ?>
                        <?php if (!empty($meta->intersted_art)) { ?>
                            <tr>
                                <td>Arts:</td>
                                <td><?php echo $meta->intersted_art; ?></td>
                            </tr>
                        <?php } ?>
                        <?php if (!empty($meta->interst)) { ?>
                            <tr>
                                <td>Interest:</td>
                                <td><?php echo $meta->interst; ?></td>
                            </tr>
                        <?php } ?>

                        <tr>
                            <td colspan="2">
                                <?php if (!empty($userRow->gallery_picture)) { ?>

                                    <div class="row no-gutter" style="margin-top: 8px;">
                                        <?php
                                        $pics = explode(',', $userRow->gallery_picture);
                                        foreach ($pics as $doc) {
                                            $galData['imgID'] = $doc;
                                            $this->load->view('imagehub/gal_doc_show', $galData);
                                        }
                                        ?>
                                    </div>
                                <?php } ?>
                            </td>
                        </tr>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>






<!--SOME AFFILIATIONS-->
<?php
if (!empty($meta->slc_affiliation)) {
    ?>
    <div class="grey-bg standard-padding round-border" style="margin-top: 15px;">
        <div class="row">

            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <h4>SLC Affiliation:</h4>
                    </div>
                    <div class="col-md-12">
                        <table class="table">
                            <?php if ($meta->slc_affiliation) { ?>
                                <tr>
                                    <td>SLC:</td>
                                    <td><strong><?php echo $meta->slc_affiliation; ?></strong></td>
                                </tr>
                            <?php } ?>
                            <?php if ($meta->slc_score) { ?>
                                <tr>
                                    <td>SLC Score:</td>
                                    <td><strong><?php echo $meta->slc_score; ?>%</strong></td>
                                </tr>
                            <?php } ?>
                            <?php if ($meta->slc_school) { ?>
                                <tr>
                                    <td>School Name:</td>
                                    <td><strong><?php echo $meta->slc_school; ?></strong></td>
                                </tr>
                            <?php } ?>
                            <?php if ($meta->slc_school_location) { ?>
                                <tr>
                                    <td>School Address:</td>
                                    <td><strong><?php echo $meta->slc_school_location; ?></strong></td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>
            </div>


            <?php
            //FINDING THE RELATIVE MARKS
            $marksRow = Modules::run('student_marks/get_by_parent_level', $userRow->ID, 'SLC');
            if ($marksRow) {
                ?>
                <div class="col-md-12">
                    <h4>SLC Scores:</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <small>
                                <table class="table">

                                    <?php foreach ($marksRow as $mk): ?>
                                        <tr>
                                            <td><?php echo $mk->subject; ?></td>
                                            <td><?php echo $mk->score; ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </table>
                            </small>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>


        </div>
    </div>
    <?php
}
?>


<?php
if (!empty($meta->plus2_affiliation)) {
    ?>
    <div class="grey-bg standard-padding round-border" style="margin-top: 15px;">
        <div class="row">

            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <h4>+2 Affiliation:</h4>
                    </div>
                    <div class="col-md-12">
                        <table class="table">
                            <?php if ($meta->plus2_affiliation) { ?>
                                <tr>
                                    <td>+2:</td>
                                    <td><strong><?php echo $meta->plus2_affiliation; ?></strong></td>
                                </tr>
                            <?php } ?>
                            <?php if ($meta->plus2_score) { ?>
                                <tr>
                                    <td>+2 Score:</td>
                                    <td><strong><?php echo $meta->plus2_score; ?>%</strong></td>
                                </tr>
                            <?php } ?>
                            <?php if ($meta->plus2_school) { ?>
                                <tr>
                                    <td>+2 School Name:</td>
                                    <td><strong><?php echo $meta->plus2_school; ?></strong></td>
                                </tr>
                            <?php } ?>
                            <?php if ($meta->plus2_school_location) { ?>
                                <tr>
                                    <td>+2 School Address:</td>
                                    <td><strong><?php echo $meta->plus2_school_location; ?></strong></td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>
            </div>


            <?php
            //FINDING THE RELATIVE MARKS
            $marksRow = Modules::run('student_marks/get_by_parent_level', $userRow->ID, 'Plus Two');
            if ($marksRow) {
                ?>
                <div class="col-md-12">
                    <h4>+2 Scores:</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <small>
                                <table class="table">

                                    <?php foreach ($marksRow as $mk): ?>
                                        <tr>
                                            <td><?php echo $mk->subject; ?></td>
                                            <td><?php echo $mk->score; ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </table>
                            </small>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>


        </div>
    </div>
    <?php
}
?>

<?php
if (!empty($meta->bachelor_affiliation)) {
    ?>
    <div class="grey-bg standard-padding round-border" style="margin-top: 15px;">
        <div class="row">

            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <h4>Bachelor Affiliation:</h4>
                    </div>
                    <div class="col-md-12">
                        <table class="table">
                            <?php if ($meta->bachelor_affiliation) { ?>
                                <tr>
                                    <td>Bachelor:</td>
                                    <td><strong><?php echo $meta->bachelor_affiliation; ?></strong></td>
                                </tr>
                            <?php } ?>
                            <?php if ($meta->bachelor_score) { ?>
                                <tr>
                                    <td>College Score:</td>
                                    <td><strong><?php echo $meta->bachelor_score; ?>%</strong></td>
                                </tr>
                            <?php } ?>
                            <?php if ($meta->bachelor_school) { ?>
                                <tr>
                                    <td>College Name:</td>
                                    <td><strong><?php echo $meta->bachelor_school; ?></strong></td>
                                </tr>
                            <?php } ?>
                            <?php if ($meta->bachelor_school_location) { ?>
                                <tr>
                                    <td>College Address:</td>
                                    <td><strong><?php echo $meta->bachelor_school_location; ?></strong></td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>
            </div>


            <?php
            //FINDING THE RELATIVE MARKS
            $marksRow = Modules::run('student_marks/get_by_parent_level', $userRow->ID, 'Bachelor');
            if ($marksRow) {
                ?>
                <div class="col-md-12">
                    <h4>Bachelor Scores:</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <small>
                                <table class="table">

                                    <?php foreach ($marksRow as $mk): ?>
                                        <tr>
                                            <td><?php echo $mk->subject; ?></td>
                                            <td><?php echo $mk->score; ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </table>
                            </small>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>


        </div>
    </div>
    <?php
}
?>

<?php
if (isset($meta->plus_2_college_entrance) && $meta->plus_2_college_entrance == 'Appeared') {
    ?>
    <div class="grey-bg standard-padding round-border" style="margin-top: 15px;">
        <div class="row">

            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <h4>+2 Entrance Appeared: Yes</h4>
                    </div>

                </div>
            </div>


            <div class="col-md-12">
                <h4>Entrance Scores:</h4>
                <div class="row">
                    <div class="col-md-12">
                        <small>
                            <table class="table">
                                <?php if ($meta->plus_2_college_entrance_marks) { ?>
                                    <tr>
                                        <td>+2 Entrance:</td>
                                        <td><strong><?php echo $meta->plus_2_college_entrance_marks; ?></strong></td>
                                    </tr>
                                <?php } ?>
                                <?php if ($meta->CMAT) { ?>
                                    <tr>
                                        <td>CMAT:</td>
                                        <td><strong><?php echo $meta->CMAT; ?>%</strong></td>
                                    </tr>
                                <?php } ?>
                                <?php if ($meta->engineering) { ?>
                                    <tr>
                                        <td>Engineering:</td>
                                        <td><strong><?php echo $meta->engineering; ?></strong></td>
                                    </tr>
                                <?php } ?>
                                <?php if ($meta->KUUMAT) { ?>
                                    <tr>
                                        <td>KUUMAT:</td>
                                        <td><strong><?php echo $meta->KUUMAT; ?></strong></td>
                                    </tr>
                                <?php } ?>
                                <?php if ($meta->medicine) { ?>
                                    <tr>
                                        <td>Medicine:</td>
                                        <td><strong><?php echo $meta->medicine; ?></strong></td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </small>
                    </div>
                </div>
            </div>



        </div>
    </div>
    <?php
}
?>

<?php echo Modules::run('sidebar/get_vertical_sidebar', 17); ?>








