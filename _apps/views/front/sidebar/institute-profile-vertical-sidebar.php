<?php

//DECLARE SELF VIEW OR NOT
$self = false;
$self = ($this->session->userdata('userID') == $userRow->ID) ? true : false;

$myadrows = Modules::run('institutead/get_rows_by_parent', $userRow->ID);
if ($myadrows) {
    ?>
    <div class="ad-box-round-corner standard-padding-little-height round-border normal-list">
        <h4><?php echo $self ? 'Your Published Ads' : 'Published Ads'; ?></h4>
        <?php if (count($myadrows) > 10) { ?>
            <div class="scrolDiv">
            <?php } ?>
            <ul>
                <?php
                foreach ($myadrows as $row):
                    if (can_show_ad($row->ID, $row->parent_ID)) {
                        ?>
                        <li>
                            <span class="which-ads" data-id="<?php echo $row->ID ?>-Institute" data-type="Institute" data-url="<?php echo base_url(); ?>">
                                Looking for: <strong><?php echo $row->looking_for; ?></strong>
                            </span>

                            <?php
                            $totalInterest = total_interest($row->ID, 'Institute');
                            if ($totalInterest > 0) {
                                ?>
                                <small>
                                    <div class="red-batch who-intrested"  data-toggle="tooltip" data-placement="top" title="Total interest <?php echo $totalInterest; ?>" data-id="<?php echo $row->ID . '-Institute'; ?>" data-url="<?php echo base_url(); ?>"><?php echo $totalInterest; ?></div>
                                </small>
                                <?php
                            }
                            ?>
                        </li>
                        <?php
                    }
                endforeach;
                ?>
            </ul>
            <?php if (count($myadrows) > 10) { ?>
            </div>
        <?php } ?>
    </div>
    <?php
}
?>



<?php
//INTERSTED COLLEGE
$interst_rows = Modules::run('interest/get_rows_by_user', $userRow->ID);
if ($interst_rows) {
    ?>
    <div class="ad-box-round-corner standard-padding-little-height round-border normal-list">
        <h4><?php echo $self ? 'Interest You Have Shown' : 'Interested On'; ?></h4>
        <?php if (count($interst_rows) > 10) { ?>
            <div class="scrolDiv">
            <?php } ?>
            <ul>
                <?php
                foreach ($interst_rows as $row):
                    $adRow = Modules::run('studentad/get_row', $row->ad_ID);
                    if ($adRow) {
                        if (can_show_ad($adRow->ID, $adRow->parent_ID)) {
                            ?>
                            <li>
                                <span class="which-ads" data-id="<?php echo $adRow->ID ?>-Student" data-type="Student" data-url="<?php echo base_url(); ?>">
                                    <strong><?php echo $adRow->looking_for; ?> </strong>
                                    <em>
                                        <small>
                                            by <?php echo Modules::run('user/get_name', $adRow->parent_ID); ?> 
                                        </small>
                                    </em>
                                </span>
                                <?php
                                $totalInterest = total_interest($adRow->ID, 'Student');
                                if ($totalInterest > 0) {
                                    ?>
                                    <small>
                                        <div class="red-batch who-intrested"  data-toggle="tooltip" data-placement="top" title="Total interest <?php echo $totalInterest; ?>" data-id="<?php echo $adRow->ID . '-Student'; ?>" data-url="<?php echo base_url(); ?>"> <?php echo $totalInterest; ?></div>
                                    </small>
                                    <?php
                                }
                                ?>
                            </li>
                            <?php
                        }
                    }
                endforeach;
                ?>
            </ul>
            <?php if (count($interst_rows) > 10) { ?>
            </div>
        <?php } ?>
    </div>

    <?php
}
?>




<?php
//STUDENT YOU HAVE OFFERED
$offer_rows = Modules::run('offer/get_offers_rows_by_institute', $userRow->ID);
if ($offer_rows) {
    ?>
    <div class="ad-box-round-corner standard-padding-little-height round-border normal-list">
        <h4><?php echo $self ? 'Student You Have Offered' : 'Student Offered'; ?></h4>
        <?php if (count($offer_rows) > 10) { ?>
            <div class="scrolDiv">
            <?php } ?>
            <ul>
                <?php
                foreach ($offer_rows as $row):
                    $adRow = Modules::run('studentad/get_row', $row->ad_ID);
                    if ($adRow) {
                        if (can_show_ad($adRow->ID, $adRow->parent_ID)) {
                            ?>
                            <li>
                                <a href="<?php echo profile_url($row->student_ID) ?>" target="_blank">
                                    <?php echo Modules::run('user/get_name', $row->student_ID); ?>
                                </a>
                            </li>
                            <?php
                        }
                    }
                endforeach;
                ?>
            </ul>
            <?php if (count($offer_rows) > 10) { ?>
            </div>
        <?php } ?>
    </div>

    <?php
}
?>




<?php
//APPLICATION MADE TO YOUR AD
$apply_rows = Modules::run('studentapply/get_apply_list_by_colID', $userRow->ID);
if ($apply_rows) {
    ?>
    <div class="ad-box-round-corner standard-padding-little-height round-border normal-list">
        <h4><?php echo $self ? 'Application Made to Your ad' : 'Application Made To Ad'; ?></h4>
        <?php if (count($apply_rows) > 10) { ?>
            <div class="scrolDiv">
            <?php } ?>
            <ul>
                <?php
                foreach ($apply_rows as $row):
                    $adRow = Modules::run('studentad/get_row', $row->ad_ID);
                    if ($adRow) {
                        if (can_show_ad($adRow->ID, $adRow->parent_ID)) {
                            ?>
                            <li>
                                <span class="which-ads" data-id="<?php echo $adRow->ID ?>-Institute" data-type="Institute" data-url="<?php echo base_url(); ?>">
                                    <strong><?php echo $adRow->looking_for; ?></strong>
                                    <em>
                                        <small>
                                            by <?php echo Modules::run('user/get_name', $row->student_ID); ?> 
                                        </small>
                                    </em>
                                </span>
                            </li>
                            <?php
                        }
                    }
                endforeach;
                ?>
            </ul>
            <?php if (count($apply_rows) > 10) { ?>
            </div>
        <?php } ?>
    </div>

    <?php
}
?>





<?php
$allVid = Modules::run('videos/get_videos', $userRow->ID);
if ($allVid) {
    ?>
    <div class="ad-box-round-corner standard-padding-little-height round-border normal-list">
        <h4>Featured Videos</h4>
        <div class="row">
            <?php
            foreach ($allVid as $vid):
                $this->load->view('videos/video_thumb_sidebar', array('video_code' => $vid->video_code));
            endforeach;
            ?>
        </div>
    </div>
    <?php
}
?>


<?php echo Modules::run('sidebar/get_vertical_sidebar', 31); ?>