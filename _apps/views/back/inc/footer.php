</section><!-- /.content -->

</div><!-- /.content-wrapper -->

<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 2.3.0
    </div>
    <strong>Copyright &copy; 2008-<?php echo date('Y') ?> <a href="http://coconutcreation.com" target="_blnak">Custom Development by: Coconut Creation</a>.</strong> All rights reserved.
</footer>



<div class="overlay"></div>
</div><!-- ./wrapper -->

<!-- jQuery 2.1.4 -->

<!-- Bootstrap 3.3.5 -->
<script src="<?php echo get_assist() ?>bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?php echo get_assist() ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo get_assist() ?>plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo get_assist() ?>plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo get_assist() ?>dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo get_assist() ?>plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo get_assist() ?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo get_assist() ?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="<?php echo get_assist() ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="<?php echo get_assist() ?>plugins/chartjs/Chart.min.js"></script>
<script src="<?php echo get_assist() ?>plugins/chartjs/Chart.min.js"></script>


<!--UNIVERSAL AJAX AREA-->
<script src="<?php echo get_assist() ?>ajax/ajax.js"></script>
<!--UNIVERSAL AJAX AREA-->


<script>
    $(function () {

        $("#justtable").DataTable({
            "iDisplayLength": 100,
            "bPaginate": false
        });
        $("#example1").DataTable({
            "iDisplayLength": 100
        });
        $("#example2").DataTable({
            "iDisplayLength": 100
        });
        $("#example3").DataTable({
            "iDisplayLength": 100
        });
        $('#example4').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });

        $("#only20").DataTable({
            "iDisplayLength": 10
        });
        $("#only20_1").DataTable({
            "iDisplayLength": 10
        });
        $("#only20_2").DataTable({
            "iDisplayLength": 10
        });
        $("#only20_3").DataTable({
            "iDisplayLength": 10
        });
        $("#only20_4").DataTable({
            "iDisplayLength": 10
        });
        $("#only20_5").DataTable({
            "iDisplayLength": 10
        });
        $("#only20_6").DataTable({
            "iDisplayLength": 10
        });
    });
</script>
<script>
    $(function () {
        $('.fixed-height').slimScroll({
            height: '250px'
        });
    });

    $('.datepicker').datepicker({
        format: 'dd-mm-yyyy'
    });
</script>
<script>
    $('.sidebar-modules img').addClass('img-responsive admin-thumb');
</script>
</body>
</html>