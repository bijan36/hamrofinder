<header class="main-header">

    <!-- Logo -->
    <a href="<?php echo site_url('appsdashbaord'); ?>" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>A</b>KB</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Admin</b> <?php echo PROJECTNAME; ?></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

                <li><a href="<?php echo site_url('appsevents'); ?>"><i class="fa fa-edit"></i> Events</a></li>
                <li><a href="<?php echo site_url('appscms'); ?>"><i class="fa fa-edit"></i> CMS</a></li>
                <li><a href="<?php echo site_url('appsnews'); ?>"><i class="fa fa-bullhorn"></i> News & Updates</a></li>


                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-edit"></i> <span>Photo Gallery</span> <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="<?php echo site_url('appsgallery'); ?>"><i class="fa fa-image"></i> Galleries</a></li>
                        <li class="divider"></li>
                        <?php
                        $alls = Modules::run('appssettings/get_all_settings', 'Gallery');
                        foreach ($alls as $asett):
                            ?>
                            <li><a href="<?php echo site_url('appssettings/formhandler/' . $asett->slug); ?>"><i class="fa fa-cog"></i> <?php echo $asett->kee; ?></a></li>
                            <?php
                        endforeach;
                        ?>
                    </ul>
                </li>


            </ul>


            <ul class="nav navbar-nav">

                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?php echo get_assist() ?>dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                        <span class="hidden-xs"><?php echo sessionData('adminEmail'); ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?php echo get_assist() ?>dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                            <p>
                                <?php echo sessionData('adminEmail'); ?> - Super Admin
                                <small>Member since Nov. 2015</small>
                            </p>
                        </li>

                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="javascript:void(0);" name="logOut" class="btn btn-default btn-flat" id="adminLogout" data-url="<?php echo base_url() . ADMINLOGOUTURL ?>">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>

            </ul>

        </div>

    </nav>
</header>




<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo get_assist() ?>dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?php echo sessionData('adminName'); ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->

        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li><a href="<?php echo site_url('appsdashboard') ?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
            <li><a href="<?php echo site_url('appsimages') ?>"><i class="fa fa-image"></i> Image Library</a></li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-graduation-cap" aria-hidden="true"></i> <span>Student</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('appsstudent') ?>"><i class="fa fa-users"></i> Students</a></li>
                    <li><a href="<?php echo site_url('appsstudentad') ?>"><i class="fa fa-newspaper-o"></i> Student Ads</a></li>
                    <li>
                        <a href="#"><i class="fa fa-cogs"></i> Student settings options<i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <?php
                            $alls = Modules::run('appssettings/get_all_settings', 'Student');
                            foreach ($alls as $asett):
                                ?>
                                <li><a href="<?php echo site_url('appssettings/formhandler/' . $asett->slug); ?>"><i class="fa fa-cog"></i> <?php echo $asett->kee; ?></a></li>
                                <?php
                            endforeach;
                            ?>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-university" aria-hidden="true"></i> <span>College</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('appscollege') ?>"><i class="fa fa-cubes"></i> Colleges</a></li>
                    <li><a href="<?php echo site_url('appscollegead') ?>"><i class="fa fa-newspaper-o"></i> College Ads</a></li>
                    <li><a href="<?php echo site_url('appscollege_membership_plans'); ?>"><i class="fa fa-image"></i> College Membership</a></li>
                    <li><a href="<?php echo site_url('appscollege_ad_pricing'); ?>"><i class="fa fa-image"></i> College Ad Pricing</a></li>
                    <li>
                        <a href="#"><i class="fa fa-cogs"></i> College settings options <i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <?php
                            $alls = Modules::run('appssettings/get_all_settings', 'College');
                            foreach ($alls as $asett):
                                ?>
                                <li><a href="<?php echo site_url('appssettings/formhandler/' . $asett->slug); ?>"><i class="fa fa-cog"></i> <?php echo $asett->kee; ?></a></li>
                                <?php
                            endforeach;
                            ?>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-university" aria-hidden="true"></i> <span>Institute</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('appsinstitute') ?>"><i class="fa fa-cubes"></i> Institutes</a></li>
                    <li><a href="<?php echo site_url('appsinstitutead') ?>"><i class="fa fa-newspaper-o"></i> Institute Ads</a></li>
                    <li><a href="<?php echo site_url('appsinstitute_membership_plans'); ?>"><i class="fa fa-image"></i> Institute Membership</a></li>
                    <li><a href="<?php echo site_url('appsinstitute_ad_pricing'); ?>"><i class="fa fa-image"></i> Institute Ad Pricing</a></li>
                    <li>
                        <a href="#"><i class="fa fa-cogs"></i> Institute settings options <i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <?php
                            $alls = Modules::run('appssettings/get_all_settings', 'Institute');
                            foreach ($alls as $asett):
                                ?>
                                <li><a href="<?php echo site_url('appssettings/formhandler/' . $asett->slug); ?>"><i class="fa fa-cog"></i> <?php echo $asett->kee; ?></a></li>
                                <?php
                            endforeach;
                            ?>
                        </ul>
                    </li>
                </ul>
            </li>


            <li class="treeview">
                <a href="#">
                    <i class="fa fa-cog"></i> <span>General Settings</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <?php
                    $alls = Modules::run('appssettings/get_all_settings', 'Website');
                    foreach ($alls as $asett):
                        ?>
                        <li><a href="<?php echo site_url('appssettings/formhandler/' . $asett->slug); ?>"><i class="fa fa-cog"></i> <?php echo $asett->kee; ?></a></li>
                        <?php
                    endforeach;
                    ?>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-envelope-o"></i> <span>Email Settings</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <?php
                    $alls = Modules::run('appsemailsettings/get_all_settings');
                    foreach ($alls as $asett):
                        ?>
                        <li><a href="<?php echo site_url('appsemailsettings/formhandler/' . $asett->slug); ?>"><i class="fa fa-cog"></i> <?php echo $asett->kee; ?></a></li>
                        <?php
                    endforeach;
                    ?>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-edit"></i> <span>Photo Gallery</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('appsgallery'); ?>"><i class="fa fa-cog"></i> Galleries</a></li>
                    <?php
                    $alls = Modules::run('appssettings/get_all_settings', 'Gallery');
                    foreach ($alls as $asett):
                        ?>
                        <li><a href="<?php echo site_url('appssettings/formhandler/' . $asett->slug); ?>"><i class="fa fa-cog"></i> <?php echo $asett->kee; ?></a></li>
                        <?php
                    endforeach;
                    ?>
                </ul>
            </li>
            <li><a href="<?php echo site_url('appsaddress') ?>"><i class="fa fa-external-link"></i> <span>Address</span></a></li>
            <li><a href="<?php echo site_url('appssidebar') ?>"><i class="fa fa-external-link"></i> <span>Sidebars</span></a></li>
            <li><a href="<?php echo site_url('appsfixedurl') ?>"><i class="fa fa-external-link"></i> <span>Some Fixed Urls</span></a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">



