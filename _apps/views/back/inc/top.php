<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo COMPANYNAME; ?></title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="<?php echo get_assist() ?>bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- DataTables -->
        <link rel="stylesheet" href="<?php echo get_assist() ?>plugins/datatables/dataTables.bootstrap.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="<?php echo get_assist() ?>plugins/jvectormap/jquery-jvectormap-1.2.2.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo get_assist() ?>dist/css/AdminLTE.min.css">

        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<?php echo get_assist() ?>dist/css/skins/_all-skins.min.css">
        <link rel="stylesheet" href="<?php echo get_assist() ?>dist/css/style.css">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script src="<?php echo get_assist() ?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
        <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
        <script>tinymce.init({
                selector: '.myEditor',
                menubar: false,
                min_height: 300,
                relative_urls: false,
                remove_script_host: false,
                convert_urls: true,
                plugins: "link image code table fullscreen media spellchecker",
                toolbar: 'undo redo | styleselect | underline | bold italic | link unlink | image  | bullist | numlist | alignleft | aligncenter | alignright | table | media | spellchecker | fullscreen | code',
                theme: 'modern',
                setup: function (editor) {
                    editor.on('change', function () {
                        editor.save();
                    });
                    editor.on('init', function ()
                    {
                        this.getDoc().body.style.fontSize = '14px';
                    });
                }
            });</script>
        <script src="<?php echo get_assist() ?>dist/js/bootstrap-datepicker.js"></script>
        <link rel="stylesheet" href="<?php echo get_assist() ?>dist/css/datepicker.css">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
    </head>

    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">