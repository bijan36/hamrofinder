<script>
    $('document').ready(function () {
        $('#admin-login').submit(function (e) {
            e.preventDefault();

            var postData = $(this).serialize();
            var formUrl = $(this).attr('action');

            $('.overlay').css('display', 'block');

            var request = $.ajax({
                url: formUrl,
                method: "POST",
                data: postData,
                dataType: "html"
            });

            request.done(function (res) {

                var x = JSON.parse(res);
                if (x.status == 'success') {
                    $('#myLogin').val('Succesfully Loged In');
                    window.location.href = '<?php echo base_url() ?>appsdashboard';
                } else {
                    $('.overlay').css('display', 'none');
                    $('.login-msg').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>')
                }
            });

            request.fail(function (jqXHR, textStatus) {
                alert("Request Fail: " + textStatus);
            });
        });
    });
</script>