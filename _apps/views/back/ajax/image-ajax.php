<script>
    $(document).ready(function () {

        //ADDING NEW IMAGE
        $('#addImages').submit(function (e) {
            e.preventDefault();
            var postData = $(this).serialize();
            var formUrl = $(this).attr('action');

            //styling css while ajaxing
            $('.overlay').css('display', 'block');
            $('#submitBtn').addClass('disabled');
            $('#submitBtn').html('<i class="fa fa-spinner fa-spin"></i> Submiting...');

            var request = $.ajax({
                url: formUrl,
                dataType: "html",
                type: "POST", // Type of request to be send, called as method
                data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false,
            });

            request.done(function (res) {
                var x = JSON.parse(res);
                if (x.status == 'success') {
                    //styling css while ajax finish
                    $('.overlay').css('display', 'none');
                    $('#submitBtn').removeClass('disabled');
                    $('#submitBtn').html('Submit');
                    $('.showMsg').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
                    window.location.reload();
                } else {
                    //styling css while ajax finish
                    $('.overlay').css('display', 'none');
                    $('#submitBtn').removeClass('disabled');
                    //putting values
                    $('#submitBtn').html('Submit');
                    $('.showMsg').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + x.msg + '</div>');
                }
            });
            request.fail(function (jqXHR, textStatus) {
                alert("Request failed: " + textStatus);
            });
        });

    });
</script>
