<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo COMPANYNAME; ?></title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="<?php echo base_url() ?>_public/back/bootstrap/css/bootstrap.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url() ?>_public/back/dist/css/AdminLTE.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="<?php echo base_url() ?>_public/back/plugins/iCheck/square/blue.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            .overlay {
                width: 100%;
                height: 100%;
                padding-top: 50%;
                background: rgba(255,255,255,0.7) url('<?php echo base_url() ?>_public/back/728.GIF') center center no-repeat;
                background-position-y: 50%;
                position: absolute;
                z-index: 99999;
                top: 0px;
                left: 0px;
                display: none;
                text-align: center;
                cursor: wait;
            }
        </style>
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <a href="#"><b>Admin</b> <?php echo COMPANYNAME;?></a>
            </div><!-- /.login-logo -->
            <div class="login-box-body"  style="position:relative;">
                <p class="login-box-msg">Sign in to start your session</p>
                <?php echo form_open('appslogin/validate', array('id' => 'admin-login')) ?>
                <div class="form-group has-feedback">
                    <input type="email" name="username" class="form-control" placeholder="User Name">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" name="password" class="form-control" placeholder="Password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <input type="submit" class="btn btn-success btn-block" id="myLogin" value="Loged Me In">
                    </div><!-- /.col -->
                </div>
                <div class="row" style="margin-top: 15px;">
                    <div class="col-xs-12">
                        <div class="login-msg"></div>
                    </div><!-- /.col -->
                </div>
                <div class="row" style="margin-top: 15px;">
                    <div class="col-xs-12">
                        Note: Only administrator are allowed, every events and IPs are monitored.
                    </div><!-- /.col -->
                </div>
                <?php echo form_close(); ?>
                <div class="overlay"></div>
            </div><!-- /.login-box-body -->

        </div><!-- /.login-box -->
        <!-- jQuery -->
        <script src="<?php echo base_url() ?>_public/back/plugins/jQuery/jQuery-2.1.4.min.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="<?php echo base_url() ?>_public/back/bootstrap/js/bootstrap.min.js"></script>
        <?php
        //LOGIN AJAX
        echo $this->load->view('back/ajax/login-ajax');
        ?>
    </body>
</html>
