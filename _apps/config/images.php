<?php
/*
|--------------------------------------------------------------------------
| Image Preset Sizes
|--------------------------------------------------------------------------
|
| Specify the preset sizes you want to use in your code. Only these preset 
| will be accepted by the controller for security.
|
| Each preset exists of a width and height. If one of the dimensions are 
| equal to 0, it will automatically calculate a matching width or height 
| to maintain the original ratio.
|
| If both dimensions are specified it will automatically crop the 
| resulting image so that it fits those dimensions.
|
*/

$config["image_sizes"]["thumb40x20"] = array(40, 20);
$config["image_sizes"]["thumb40x40"] = array(40, 40);
$config["image_sizes"]["thumb50x50"] = array(50, 50);
$config["image_sizes"]["thumb55x55"] = array(55, 55);
$config["image_sizes"]["thumb60x60"] = array(60, 60);
$config["image_sizes"]["thumb70x70"] = array(70, 70);
$config["image_sizes"]["thumb65x65"] = array(65, 65);
$config["image_sizes"]["thumb80x80"] = array(80, 80);
$config["image_sizes"]["thumb100x100"] = array(100, 100);
$config["image_sizes"]["thumb110x110"] = array(110, 110);
$config["image_sizes"]["thumb120x120"] = array(120, 120);
$config["image_sizes"]["thumb150x150"] = array(150, 150);
$config["image_sizes"]["thumb180x180"] = array(180, 180);
$config["image_sizes"]["thumb210x210"] = array(210, 210);
$config["image_sizes"]["thumb260x260"] = array(260, 260);
$config["image_sizes"]["thumb300x300"] = array(300, 300);
$config["image_sizes"]["thumb300x200"] = array(300, 200);
$config["image_sizes"]["thumb350x350"] = array(350, 350);
$config["image_sizes"]["thumb400x400"] = array(400, 400);
$config["image_sizes"]["thumb400x250"] = array(400, 250);
$config["image_sizes"]["thumb450x450"] = array(450, 450);
$config["image_sizes"]["thumb500x500"] = array(500, 500);
$config["image_sizes"]["thumb600X600"] = array(600, 600);
$config["image_sizes"]["thumb1276x495"] = array(1276, 495);
$config["image_sizes"]["profilethumb"] = array(200, 256);
$config["image_sizes"]["profilethumbReal"] = array(250, 0);




$config["image_sizes"]["slider"] = array(1900, 630);
$config["image_sizes"]["newsThumb"] = array(500, 329);









