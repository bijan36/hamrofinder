<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'site';
$route['media/(:any)'] = 'media/resize/$1';
$route['content/(:any)'] = 'content/show/$1';
$route['contact'] = 'content/contact';
$route['photogallery/(:any)'] = "photogallery/index/$1";
$route['news/(:any)'] = "news/index/$1";
$route['plans'] = "myplans/ad_plans";
$route['payment-plans'] = "myplans/payment_plans"; // MEMBERSHIP PLANS
$route['event-plans'] = "events/events_plans"; //EVENT PLANS
$route['show-event/(:num)'] = "events/show_event/$1"; //SINGLE EVENT DETAILS PAGE
$route['ad-plans'] = "myplans/display_ad_plans"; //AD PA

//FOR PAGINATION
$route['appsstudent/(:num)'] = "appsstudent/index/$1";
$route['appsstudentad/(:num)'] = "appsstudentad/index/$1";
$route['appscollege/(:num)'] = "appscollege/index/$1";
$route['appscollegead/(:num)'] = "appscollegead/index/$1";
$route['appsinstitute/(:num)'] = "appsinstitute/index/$1";
$route['appsinstitutead/(:num)'] = "appsinstitutead/index/$1";
$route['appsimages/(:num)'] = "appsimages/index/$1";




$route['404_override'] = 'notfound';
$route['translate_uri_dashes'] = FALSE;
