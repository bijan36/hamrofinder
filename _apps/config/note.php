<?php

/* 
 * SYSTEMPRODUCT_META
 * 
 * @ type = Product
 * 
 * new_product - Yes/No
 *    internal_ID (Yes)
 *     - new_remove_date (Y-m-d)
 * 
 * Product Color - Yes/No (Select Box - Pre Set)
 *    internal_ID (Yes)
 *     - color = array('color_name'=>'Black','avaiblity'=>'Available','image'=>'5');
 *     - color = array('color_name'=>'White','avaiblity'=>'Not Available','image'=>'15');
 *     - color = array('color_name'=>'Red','avaiblity'=>'Not Available','image'=>'10');
 * 
 * Free Items - Yes/No
 *     internal_ID (Yes)
 *     - Free Items Details - array('Item Title'=>'Titen Watch','image'=>'24','Item Details'=>'some details')
 *     - Free Items Details - array('Item Title'=>'Titen Watch','image'=>'24','Item Details'=>'some details')
 *     - Free Items Details - array('Item Title'=>'Titen Watch','image'=>'24','Item Details'=>'some details')
 * 
 * 
 * Warranty - Yes/No (Select Box - Pre Set)
 *     internal_ID (Yes)
 *     - Warranty Period = 1 Year
 * 
 * 
 * Money Back - Yes/No 
 *     internal_ID (Yes)
 *      - Money Back Details = array('Currency'=>'$currency','Back Amount'=>'152')
 *      - Money Back Details = array('Currency'=>'$currency','Back Amount'=>'152')
 *      - Money Back Details = array('Currency'=>'$currency','Back Amount'=>'152')
 * 
 * 
 * PRODUCT NOTES =================================
 * Scratch & Win - Yes/No - editable
 *    internal_ID (Yes)
 *     product note details  array( Title = 'Bingo Scratch','Image'=>'5', Details = 'Wow scratch and win 15 Inch color tv' (Text Area)
 * 
 * Lucky Draw - Yes/No - editable
 *    internal_ID (Yes)
 *     - alternative_text = 'Bingo Lucky Draw'
 *     - content = 'Wow Lucky Draw and win 15 Inch color tv' (Text Area)
 * 
 * Special Offer - Yes/No - editable
 *     internal_ID (Yes)
 *      - alternative_text = 'Bingo Special Offer'
 *      - content = 'Wow Special Offer and win 15 Inch color tv' (Text Area)
 *  * PRODUCT NOTES ===============================
 * 
 * 
 * 
 * Shipping Charges - Yes/No
 *     internal_ID (Yes)      
 *      - content = Preset content in editor
 * 
 * 
 * 
 * Shipping Time - Yes/No
 *     internal_ID (Yes)      
 *      - content = Preset content in editor
 * 
 * Delivery Options - Yes/No
 *     internal_ID (Yes)      
 *      - COD
 * 
 * 
 * Payment Getway - Yes/No
 *     internal_ID (Yes)      
 *      - COD
 * 
 * 
 * 
 */

