<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Address extends Frontendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('Address_m', 'faddress');
    }

    //STUDENT LIST
    public function index() {
        $data['alldata'] = $this->faddress->order_by('position', 'ASC')->get_many_by(array('parent_ID' => 0));
        $this->template->load(get_template('default'), 'list', $data);
    }

    public function get_select_box($parentID, $level, $field, $name, $selected = null) {
        $rows = $this->faddress->order_by('position', 'ASC')->get_many_by(array('parent_ID' => $parentID, 'level' => $level));
        $output = '<label>' . $level . '</label><div class="drop_list"><select class="form-control" name="' . $name . '" id="' . $name . '">';
        $sel = '';
        $output .= '<option selected value="0">Select One</option>';
        foreach ($rows as $row):
            if ($selected) {
                $sel = ($row->$field == $selected) ? 'selected' : '';
            }
            $output .= '<option value="' . $row->$field . '" ' . $sel . '>' . $row->$field . '</option>';
        endforeach;
        $output .= '</select></div>';
        return $output;
    }

    //BY FINDING THE PARENT 
    public function get_select_box_find_parent($parent_name, $level, $field, $name, $selected = null) {

        $parentLevel = array(
            'Province' => 'Country',
            'District' => 'Province',
            'Local Level' => 'District',
            'Ward No' => 'Local Level'
        );

        $parentRow = $this->faddress->get_by(array('name' => $parent_name, 'level' => $parentLevel[$level]));
        if ($parentRow) {
            $rows = $this->faddress->order_by('position', 'ASC')->get_many_by(array('parent_ID' => $parentRow->ID, 'level' => $level));
            $output = '<label>' . ucfirst($level) . '</label><div class="drop_list"><select class="form-control" name="' . $name . '" id="' . $name . '">';
            $sel = '';
            $output .= '<option selected value="0">Select One</option>';
            foreach ($rows as $row):
                if ($selected) {
                    $sel = ($row->$field == $selected) ? 'selected' : '';
                }
                $output .= '<option value="' . $row->$field . '" ' . $sel . '>' . $row->$field . '</option>';
            endforeach;
            $output .= '</select></div>';
            return $output;
        } else {
            //NO PARENT LEVEL FOUND
            $output = '<label>' . ucfirst($level) . '</label><div class="drop_list"><select class="form-control" name="' . $name . '" id="' . $name . '">';
            $sel = '';
            $output .= '<option selected value="0">Select One</option>';
            $output .= '</select></div>';
            return $output;
        }
    }

    //MAKING SELECT RESETTIR
    public function resetter($lebel, $name) {
        $output = '<label>' . ucfirst($lebel) . '</label><div class="drop_list"><select class="form-control" name="' . $name . '" id="' . $name . '">';
        $sel = '';
        $output .= '<option selected value="0">Select One</option>';
        $output .= '</select></div>';
        return $output;
    }

    public function get_all() {
        $rows = $this->faddress->get_all();
        return $rows ? $rows : false;
    }

    public function get_countries() {
        $rows = $this->faddress->order_by('position', 'ASC')->get_many_by(array('parent_ID' => '0', 'level' => 'Country'));
        return $rows ? $rows : false;
    }

    public function get_provinces($parent_ID) {
        $rows = $this->faddress->order_by('position', 'ASC')->get_many_by(array('parent_ID' => $parent_ID, 'level' => 'Province'));
        return $rows ? $rows : false;
    }

    public function get_districts($parent_ID) {
        $rows = $this->faddress->order_by('position', 'ASC')->get_many_by(array('parent_ID' => $parent_ID, 'level' => 'District'));
        return $rows ? $rows : false;
    }

    //COUNTRY CHANGE FUNCTION
    public function on_change_country() {
        $country = $this->input->post('country');
        if (isset($country) && !empty($country)) {
            $countryRow = $this->faddress->get_by(array('name' => $country, 'level' => 'Country'));

            if ($countryRow) {
                $provinceSelect = $this->get_select_box_find_parent($country, 'Province', 'name', 'province');
                $data = array(
                    'status' => 'success',
                    'resultSelect' => $provinceSelect,
                    'selectDistrict' => $this->resetter('District', 'district'),
                    'selectLocalLevel' => $this->resetter('Local Level', 'local_level'),
                    'selectWordNo' => $this->resetter('Ward No', 'ward_no'),
                );
            } else {
                //IF NO SUCH COUNTRY FOUND
                $data = array(
                    'status' => 'error',
                    'selectProvince' => $this->resetter('Province', 'province'),
                    'selectDistrict' => $this->resetter('District', 'district'),
                    'selectLocalLevel' => $this->resetter('Local Level', 'local_level'),
                    'selectWordNo' => $this->resetter('Ward No', 'ward_no'),
                    'msg' => 'No such data found'
                );
            }
        } else {
            //NO COUNTRY SLECTED 
            $data = array(
                'status' => 'error',
                'selectProvince' => $this->resetter('Province', 'province'),
                'selectDistrict' => $this->resetter('District', 'district'),
                'selectLocalLevel' => $this->resetter('Local Level', 'local_level'),
                'selectWordNo' => $this->resetter('Ward No', 'ward_no'),
                'msg' => 'Please select country'
            );
        }
        echo json_encode($data);
        exit();
    }

    //PROVINCE CHANGE
    public function on_change_province() {
        $province = $this->input->post('province');
        if (isset($province) && !empty($province)) {
            $provinceRow = $this->faddress->get_by(array('name' => $province, 'level' => 'Province'));

            if ($provinceRow) {
                $districtSelect = $this->get_select_box_find_parent($province, 'District', 'name', 'district');
                $data = array(
                    'status' => 'success',
                    'selectDistrict' => $districtSelect,
                    'selectLocalLevel' => $this->resetter('Local Level', 'local_level'),
                    'selectWordNo' => $this->resetter('Ward No', 'ward_no'),
                );
            } else {
                //IF NO SUCH COUNTRY FOUND
                $data = array(
                    'status' => 'error',
                    'selectDistrict' => $this->resetter('District', 'district'),
                    'selectLocalLevel' => $this->resetter('Local Level', 'local_level'),
                    'selectWordNo' => $this->resetter('Ward No', 'ward_no'),
                    'msg' => 'No such data found'
                );
            }
        } else {
            //NO COUNTRY SLECTED 
            $data = array(
                'status' => 'error',
                'selectDistrict' => $this->resetter('District', 'district'),
                'selectLocalLevel' => $this->resetter('Local Level', 'local_level'),
                'selectWordNo' => $this->resetter('Ward No', 'ward_no'),
                'msg' => 'Please select province'
            );
        }
        echo json_encode($data);
        exit();
    }

    //DISTRICT CHANGE
    public function on_change_district() {
        $district = $this->input->post('district');
        if (isset($district) && !empty($district)) {
            $districtRow = $this->faddress->get_by(array('name' => $district, 'level' => 'District'));

            if ($districtRow) {
                $localLevelSelect = $this->get_select_box_find_parent($district, 'Local Level', 'name', 'local_level');
                $data = array(
                    'status' => 'success',
                    'selectLocalLevel' => $localLevelSelect,
                    'selectWordNo' => $this->resetter('Ward No', 'ward_no'),
                );
            } else {
                //IF NO SUCH COUNTRY FOUND
                $data = array(
                    'status' => 'error',
                    'selectLocalLevel' => $this->resetter('Local Level', 'local_level'),
                    'selectWordNo' => $this->resetter('Ward No', 'ward_no'),
                    'msg' => 'No such data found'
                );
            }
        } else {
            //NO COUNTRY SLECTED 
            $data = array(
                'status' => 'error',
                'selectLocalLevel' => $this->resetter('Local Level', 'local_level'),
                'selectWordNo' => $this->resetter('Ward No', 'ward_no'),
                'msg' => 'Please select district'
            );
        }
        echo json_encode($data);
        exit();
    }

    //LOCAL LEVEL CHNAGE
    public function on_change_local_level() {
        $local_level = $this->input->post('local_level');
        if (isset($local_level) && !empty($local_level)) {
            $local_levelRow = $this->faddress->get_by(array('name' => $local_level, 'level' => 'Local Level'));

            if ($local_levelRow) {
                $wardnoSelect = $this->get_select_box_find_parent($local_level, 'Ward No', 'name', 'ward_no');
                $data = array(
                    'status' => 'success',
                    'selectWordNo' => $wardnoSelect
                );
            } else {
                //IF NO SUCH COUNTRY FOUND
                $data = array(
                    'status' => 'error',
                    'selectWordNo' => $this->resetter('Ward No', 'ward_no'),
                    'msg' => 'No such data found'
                );
            }
        } else {
            //NO COUNTRY SLECTED 
            $data = array(
                'status' => 'error',
                'selectWordNo' => $this->resetter('Ward No', 'ward_no'),
                'msg' => 'Please select local level'
            );
        }
        echo json_encode($data);
        exit();
    }

    //FOR BROWSE ===============================================================
    //BROSE GET SELECT BOX
    public function browse_get_select_box($parentID, $level, $field, $name, $type) {
        $rows = $this->faddress->order_by('position', 'ASC')->get_many_by(array('parent_ID' => $parentID, 'level' => $level));
        $output = '<label>' . $level . '</label><div class="drop_list"><select class="form-control ' . $name . '" name="' . $name . '" id="' . $name . '-' . $type . '"  data-type="' . $type . '">';
        $output .= '<option selected value="0">Select One</option>';
        foreach ($rows as $row):
            $output .= '<option value="' . $row->$field . '">' . $row->$field . '</option>';
        endforeach;
        $output .= '</select></div>';
        return $output;
    }

    //BY FINDING THE PARENT for BROWSE DATA
    public function browse_get_select_box_find_parent($parent_name, $level, $field, $name, $type) {

        $parentLevel = array(
            'Province' => 'Country',
            'District' => 'Province',
            'Local Level' => 'District',
            'Ward No' => 'Local Level',
            'Address' => 'Ward No'
        );

        $parentRow = $this->faddress->get_by(array('name' => $parent_name, 'level' => $parentLevel[$level]));
        if ($parentRow) {
            $rows = $this->faddress->order_by('position', 'ASC')->get_many_by(array('parent_ID' => $parentRow->ID, 'level' => $level));
            $output = '<label>' . ucfirst($level) . '</label><div class="drop_list"><select class="form-control ' . $name . '" name="' . $name . '" id="' . $name . '-' . $type . '" data-type="' . $type . '">';
            $output .= '<option selected value="0">Select One</option>';
            foreach ($rows as $row):
                $output .= '<option value="' . $row->$field . '">' . $row->$field . '</option>';
            endforeach;
            $output .= '</select></div>';
            return $output;
        } else {
            //NO PARENT LEVEL FOUND
            $output = '<label>' . ucfirst($level) . '</label><div class="drop_list"><select class="form-control ' . $name . '" name="' . $name . '" id="' . $name . '-' . $type . '">';
            $sel = '';
            $output .= '<option selected value="0">Select One</option>';
            $output .= '</select></div>';
            return $output;
        }
    }

    //MAKING SELECT RESETTIR
    public function browse_resetter($lebel, $name, $type) {
        $output = '<label>' . ucfirst($lebel) . '</label><div class="drop_list"><select class="form-control ' . $name . '" name="' . $name . '" id="' . $name . '-' . $type . '" data-type="' . $type . '">';
        $sel = '';
        $output .= '<option selected value="0">Select One</option>';
        $output .= '</select></div>';
        return $output;
    }

    //COUNTRY CHANGE FUNCTION===================================================
    public function browse_change_country() {
        $country = $this->input->post('country');
        $type = $this->input->post('type');
        if (isset($country) && !empty($country)) {
            $countryRow = $this->faddress->get_by(array('name' => $country, 'level' => 'Country'));

            if ($countryRow) {
                $provinceSelect = $this->browse_get_select_box_find_parent($country, 'Province', 'name', 'browseprovince', $type);

                $data = array(
                    'status' => 'success',
                    'resultSelect' => $provinceSelect,
                    'selectDistrict' => $this->browse_resetter('District', 'browsedistrict', $type),
                    'selectLocalLevel' => $this->browse_resetter('Local Level', 'browselocal_level', $type),
                    'selectWordNo' => $this->browse_resetter('Ward No', 'browseward_no', $type),
                    'selectAddress' => $this->browse_resetter('Address', 'browseaddress', $type),
                );
            } else {
                //IF NO SUCH COUNTRY FOUND
                $data = array(
                    'status' => 'error',
                    'selectProvince' => $this->browse_resetter('Province', 'browseprovince', $type),
                    'selectDistrict' => $this->browse_resetter('District', 'browsedistrict', $type),
                    'selectLocalLevel' => $this->browse_resetter('Local Level', 'browselocal_level', $type),
                    'selectWordNo' => $this->browse_resetter('Ward No', 'browseward_no', $type),
                    'selectAddress' => $this->browse_resetter('Address', 'browseaddress', $type),
                    'msg' => 'No such data found'
                );
            }
        } else {
            //NO COUNTRY SLECTED 
            $data = array(
                'status' => 'error',
                'selectProvince' => $this->browse_resetter('Province', 'browseprovince', $type),
                'selectDistrict' => $this->browse_resetter('District', 'browsedistrict', $type),
                'selectLocalLevel' => $this->browse_resetter('Local Level', 'browselocal_level', $type),
                'selectWordNo' => $this->browse_resetter('Ward No', 'browseward_no', $type),
                'selectAddress' => $this->browse_resetter('Address', 'browseaddress', $type),
                'msg' => 'Please select country'
            );
        }
        echo json_encode($data);
        exit();
    }

    //PROVINCE CHANGE BROSE
    public function browse_change_province() {
        $province = $this->input->post('province');
        $type = $this->input->post('type');
        if (isset($province) && !empty($province)) {
            $provinceRow = $this->faddress->get_by(array('name' => $province, 'level' => 'Province'));

            if ($provinceRow) {
                $districtSelect = $this->browse_get_select_box_find_parent($province, 'District', 'name', 'browsedistrict', $type);
                $data = array(
                    'status' => 'success',
                    'selectDistrict' => $districtSelect,
                    'selectLocalLevel' => $this->browse_resetter('Local Level', 'browselocal_level', $type),
                    'selectWordNo' => $this->browse_resetter('Ward No', 'browseward_no', $type),
                    'selectAddress' => $this->browse_resetter('Address', 'browseaddress', $type),
                );
            } else {
                //IF NO SUCH COUNTRY FOUND
                $data = array(
                    'status' => 'error',
                    'selectDistrict' => $this->browse_resetter('District', 'browsedistrict', $type),
                    'selectLocalLevel' => $this->browse_resetter('Local Level', 'browselocal_level', $type),
                    'selectWordNo' => $this->browse_resetter('Ward No', 'browseward_no', $type),
                    'selectAddress' => $this->browse_resetter('Address', 'browseaddress', $type),
                    'msg' => 'No such data found'
                );
            }
        } else {
            //NO COUNTRY SLECTED 
            $data = array(
                'status' => 'error',
                'selectDistrict' => $this->browse_resetter('District', 'browsedistrict', $type),
                'selectLocalLevel' => $this->browse_resetter('Local Level', 'browselocal_level', $type),
                'selectWordNo' => $this->browse_resetter('Ward No', 'browseward_no', $type),
                'selectAddress' => $this->browse_resetter('Address', 'browseaddress', $type),
                'msg' => 'Please select province'
            );
        }
        echo json_encode($data);
        exit();
    }

    //DISTRICT CHANGE
    public function browse_change_district() {
        $district = $this->input->post('district');
        $type = $this->input->post('type');
        if (isset($district) && !empty($district)) {
            $districtRow = $this->faddress->get_by(array('name' => $district, 'level' => 'District'));

            if ($districtRow) {
                $localLevelSelect = $this->browse_get_select_box_find_parent($district, 'Local Level', 'name', 'browselocal_level', $type);
                $data = array(
                    'status' => 'success',
                    'selectLocalLevel' => $localLevelSelect,
                    'selectWordNo' => $this->browse_resetter('Ward No', 'browseward_no', $type),
                    'selectAddress' => $this->browse_resetter('Address', 'browseaddress', $type),
                );
            } else {
                //IF NO SUCH COUNTRY FOUND
                $data = array(
                    'status' => 'error',
                    'selectLocalLevel' => $this->browse_resetter('Local Level', 'browselocal_level', $type),
                    'selectWordNo' => $this->browse_resetter('Ward No', 'browseward_no', $type),
                    'selectAddress' => $this->browse_resetter('Address', 'browseaddress', $type),
                    'msg' => 'No such data found'
                );
            }
        } else {
            //NO COUNTRY SLECTED 
            $data = array(
                'status' => 'error',
                'selectLocalLevel' => $this->browse_resetter('Local Level', 'browselocal_level', $type),
                'selectWordNo' => $this->browse_resetter('Ward No', 'browseward_no', $type),
                'selectAddress' => $this->browse_resetter('Address', 'browseaddress', $type),
                'msg' => 'Please select district'
            );
        }
        echo json_encode($data);
        exit();
    }

    //LOCAL LEVEL CHNAGE
    public function browse_change_local_level() {
        $local_level = $this->input->post('local_level');
        $type = $this->input->post('type');
        if (isset($local_level) && !empty($local_level)) {
            $local_levelRow = $this->faddress->get_by(array('name' => $local_level, 'level' => 'Local Level'));

            if ($local_levelRow) {
                $wardnoSelect = $this->browse_get_select_box_find_parent($local_level, 'Ward No', 'name', 'browseward_no', $type);
                $data = array(
                    'status' => 'success',
                    'selectWordNo' => $wardnoSelect,
                    'selectAddress' => $this->browse_resetter('Address', 'browseaddress', $type),
                );
            } else {
                //IF NO SUCH COUNTRY FOUND
                $data = array(
                    'status' => 'error',
                    'selectWordNo' => $this->browse_resetter('Ward No', 'browseward_no', $type),
                    'selectAddress' => $this->browse_resetter('Address', 'browseaddress', $type),
                    'msg' => 'No such data found'
                );
            }
        } else {
            //NO COUNTRY SLECTED 
            $data = array(
                'status' => 'error',
                'selectWordNo' => $this->browse_resetter('Ward No', 'browseward_no', $type),
                'selectAddress' => $this->browse_resetter('Address', 'browseaddress', $type),
                'msg' => 'Please select local level'
            );
        }
        echo json_encode($data);
        exit();
    }

    //ADVANCE SEARCH ======================================================
    public function adv_get_select_box($parentID, $level, $field, $name, $type) {
        $rows = $this->faddress->order_by('position', 'ASC')->get_many_by(array('parent_ID' => $parentID, 'level' => $level));
        $nameID = $name . '-' . $type;
        $output = '<label>' . $level . '</label><div class="drop_list"><select class="form-control ' . $name . '" name="' . $nameID . '" id="' . $nameID . '"  data-type="' . $type . '">';
        $output .= '<option selected value="0">Select One</option>';
        foreach ($rows as $row):
            $output .= '<option value="' . $row->$field . '">' . $row->$field . '</option>';
        endforeach;
        $output .= '</select></div>';
        return $output;
    }

    //BY FINDING THE PARENT for BROWSE DATA
    public function adv_get_select_box_find_parent($parent_name, $level, $field, $name, $type) {

        $parentLevel = array(
            'Province' => 'Country',
            'District' => 'Province',
            'Local Level' => 'District',
            'Ward No' => 'Local Level',
            'Address' => 'Ward No'
        );

        $parentRow = $this->faddress->get_by(array('name' => $parent_name, 'level' => $parentLevel[$level]));
        if ($parentRow) {
            $rows = $this->faddress->order_by('position', 'ASC')->get_many_by(array('parent_ID' => $parentRow->ID, 'level' => $level));
            $nameID = $name . '-' . $type;
            $output = '<label>' . ucfirst($level) . '</label><div class="drop_list"><select class="form-control ' . $name . '" name="' . $nameID . '" id="' . $nameID . '" data-type="' . $type . '">';
            $output .= '<option selected value="0">Select One</option>';
            foreach ($rows as $row):
                $output .= '<option value="' . $row->$field . '">' . $row->$field . '</option>';
            endforeach;
            $output .= '</select></div>';
            return $output;
        } else {
            $nameID = $name . '-' . $type;
            //NO PARENT LEVEL FOUND
            $output = '<label>' . ucfirst($level) . '</label><div class="drop_list"><select class="form-control ' . $name . '" name="' . $nameID . '" id="' . $nameID . '">';
            $sel = '';
            $output .= '<option selected value="0">Select One</option>';
            $output .= '</select></div>';
            return $output;
        }
    }

    //MAKING SELECT RESETTIR
    public function adv_resetter($lebel, $name, $type) {
        $nameID = $name . '-' . $type;
        $output = '<label>' . ucfirst($lebel) . '</label><div class="drop_list"><select class="form-control ' . $name . '" name="' . $nameID . '" id="' . $nameID .'" data-type="' . $type . '">';
        $sel = '';
        $output .= '<option selected value="0">Select One</option>';
        $output .= '</select></div>';
        return $output;
    }

    public function adv_change_country() {
        $country = $this->input->post('country');
        $type = $this->input->post('type');
        if (isset($country) && !empty($country)) {
            $countryRow = $this->faddress->get_by(array('name' => $country, 'level' => 'Country'));

            if ($countryRow) {
                $provinceSelect = $this->adv_get_select_box_find_parent($country, 'Province', 'name', 'advprovince', $type);

                $data = array(
                    'status' => 'success',
                    'resultSelect' => $provinceSelect,
                    'selectDistrict' => $this->adv_resetter('District', 'advdistrict', $type),
                    'selectLocalLevel' => $this->adv_resetter('Local Level', 'advlocal_level', $type),
                    'selectWordNo' => $this->adv_resetter('Ward No', 'advward_no', $type),
                    'selectAddress' => $this->adv_resetter('Address', 'advaddress', $type),
                );
            } else {
                //IF NO SUCH COUNTRY FOUND
                $data = array(
                    'status' => 'error',
                    'selectProvince' => $this->adv_resetter('Province', 'advprovince', $type),
                    'selectDistrict' => $this->adv_resetter('District', 'advdistrict', $type),
                    'selectLocalLevel' => $this->adv_resetter('Local Level', 'advlocal_level', $type),
                    'selectWordNo' => $this->adv_resetter('Ward No', 'advward_no', $type),
                    'selectAddress' => $this->adv_resetter('Address', 'advaddress', $type),
                    'msg' => 'No such data found'
                );
            }
        } else {
            //NO COUNTRY SLECTED 
            $data = array(
                'status' => 'error',
                'selectProvince' => $this->adv_resetter('Province', 'advprovince', $type),
                'selectDistrict' => $this->adv_resetter('District', 'advdistrict', $type),
                'selectLocalLevel' => $this->adv_resetter('Local Level', 'advlocal_level', $type),
                'selectWordNo' => $this->adv_resetter('Ward No', 'advward_no', $type),
                'selectAddress' => $this->adv_resetter('Address', 'advaddress', $type),
                'msg' => 'Please select country'
            );
        }
        echo json_encode($data);
        exit();
    }

    //PROVINCE CHANGE BROSE
    public function adv_change_province() {
        $province = $this->input->post('province');
        $type = $this->input->post('type');
        if (isset($province) && !empty($province)) {
            $provinceRow = $this->faddress->get_by(array('name' => $province, 'level' => 'Province'));

            if ($provinceRow) {
                $districtSelect = $this->adv_get_select_box_find_parent($province, 'District', 'name', 'advdistrict', $type);
                $data = array(
                    'status' => 'success',
                    'selectDistrict' => $districtSelect,
                    'selectLocalLevel' => $this->adv_resetter('Local Level', 'advlocal_level', $type),
                    'selectWordNo' => $this->adv_resetter('Ward No', 'advward_no', $type),
                    'selectAddress' => $this->adv_resetter('Address', 'advaddress', $type),
                );
            } else {
                //IF NO SUCH COUNTRY FOUND
                $data = array(
                    'status' => 'error',
                    'selectDistrict' => $this->adv_resetter('District', 'advdistrict', $type),
                    'selectLocalLevel' => $this->adv_resetter('Local Level', 'advlocal_level', $type),
                    'selectWordNo' => $this->adv_resetter('Ward No', 'advward_no', $type),
                    'selectAddress' => $this->adv_resetter('Address', 'advaddress', $type),
                    'msg' => 'No such data found'
                );
            }
        } else {
            //NO COUNTRY SLECTED 
            $data = array(
                'status' => 'error',
                'selectDistrict' => $this->adv_resetter('District', 'advdistrict', $type),
                'selectLocalLevel' => $this->adv_resetter('Local Level', 'advlocal_level', $type),
                'selectWordNo' => $this->adv_resetter('Ward No', 'advward_no', $type),
                'selectAddress' => $this->adv_resetter('Address', 'advaddress', $type),
                'msg' => 'Please select province'
            );
        }
        echo json_encode($data);
        exit();
    }

    //DISTRICT CHANGE
    public function adv_change_district() {
        $district = $this->input->post('district');
        $type = $this->input->post('type');
        if (isset($district) && !empty($district)) {
            $districtRow = $this->faddress->get_by(array('name' => $district, 'level' => 'District'));

            if ($districtRow) {
                $localLevelSelect = $this->adv_get_select_box_find_parent($district, 'Local Level', 'name', 'advlocal_level', $type);
                $data = array(
                    'status' => 'success',
                    'selectLocalLevel' => $localLevelSelect,
                    'selectWordNo' => $this->adv_resetter('Ward No', 'advward_no', $type),
                    'selectAddress' => $this->adv_resetter('Address', 'advaddress', $type),
                );
            } else {
                //IF NO SUCH COUNTRY FOUND
                $data = array(
                    'status' => 'error',
                    'selectLocalLevel' => $this->adv_resetter('Local Level', 'advlocal_level', $type),
                    'selectWordNo' => $this->adv_resetter('Ward No', 'advward_no', $type),
                    'selectAddress' => $this->adv_resetter('Address', 'advaddress', $type),
                    'msg' => 'No such data found'
                );
            }
        } else {
            //NO COUNTRY SLECTED 
            $data = array(
                'status' => 'error',
                'selectLocalLevel' => $this->adv_resetter('Local Level', 'advlocal_level', $type),
                'selectWordNo' => $this->adv_resetter('Ward No', 'advward_no', $type),
                'selectAddress' => $this->adv_resetter('Address', 'advaddress', $type),
                'msg' => 'Please select district'
            );
        }
        echo json_encode($data);
        exit();
    }

    //LOCAL LEVEL CHNAGE
    public function adv_change_local_level() {
        $local_level = $this->input->post('local_level');
        $type = $this->input->post('type');
        if (isset($local_level) && !empty($local_level)) {
            $local_levelRow = $this->faddress->get_by(array('name' => $local_level, 'level' => 'Local Level'));

            if ($local_levelRow) {
                $wardnoSelect = $this->adv_get_select_box_find_parent($local_level, 'Ward No', 'name', 'advward_no', $type);
                $data = array(
                    'status' => 'success',
                    'selectWordNo' => $wardnoSelect,
                    'selectAddress' => $this->adv_resetter('Address', 'advaddress', $type),
                );
            } else {
                //IF NO SUCH COUNTRY FOUND
                $data = array(
                    'status' => 'error',
                    'selectWordNo' => $this->adv_resetter('Ward No', 'advward_no', $type),
                    'selectAddress' => $this->adv_resetter('Address', 'advaddress', $type),
                    'msg' => 'No such data found'
                );
            }
        } else {
            //NO COUNTRY SLECTED 
            $data = array(
                'status' => 'error',
                'selectWordNo' => $this->adv_resetter('Ward No', 'advward_no', $type),
                'selectAddress' => $this->adv_resetter('Address', 'advaddress', $type),
                'msg' => 'Please select local level'
            );
        }
        echo json_encode($data);
        exit();
    }

}
