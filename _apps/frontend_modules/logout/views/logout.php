
<h1>You are logged out!</h1>
<div class="row">
    <div class="col-md-12">
        <div class="blue-bg standard-padding round-border">
            <p>Your session has been successfully logged out. Thank you for your precious time.</p>
            <p><a href="<?php echo site_url('login') ?>">Login</a> in again or back to <a href="<?php echo site_url() ?>">home page</a></p>
        </div>

    </div>
</div>
