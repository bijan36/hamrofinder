<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Student_marks extends Frontendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('Student_marks_m', 'fstudentmarks');
    }

    //USER DASHBOARD
    public function index() {
        redirect('site');
        exit();
    }

    //ADDING THE MARKS
    public function add() {

        //SOME FORM VALIDATION HERE
        $this->form_validation->set_rules('id', 'Valid data', 'required|is_natural_no_zero');
        $this->form_validation->set_rules('subject', 'Subject', 'required');
        $this->form_validation->set_rules('score', 'Score', 'required|numeric');
        $this->form_validation->set_rules('level', 'Level', 'required');
        $this->form_validation->set_rules('prefix', 'prefix', 'required');

        if ($this->form_validation->run() == TRUE) {

            $id = $this->input->post('id');
            $subject = $this->input->post('subject');
            $score = $this->input->post('score');
            $level = $this->input->post('level');
            $prefix = $this->input->post('prefix');

            if (!empty($id) && !empty($subject) && !empty($score) && !empty($level) && !empty($prefix)) {
                $row = $this->fstudentmarks->get_by(array('subject' => $subject, 'level' => $level, 'parent_ID' => $id));
                if (!$row) {

                    $dataArray = array(
                        'parent_ID' => $id,
                        'level' => $level,
                        'subject' => $subject,
                        'score' => $score
                    );

                    if ($this->fstudentmarks->insert($dataArray)) {
                        $lastID = $this->fstudentmarks->get_last_id();
                        $data = array(
                            'status' => 'success',
                            'newsubject' => "<tr><td>" . $subject . "</td><td>" . $score . "</td><td><a href='#' data-id='" . $lastID . "' class='removeScore remove-btn' data-prefix='" . $prefix . "error'><i class='fa fa-times fa-fw'></i></a></td></tr>"
                        );
                        echo json_encode($data);
                        exit();
                    } else {
                        $data = array(
                            'status' => 'error',
                            'msg' => 'Unable to add subject and marks please try again'
                        );
                        echo json_encode($data);
                        exit();
                    }
                } else {
                    $data = array(
                        'status' => 'error',
                        'msg' => 'The subject you are trying to add already added please try different subject'
                    );
                    echo json_encode($data);
                    exit();
                }
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => 'Unable to add with blank fields!'
                );
                echo json_encode($data);
                exit();
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => validation_errors()
            );
            echo json_encode($data);
            exit();
        }
    }

    public function remove() {
        $id = $this->input->post('id');
        if (!empty($id)) {
            $row = $this->fstudentmarks->get($id);
            if ($row) {
                if ($this->fstudentmarks->delete($id)) {
                    $data = array(
                        'status' => 'success',
                    );
                    echo json_encode($data);
                    exit();
                } else {
                    $data = array(
                        'status' => 'error',
                        'msg' => 'Unable to remove it!'
                    );
                    echo json_encode($data);
                    exit();
                }
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => 'Nothing found to remove!!'
                );
                echo json_encode($data);
                exit();
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => 'Unable to remove it'
            );
            echo json_encode($data);
            exit();
        }
    }

    //GET SAFE DATA BY USER ID
    public function get_row($ID) {
        $userRow = $this->fstudentmarks->get($ID);
        return $userRow ? $userRow : false;
    }

    //GET SAFE DATA BY USER PARENT
    public function get_by_parent_level($parent_ID, $level) {
        $userRows = $this->fstudentmarks->get_many_by(array('parent_ID' => $parent_ID, 'level' => $level));
        return $userRows ? $userRows : false;
    }

}
