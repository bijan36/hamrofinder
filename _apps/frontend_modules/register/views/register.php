<div class="row">
    <div class="col-md-6">
        <div class="login-cover  wow bounceInLeft">
            <div class="login-cover-images">
                <?php if (isset($logout)) { ?>
                    <img src="<?php echo base_url() ?>_public/front/assets/img/logout.jpg" class="img-responsive">
                <?php } else { ?>
                    <img src="<?php echo base_url() ?>_public/front/assets/img/register-cover.jpg" class="img-responsive">
                <?php } ?>
            </div>
            <div class="login-cover-title">
                <?php if (isset($logout)) { ?>
                    <h1>Thanks for stopping by!</h1>
                    <p>
                        We hope to see you again soon.
                    </p>
                <?php } else { ?>
                    <h1>Select your college / students.</h1>
                    <p>
                        Subscribe us to enhance your study and business.
                    </p>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div id="loginBox" class="standard-padding wow pulse">
            <h1><i class="fa fa-user-plus fa-fw"></i> User Registration</h1>
            <div class="row">
                <?php echo form_open('register/register_check', array('id' => 'registeration-form')) ?>
                <div class="col-md-12">
                    <div class="showMsg"></div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="searchtype">I am:</label>
                        <select name="searchtype" id="searchtypereg" class="form-control" onchange="twisterlebal(this);">
                            <option value="<?php echo COLLEGE; ?>"><?php echo strtoupper(COLLEGE); ?></option>
                            <option value="<?php echo STUDENT; ?>" selected><?php echo strtoupper(STUDENT); ?></option>
                            <option value="<?php echo INSTITUTE; ?>" ><?php echo strtoupper(INSTITUTE); ?></option>
                        </select>
                    </div>
                </div>    
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="name" id="label_name">Full Name:</label>
                        <input type="text" name="name" class="form-control" value="" placeholder="eg. Name Surname">
                    </div>
                </div>
                
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="contact_no">Contact No.:</label>
                        <input type="text" name="contact_no" class="form-control" value="" placeholder="eg.+977-1-XXXXXXX">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="emailAddress">Email Address:</label>
                        <input type="email" name="emailAddress" class="form-control" value="">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="passCode">Create New Password:</label>
                        <input type="password" name="password" class="form-control" value="">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="passCodeConfirm">Re-type New Password:</label>
                        <input type="password" name="passCodeConfirm" class="form-control" value="">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <button type="submit" class="blueButton">Register</button>
                    </div>
                </div>
                <?php
                echo form_close();
                ?>
                <div class="col-md-12">
                    <span style="margin-top: 15px; display: block; font-size: 15px;">
                        Already have an account with us? <a href="<?php echo site_url('login'); ?>" ><i class="fa fa-sign-in fa-fw"></i> Login Here</a>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>