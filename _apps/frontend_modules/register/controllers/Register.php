<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends Frontendcontroller {

    public function __construct() {
        parent::__construct();
        if (is_user_login()) {
            redirect('dashboard');
            exit();
        }
    }

    //DEFAULT TO DASHBOARD
    public function index() {
        $this->template->load(get_template('default'), 'register');
    }

    //USER REGISTER CHECK POINT
    public function register_check() {

        //FORM VALIDATION
        $this->form_validation->set_rules('searchtype', 'Search type', 'required|in_list[student,college,institute]');
        $this->form_validation->set_rules('name', 'Name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('contact_no', 'Contact No', 'required|trim|xss_clean');
        $this->form_validation->set_rules('emailAddress', 'Email Address', 'required|valid_email');
        //$this->form_validation->set_rules('password', 'Password', 'required|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|max_length[30]', array('required' => 'You must provide a %s.')
        );
        $this->form_validation->set_rules('passCodeConfirm', 'Confirm Password', 'required|trim|matches[password]', array(
            'required' => 'Confirm password is required',
            'matches' => 'Confirm password is not matched'
                )
        );

        if ($this->form_validation->run() == TRUE) {
            //REGISTER HERE
            $user = Modules::run('user/is_exist', $this->input->post('emailAddress'));
            if ($user) {
                $data = array(
                    'status' => 'error',
                    'msg' => 'You are already registered with us. Please login to continue.'
                );
            } else {

                $searchType = $this->input->post('searchtype');
                //$type = $searchType == 'student' ? 'College' : 'Student';
                //$type = $searchType;
                //DEFAULT LEVEL
                $level = $searchType == 'student' ? 'Registered' : 'Registered'; //start user level as Registered in both way

                $rowPassword = $this->input->post('password');
                $code = get_hash();

                $name = $this->input->post('name');
                $email = $this->input->post('emailAddress');


                $dataRegisted = array(
                    'type' => $searchType,
                    'name' => $name,
                    'contact' => $this->input->post('contact_no'),
                    'email' => $this->input->post('emailAddress'),
                    'level' => $level,
                    'join_date' => get_date_time(),
                    'last_IP' => $this->input->ip_address(),
                    'passcode' => Modules::run('user/makepassword', $rowPassword),
                    'code' => $code
                );

                $reg = Modules::run('user/get_registered', $dataRegisted);

                if ($reg) {
                    //LETS DO EMAIL TO USER
                    $data['to'] = $email;
                    $data['toName'] = $name;
                    $data['subject'] = 'hamrofinder.com user verification';
                    $data['template'] = 'user_registration_activation_email';
                    $data['others'] = array(
                        'userfullname' => $name,
                        'emailaddress' => $email,
                        'password' => $rowPassword,
                        'activation_url' => site_url('user/activate/' . $reg . '/' . $code)
                    );
                    Modules::run('emailsystem/doEmail', $data);
                    //LETS DO EMAIL TO USER

                    $data = array(
                        'status' => 'success',
                        'msg' => 'Congratulations! ' . $name . '<br/>Please verify our verification email soon. Please do check Junk/Spam folder. First emails sometimes hide there.'
                    );
                } else {
                    $data = array(
                        'status' => 'error',
                        'msg' => 'Unable to register now, Please try again later'
                    );
                }
            }
        } else {
            //FORM VALIDAITON ERROR
            $data = array(
                'status' => 'error',
                'msg' => validation_errors()
            );
        }

        echo json_encode($data);
        exit();
    }

}
