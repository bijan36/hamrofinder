<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sidebar extends Frontendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: SIDEBAR CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('sidebar_m', 'fsidebars');
    }

    //sidebar
    public function index() {
        redirect('site');
        exit();
    }

    //getting FRONT PAGE VERTICAL SIDBAR
    public function get_vertical_sidebar($ID) {
        $sidebarContent = $this->fsidebars->get($ID);
        if (!empty($sidebarContent->content)) {
            $parts = explode('[sep]', $sidebarContent->content);
        }

        if (!empty($parts)) {
            foreach ($parts as $part):
                $fistLetter = substr($part, 0, 4);
                if ($fistLetter == 'http') {
                    $anotherParts = explode('[url]', $part);
                    $link = $anotherParts[1];
                    if ($link == '#') {
                        echo '<div class="ad-box"><img src="' . $anotherParts[0] . '" class="img-responsive"></div>';
                    } else {
                        echo '<div class="ad-box"><a href="' . $anotherParts[1] . '" target="_blank"><img src="' . $anotherParts[0] . '" class="img-responsive"></a></div>';
                    }
                } else {
                    echo '<div class="ad-text">' . $part . '</div>';
                }
            endforeach;
        }
    }

    //getting FRONT PAGE HORIZONTAL SIDBAR
    public function get_horizontal_sidebar($ID) {
        $sidebarContent = $this->fsidebars->get($ID);
        if (!empty($sidebarContent->content)) {
            $parts = explode('[sep]', $sidebarContent->content);
        }

        if (!empty($parts)) {
            echo '<div class="row">';
            foreach ($parts as $part):
                $fistLetter = substr($part, 0, 4);
                if ($fistLetter == 'http') {
                    $anotherParts = explode('[url]', $part);
                    $link = $anotherParts[1];
                    if ($link == '#') {
                        echo '<div class="col-md-4"><div class="ad-box"><img src="' . $anotherParts[0] . '" class="img-responsive"></div></div>';
                    } else {
                        echo '<div class="col-md-4"><div class="ad-box"><a href="' . $anotherParts[1] . '" target="_blank"><img src="' . $anotherParts[0] . '" class="img-responsive"></a></div></div>';
                    }
                } else {
                    echo '<div class="col-md-4"><div class="ad-text-horizontal">' . $part . '</div></div>';
                }
            endforeach;
            echo '</div>';
        }
    }

    //getting FRONT PAGE HORIZONTAL SIDBAR
    public function get_horizontal_sidebar_front($ID) {
        $sidebarContent = $this->fsidebars->get($ID);
        if (!empty($sidebarContent->content)) {
            $parts = explode('[sep]', $sidebarContent->content);
        }

        if (!empty($parts)) {
            echo '<div class="row">';
            $i=1;
            foreach ($parts as $part):
                $fistLetter = substr($part, 0, 4);

                if ($i % 2 == 0) {
                    //even
                    if ($fistLetter == 'http') {
                        $anotherParts = explode('[url]', $part);
                        $link = $anotherParts[1];
                        if ($link == '#') {
                            echo '<div class="col-md-12"><div class="ad-box"><img src="' . $anotherParts[0] . '" class="img-responsive"></div></div>';
                        } else {
                            echo '<div class="col-md-12"><div class="ad-box"><a href="' . $anotherParts[1] . '" target="_blank"><img src="' . $anotherParts[0] . '" class="img-responsive"></a></div></div>';
                        }
                    } else {
                        echo '<div class="col-md-12"><div class="ad-text-horizontal">' . $part . '</div></div>';
                    }
                } else {
                    //odd
                    if ($fistLetter == 'http') {
                        $anotherParts = explode('[url]', $part);
                        $link = $anotherParts[1];
                        if ($link == '#') {
                            echo '<div class="col-md-12"><div class="ad-box"><img src="' . $anotherParts[0] . '" class="img-responsive"></div></div>';
                        } else {
                            echo '<div class="col-md-12"><div class="ad-box"><a href="' . $anotherParts[1] . '" target="_blank"><img src="' . $anotherParts[0] . '" class="img-responsive"></a></div></div>';
                        }
                    } else {
                        echo '<div class="col-md-12"><div class="ad-text-horizontal">' . $part . '</div></div>';
                    }
                }
                $i++;
            endforeach;
            echo '</div>';
        }
    }

}
