<div class="row">
    <div class="col-md-6">
        <div class="login-cover wow bounceInLeft">
            <div class="login-cover-images">
                <img src="<?php echo base_url() ?>_public/front/assets/img/login-cover.jpg" class="img-responsive">
            </div>
            <div class="login-cover-title">
                <h1>Select your college / students.</h1>
                <p>
                    Subscribe us to enhance your study and business.
                </p>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div id="loginBox" class="standard-padding wow pulse">
            <h1><i class="fa fa-magic fa-fw"></i> Forgot Password</h1>
            <div class="row">


                <?php echo form_open('recover/usernamehandeler', array('id' => 'userRecover')); ?>
                <div class="col-md-12 recover-box">
                    <div class="round-border">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="showMsg"></div>
                                <p>Type your email address below to recover your password.</p>
                                <div class="form-group">
                                    <label for="emailAddress">Your Email Address</label>
                                    <input type="email" name="emailAddress" class="form-control" value="">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="blueButton">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>

                <div class="col-md-12">
                    <span style="margin-top: 10px; display: block; font-size: 16px;">
                        Don’t have a hamrofinder account?  <a href="<?php echo site_url('register') ?>">Sign up now!</a>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>







