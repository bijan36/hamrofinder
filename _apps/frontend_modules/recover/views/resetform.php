<div class="row">
    <div class="col-md-6">
        <div class="login-cover wow bounceInLeft">
            <div class="login-cover-images">
                <img src="<?php echo base_url() ?>_public/front/assets/img/reset-password.jpg" class="img-responsive">
            </div>
            <div class="login-cover-title">
                <h1>Forgot password?</h1>
                <p>
                    Don't panic, just simply reset your new password.
                </p>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div id="loginBox" class="standard-padding wow pulse">
            <h1><i class="fa fa-magic fa-fw"></i> Reset your password</h1>
            <div class="row">


                <?php echo form_open('recover/passwordreset', array('id' => 'passReset')); ?>
                <div class="col-md-12 recover-box">
                    <div class="round-border">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="showMsg"></div>
                                <p>Type new password for your account.</p>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" name="password" class="form-control" value="">
                                </div>
                                <div class="form-group">
                                    <label for="passCodeConfirm">Retype Password</label>
                                    <input type="password" name="passCodeConfirm" class="form-control" value="">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="blueButton">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>

                <div class="col-md-12 hideAfter">
                    <span style="margin-top: 10px; display: block; font-size: 16px;">
                        Did you remember your password?  <a href="<?php echo site_url('login') ?>">Login Here</a>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>







