<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Recover extends Frontendcontroller {

    public function __construct() {
        parent::__construct();
        if (is_user_login()) {
            redirect('dashboard');
            exit();
        }
    }

    //DEFAULT
    public function index() {
        $this->template->load(get_template('default'), 'recover');
    }

    //HANDLE THE USERNAME
    public function usernamehandeler() {
        $this->form_validation->set_rules('emailAddress', 'Email Address', 'required|valid_email');
        if ($this->form_validation->run() == TRUE) {
            $email = $this->input->post('emailAddress');
            //CHECK THE EMAIL IS REGISTERED OR NOT
            $userRow = Modules::run('user/get_row_where', array('email' => $email));
            if ($userRow) {
                $status = $userRow->status;
                $name = $userRow->name;
                if ($status == ACTIVE) {

                    //GENERATING NEW KEY
                    $ID = $userRow->ID;
                    $code = get_hash();
                    $recoverURL = site_url('recover/changepassword/' . $ID . '/' . $code);

                    //UPDATING HASH
                    Modules::run('user/updateHash', $ID, $code);
                    //LETS DO EMAIL TO USER
                    $data['to'] = $email;
                    $data['toName'] = $name;
                    $data['subject'] = 'Password recover email.';
                    $data['template'] = 'user_password_recover';
                    $data['others'] = array(
                        'userfullname' => $name,
                        'emailaddress' => $email,
                        'recover_url' => $recoverURL
                    );
                    Modules::run('emailsystem/doEmail', $data);

                    $data = array(
                        'status' => 'success',
                        'msg' => 'A password recover link has been sent to your email(' . $email . '), please follow the process.'
                    );
                    echo json_encode($data);
                    exit();
                } else {
                    //EMAIL NOT FOUND IN DATABASE
                    $data = array(
                        'status' => 'error',
                        'msg' => 'Sorry we can not process password recovery for this email address.'
                    );
                    echo json_encode($data);
                    exit();
                }
            } else {
                //FORM VALIDAITON ERROR
                $data = array(
                    'status' => 'error',
                    'msg' => 'Sorry we can not process password recovery for this email address.'
                );
                echo json_encode($data);
                exit();
            }
        } else {
            //FORM VALIDAITON ERROR
            $data = array(
                'status' => 'error',
                'msg' => validation_errors()
            );
            echo json_encode($data);
            exit();
        }
    }

    //RECOVER HERE
    public function changepassword($ID, $hash) {
        if (isset($ID) && isset($hash) && !empty($ID) && !empty($hash)) {
            $ID = $ID;
            $hash = $hash;

            $userRow = Modules::run('user/get_row_code', $ID);
            if ($userRow) {
                //IF CODE MATCHES
                if ($userRow->code == $hash) {
                    //LETS SAVE ID IN SESSION
                    $this->session->set_userdata(array('recoverID' => $userRow->ID));
                    //CHANGE PASSWORD FORM GOES HERE
                    $this->template->load(get_template('default'), 'resetform');
                } else {
                    //IF CODE NOT MATCHED ASSUMED TO BE LINK EXPIRED
                    //LINK EXPIRED OR BROKEN
                    $this->template->load(get_template('default'), 'front/templates/link-expired');
                }
            } else {
                redirect('notfound');
                exit();
            }
        } else {
            redirect('notfound');
            exit();
        }
    }

    //RECOVER PROCESS HERE
    public function passwordreset() {

        //CHECK FOR SESSION ID
        if (!$this->session->userdata('recoverID')) {
            //FORM VALIDAITON ERROR
            $data = array(
                'status' => 'error',
                'msg' => 'Sorry we can not process password recovery. Please contact site administration.'
            );
            echo json_encode($data);
            exit();
        }


        //LETS VALIDATE FORM
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|max_length[30]', array('required' => 'You must provide a %s.')
        );
        $this->form_validation->set_rules('passCodeConfirm', 'Confirm Password', 'required|trim|matches[password]', array(
            'required' => 'Confirm password is required',
            'matches' => 'Confirm password is not matched'
                )
        );
        if ($this->form_validation->run() == TRUE) {
            $resetID = $this->session->userdata('recoverID');
            //CHECK THE EMAIL IS REGISTERED OR NOT
            $userRow = Modules::run('user/get_row', $resetID);
            if ($userRow) {
                $status = $userRow->status;
                if ($status == ACTIVE) {
                    $password = $this->input->post('password');
                    $enPassword = Modules::run('user/makepassword', $password);

                    $name = $userRow->name;
                    $email = $userRow->email;
                    //GENERATING NEW KEY
                    $code = get_hash();
                    $ID = $userRow->ID;


                    //UPDATING HASH & PASSWORD
                    Modules::run('user/updateHashPass', $ID, $code, $enPassword);
                    //LETS DO EMAIL TO USER
                    $data['to'] = $email;
                    $data['toName'] = $name;
                    $data['subject'] = 'Congratulations! your password has been changed.';
                    $data['template'] = 'user_password_change';
                    $data['others'] = array(
                        'userfullname' => $name,
                        'emailaddress' => $email,
                        'NewPassword' => $password
                    );
                    Modules::run('emailsystem/doEmail', $data);

                    $data = array(
                        'status' => 'success',
                        'msg' => 'Congratulations! your password has been changed.<br/><a href="' . site_url('login') . '">Click here</a> to login your account.'
                    );
                    echo json_encode($data);
                    exit();
                } else {
                    //EMAIL NOT FOUND IN DATABASE
                    $data = array(
                        'status' => 'error',
                        'msg' => 'Sorry we can not process password recovery for this email address.'
                    );
                    echo json_encode($data);
                    exit();
                }
            } else {
                //FORM VALIDAITON ERROR
                $data = array(
                    'status' => 'error',
                    'msg' => 'Sorry we can not process password recovery for this email address.'
                );
                echo json_encode($data);
                exit();
            }
        } else {
            //FORM VALIDAITON ERROR
            $data = array(
                'status' => 'error',
                'msg' => validation_errors()
            );
            echo json_encode($data);
            exit();
        }
    }

}
