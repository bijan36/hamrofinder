<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Emailsettings extends Frontendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('Emailsettings_m', 'fesettings');
    }

    //USER DASHBOARD
    public function index() {
        redirect('site');
        exit();
    }

    //GETTING VALUES
    public function get_value($slug) {
        if ($slug) {
            $row = $this->fesettings->get_by(array('slug' => $slug));
            if ($row) {
                return $row->val;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}
