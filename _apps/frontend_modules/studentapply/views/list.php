<?php
if (!empty($row)) {
    ?>
    <div class="offer-box">
        <div class="row">
            <div class="col-md-1">
                <?php
                $studentRow = Modules::run('user/get_row', $row->student_ID);
                ?>
                <a href="<?php echo profile_url($studentRow->ID) ?>" title="<?php echo $studentRow->name; ?>">
                    <?php
                    $proImgID = $studentRow->profile_picture ? $studentRow->profile_picture : '0';
                    $profile_image = Modules::run('imagehub/get_profile_image', $proImgID);
                    echo $profile_image;
                    ?>
                </a>
            </div>
            <div class="col-md-11">
                <a href="<?php echo profile_url($studentRow->ID) ?>" title="<?php echo $studentRow->name; ?>">
                    <?php echo $studentRow->name; ?>
                </a>
                <br/>
                <small><?php echo Modules::run('user/get_full_address', $studentRow->ID); ?></small>
            </div>
            <div class="col-md-12">
                <?php echo $row->message; ?>
            </div>
        </div>

    </div>
    <?php
}
?>