<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Studentapply extends Frontendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('studentapply_m', 'fstapply');
    }

    //USER DASHBOARD
    public function index() {
        redirect('site');
        exit();
    }

    //validating apply now btn here
    public function validate_apply() {
        $adID = $this->input->post('id');

        //CHECK USER LOGIN
        if (is_user_login()) {
            $studentID = get_user_ID();

            //FIND THE USER TYPE
            $userType = $this->session->userdata('userType');

            //IF THE USER TYPE IS MATCHED NOT ALLOW TO INTEREST THEM
            if ($userType == 'College' || $userType == 'Institute') {
                $data = array(
                    'status' => 'error',
                    'msg' => INVALIDMSG,
                    'do' => 'sametype'
                );
                echo json_encode($data);
                exit();
            } else {
                $idParts = explode('-', $adID);
                $adID = $idParts[0];
                $adType = $idParts[1];

                if ($adType == 'College') {

                    //FINDING COLLEGE AD ROW
                    $collegeAdRow = Modules::run('collegead/get_row', $adID);
                    if ($collegeAdRow) {
                        if ($collegeAdRow->status == ACTIVE) {
                            $collegeID = $collegeAdRow->parent_ID;
                            $userRow = Modules::run('user/get_row_where', array('ID' => $collegeID, 'type' => 'College', 'status' => ACTIVE));
                            if ($userRow) {
                                $data = array(
                                    'status' => 'success',
                                    'toApply' => $userRow->name . ' (College)',
                                    'adID' => $adID,
                                    'adType' => $adType
                                );
                                echo json_encode($data);
                                exit();
                            } else {
                                $data = array(
                                    'status' => 'error',
                                    'msg' => INVALIDMSG,
                                    'do' => 'nothing'
                                );
                                echo json_encode($data);
                                exit();
                            }
                        } else {
                            $data = array(
                                'status' => 'error',
                                'msg' => 'Can not apply to this ad.',
                                'do' => 'nothing'
                            );
                            echo json_encode($data);
                            exit();
                        }
                    } else {
                        $data = array(
                            'status' => 'error',
                            'msg' => 'No such ads found for apply.',
                            'do' => 'nothing'
                        );
                        echo json_encode($data);
                        exit();
                    }
                } elseif ($adType == 'Institute') {

                    //FINDING INSTITUTE AD ROW
                    $instAdRow = Modules::run('institutead/get_row', $adID);
                    if ($instAdRow) {
                        if ($instAdRow->status == ACTIVE) {
                            $insID = $instAdRow->parent_ID;
                            $userRow = Modules::run('user/get_row_where', array('ID' => $insID, 'type' => 'Institute', 'status' => ACTIVE));
                            if ($userRow) {
                                $data = array(
                                    'status' => 'success',
                                    'toApply' => $userRow->name . ' (Institute)',
                                    'adID' => $adID,
                                    'adType' => $adType
                                );
                                echo json_encode($data);
                                exit();
                            } else {
                                $data = array(
                                    'status' => 'error',
                                    'msg' => INVALIDMSG,
                                    'do' => 'nothing'
                                );
                                echo json_encode($data);
                                exit();
                            }
                        } else {
                            $data = array(
                                'status' => 'error',
                                'msg' => 'Can not apply to this ad.',
                                'do' => 'nothing'
                            );
                            echo json_encode($data);
                            exit();
                        }
                    } else {
                        $data = array(
                            'status' => 'error',
                            'msg' => 'No such ads found for apply.',
                            'do' => 'nothing'
                        );
                        echo json_encode($data);
                        exit();
                    }
                } else {
                    $data = array(
                        'status' => 'error',
                        'msg' => 'No such ads found for apply.',
                        'do' => 'nothing'
                    );
                    echo json_encode($data);
                    exit();
                }
            }
        } else {
            //REDIRECT TO LOGIN PAGE
            $data = array(
                'status' => 'error',
                'msg' => 'To apply, please register or login.',
                'do' => 'login'
            );
            echo json_encode($data);
            exit();
        }
    }

    //MAKE APPLY STUFF WITH EMAILS NOTIFICTIONS
    public function make_apply() {
        $adID = $this->input->post('adID');
        $adType = $this->input->post('adType');
        $message = $this->input->post('message');

        if (isset($adID) && !empty($adID) && $adID > 0) {
            if (isset($message) && !empty($message)) {

                if ($adType == 'College') {

                    $collegeAdRow = Modules::run('collegead/get_row', $adID);
                    //IF AD ROW
                    if ($collegeAdRow && $collegeAdRow->status == ACTIVE) {

                        $collegeRow = Modules::run('user/get_row', $collegeAdRow->parent_ID);
                        //SAVAING OFFER TO DATABASE
                        $dataSave = array(
                            'ad_ID' => $adID,
                            'college_ID' => $collegeAdRow->parent_ID,
                            'student_ID' => $this->session->userdata('userID'),
                            'message' => $message,
                            'ipaddress' => $this->input->ip_address(),
                            'timedate' => get_date_time()
                        );

                        if ($this->fstapply->insert($dataSave)) {
                            $studentName = $this->session->userdata('userName');
                            $studentEmail = $this->session->userdata('userEmail');
                            $collegeName = $collegeRow->name;
                            $collegeEmail = $collegeRow->email;
                            //EMAIL NOTIFICTION TO COLLEGE
                            $data['to'] = $collegeRow->email;
                            $data['toName'] = $collegeRow->name;
                            $data['subject'] = 'An application has been reveived from ' . $studentName . ' (' . $studentEmail . ')';
                            $data['template'] = 'college_apply_notification';
                            $data['others'] = array(
                                'userfullname' => $collegeName,
                                'emailaddress' => $collegeEmail,
                                'applicationmsg' => $message,
                                'studentname' => $studentName,
                                'studentemail' => $studentEmail
                            );
                            Modules::run('emailsystem/doEmail', $data);

                            //EMAIL NOTIFICTION TO WEBSITE ADMINISTRATOR
                            $dataa['to'] = admin_notification_email();
                            $dataa['toName'] = admin_notification_email_name();
                            $dataa['subject'] = $studentName . ' did application message to ' . $collegeName;
                            $dataa['template'] = 'admin_apply_notification';
                            $dataa['others'] = array(
                                'studentName' => $studentName,
                                'studentEmail' => $studentEmail,
                                'collegeName' => $collegeName,
                                'collegeEmail' => $collegeEmail,
                                'applicationmsg' => $message,
                                'adID' => $adID
                            );
                            Modules::run('emailsystem/doEmail', $dataa);

                            //EMAIL NOTIFICTION TO STUDENT
                            $datac['to'] = $studentEmail;
                            $datac['toName'] = $studentName;
                            $datac['subject'] = 'You application has been sent to college successfully.';
                            $datac['template'] = 'student_apply_notification';
                            $datac['others'] = array(
                                'studentName' => $studentName,
                                'studentEmail' => $studentEmail,
                                'collegeName' => $collegeName,
                                'collegeEmail' => $collegeEmail,
                                'applicationmsg' => $message
                            );
                            Modules::run('emailsystem/doEmail', $datac);

                            $data = array(
                                'status' => 'success',
                                'msg' => 'Your offer message has been sent to student. Thank you.',
                            );
                            echo json_encode($data);
                            exit();
                        } else {
                            $data = array(
                                'status' => 'error',
                                'msg' => 'Sorry unable to post your offer message right now.',
                            );
                            echo json_encode($data);
                            exit();
                        }
                    } else {
                        $data = array(
                            'status' => 'error',
                            'msg' => 'Sorry unable to post your offer message right now.',
                        );
                        echo json_encode($data);
                        exit();
                    }


                    //IF THE AD TYPE IS INSTITUTE
                } elseif ($adType == 'Institute') {
                    $insAdRow = Modules::run('institutead/get_row', $adID);
                    //IF AD ROW
                    if ($insAdRow && $insAdRow->status == ACTIVE) {

                        $collegeRow = Modules::run('user/get_row', $insAdRow->parent_ID);
                        //SAVAING OFFER TO DATABASE
                        $dataSave = array(
                            'ad_ID' => $adID,
                            'college_ID' => $insAdRow->parent_ID,
                            'student_ID' => $this->session->userdata('userID'),
                            'message' => $message,
                            'ipaddress' => $this->input->ip_address(),
                            'timedate' => get_date_time()
                        );

                        if ($this->fstapply->insert($dataSave)) {
                            $studentName = $this->session->userdata('userName');
                            $studentEmail = $this->session->userdata('userEmail');
                            $collegeName = $collegeRow->name;
                            $collegeEmail = $collegeRow->email;
                            //EMAIL NOTIFICTION TO COLLEGE
                            $data['to'] = $collegeRow->email;
                            $data['toName'] = $collegeRow->name;
                            $data['subject'] = 'An application has been reveived from ' . $studentName . ' (' . $studentEmail . ')';
                            $data['template'] = 'college_apply_notification';
                            $data['others'] = array(
                                'userfullname' => $collegeName,
                                'emailaddress' => $collegeEmail,
                                'applicationmsg' => $message,
                                'studentname' => $studentName,
                                'studentemail' => $studentEmail
                            );
                            Modules::run('emailsystem/doEmail', $data);

                            //EMAIL NOTIFICTION TO WEBSITE ADMINISTRATOR
                            $dataa['to'] = admin_notification_email();
                            $dataa['toName'] = admin_notification_email_name();
                            $dataa['subject'] = $studentName . ' did application message to ' . $collegeName;
                            $dataa['template'] = 'admin_apply_notification';
                            $dataa['others'] = array(
                                'studentName' => $studentName,
                                'studentEmail' => $studentEmail,
                                'collegeName' => $collegeName,
                                'collegeEmail' => $collegeEmail,
                                'applicationmsg' => $message,
                                'adID' => $adID
                            );
                            Modules::run('emailsystem/doEmail', $dataa);

                            //EMAIL NOTIFICTION TO STUDENT
                            $datac['to'] = $studentEmail;
                            $datac['toName'] = $studentName;
                            $datac['subject'] = 'You application has been sent to college successfully.';
                            $datac['template'] = 'student_apply_notification';
                            $datac['others'] = array(
                                'studentName' => $studentName,
                                'studentEmail' => $studentEmail,
                                'collegeName' => $collegeName,
                                'collegeEmail' => $collegeEmail,
                                'applicationmsg' => $message
                            );
                            Modules::run('emailsystem/doEmail', $datac);

                            $data = array(
                                'status' => 'success',
                                'msg' => 'Your offer message has been sent to student. Thank you.',
                            );
                            echo json_encode($data);
                            exit();
                        } else {
                            $data = array(
                                'status' => 'error',
                                'msg' => 'Sorry unable to post your offer message right now.',
                            );
                            echo json_encode($data);
                            exit();
                        }
                    } else {
                        $data = array(
                            'status' => 'error',
                            'msg' => 'Sorry unable to post your offer message right now.',
                        );
                        echo json_encode($data);
                        exit();
                    }
                } else {
                    $data = array(
                        'status' => 'error',
                        'msg' => 'Your are not allowed to offer.',
                    );
                    echo json_encode($data);
                    exit();
                }
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => 'Please type your offer message and try submiting again.',
                );
                echo json_encode($data);
                exit();
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => 'Your are not allowed to offer.',
            );
            echo json_encode($data);
            exit();
        }
    }

    //GET OFFERS LIST BY AD ID
    public function get_apply_list_by_adID($adID) {
        $rows = $this->fstapply->get_many_by(array('ad_ID' => $adID));
        return $rows ? $rows : false;
    }

    //GET ROWS BY COLLEGE id
    public function get_apply_list_by_colID($colID) {
        $rows = $this->fstapply->get_many_by(array('college_ID' => $colID));
        return $rows ? $rows : false;
    }

    //GETTING TOTAL BY AD
    public function get_total_by_ad($adID, $type) {
        return $this->fstapply->count_by(array('ad_ID' => $adID, 'type' => $type));
    }

    //GET ROWS BY USER
    public function get_rows_by_user($userID) {
        $rows = $this->fstapply->get_many_by(array('interest_ID' => $userID));
        return $rows ? $rows : false;
    }

    //GET THE COLLEGE ROWS APPLIED BY STUDENT
    public function get_rows_by_student_ID($studnet_ID) {
        $rows = $this->fstapply->order_by('ID', 'DESC')->get_many_by(array('student_ID' => $studnet_ID));
        return $rows ? $rows : false;
    }

}
