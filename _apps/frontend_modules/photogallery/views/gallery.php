<div class="content">
    <h1>Photo Gallery</h1>
    <?php
    if (!empty($alldata)) {
        $galleries = explode(',', $alldata);
        foreach ($galleries as $gallery):
            ?>


            <?php $images = Modules::run('photogallery/get_gal_images', $gallery, 4); ?>
            <?php if ($images) { ?>
                <div class="row no-gutter big-gallery" style="margin-top: 10px; margin-bottom: 20px;">
                    <?php foreach ($images as $image): ?>

                        <div class="col-md-3">
                            <img src="<?php echo $image->url ?>" class="img-responsive">
                        </div>

                    <?php endforeach; ?>
                    <a href="<?php echo site_url('photogallery/' . $gallery) ?>">
                        <div class="gallery-overlay">
                            <?php echo $gallery; ?>
                        </div>
                    </a>
                </div>
                <?php
            }
            ?>



            <?php
        endforeach;
    } else {
        echo 'No image gallery found';
    }
    ?>
</div>