<div class="content">
    <h1><a href="<?php echo site_url('photogallery'); ?>">Photo Gallery</a> <i class="fa fa-angle-right fa-fw"></i> <?php echo $gallery_title; ?></h1>

    <?php
    if (!empty($gal_images)) {
        ?>
        <div class="row no-gutter" style="margin-top: 20px; margin-bottom: 20px;">
            <?php
            foreach ($gal_images as $gallery):
                ?>
                <?php if ($gallery->url) { ?>
                    <?php if ($gallery->type == 'Small') { ?>
                        <div class="col-md-6" style="margin-bottom:5px; max-height: 180px; overflow: hidden;">
                            <a class="group1" href="<?php echo $gallery->url ?>" title="<?php echo $gallery_title; ?>">
                                <img src="<?php echo $gallery->url ?>" class="img-responsive">
                            </a>
                        </div>
                        <?php
                    } else {
                        ?>
                        <div class="col-md-12" style="margin-bottom:5px;">
                            <a class="group1" href="<?php echo $gallery->url ?>" title="<?php echo $gallery_title; ?>">
                                <img src="<?php echo $gallery->url ?>" class="img-responsive">
                            </a>
                        </div>
                        <?php
                    }
                }
                ?>
                <?php
            endforeach;
            ?>
        </div>
        <?php
    } else {
        echo 'No image gallery found';
    }
    ?>
</div>