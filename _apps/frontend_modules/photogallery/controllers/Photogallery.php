<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Photogallery extends Frontendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: PHOTO GALLERY MODULES
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('photogallery_m', 'fgal');
    }

    //PHOTOGALLERY
    public function index($gallery=null) {
        if ($gallery) {
            $param = urldecode($gallery);
            $all_images = $this->get_gal_images($param);
            if ($all_images) {
                $data['gal_images'] = $all_images;
                $data['gallery_title'] = $param;
                $this->template->load('front/templates/gallery/right-sidebar-details', 'full_gallery', $data);
            } else {
                echo 'no images';
            }
        } else {
            $data['alldata'] = Modules::run('settings/get_value', 'gallery-category');
            $this->template->load('front/templates/gallery/right-sidebar', 'gallery', $data);
        }
    }

    //GET THE GALLERY IMAGES
    public function get_gal_images($cat, $limit = null) {
        if ($limit) {
            $rows = $this->fgal->order_by('position', 'DESC')->limit($limit)->get_many_by(array('Category' => $cat));
        } else {
            $rows = $this->fgal->order_by('position', 'DESC')->get_many_by(array('Category' => $cat));
        }
        return $rows ? $rows : false;
    }

}
