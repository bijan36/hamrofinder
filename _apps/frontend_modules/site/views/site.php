<section>
    <div class="container">
        <div class="main white-bg standard-padding margin-bottom-5">
            <div class="row">

                <?php echo form_open('search/search_ads', array('id' => 'search')); ?>
                <div class="col-md-8">
                    <div class="blue-bg standard-padding round-border">
                        <div class="hero-text standard-padding text-center blue-text">
                            Find Colleges / Students / Institutes
                        </div>
                        <div class="text-center standard-padding" style="padding-top:0px; padding-bottom:0px;">
                            <span class="big-text"><input type="radio" name="destination" value="nepal" checked> Nepal 
                                <input type="radio" name="destination" value="abroad"> Abroad </span>
                        </div>
                        <div class="hero-text-medium standard-padding text-center" style="padding-bottom: 10px; padding-top: 0px;">
                            Quick One Word Search
                        </div>
                        <div class="hero-text-medium text-center">
                            <input type="text" class="form-control input-lg text-center" name="search-key-word" placeholder="Area, Name of College, Course, Fee">
                        </div>
                        <div class="text-center standard-padding">
                            <span class="big-text"><input type="radio" name="adtype" value="Student"> Student Ad Search</span>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <span class="big-text"><input type="radio" name="adtype" value="College" checked> College Ad Search </span>
                            &nbsp;&nbsp;&nbsp;&nbsp;

                            <span class="big-text"><input type="radio" name="adtype" value="Institute"> Institute Ad Search</span>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn center-block yelloButton-hero">Search Now</button>
                            <input type="hidden" name="search_now" value="YES">
                        </div>
                    </div>

                    <div class="row" style="margin-top: 25px;">
                        <!--ad area-->
                        <div class="col-md-12">
                            <?php $this->load->view('front/sidebar/front-page-horizontal-sidebar'); ?>    
                        </div>
                        <!--ad area-->
                    </div>
                </div>
                <?php echo form_close(); ?>

                <div class="col-md-4">
                    <?php $this->load->view('front/sidebar/front-page-vertical-sidebar'); ?>
                </div>

            </div>
        </div>
    </div>
</section>




<section>
    <div class="container">
        <div class="front-intro margin-bottom-5">
            <h1 style="font-size: 30px; font-weight: 300;">Who we are?</h1>
            <p>We at hamrofinder think laterally. We believe we are well educated and that education will in turn make us productive thinkers. This is our start-up. We see a lot of possibilities in the horizon and to begin, education sector has become our stop...&nbsp&nbsp&nbsp<a href="<?php echo site_url('content/about-us') ?>" title="Explore More about Us">Read More <i class="fa fa-angle-double-right fa-fw"></i></a></p>
        </div>
    </div>
</section>






<?php
$feature_student_ads = Modules::run('studentad/get_feature_student_ads_random');
if ($feature_student_ads) {
    $adParts = array_chunk($feature_student_ads, 4);
    ?>
    <section>
        <div class="container">
            <div class="white-bg standard-padding standard-margin-bottom margin-bottom-5">
                <h2>Featured Students</h2>
                <div id="student-slider-front" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                        <?php
                        $stsn = 1;
                        foreach ($adParts as $part):
                            $totalItms = count($part);
                            ?>
                            <div class="item <?php echo $stsn == 1 ? 'active' : ''; ?>">
                                <div class="row equal-item-holder  <?php echo $totalItms == 1 ? 'no-gutter-single-item' : 'no-gutter' ?>">
                                    <?php
                                    foreach ($part as $fsa):
                                        $data['row'] = $fsa;
                                        $this->load->view('studentad/loop_student_common', $data);
                                    endforeach;
                                    ?>
                                </div>
                            </div>
                            <?php
                            $stsn++;
                        endforeach;
                        ?>
                    </div>
                    <a class="left carousel-control student-scroller" href="#student-slider-front" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control student-scroller" href="#student-slider-front" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <?php
}
?>




<?php
$feature_college_ads = Modules::run('collegead/get_feature_college_ads_random');
if ($feature_college_ads) {
    $adParts = array_chunk($feature_college_ads, 4);
    ?>
    <section>
        <div class="container">
            <div class="white-bg standard-padding margin-bottom-5">
                <h2>Featured Colleges</h2>
                <div id="college-slider-front" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                        <?php
                        $stsn = 1;
                        foreach ($adParts as $part):
                            $totalItms = count($part);
                            ?>
                            <div class="item <?php echo $stsn == 1 ? 'active' : ''; ?>">
                                <div class="row equal-item-holder <?php echo $totalItms == 1 ? 'no-gutter-single-item' : 'no-gutter' ?>">
                                    <?php
                                    foreach ($part as $fsa):
                                        $data['row'] = $fsa;
                                        $this->load->view('collegead/loop_college_common', $data);
                                    endforeach;
                                    ?>
                                </div>
                            </div>
                            <?php
                            $stsn++;
                        endforeach;
                        ?>
                    </div>
                    <a class="left carousel-control student-scroller" href="#college-slider-front" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control student-scroller" href="#college-slider-front" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <?php
}
?>



<?php
$feature_institute_ads = Modules::run('institutead/get_feature_institute_ads_random');
if ($feature_institute_ads) {
    $adParts = array_chunk($feature_institute_ads, 4);
    ?>
    <section>
        <div class="container">
            <div class="white-bg standard-padding margin-bottom-5">
                <h2>Featured Institute</h2>
                <div id="institute-slider-front" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                        <?php
                        $stsn = 1;
                        foreach ($adParts as $part):
                            $totalItms = count($part);
                            ?>
                            <div class="item <?php echo $stsn == 1 ? 'active' : ''; ?>">
                                <div class="row  equal-item-holder <?php echo $totalItms == 1 ? 'no-gutter-single-item' : 'no-gutter' ?>" >
                                    <?php
                                    foreach ($part as $fsa):
                                        $data['row'] = $fsa;
                                        $this->load->view('institutead/loop_institute_common', $data);
                                    endforeach;
                                    ?>
                                </div>
                            </div>
                            <?php
                            $stsn++;
                        endforeach;
                        ?>
                    </div>
                    <a class="left carousel-control student-scroller" href="#institute-slider-front" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control student-scroller" href="#institute-slider-front" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <?php
}
?>