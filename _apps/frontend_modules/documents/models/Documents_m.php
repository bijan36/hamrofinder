<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Documents_m extends MY_Model {

    /**
     * @ Author: Suraj Bajracharya
     * @ Author URL : www.indesignmedia.net
     * @see http://indesignmedia.net
     */
    protected $_table = SYSTEMDOCUMENT;

    public function __construct() {
        parent::__construct();
        $this->_database = $this->db;
        $this->primary_key = 'ID';
    }

    public function index() {
        echo "No good stuff";
    }

    /*
     * **************************************************************
     * @ TAKING CARE ABOUT PRODUCT CATEGORY
     * ************************************************************* 
     */

    public function do_upload_ajax_gallery($filename, $class = null) {
        $config = array(
            'upload_path' => "./documents/",
            'allowed_types' => "pdf|PDF",
            'overwrite' => FALSE,
            'max_size' => MAXDOCUPLOADSIZE 
            );
        $this->load->library('upload', $config);
        
        if ($this->upload->do_upload($filename)) {
            $data = array(
                'upload_data' => $this->upload->data()
                );

            //MAKING TITLE FROM THE FILE NAME
            $imgTitle = make_image_title_no_company($data['upload_data']['orig_name']);
            $parent_ID = $this->session->userdata('userID');
            $owner = strtolower($this->session->userdata('userType'));
            if ($parent_ID && $owner) {

                if ($owner == COLLEGE || $owner == INSTITUTE) {

                    $fileName = $data['upload_data']['file_name'];

                    $dataIns = array(
                        'parent_ID' => $parent_ID,
                        'owner' => $owner,
                        'title' => $imgTitle,
                        'filename' => $fileName,
                        'addedon' => date('Y-m-d H:i:s'),
                        'ipaddress'=>$this->input->ip_address()
                        );

                    if ($this->insert($dataIns)) {

                        $imgID = $this->get_last_id();

                        //$dataview['fileNameTo'] = $data['upload_data']['file_name'];
                        //$dataview['fileID'] = $imgID;
                        //$dataview['class'] = $class ? $class : '';

                        $imgPath = base_url() . 'documents/sample.jpg';
                        $imgSrc = '<div class="col-md-2 docholder-' . $imgID . '"><div class="doc_img_cover"><a href="'.base_url().'documents/'.$fileName.'" target="_balnk" title="'.$imgTitle.'"><img src="' . $imgPath . '"  class="img-responsive"><div class="docname">'.$imgTitle.'</div></a><a href="#" class="doc_del" data-id="' . $imgID . '"><i class="fa fa-times"></i></a></div></div>';

                        $returnData = array(
                            'status' => 'success',
                            'msg' => 'Document has been uploaded',
                            'img_id' => $this->get_last_id(),
                            'img_src' => $imgSrc
                            );
                    } else {
                        $returnData = array(
                            'status' => 'error',
                            'msg' => 'Unable to upload document'
                            );
                    }
                } else {
                    $returnData = array(
                        'status' => 'error',
                        'msg' => 'Unable to upload document'
                        );
                }
            } else {
                $returnData = array(
                    'status' => 'error',
                    'msg' => 'Unable to upload document'
                    );
            }
        } else {
            $returnData = array(
                'status' => 'error',
                'msg' => $this->upload->display_errors()
                );
        }
        return $returnData;
    }

    //GET LAT ID
    public function get_last_id() {
        $this->db->select_max('ID');
        $result = $this->db->get(SYSTEMDOCUMENT)->result_array();
        return $result[0]['ID'];
    }

}
