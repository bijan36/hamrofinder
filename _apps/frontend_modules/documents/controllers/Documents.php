<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Documents extends Frontendcontroller {

    /**
     * @ Author: Suraj Bajracharya
     * @ Author URL : www.indesignmedia.net
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     *
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('documents_m', 'docm');
    }

    /*
     * **************************************************************
     * @ IMAGE CONTROLLERS
     * ************************************************************* 
     */

    public function index() {
        
    }

    //GET THE RELATED DOCUMENTS
    public function get_docs() {
        $parent_ID = $this->session->userdata('userID');
        $owner = strtolower($this->session->userdata('userType'));
        $docs = $this->docm->get_many_by(array('parent_ID' => $parent_ID, 'owner' => $owner));
        return $docs ? $docs : false;
    }

    public function get_docs_by($parent_ID, $owner) {
        if (isset($parent_ID) && isset($owner)) {
            $docs = $this->docm->get_many_by(array('parent_ID' => $parent_ID, 'owner' => $owner));
            return $docs ? $docs : false;
        } else {
            return false;
        }
    }

    //GALLERY IMAGE UPLOAD
    public function documents_upload() {
        $parent_ID = $this->session->userdata('userID');
        $owner = strtolower($this->session->userdata('userType'));
        $documents = $this->docm->get_many_by(array('parent_ID' => $parent_ID, 'owner' => $owner));
        //DON NOT ALLOW MORE THEN 6 DOCUMENTS UPLOAD
        if ($documents) {
            if (count($documents) >= MAXDOCUPLOAD) {
                $datas = array(
                    'status' => 'error',
                    'msg' => 'Max ' . MAXDOCUPLOAD . ' documents can be uploaded!'
                );
                echo json_encode($datas);
                exit();
            }
        }
        //ONLY ALLOW COLLEGE AND INSTITUTE TO UPLOAD THE DOCUMENT
        if ($owner == COLLEGE || $owner == INSTITUTE) {
            if ($this->input->post('addtodoc') == 'yes') {
                $fileNm = $_FILES['docfiles']['name'];
                if (!empty($fileNm)) {
                    $datas = $this->docm->do_upload_ajax_gallery('docfiles');
                    echo json_encode($datas);
                    exit();
                } else {
                    $datas = array(
                        'status' => 'error',
                        'msg' => 'Unable to update document'
                    );
                    echo json_encode($datas);
                    exit();
                }
            } else {
                $datas = array(
                    'status' => 'error',
                    'msg' => 'Missing fields'
                );
                echo json_encode($datas);
                exit();
            }
        } else {
            $datas = array(
                'status' => 'error',
                'msg' => 'Unable to upload document'
            );
            echo json_encode($datas);
            exit();
        }
    }

    public function del_doc() {
        $docID = $this->input->post('id');
        $userID = $this->session->userdata('userID');
        $owner = $this->session->userdata('userType');

        $docRow = $this->docm->get_by(array('ID' => $docID, 'parent_ID' => $userID, 'owner' => $owner));
        if ($docRow) {
            if (is_file('./documents/' . $docRow->filename)) {
                unlink('./documents/' . $docRow->filename);
            }

            if ($this->docm->delete($docID)) {
                $datas = array(
                    'status' => 'success',
                    'msg' => 'Document has been removed'
                );
            } else {
                $datas = array(
                    'status' => 'error',
                    'msg' => 'Unable to remove document'
                );
            }
        } else {
            $datas = array(
                'status' => 'error',
                'msg' => 'Your are not authorise to remove this document'
            );
        }
        echo json_encode($datas);
        exit();
    }

}
