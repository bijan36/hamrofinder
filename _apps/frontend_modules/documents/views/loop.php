<?php
$boxSize = '2';
if (isset($total)) {
    if ($total <= 3) {
        $boxSize = '4';
    } elseif ($total == 4) {
        $boxSize = '3';
    }else{
        $boxSize = '2';
    }
}
?>
<div class="col-md-<?php echo $boxSize; ?>">
    <div class="doc_img_cover_small">
        <a href="<?php echo base_url() ?>documents/<?php echo $doc->filename ?>" target="_blank" title="<?php echo $doc->title; ?>">
            <img src="<?php echo base_url(); ?>documents/sample.jpg"  class="img-responsive">
            <div class="docname_small"><?php echo $doc->title; ?></div>
        </a>
    </div>
</div>