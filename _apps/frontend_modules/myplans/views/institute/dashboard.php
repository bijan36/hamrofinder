<div class="student-menu white-bg standard-padding-little-height">
    <div class="row">
        <div class="col-md-2">
            <h1>My Plans <i class="fa fa-angle-double-right fa-fw"></i></h1>
        </div>
    </div>
</div>
<div class="main white-bg standard-padding margin-bottom-5">
    <div class="row">
        <div class="col-md-4">
            <div class="ad-box-round-corner standard-padding-little-height round-border">
                <?php
                $currentPlan = Modules::run('user/get_current_plan');
                ?>
                <h1>Current Plan: <?php echo $currentPlan; ?></h1>
                <?php
                $this->load->view('myplans/institute/current-plan', array('currentPlan' => $currentPlan));
                ?>
            </div>
        </div>
        <div class="col-md-8">
            <div class="ad-box-round-corner standard-padding-little-height round-border">
                <?php
                //GET ALL THE PAYMNETS DETAILS BY USER
                $pRows = Modules::run('ipn/get_all_the_payment_by', get_institute_ID());
                $latestPlanPayment = Modules::run('ipn/latest_plan_payment_by');
                $cuPlanID = $latestPlanPayment ? $latestPlanPayment->ID : '0';
                if ($pRows) {
                    ?>
                    <h3>Payment Statement</h3>
                    <table class="table table-bordered">
                        <tr>
                            <th>SN</th>
                            <th>Paid Date</th>
                            <th>Type</th>
                            <th>Payment For</th>
                            <th>Amount</th>
                            <th>Ads</th>
                            <th>Start</th>
                            <th>Expire</th>
                            <th>Status</th>
                        </tr>
                        <?php
                        $pSn = 1;
                        $pTotal = 0;
                        $pTotalAd = 0;
                        foreach ($pRows as $pRow):
                            $pTotal = $pTotal + $pRow->price;
                            //COUNT THE AD ONLY OF ACTIVE PLAN
                            if ($pRow->purchase_type == 'plan') {
                                if ($cuPlanID == $pRow->ID) {
                                    $pTotalAd = $pTotalAd + $pRow->free_ad;
                                }
                            } else {
                                if ($pRow->end_date >= today_date() && $pRow->start_date <= today_date()) {
                                    $pTotalAd = $pTotalAd + $pRow->free_ad;
                                }
                            }
                            ?>
                            <?php
                            //MAKING TR DISABLE STYLE
                            if ($pRow->purchase_type == 'plan') {
                                if ($cuPlanID == $pRow->ID) {
                                    echo '<tr>';
                                } else {
                                    ?>
                                    <tr style="color: #ccc;">
                                        <?php
                                    }
                                } else {
                                    if ($pRow->end_date >= today_date() && $pRow->start_date <= today_date()) {
                                        echo '<tr>';
                                    } else {
                                        echo '<tr style="color: #ccc;">';
                                    }
                                }
                                ?>

                                <td><?php echo $pSn++; ?></td>
                                <td><?php echo $pRow->created_at; ?></td>
                                <td>
                                    <?php
                                    $pType = $pRow->purchase_type;
                                    echo ucfirst($pType);
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if ($pType == 'plan') {
                                        echo $pRow->title;
                                    } else {
                                        echo $pRow->free_ad . ' Advertise';
                                    }
                                    ?>
                                </td>
                                <td>NRs. <?php echo $pRow->price; ?></td>
                                <td><?php echo $pRow->free_ad; ?></td>
                                <td><?php echo convert_date($pRow->start_date); ?></td>
                                <td><?php echo convert_date($pRow->end_date); ?></td>
                                <td><?php echo $pRow->payment_status; ?></td>
                            </tr>
                            <?php
                        endforeach;
                        ?>
                        <tr>
                            <td colspan="4" class="text-right"><strong>Total: </strong></td>
                            <td><strong>NRs. <?php echo makeMoney($pTotal); ?></strong></td>
                            <td colspan="4"><strong><?php echo $pTotalAd; ?> Ads allowed</strong></td>
                        </tr>
                    </table>
                    <?php
                } else {
                    echo 'No latest payment found!';
                }
                ?>
            </div>
        </div>
    </div>
</div>




