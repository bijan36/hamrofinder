<?php
if (is_user_institute()) {
    $planRow = Modules::run('institute_membership_plans/get_plan_by_name', $currentPlan);

    if ($planRow) {
        //print_r($planRow);
        ?>
        <div class="plan-cover <?php echo $planRow->title; ?>">
            <div class="headbox">
                <h1><?php echo $planRow->title; ?> Member</h1>
                <p>
                    for College<br/>
                    Your membership gives<br/>
                    you the following privileges: 
                </p>
            </div>
            <div class="checkList">
                <?php
                $prefv = unserialize($planRow->privileged);
                if ($prefv) {
                    if (is_array($prefv) && !empty($prefv)) {
                        ?>
                        <ul>
                            <?php if ($planRow->title == 'Registered') { ?>
                                <li class="classcannot">Can not create ads.</li>
                            <?php } else { ?>
                                <li class="classcan-<?php echo $planRow->title; ?>">Can create ads.</li>
                                <?php
                            }
                            ?>
                            <?php
                            foreach ($prefv as $kks => $pfv):
                                if ($pfv == 'Yes') {
                                    //YES CAN ROWS
                                    ?>
                                    <li class="classcan-<?php echo $planRow->title; ?>"><?php echo positive_narration($kks); ?></li>
                                    <?php
                                } else {
                                    //NO CAN'T ROWS
                                    ?>
                                    <li class="classcannot"><?php echo negative_narration($kks); ?></li>
                                    <?php
                                }
                            endforeach;
                            ?>
                        </ul>
                        <?php
                    }
                }
                ?>
            </div>
            <div class="pricebox">
                NPR <?php echo $planRow->per_year_fee; ?>/<span class="smllText">Per Year</span> 
            </div>
            <div class="planInfobox">
                (Comes with <?php echo $planRow->free_ad; ?> free ads) 
            </div>
        </div>
        <?php
    } else {
        echo 'Sorry no such plans we found in our system';
    }
} elseif (is_user_institute()) {
    
} else {
    echo "Sorry you are not in our plan.";
}
?>