<div class="main white-bg">

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li  id="search1" role="presentation" class="active"><a href="#collegePlans" aria-controls="collegePlans" role="tab" data-toggle="tab"><strong>College Membership Plans</strong></a></li>
        <li  id="search2" role="presentation"><a href="#institutePlans" aria-controls="institutePlans" role="tab" data-toggle="tab"><strong>Institute Membership Plans</strong></a></li>
    </ul>


    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="collegePlans">
            <div class="grey-bg-gradient standard-padding browseBox">


                <div class="row">
                    <div class="col-md-12">
                        <span class="bigTextLightYellow">
                            Better Your <strong>Search.</strong> Be registered.<br/>
                            Choose <strong>Better Plan for College.</strong>
                        </span>
                    </div>
                    <?php
                    if (!empty($collegePlans)) {
                        foreach ($collegePlans as $plan):
                            ?>
                            <div class="col-md-3">
                                <?php
                                $planData['planData'] = $plan;
                                $this->load->view('myplans/plan-loop-college', $planData);
                                ?>
                            </div>
                            <?php
                        endforeach;
                        $planData['planData'] = array();
                    }
                    ?>
                </div>

            </div>
        </div>
        
        
        
        <div role="tabpanel" class="tab-pane" id="institutePlans">
            <div class="grey-bg-gradient standard-padding browseBox">
                <div class="row">
                    <div class="col-md-12">
                        <span class="bigTextLightYellow">
                            Better Your <strong>Search.</strong> Be registered.<br/>
                            Choose <strong>Better Plan for Institute.</strong>
                        </span>
                    </div>
                    <?php
                    if (!empty($institutePlans)) {
                        foreach ($institutePlans as $plan):
                            ?>
                            <div class="col-md-3">
                                <?php
                                $planData['planData'] = $plan;
                                $this->load->view('myplans/plan-loop-institute', $planData);
                                ?>
                            </div>
                            <?php
                        endforeach;
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>