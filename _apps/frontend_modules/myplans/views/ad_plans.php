<div class="main white-bg">

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li  id="search1" role="presentation" class="active"><a href="#collegePlans" aria-controls="collegePlans" role="tab" data-toggle="tab"><strong>College Purchase Plans</strong></a></li>
        <li  id="search2" role="presentation"><a href="#institutePlans" aria-controls="institutePlans" role="tab" data-toggle="tab"><strong>Institute Purchase Plans</strong></a></li>
    </ul>


    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="collegePlans">
            <div class="grey-bg-gradient standard-padding browseBox">
                <div class="row">
                    <?php $this->load->view('college_ad_pricing/display_ad_price_list'); ?>
                </div>
            </div>
        </div>
        
        
        
        <div role="tabpanel" class="tab-pane" id="institutePlans">
            <div class="grey-bg-gradient standard-padding browseBox">
                <div class="row">
                    <?php $this->load->view('institute_ad_pricing/display_ad_price_list'); ?>
                </div>
            </div>
        </div>
    </div>
</div>