<?php
if (is_user_college()) {
    $planRow = Modules::run('college_membership_plans/get_plan_by_name', $currentPlan);

    if ($planRow) {
        ?>
        <div class="plan-cover <?php echo $planRow->title; ?>">

            <?php
            if ($planRow->title == $activePlan) {
                ?>
                <div class="PlanSelected"><i class="fa fa-check fa-fw fa-2x"></i></div>
                <?php
            }
            ?>

            <div class="headbox">
                <h1><?php echo $planRow->title; ?> Member</h1>
                <p>
                    for College<br/>
                    Your membership gives<br/>
                    you the following privileges: 
                </p>
            </div>
            <div class="checkList">
                <?php
                $prefv = unserialize($planRow->privileged);
                if ($prefv) {
                    if (is_array($prefv) && !empty($prefv)) {
                        ?>
                        <ul>
                            <?php if ($planRow->title == 'Registered') { ?>
                                <li class="classcannot">Can not create ads.</li>
                            <?php } else { ?>
                                <li class="classcan-<?php echo $planRow->title; ?>">Can create ads.</li>
                                <?php
                            }
                            ?>
                            <?php
                            foreach ($prefv as $kks => $pfv):
                                if ($pfv == 'Yes') {
                                    //YES CAN ROWS
                                    ?>
                                    <li class="classcan-<?php echo $planRow->title; ?>"><?php echo positive_narration($kks); ?></li>
                                    <?php
                                } else {
                                    //NO CAN'T ROWS
                                    ?>
                                    <li class="classcannot"><?php echo negative_narration($kks); ?></li>
                                    <?php
                                }
                            endforeach;
                            ?>
                        </ul>
                        <?php
                    }
                }
                ?>
            </div>
            <div class="pricebox">
                NPR <?php echo $planRow->per_year_fee; ?>/<span class="smllText">Per Year</span> 
            </div>

            <div class="planInfobox">
                (Comes with <?php echo $planRow->free_ad; ?> free ads) 
            </div>

            <?php
            if ($planRow->per_year_fee > $currentPrice) {
                ?>
                <div class="btnHolder">
                    <form action = "<?php echo $this->config->item('esewa')['esewa_url'] ?>" method="POST">
                        <input value="<?php echo $planRow->per_year_fee; ?>" name="tAmt" type="hidden">
                        <input value="<?php echo $planRow->per_year_fee; ?>" name="amt" type="hidden">
                        <input value="0" name="txAmt" type="hidden">
                        <input value="0" name="psc" type="hidden">
                        <input value="0" name="pdc" type="hidden">
                        <input value="<?php echo $this->config->item('esewa')['merchant_id'] ?>" name="scd" type="hidden">
                        <input value="<?php echo unique_hash($this->session->userdata('userType'), $planRow->ID); ?>" name="pid" type="hidden">
                        <input value="<?php echo site_url('ipn/success'); ?>" type="hidden" name="su">
                        <input value="<?php echo site_url('ipn/fail'); ?>" type="hidden" name="fu">
                        <input  class="btn btn<?php echo $planRow->title; ?> btn-block" value="Get Started!" type="submit">
                    </form>
                </div>
            <?php } else { ?>
                <div class="btnHolder-<?php echo $planRow->title; ?>"></div>
            <?php } ?>



        </div>
        <?php
    } else {
        echo 'Sorry no such plans we found in our system';
    }
} else {
    echo "Sorry you are not in our plan.";
}
?>