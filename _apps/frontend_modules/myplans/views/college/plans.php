<div class="student-menu white-bg standard-padding-little-height">
    <div class="row">
        <div class="col-md-6">
            <h1>Upgrade/Membership/Purchase Ads <i class="fa fa-angle-double-right fa-fw"></i></h1>
        </div>
    </div>
</div>
<div class="main white-bg standard-padding margin-bottom-5">
    <div class="row">

        <div class="col-md-12">
            <span class="bigTextLightYellow">
                Better Your <strong>Search.</strong> Be registered.<br/>
                Choose <strong>Better Membership Plan for College.</strong>
                <p>To know more about the membership plans and tariff please go through our FAQ section or simply contact us.</p>
            </span>
        </div>
        <?php
        $currentPlan = $userRow->level;
        $currentPrice = 0;

        if (!empty($allPlans)) {
            foreach ($allPlans as $pln):
                if ($pln->title == $currentPlan) {
                    $currentPrice = $pln->per_year_fee;
                }
            endforeach;
        }

        if (!empty($allPlans)) {
            foreach ($allPlans as $plan):
                ?>
                <div class="col-md-3">
                    <?php $this->load->view('myplans/college/plan-loop', array('currentPrice' => $currentPrice, 'currentPlan' => $plan->title, 'activePlan' => $currentPlan)); ?>
                </div>
                <?php
            endforeach;
        }
        ?>
        <div class="col-md-12"></div>
        <?php $this->load->view('college_ad_pricing/ad_price_list'); ?>
        <?php $this->load->view('events/current_events'); ?>
    </div>
</div>