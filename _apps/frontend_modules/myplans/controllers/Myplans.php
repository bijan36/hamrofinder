<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Myplans extends Frontendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('Myplans_m', 'fmyplans');
    }

    //USER DASHBOARD
    public function index() {
        if (is_user_college() || is_user_institute()) {
            $userID = (int) $this->session->userdata('userID');
            $userRow = Modules::run('user/get_row', $userID);
            if ($userRow) {
                $prefix = strtolower($userRow->type);
                $data['basicInfo'] = $userRow;
                $data['userRow'] = $userRow;
                $data['metaInfo'] = Modules::run($prefix . '/get_row_by_parent', $userID);
                $data['userID'] = $userID;
                $this->template->load(get_template('plans-' . $prefix), $prefix . '/dashboard', $data);
            } else {
                redirect('site');
                exit();
            }
        } else {
            redirect('site');
            exit();
        }
    }

    //DISPLAY FOR THE PLANS
    public function ad_plans() {
        if (is_user_college() || is_user_institute()) {
            $userID = (int) $this->session->userdata('userID');
            $userRow = Modules::run('user/get_row', $userID);
            if ($userRow) {
                $prefix = strtolower($userRow->type);
                $data['basicInfo'] = $userRow;
                $data['userRow'] = $userRow;
                $data['metaInfo'] = Modules::run($prefix . '/get_row_by_parent', $userID);
                $data['userID'] = $userID;

                $allPlans = array();
                if ($userRow->type == 'College') {
                    $allPlans = Modules::run('college_membership_plans/get_all_plans');
                } elseif ($userRow->type == 'Institute') {
                    $allPlans = Modules::run('institute_membership_plans/get_all_plans');
                } else {
                    redirect('site');
                    exit();
                }
                $data['allPlans'] = $allPlans;
                $data['prefix'] = $prefix;

                $this->template->load(get_template('plans-' . $prefix), $prefix . '/plans', $data);
            } else {
                redirect('site');
                exit();
            }
        } else {
            redirect('site');
            exit();
        }
    }

    //DISPLAY PLANS FOR NON LOGIN USERS
    public function payment_plans() {

        $collegePlans = Modules::run('college_membership_plans/get_all_plans');
        $institutePlans = Modules::run('institute_membership_plans/get_all_plans');

        $data['collegePlans'] = $collegePlans;
        $data['institutePlans'] = $institutePlans;
        $this->template->load(get_template('payment-plans'), 'payment_plans', $data);
    }

    //DISPLAY PLANS FOR FRONT END
    public function display_ad_plans() {

        $collegeAdPlans = Modules::run('college_membership_plans/get_all_plans');
        $instituteAdPlans = Modules::run('institute_membership_plans/get_all_plans');

        $data['collegeAdPlans'] = $collegeAdPlans;
        $data['instituteAdPlans'] = $instituteAdPlans;

        $this->template->load(get_template('ad-plans'), 'ad_plans', $data);
    }

}
