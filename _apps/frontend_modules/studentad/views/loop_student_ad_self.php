<?php
if (isset($row) && !empty($row)) {
    if ($row->parent_ID == $this->session->userdata('userID')) {
        $userInfo = Modules::run('user/get_row', $row->parent_ID);
        $studentInfo = Modules::run('student/get_row_by_parent', $userInfo->ID);
        ?>
        <div class="student-box round-border-with-border grey-bg adbox<?php echo $row->ID; ?>"  style="margin-bottom: 10px;">
            <div class="row no-gutter">
                <div class="col-md-2">
                    <?php
                    $profile_image = Modules::run('imagehub/get_profile_image', $userInfo->profile_picture);
                    echo $profile_image;
                    ?>
                </div>
                <div class="col-md-10">
                    <table class="table">
                        <tr>
                            <td style="border-top: none;">
                                <span class="bold-uppercase-blue"><?php echo $userInfo->name; ?></span>
                                <br/>
                                <span class="text-uppercase"><?php echo $userInfo->address; ?></span>
                            </td>
                            <td style="border-top: none;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <button class="blueCompact-small badge who-intrested"  data-id="<?php echo $row->ID . '-Student'; ?>">Interest: <span class="interest-span<?php echo $row->ID; ?>"><?php echo total_interest($row->ID, 'Student'); ?></span></button>
                                        <?php
                                        //FIND THE CURRENT STATUS OF AD
                                        echo Modules::run('studentad/get_ad_status_label', $row->ID);
                                        ?>
                                        <?php
                                        //FEATUE AD
                                        if ($row->feature == 'Yes') {
                                            echo '<span class="featured-span badge">Featured Ad</span>';
                                        }
                                        ?>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Gender: <span class="bold-uppercase"><?php echo $studentInfo->gender; ?></span></td>
                            <td>Looking for: <span class="bold-uppercase"><?php echo $row->looking_for; ?></span></td>
                        </tr>
                        <tr>
                            <td>Grade: <span class="bold-uppercase"><?php echo $row->achieved_grade; ?></span></td>
                            <td>
                                <span class="bold-uppercase">
                                    <?php
                                    if (strlen($row->affiliation) > 23) {
                                        if ($row->ad_for == 'college') {
                                            echo 'Affiliated:';
                                        } else {
                                            echo 'Certified:';
                                        }
                                        echo ' <span class="single-line-address">' . $row->affiliation . '</span>';
                                    } else {
                                        if ($row->ad_for == 'college') {
                                            echo 'Affiliated: ';
                                        } else {
                                            echo 'Certified: ';
                                        }
                                        echo ' <span class="single-line-address">' . $row->affiliation . '</span>';
                                    }
                                    ?>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">Looking for course Fee: <span class="bold-uppercase"><?php echo $row->tution_fee_range; ?> Lakh</span></td>
                        </tr>
                        <tr>
                            <td colspan="2">Can admit in: <span class="bold-uppercase"><?php echo $row->admission_within; ?> Days</span> 
                                <?php echo $row->ad_end_date != 0 ? "(Ad expiry:" . $row->ad_end_date . ")" : ''; ?>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-12">
                    <div class="student-box-message">
                        <?php echo $row->message; ?>
                    </div>
                </div>
                <div class="col-md-12">
                    <?php
                    $offers_list = Modules::run('offer/get_offer_list_by_adID', $row->ID);
                    if ($offers_list):
                        ?>
                        <div class="bold-uppercase-blue" style="padding-bottom:10px; padding-top:10px; border-bottom: 1px solid #ccc;">Offer on this ad</div>
                        <?php
                        foreach ($offers_list as $offer):
                            ?>

                            <?php
                            $datas['row'] = $offer;
                            $this->load->view('offer/list', $datas);
                            ?>

                            <?php
                        endforeach;
                    endif;
                    ?>
                </div>
            </div>
            <div class="remove-btn remove-student-ad" data-id="<?php echo $row->ID; ?>"><i class="fa fa-times fa-fw"></i></div>
        </div>
        <?php
    }
}
?>