<?php
if (isset($row) && !empty($row)) {
    $userInfo = Modules::run('user/get_row', $row->parent_ID);
    $studentInfo = Modules::run('student/get_row_by_parent', $userInfo->ID);
    ?>

    <div class="student-box round-border-with-border grey-bg hvr-float"  style="margin-bottom: 10px;">
        <div class="row no-gutter">
            <div class="col-md-3">
                <?php
                $profile_image = Modules::run('imagehub/get_profile_image_real', $userInfo->profile_picture);
                ?>
                <div class="relativeBox">
                    <?php echo $profile_image; ?>
                    <?php echo Modules::run('user/get_online_status', $userInfo->ID); ?>
                </div>
                <span class="bold-uppercase-highlight center-block" style="margin-top: 5px; margin-bottom: 5px;"><?php echo $userInfo->type; ?></span>

            </div>
            <div class="col-md-9" style="padding-left: 15px;">
                <table class="table">
                    <tr>
                        <td style="border-top: none;">
                            <span class="bold-uppercase-blue"><?php echo $userInfo->name; ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="text-uppercase"><?php echo Modules::run('user/get_full_address', $userInfo->ID); ?></span>
                        </td>
                    </tr>
                </table>
                <table class="table">

                    <tr>
                        <td>Looking for:</td>
                        <td><span class="bold-uppercase-highlight"><?php echo $row->looking_for; ?></span></td>
                    </tr>
                    <tr>
                        <td style="width: 150px">Gender:</td>
                        <td><?php echo $studentInfo->gender; ?></td>
                    </tr>
                    <tr>
                        <td>Achieved Grade(%):</td>
                        <td><?php echo $row->achieved_grade; ?></td>
                    </tr>
                    <tr>
                        <td>Affiliated:</td>
                        <td><?php echo $row->affiliation; ?></td>
                    </tr>
                    <tr>
                        <td>Looking for course Fee:</td>
                        <td><?php echo $row->tution_fee_range; ?></td>
                    </tr>
                    <tr>
                        <td>Can admit in:</td>
                        <td><?php echo $row->admission_within; ?> Days</td>
                    </tr>
                </table>
            </div>
            <div class="col-md-12">
                <div class="student-box-message">
                    <?php
                    echo $row->message;
                    $type = 'Student';
                    ?>
                </div>
            </div>
            <div class="col-md-3">
                <button class="btn yellowCompact btn-block top-5 interest-btn"  data-url="<?php echo base_url(); ?>"   data-id="<?php echo $row->ID . '-' . $type; ?>">Show Interest</button>
            </div>
            <div class="col-md-3">
                <button class="btn yellowCompact btn-block top-5 offer_btn"   data-url="<?php echo base_url(); ?>" data-id="<?php echo $row->ID . '-' . $userInfo->ID . '-Student'; ?>">Make Offer</button>
            </div>
            <div class="col-md-3">
                <button class="btn blueCompact btn-block top-5 who-intrested" data-url="<?php echo base_url(); ?>" data-id="<?php echo $row->ID . '-' . $type; ?>">Interest: <span class="interest-span<?php echo $row->ID . '-' . $type; ?>"><?php echo total_interest($row->ID, $type); ?></span></button>
            </div>
            <div class="col-md-3">
                <button class="btn blueCompact btn-block top-5 save-ad-btn"  data-url="<?php echo base_url(); ?>"   data-id="<?php echo $row->ID . '-' . $type; ?>">Save This Ad</button>
            </div>
        </div>
    </div>

    <?php
}
?>