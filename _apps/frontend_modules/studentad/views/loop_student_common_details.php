<?php
$type = 'Student';
if (isset($row) && !empty($row)) {
    $userInfo = Modules::run('user/get_row', $row->parent_ID);
    $studentInfo = Modules::run('student/get_row_by_parent', $userInfo->ID);
    ?>

    <div class="student-box round-border-with-border grey-bg"  style="margin-bottom: 10px;">
        <div class="row no-gutter">
            <div class="col-md-3">
                <?php
                $profile_image = Modules::run('imagehub/get_profile_image', $userInfo->profile_picture);
                ?>
                <a href="<?php echo profile_url($userInfo->ID) ?>">
                    <div class="relativeBox">
                        <?php echo $profile_image; ?>
                        <?php echo Modules::run('user/get_online_status', $userInfo->ID); ?>
                    </div>
                </a>
                <span class="bold-uppercase-highlight center-block" style="margin-top: 5px; margin-bottom: 5px;"><?php echo $userInfo->type; ?></span>

            </div>
            <div class="col-md-9" style="padding-left: 15px;">
                <table class="table">
                    <tr>
                        <td style="border-top: none;">
                            <span class="bold-uppercase-blue">
                                <a href="<?php echo profile_url($userInfo->ID) ?>">
                                    <?php echo $userInfo->name; ?>
                                </a>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="text-uppercase"><?php echo Modules::run('user/get_full_address', $userInfo->ID); ?></span>
                        </td>
                    </tr>
                </table>
                <table class="table">
                    <tr>
                        <td style="width:140px;">Looking for:</td>
                        <td><span class="bold-uppercase-highlight"><?php echo $row->looking_for; ?></span></td>
                    </tr>
                    <tr>
                        <td>Gender:</td>
                        <td><?php echo $studentInfo->gender; ?></td>
                    </tr>
                    <tr>
                        <td>Achieved Grade(%):</td>
                        <td><?php echo $row->achieved_grade; ?></td>
                    </tr>
                    <tr>

                    <span class="bold-uppercase">
                        <?php
                        if (strlen($row->affiliation) > 23) {
                            if ($row->ad_for == 'college') {
                                echo '<td>Affiliated:</td>';
                            } else {
                                echo '<td>Certified:</td>';
                            }
                            echo ' <td>' . $row->affiliation . '</td>';
                        } else {
                            if ($row->ad_for == 'college') {
                                echo '<td>Affiliated:</td> ';
                            } else {
                                echo '<td>Certified:</td> ';
                            }
                           echo ' <td>' . $row->affiliation . '</td>';
                        }
                        ?>
                    </span>


                    <td>Affiliated:</td>
                    <td><?php echo $row->affiliation; ?></td>
                    </tr>
                    <tr>
                        <td>Looking for course Fee:</td>
                        <td><?php echo $row->tution_fee_range; ?></td>
                    </tr>
                    <tr>
                        <td>Can admit in:</td>
                        <td><?php echo $row->admission_within; ?> Days</td>
                    </tr>
                </table>
            </div>
            <div class="col-md-12">
                <div class="student-box-message">
                    <?php echo $row->message; ?>
                </div>
            </div>

        </div>
    </div>

    <?php
}
?>