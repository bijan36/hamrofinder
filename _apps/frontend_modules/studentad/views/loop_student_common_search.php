<?php
$type = 'Student';
//row is adRow
if (isset($row) && !empty($row)) {
    $userInfo = Modules::run('user/get_row', $row->parent_ID);
    //print_r($userInfo);
    $studentInfo = Modules::run('student/get_row_by_parent', $userInfo->ID);
    $dyClass = '';
    if (isset($dynamicTag) && !empty($dynamicTag)) {

        foreach ($dynamicTag as $dtk => $dt):
            if (in_array($row->ID, $dt)) {
                $dyClass .= $dtk . ' ';
            }
        endforeach;

        //ADDING EXTRA ADDRESS TAGS IF FOUND
        if (!empty($addressValues) && isset($addressValues)) {
            if (isset($addressValues['country']) && $addressValues['country'] == $userInfo->country) {
                $dyClass .= 'advcountry-student ';
            }
            if (isset($addressValues['province']) && $addressValues['province'] == $userInfo->province) {
                $dyClass .= 'advprovince-student ';
            }
            if (isset($addressValues['district']) && $addressValues['district'] == $userInfo->district) {
                $dyClass .= 'advdistrict-student ';
            }
            if (isset($addressValues['local_level']) && $addressValues['local_level'] == $userInfo->local_level) {
                $dyClass .= 'advlocal_level-student ';
            }
            if (isset($addressValues['ward_no']) && $addressValues['ward_no'] == $userInfo->ward_no) {
                $dyClass .= 'advward_no-student ';
            }
            if (isset($addressValues['address']) && $addressValues['address'] == $userInfo->address) {
                $dyClass .= 'advaddress-student ';
            }
        }
    }
    ?>
    <div class="col-md-3 col-sm-6 col-xs-12 equal-item <?php echo $dyClass; ?>">
        <div class="student-box round-border-with-border grey-bg hvr-float"  style="margin-bottom: 10px;">
            <div class="row no-gutter">

                <div class="col-md-5">
                    <div class="main-profile-holder relativeBox">
                        <?php
                        $profile_image = Modules::run('imagehub/get_profile_image_real', $userInfo->profile_picture);
                        ?>
                        <a href="<?php echo profile_url($userInfo->ID) ?>">
                            <?php echo $profile_image; ?>
                            <?php echo Modules::run('user/get_online_status', $userInfo->ID); ?>
                        </a>
                        <div class="userTypeShow">
                            <span class="expired-span badge"><?php echo $userInfo->type; ?></span>
                        </div>
                    </div>
                </div>

                <div class="col-md-7">
                    <div class="row bigger-gutter">
                        <div class="col-md-12">
                            <button class="btn yellowCompact btn-block top-5 interest-btn" data-url="<?php echo base_url(); ?>"  data-id="<?php echo $row->ID . '-Student'; ?>">Show Interest</button>
                        </div>
                        <div class="col-md-12">
                            <button class="btn yellowCompact btn-block top-5 offer_btn" data-id="<?php echo $row->ID . '-' . $userInfo->ID . '-Student'; ?>">Make Offer</button>
                        </div>
                        <div class="col-md-6">
                            <button class="btn yellowCompact btn-block top-5 more-info-btn" data-url="<?php echo base_url(); ?>" data-id="<?php echo $row->ID . '-' . $userInfo->ID . '-Student'; ?>">More Info</button>
                        </div>
                        <div class="col-md-6">
                            <button class="btn blueCompact btn-block top-5 who-intrested" data-url="<?php echo base_url(); ?>" data-id="<?php echo $row->ID . '-Student'; ?>">Interest: <span class="interest-span<?php echo $row->ID; ?>-Student"><?php echo total_interest($row->ID, $type); ?></span></button>
                        </div>
                        <div class="col-md-12">
                            <button class="btn blueCompact btn-block top-5 save-ad-btn" data-url="<?php echo base_url(); ?>"  data-id="<?php echo $row->ID . '-Student'; ?>">Save This Ad</button>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <table class="table">
                        <tr>
                            <td style="border-top: none;">
                                <span class="bold-uppercase-blue">
                                    <a href="<?php echo profile_url($userInfo->ID) ?>">
                                        <?php echo $userInfo->name; ?>
                                    </a>
                                </span>
                                <br/>
                                <span class="single-line-address"><small><?php echo Modules::run('user/get_full_address', $userInfo->ID); ?></small></span>
                            </td>
                        </tr>
                        <tr>
                            <td>Gender: <span class="bold-uppercase"><?php echo $studentInfo->gender; ?></span></td>
                        </tr>
                        <tr>
                            <td>Looking for: <span class="bold-uppercase-highlight"><?php echo $row->looking_for; ?></span></td>
                        </tr>
                        <tr>
                            <td>
                                
                                
                                
                                
                                <span class="bold-uppercase">
                                    <?php
                                    if (strlen($row->affiliation) > 23) {
                                        if ($row->ad_for == 'college') {
                                            echo 'Affiliated:';
                                        } else {
                                            echo 'Certified:';
                                        }
                                        echo ' <span class="single-line-address">' . $row->affiliation . '</span>';
                                    } else {
                                        if ($row->ad_for == 'college') {
                                            echo 'Affiliated: ';
                                        } else {
                                            echo 'Certified: ';
                                        }
                                        echo $row->affiliation;
                                    }
                                    ?>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>Achieved grade: <span class="bold-uppercase"><?php echo $row->achieved_grade; ?> %</span></td>
                        </tr>

                        <tr>
                            <td>Looking for fee: <span class="bold-uppercase"><?php echo $row->tution_fee_range; ?> Lakh</span></td>
                        </tr>
                        <tr>
                            <td>Can admit in: <span class="bold-uppercase"><?php echo $row->admission_within; ?> Days</span></td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-12">
                    <div class="student-box-message multi-lines-fixed-height">
                        <?php echo get_limited_words($row->message, 160); ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <?php
}
?>