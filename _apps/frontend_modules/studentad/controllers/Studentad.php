<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Studentad extends Frontendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('Studentad_m', 'fstudentad');
    }

    //USER DASHBOARD
    public function index() {
        redirect('site');
        exit();
    }

    //GET SAFE DATA BY USER ID
    public function get_row($ID) {
        $userRow = $this->fstudentad->get($ID);
        return $userRow ? $userRow : false;
    }

    //GET SAFE DATA BY STUDENTS ADS
    public function get_rows_order($field1, $field2, $where) {
        $userRows = $this->fstudentad->order_by($field1, $field2)->get_many_by($where);
        $res = array();
        if ($userRows) {
            foreach ($userRows as $row):
                $res[] = $row;
            endforeach;
            return $res;
        } else {
            return false;
        }
    }

    //SHOWING ADS/ AD DETAILS PAGE
    public function ads($ID) {
        if (isset($ID)) {

            if (is_user_login()) {
                $adRow = $this->fstudentad->get_by(array('ID' => $ID, 'status' => ACTIVE));
                if ($adRow) {
                    $userID = $adRow->parent_ID;
                    //CAN THIS AD BE SHOWN CHEKED HERE
                    if (can_show_ad($ID, $userID)) {
                        $CurrentUserType = $this->session->userdata('userType');
                        $CurrentUserID = $this->session->userdata('userID');
                        //only college can view the ads
                        if ($CurrentUserType == 'College' || $CurrentUserType == 'Institute' || $CurrentUserID == $userID) {
                            $userRow = Modules::run('user/get_row', $userID);
                            $data['adRow'] = $adRow;
                            $data['userRow'] = $userRow;
                            $this->template->load(get_template('Student-ads'), 'details', $data);
                        } else {
                            redirect('invalid');
                            exit();
                        }
                    } else {
                        redirect('invalid');
                        exit();
                    }
                } else {
                    redirect('notfound');
                    exit();
                }
            } else {
                redirect('login');
                exit();
            }
        } else {
            redirect('notfound');
            exit();
        }
    }

    //WHICH ADS
    public function which_ads() {
        $adID = $this->input->post('id');
        $parts = explode('-', $adID);
        $adID = $parts[0];
        $adType = $parts[1];
        if (isset($adID) && !empty($adID) && $adID > 0) {

            $row = $this->fstudentad->get($adID);
            if ($row) {

                $list = '';
                $data['row'] = $row;
                $list .= $this->load->view('studentad/loop_student_common_details', $data, true);

                //finding he ad url
                $adURL = get_ads_permalink($adID, $row->parent_ID);

                $data = array(
                    'status' => 'success',
                    'type' => $adType,
                    'adURL' => $adURL,
                    'intersted_list' => $list
                );
                echo json_encode($data);
                exit();
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => 'Your are not authorised to view.',
                );
                echo json_encode($data);
                exit();
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => 'Your are not authorised to view this list.',
            );
            echo json_encode($data);
            exit();
        }
    }

    //GET SAFE DATA BY USER PARENT
    public function get_row_by_parent($parent_ID) {
        $userRow = $this->fstudentad->get_by(array('parent_ID' => $parent_ID));
        return $userRow ? $userRow : false;
    }

    //GET SAFE DATA BY USER PARENT
    public function get_rows_by_parent($parent_ID) {
        $userRow = $this->fstudentad->order_by('ID', 'DESC')->get_many_by(array('parent_ID' => $parent_ID));
        return $userRow ? $userRow : false;
    }

    //GET LOGIN USER'S AD
    public function get_my_ads() {
        if (is_user_login()) {
            if (is_user_student()) {
                $ads = $this->fstudentad->order_by('ID','DESC')->get_many_by(array('parent_ID' => $this->session->userdata('userID')));
                if ($ads) {
                    return $ads;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    //ADDING NEW AD
    public function add() {
        //CHECK SESSION IS ON
        if (!is_user_login()) {
            $data = array(
                'status' => 'error',
                'msg' => 'You are not authorised to update profile.'
            );
            echo json_encode($data);
            exit();
        }
        //CONFIRM THIS SESSION IS STUDENT
        if (is_user_student() == false) {
            $data = array(
                'status' => 'error',
                'msg' => 'You are not authorised to update profile.'
            );
            echo json_encode($data);
            exit();
        }

        //CONFIRM THE STUDENT CAN POST THE AD
        $ability = is_user_student_able();
        if ($ability['status'] == 'error') {
            echo json_encode($ability);
            exit();
        }

        //CONFIRM STUDENT ALREADY SET THE PROFILE
        $metaRow = Modules::run('student/get_row_by_parent', $this->session->userdata('userID'));
        if (!$metaRow) {
            $data = array(
                'status' => 'error',
                'msg' => 'Please complete your profile before posting an ad. <a href="' . site_url("dashboard") . '">Completer profile now.</a>'
            );
            echo json_encode($data);
            exit();
        } else {
            //IF ROW BUT CHECK THE REQUIRED FIELDS
            if (empty($metaRow->country) || empty($metaRow->gender)) {
                $data = array(
                    'status' => 'error',
                    'msg' => 'Please complete your profile before posting an ad. <a href="' . site_url("dashboard") . '">Completer profile now.</a>'
                );
                echo json_encode($data);
                exit();
            }
        }


        //FROM VALIDATION
        $this->form_validation->set_rules('looking_for_type', 'Looking for', 'required|trim');
        $this->form_validation->set_rules('looking_for', 'Looking for', 'required|trim');
        $this->form_validation->set_rules('affiliation', 'Affiliation', 'required|trim');
        $this->form_validation->set_rules('admission_within', 'Admission within', 'required|trim');
        $this->form_validation->set_rules('achieved_grade', 'Achieved Grade', 'required|trim');
        $this->form_validation->set_rules('fee_range_looking_for', 'Fee range', 'required|trim');

        if ($this->form_validation->run() == TRUE) {

            $looking_for = $this->input->post('looking_for');
            $affiliation_chk = $this->input->post('looking_for');

            //CHECK THE LOOKING_FOR & AFFILIATION FIELDS ARE NOT SAME, ALREADY SUBMITTED IN DATABASE BY SAME STUDENT OR NOT IF YES PREVIENT IT
            $dub_row = $this->fstudentad->get_by(array('parent_ID' => get_student_ID(), 'looking_for' => $looking_for, 'affiliation' => $affiliation_chk));
            if ($dub_row) {
                $data = array(
                    'status' => 'error',
                    'msg' => 'Sorry! "Looking for" and "Affiliation" cannot be repeated.'
                );
                echo json_encode($data);
                exit();
            }


            $affiliation = $this->input->post('affiliation');
            $admission_within = $this->input->post('admission_within');
            $achieved_grade = $this->input->post('achieved_grade');
            $tution_fee_range = $this->input->post('fee_range_looking_for');
            $message = $this->input->post('student_add_message');
            $ad_submit_date = get_date_time();
            $ad_submit_ip = $this->input->ip_address();
            $ad_for = $this->input->post('looking_for_type');

            $dataIns = array(
                'parent_ID' => get_student_ID(),
                'ad_for' => $ad_for,
                'looking_for' => $looking_for,
                'affiliation' => $affiliation,
                'admission_within' => $admission_within,
                'achieved_grade' => $achieved_grade,
                'tution_fee_range' => $tution_fee_range,
                'message' => $message,
                'ad_submit_date' => $ad_submit_date,
                'ad_submit_ip' => $ad_submit_ip
            );

            //INSERT HERE
            if ($this->fstudentad->insert($dataIns)) {

                $adID = $this->fstudentad->get_last_id();

                //LETS EMAIL TO ADMIN
                $data['to'] = admin_notification_email();
                $data['toName'] = admin_notification_email_name();
                $data['subject'] = 'Student ad waiting for approve.';
                $data['template'] = 'admin_student_ad_notification';
                $data['others'] = array(
                    'userfullname' => $this->session->userdata('userName'),
                    'emailaddress' => $this->session->userdata('userEmail'),
                    'adID' => $adID
                );
                Modules::run('emailsystem/doEmail', $data);
                //LETS DO EMAIL TO ADMIN
                //LETS EMAIL TO USER
                $data['to'] = $this->session->userdata('userEmail');
                $data['toName'] = $this->session->userdata('userName');
                $data['from'] = admin_notification_email();
                $data['fromName'] = admin_notification_email_name();
                $data['subject'] = 'Ad has been submitted successfully and waiting for approval.';
                $data['template'] = 'student_student_ad_notification';
                $data['others'] = array(
                    'userfullname' => $this->session->userdata('userName'),
                    'emailaddress' => $this->session->userdata('userEmail'),
                    'adID' => $adID
                );
                Modules::run('emailsystem/doEmail', $data);
                //LETS DO EMAIL TO USER

                $data = array(
                    'status' => 'success',
                    'msg' => 'Your ad has been submitted. Once it is verified, you will be notified by email.'
                );
                echo json_encode($data);
                exit();
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => 'Unable to process your ad ' . COMMONERROR
                );
                echo json_encode($data);
                exit();
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => validation_errors()
            );
            echo json_encode($data);
            exit();
        }
    }

    //REMOVING STUDENT AD
    public function remove_student_ad() {
        $adID = $this->input->post('id');
        $adRow = $this->fstudentad->get($adID);
        if ($adRow) {
            if ($adRow->parent_ID == get_student_ID()) {
                if ($this->fstudentad->delete($adID)) {

                    //DELETE RELATED OFFERS
                    //WORK HERE FOR DELETE BUT AFTER CONFIRMATION BY CLIENTS

                    $data = array(
                        'status' => 'success',
                        'msg' => 'Ad has been removed successfully.'
                    );
                    echo json_encode($data);
                    exit();
                } else {
                    $data = array(
                        'status' => 'error',
                        'msg' => 'Unable to remove your ad ' . COMMONERROR
                    );
                    echo json_encode($data);
                    exit();
                }
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => 'You are not authorised to perform such action ' . COMMONERROR
                );
                echo json_encode($data);
                exit();
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => 'Unable to remove your ad ' . COMMONERROR
            );
            echo json_encode($data);
            exit();
        }
    }

    //GETTING RANDOM ADS OF STUDENTS 
    public function get_feature_student_ads_random() {
        //SELECT THE RANDOM FEATURE 
        //NO EXPIRED
        //NO INACTIVE OR EXPIRED
        $rows = $this->fstudentad->order_by('ID', 'Random')->get_many_by(array('status' => ACTIVE, 'feature' => 'Yes', 'ad_end_date>=' => today_date()));
        // print_r($rows);

        if ($rows) {
            $qualified = array();
            foreach ($rows as $row):
                //CHECK THE USER IS VALID OR NOT
                if (can_show_ad($row->ID, $row->parent_ID)) {
                    $qualified[] = $row;
                }
            endforeach;
            return $qualified;
        } else {
            return false;
        }
    }

    //IS BELONGS TO
    public function is_belongs_to($adID, $userID) {
        $row = $this->fstudentad->get_by(array('ID' => $adID, 'parent_ID' => $userID));
        if ($row) {
            return true;
        } else {
            return false;
        }
    }

    //GET AD STATUS
    public function get_ad_status_label($ID) {
        if (isset($ID) && !empty($ID)) {
            //get ad row
            $adRow = $this->fstudentad->get($ID);
            if ($adRow) {
                if ($adRow->ad_end_date == 0) {
                    echo '<span class="unknown-span badge">In Process</span>';
                } elseif ($adRow->ad_end_date < today_date()) {
                    echo '<span class="expired-span badge">Expired</span>';
                } elseif ($adRow->status == 'Active') {
                    echo '<span class="active-span badge">Active</span>';
                } else {
                    echo '<span class="inactive-span badge">Inactive</span>';
                }
            } else {
                echo '<span class="unknown-span badge">Unknown</span>';
            }
        } else {
            echo '<span class="unknown-span badge">Unknown</span>';
        }
    }

    public function get_course_list($selected = null) {
        $allCourse = $this->fstudentad->get_many_by(array('status' => ACTIVE));
        $output = '<option value="">Select One</option>';
        if ($allCourse) {

            $ads = array();
            foreach ($allCourse as $st):
                $ads[] = $st->looking_for;
            endforeach;

            $ads = array_unique($ads);

            sort($ads);

            foreach ($ads as $ro):
                if ($selected) {
                    $sel = $selected == $ro ? 'selected' : '';
                    $output .= '<option value="' . trim($ro) . '" ' . $sel . '>' . $ro . '</option>';
                } else {
                    $output .= '<option value="' . trim($ro) . '">' . $ro . '</option>';
                }
            endforeach;
        } else {
            $output .= '<option value="0">Not Found!</option>';
        }
        return $output;
    }

    //AFFILIATION
    public function get_affiliation_list($selected = null) {
        $allCourse = $this->fstudentad->get_many_by(array('status' => ACTIVE));
        $output = '<option value="">Select One</option>';
        if ($allCourse) {

            $ads = array();
            foreach ($allCourse as $st):
                $ads[] = $st->affiliation;
            endforeach;

            $ads = array_unique($ads);

            sort($ads);

            foreach ($ads as $ro):
                if ($selected) {
                    $sel = $selected == $ro ? 'selected' : '';
                    $output .= '<option value="' . trim($ro) . '" ' . $sel . '>' . $ro . '</option>';
                } else {
                    $output .= '<option value="' . trim($ro) . '">' . $ro . '</option>';
                }
            endforeach;
        } else {
            $output .= '<option value="0">Not Found!</option>';
        }
        return $output;
    }

    //FREE
    public function get_fee_list($selected = null) {
        $allCourse = $this->fstudentad->get_many_by(array('status' => ACTIVE));
        $output = '<option value="">Select One</option>';
        if ($allCourse) {

            $ads = array();
            foreach ($allCourse as $st):
                $ads[] = $st->tution_fee_range;
            endforeach;

            $ads = array_unique($ads);

            sort($ads);

            foreach ($ads as $ro):
                if ($selected) {
                    $sel = $selected == $ro ? 'selected' : '';
                    $output .= '<option value="' . trim($ro) . '" ' . $sel . '>' . $ro . '</option>';
                } else {
                    $output .= '<option value="' . trim($ro) . '">' . $ro . '</option>';
                }
            endforeach;
        } else {
            $output .= '<option value="0">Not Found!</option>';
        }
        return $output;
    }

    //SOME METHODS FOR SEARCH 
    //GET STUDENT ADS IDS BY TUTION FEE RANGE
    public function search_ad_by_fee_range($fee_range, $gender) {
        $resArray = array();
        $arg = array(
            'tution_fee_range' => $fee_range,
            'status' => ACTIVE
        );
        $ads = $this->fstudentad->get_many_by($arg);
        if ($ads) {
            foreach ($ads as $ad):
                if (can_show_ad($ad->ID, $ad->parent_ID)) {
                    //FILTER BY GENDER HERE
                    if ($gender == 'Both') {
                        $resArray[] = $ad->ID;
                    } else {
                        $studentMeta = Modules::run('student/get_row_by_parent', $ad->parent_ID);
                        if ($studentMeta) {
                            if ($studentMeta->gender == $gender) {
                                $resArray[] = $ad->ID;
                            }
                        }
                    }
                }
            endforeach;
        }
        return $resArray;
    }

    //SEARCH ADS BY COURSE
    public function search_ad_by_course($course, $gender) {
        $resArray = array();
        $arg = array(
            'looking_for' => $course,
            'status' => ACTIVE
        );
        $ads = $this->fstudentad->get_many_by($arg);
        if ($ads) {
            foreach ($ads as $ad):
                if (can_show_ad($ad->ID, $ad->parent_ID)) {
                    //FILTER BY GENDER HERE
                    if ($gender == 'Both') {
                        $resArray[] = $ad->ID;
                    } else {
                        $studentMeta = Modules::run('student/get_row_by_parent', $ad->parent_ID);
                        if ($studentMeta) {
                            if ($studentMeta->gender == $gender) {
                                $resArray[] = $ad->ID;
                            }
                        }
                    }
                }
            endforeach;
        }
        return $resArray;
    }

    //SEARCH ADS BY AFFILIATION
    public function search_ad_by_affiliation($affiliation, $gender) {
        $resArray = array();
        $arg = array(
            'affiliation' => $affiliation,
            'status' => ACTIVE
        );
        $ads = $this->fstudentad->get_many_by($arg);
        if ($ads) {
            foreach ($ads as $ad):
                if (can_show_ad($ad->ID, $ad->parent_ID)) {
                    //FILTER BY GENDER HERE
                    if ($gender == 'Both') {
                        $resArray[] = $ad->ID;
                    } else {
                        $studentMeta = Modules::run('student/get_row_by_parent', $ad->parent_ID);
                        if ($studentMeta) {
                            if ($studentMeta->gender == $gender) {
                                $resArray[] = $ad->ID;
                            }
                        }
                    }
                }
            endforeach;
        }
        return $resArray;
    }

    //SEARCH ADS BY ADDRESS
    public function search_ad_by_address($address, $gender) {
        $resArray = array();
        //GET THE ALL USER IDS BY ADDRESS
        $userIDsByAddress = Modules::run('user/get_student_by_address', $address);

        if (!empty($userIDsByAddress)) {
            foreach ($userIDsByAddress as $parent_ID) {
                $arg = array(
                    'parent_ID' => $parent_ID,
                    'status' => ACTIVE
                );
                $ads = $this->fstudentad->get_many_by($arg);
                if ($ads) {
                    foreach ($ads as $ad):
                        if (can_show_ad($ad->ID, $ad->parent_ID)) {
                            //FILTER BY GENDER HERE
                            if ($gender == 'Both') {
                                $resArray[] = $ad->ID;
                            } else {
                                $studentMeta = Modules::run('student/get_row_by_parent', $ad->parent_ID);
                                if ($studentMeta) {
                                    if ($studentMeta->gender == $gender) {
                                        $resArray[] = $ad->ID;
                                    }
                                }
                            }
                        }
                    endforeach;
                }
            }
        }
        return $resArray;
    }
    //EXTENDED FOR THE ADVANCE SEARCH AFTER MAKING NEW ADDRESS SYSTEM
    public function search_ad_by_address_extended($addressArray, $gender) {
        $resArray = array();
        //GET THE ALL USER IDS BY ADDRESS
        $userIDsByAddress = Modules::run('user/get_student_by_address_extended', $addressArray);

        if (!empty($userIDsByAddress)) {
            foreach ($userIDsByAddress as $parent_ID) {
                $arg = array(
                    'parent_ID' => $parent_ID,
                    'status' => ACTIVE
                );
                $ads = $this->fstudentad->get_many_by($arg);
                if ($ads) {
                    foreach ($ads as $ad):
                        if (can_show_ad($ad->ID, $ad->parent_ID)) {
                            //FILTER BY GENDER HERE
                            if ($gender == 'Both') {
                                $resArray[] = $ad->ID;
                            } else {
                                $studentMeta = Modules::run('student/get_row_by_parent', $ad->parent_ID);
                                if ($studentMeta) {
                                    if ($studentMeta->gender == $gender) {
                                        $resArray[] = $ad->ID;
                                    }
                                }
                            }
                        }
                    endforeach;
                }
            }
        }
        return $resArray;
    }

    //SEARCH ADS BY AFFILIATION
    public function search_ad_by_admission_within($admission_within, $gender) {
        $resArray = array();
        $arg = array(
            'admission_within' => $admission_within,
            'status' => ACTIVE
        );
        $ads = $this->fstudentad->get_many_by($arg);
        if ($ads) {
            foreach ($ads as $ad):
                if (can_show_ad($ad->ID, $ad->parent_ID)) {
                    //FILTER BY GENDER HERE
                    if ($gender == 'Both') {
                        $resArray[] = $ad->ID;
                    } else {
                        $studentMeta = Modules::run('student/get_row_by_parent', $ad->parent_ID);
                        if ($studentMeta) {
                            if ($studentMeta->gender == $gender) {
                                $resArray[] = $ad->ID;
                            }
                        }
                    }
                }
            endforeach;
        }
        return $resArray;
    }

    //SEARCH ADS BY ACHIEVE GRADE
    public function search_ad_by_achieved_grade($achieved_grade, $gender) {
        $resArray = array();
        $arg = array(
            'achieved_grade' => $achieved_grade,
            'status' => ACTIVE
        );
        $ads = $this->fstudentad->get_many_by($arg);
        if ($ads) {
            foreach ($ads as $ad):
                if (can_show_ad($ad->ID, $ad->parent_ID)) {
                    //FILTER BY GENDER HERE
                    if ($gender == 'Both') {
                        $resArray[] = $ad->ID;
                    } else {
                        $studentMeta = Modules::run('student/get_row_by_parent', $ad->parent_ID);
                        if ($studentMeta) {
                            if ($studentMeta->gender == $gender) {
                                $resArray[] = $ad->ID;
                            }
                        }
                    }
                }
            endforeach;
        }
        return $resArray;
    }
    
    
    

}
