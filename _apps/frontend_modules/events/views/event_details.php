<?php
if (!empty($row)) {
    ?>
    <div class="col-md-12">
        <div class="row">

            <div class="col-md-6">
                <div class="eventBox">
                    <div class="eventPriceBox">NPR. <?php echo $row->price; ?></div>
                    <img src="<?php echo $row->event_banner; ?>" class="img-responsive">
                    <?php if ($this->session->userdata('userLogin')) { ?>
                        <form action = "<?php echo $this->config->item('esewa')['esewa_url'] ?>" method="POST">
                            <input value="<?php echo $row->price; ?>" name="tAmt" type="hidden">
                            <input value="<?php echo $row->price; ?>" name="amt" type="hidden">
                            <input value="0" name="txAmt" type="hidden">
                            <input value="0" name="psc" type="hidden">
                            <input value="0" name="pdc" type="hidden">
                            <input value="<?php echo $this->config->item('esewa')['merchant_id'] ?>" name="scd" type="hidden">
                            <input value="<?php echo unique_hash($this->session->userdata('userType'), $row->ID); ?>" name="pid" type="hidden">
                            <input value="<?php echo site_url('ipn/even_success'); ?>" type="hidden" name="su">
                            <input value="<?php echo site_url('ipn/even_fail'); ?>" type="hidden" name="fu">
                            <input  class="blockBtn" value="Buy Now" type="submit">
                        </form>   
                    <?php } else { ?>
                        <a href="<?php echo site_url('login'); ?>" class="blockBtn">Login to Buy</a>
                    <?php } ?>
                </div>
            </div>

            <div class="col-md-6">

                <div class="eventDetails" style="padding-top: 0px;">
                    
                    <h1  style="padding-top: 0px; margin-top: 0px; font-size: 30px"><?php echo $row->title; ?></h1>


                    <?php echo $row->description; ?>
                    <table class="table" style="font-size: 12px;">
                        <tr>
                            <td style="width: 120px; font-weight: 700;">Venue:</td>
                            <td><?php echo $row->venue; ?></td>
                        </tr>
                        <tr>
                            <td style="width: 120px; font-weight: 700;">Event Date:</td>
                            <td><?php echo $row->event_date; ?></td>
                        </tr>
                        <tr>
                            <td style="width: 120px; font-weight: 700;">Event Time:</td>
                            <td><?php echo $row->event_time; ?></td>
                        </tr>
                        <tr>
                            <td style="width: 120px; font-weight: 700;">Total Sets:</td>
                            <td><?php echo $row->total_sets; ?></td>
                        </tr>
                        <tr>
                            <td style="width: 120px; font-weight: 700;">Available Sets:</td>
                            <td><?php echo $row->total_sets; ?></td>
                        </tr>
                        <tr>
                            <td style="width: 120px; font-weight: 700;">Price:</td>
                            <td>NPR. <?php echo $row->price; ?></td>
                        </tr>
                    </table>
                    
                </div>
            </div>

        </div>
    </div>

    <?php
} else {
    ?>
    <div style="padding: 100px 30px; border: 1px solid #f0f0f0; font-size: 18px; text-align: center;">
        No such event found!
    </div>

    <?php
}
?>