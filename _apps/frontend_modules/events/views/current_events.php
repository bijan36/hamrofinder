<?php
$activeEvents = Modules::run('events/get_active_all');
if (!empty($activeEvents)) {
    ?>
    <div class="col-md-12" style="margin-top: 50px; padding-top: 50px; border-top: 1px solid #e2e1e1;">
        <span class="bigTextLightYellow">
            Be a Member of  <strong>hamrofinder.com</strong><br/>
            <strong>Get Informed</strong> About Each & Every Events.
            <p>To know more about hamrofinder events and activities tariff please go through our FAQ section or simply contact us.</p>
        </span>
    </div>
    <div class="col-md-12">
        <div class="row">
            <?php foreach ($activeEvents as $activeEvent): ?>
                <div class="col-md-4">
                    <div class="eventBox">
                        <div class="eventPriceBox">NPR. <?php echo $activeEvent->price; ?></div>
                        <img src="<?php echo $activeEvent->event_banner; ?>" class="img-responsive">
                        <div class="eventTitle"><?php echo $activeEvent->title; ?></div>
                        <div class="eventDetails">
                            <?php $classGen = random_string('alnum', '8'); ?>
                            <div class="hideBox <?php echo $classGen; ?>">
                                <?php echo $activeEvent->description; ?>
                            </div>
                            <div class="expandBtnEvent <?php echo $classGen; ?>-btn" data-code="<?php echo $classGen; ?>">
                                <i class="fa fa-angle-double-down"></i> Expand
                            </div>
                            <table class="table" style="font-size: 12px;">
                                <tr>
                                    <td style="width: 120px; font-weight: 700;">Venue:</td>
                                    <td><?php echo $activeEvent->venue; ?></td>
                                </tr>
                                <tr>
                                    <td style="width: 120px; font-weight: 700;">Event Date:</td>
                                    <td><?php echo $activeEvent->event_date; ?></td>
                                </tr>
                                <tr>
                                    <td style="width: 120px; font-weight: 700;">Event Time:</td>
                                    <td><?php echo $activeEvent->event_time; ?></td>
                                </tr>
                                <tr>
                                    <td style="width: 120px; font-weight: 700;">Total Sets:</td>
                                    <td><?php echo $activeEvent->total_sets; ?></td>
                                </tr>
                                <tr>
                                    <td style="width: 120px; font-weight: 700;">Available Sets:</td>
                                    <td><?php echo $activeEvent->total_sets; ?></td>
                                </tr>
                                <tr>
                                    <td style="width: 120px; font-weight: 700;">Price:</td>
                                    <td>NPR. <?php echo $activeEvent->price; ?></td>
                                </tr>
                            </table>

                        </div>

                        <?php if ($this->session->userdata('userLogin')) { ?>
                            <form action = "<?php echo $this->config->item('esewa')['esewa_url'] ?>" method="POST">
                                <input value="<?php echo $activeEvent->price; ?>" name="tAmt" type="hidden">
                                <input value="<?php echo $activeEvent->price; ?>" name="amt" type="hidden">
                                <input value="0" name="txAmt" type="hidden">
                                <input value="0" name="psc" type="hidden">
                                <input value="0" name="pdc" type="hidden">
                                <input value="<?php echo $this->config->item('esewa')['merchant_id'] ?>" name="scd" type="hidden">
                                <input value="<?php echo unique_hash($this->session->userdata('userType'), $activeEvent->ID); ?>" name="pid" type="hidden">
                                <input value="<?php echo site_url('ipn/even_success'); ?>" type="hidden" name="su">
                                <input value="<?php echo site_url('ipn/even_fail'); ?>" type="hidden" name="fu">
                                <input  class="blockBtn" value="Buy Now" type="submit">
                            </form>   
                        <?php } else { ?>
                            <a href="<?php echo site_url('login'); ?>" class="blockBtn">Login to Buy</a>
                        <?php } ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

    <?php
} else {
    ?>
    <div style="padding: 100px 30px; border: 1px solid #f0f0f0; font-size: 18px; text-align: center;">
        Currently there are no any events & activities available :(
    </div>

    <?php
}
?>