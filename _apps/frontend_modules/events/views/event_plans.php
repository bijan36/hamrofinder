<?php
$activeEvents = Modules::run('events/get_active_all');
if (!empty($activeEvents)) {
    ?>

    <div class="row">
        <?php foreach ($activeEvents as $activeEvent): ?>
            <div class="col-md-4">
                <div class="eventBox">
                    <div class="eventPriceBox">NPR. <?php echo $activeEvent->price; ?></div>
                    <img src="<?php echo $activeEvent->event_banner; ?>" class="img-responsive">
                    <div class="eventTitle"><?php echo $activeEvent->title; ?></div>
                    <div class="eventDetails">
                        <?php $classGen = random_string('alnum', '8'); ?>
                        <div class="hideBox <?php echo $classGen; ?>">
                            <?php echo $activeEvent->description; ?>
                        </div>
                        <div class="expandBtnEvent <?php echo $classGen; ?>-btn" data-code="<?php echo $classGen; ?>">
                            <i class="fa fa-angle-double-down"></i> Expand
                        </div>
                        <table class="table" style="font-size: 12px;">
                            <tr>
                                <td style="width: 120px; font-weight: 700;">Venue:</td>
                                <td><?php echo $activeEvent->venue; ?></td>
                            </tr>
                            <tr>
                                <td style="width: 120px; font-weight: 700;">Event Date:</td>
                                <td><?php echo $activeEvent->event_date; ?></td>
                            </tr>
                            <tr>
                                <td style="width: 120px; font-weight: 700;">Event Time:</td>
                                <td><?php echo $activeEvent->event_time; ?></td>
                            </tr>
                            <tr>
                                <td style="width: 120px; font-weight: 700;">Total Sets:</td>
                                <td><?php echo $activeEvent->total_sets; ?></td>
                            </tr>
                            <tr>
                                <td style="width: 120px; font-weight: 700;">Available Sets:</td>
                                <td><?php echo $activeEvent->total_sets; ?></td>
                            </tr>
                            <tr>
                                <td style="width: 120px; font-weight: 700;">Price:</td>
                                <td><strong>NPR. <?php echo $activeEvent->price; ?></strong></td>
                            </tr>
                        </table>
                        
                    </div>
                    
                    <a href="<?php echo site_url('show-event/'.$activeEvent->ID);?>" class="blockBtn">View Details</a>
                   
                </div>
            </div>
        <?php endforeach; ?>
    </div>


    <?php
}else{
    ?>

<div style="padding: 100px 30px; border: 1px solid #f0f0f0; font-size: 18px; text-align: center;">
    Currently there are no any events & activities available :(
</div>
<?php
}
?>