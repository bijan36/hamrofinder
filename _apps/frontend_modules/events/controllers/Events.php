<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Events extends Frontendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('Events_m', 'fevent');
    }

    //STUDENT LIST
    public function index() {
        redirect('site');
        exit();
    }

    //EVENT DETAILS PAGE
    public function show_event($ID) {
        if (isset($ID)) {

            $row = $this->fevent->get_by(array('booking_open_date<=' => date('Y-m-d'), 'booking_close_date>=' => date('Y-m-d'), 'ID' => $ID));
            if ($row) {
                $data['row'] = $row;
                $this->template->load(get_template('event-plans'), 'event_details', $data);
            } else {
                echo '1';
                //redirect('site');
                exit();
            }
        } else {
            echo '2';
            //redirect('site');
            exit();
        }
    }

    //GET SINGLE EVENT ROW ONLY VALID
    public function get_valid_event_row($eventID) {
        $row = $this->fevent->get($eventID);
        if ($row) {
            $bookOpen = $row->booking_open_date;
            $bookClose = $row->booking_close_date;
            $total = $row->total_sets;
            $booked = $row->booked_sets;
            $today = today_date();
            if ($today >= $bookOpen && $today <= $bookClose) {
                if ($total > $booked) {
                    return $row;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    //GET ALL
    public function get_active_all() {
        $rows = $this->fevent->get_many_by(array('booking_open_date<=' => date('Y-m-d'), 'booking_close_date>=' => date('Y-m-d')));
        return $rows ? $rows : false;
    }

    //DISPLAY PRICES FOR THE EVENTS
    public function events_plans() {
        $this->template->load(get_template('event-plans'), 'event_plans');
    }

}
