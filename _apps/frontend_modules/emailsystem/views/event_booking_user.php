<body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
<center style="background-color:#E1E1E1;">
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="table-layout: fixed;max-width:100% !important;width: 100% !important;min-width: 100% !important;">
        <tr>
            <td align="center" valign="top" id="bodyCell">
                <table bgcolor="#E1E1E1" border="0" cellpadding="0" cellspacing="0" width="500" id="emailHeader">
                    <tr>
                        <td align="center" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="center" valign="top">
                                        <table border="0" cellpadding="10" cellspacing="0" width="500" class="flexibleContainer">
                                            <tr>
                                                <td valign="top" width="500" class="flexibleContainerCell">
                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td align="left" valign="middle" id="invisibleIntroduction" class="flexibleContainerBox" style="display:none !important; mso-hide:all;">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
                                                                    <tr>
                                                                        <td align="left" class="textContent">
                                                                            <div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
                                                                                Event booking has been completed by <?php echo $others['userfullname'] ?>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td align="right" valign="middle" class="flexibleContainerBox">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table bgcolor="#FFFFFF"  border="0" cellpadding="0" cellspacing="0" width="500" id="emailBody">
                    <tr>
                        <td align="center" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="color:#FFFFFF;" bgcolor="#FFFFFF">
                                <tr>
                                    <td align="center" valign="top">
                                        <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
                                            <tr>
                                                <td align="center" valign="top" width="500" class="flexibleContainerCell">
                                                    <table border="0" cellpadding="30" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td align="center" valign="top" class="textContent">
                                                                <a href='<?php echo site_url() ?>' title="<?php echo COMPANYNAME; ?>">
                                                                    <img src="<?php echo base_url('_public/front/assets/img/logo/logo.png') ?>" alt="<?php echo COMPANYNAME; ?>">
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#FFFFFF">
                                <tr>
                                    <td align="center" valign="top">
                                        <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
                                            <tr>
                                                <td align="center" valign="top" width="500" class="flexibleContainerCell">
                                                    <table border="0" cellpadding="30" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td align="center" valign="top">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td valign="top" class="textContent">
                                                                            <h3 mc:edit="header" style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">Hi <?php echo $others['userfullname'] ?>,</h3><br/>
                                                                            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                                                                                <?php echo $others['msg']; ?>
                                                                            </div>
                                                                            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                                                                                <br/><br/>

                                                                                <strong>Details:</strong><br/>
                                                                                <table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; box-sizing: border-box;" width="100%">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td align="left" style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding: 10px; background:#d0e8ff;" valign="top">
                                                                                                Email Address:
                                                                                            </td>
                                                                                            <td align="left" style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding: 10px; background:#d0e8ff;" valign="top">
                                                                                                <?php echo $others['emailaddress']; ?>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="left" style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding: 10px;" valign="top">
                                                                                                Event:
                                                                                            </td>
                                                                                            <td align="left" style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding: 10px;" valign="top">
                                                                                                <?php echo $others['fromTo']; ?>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="left" style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding: 10px; background:#d0e8ff;" valign="top">
                                                                                                Event Date:
                                                                                            </td>
                                                                                            <td align="left" style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding: 10px; background:#d0e8ff;" valign="top">
                                                                                                <?php echo $others['event_start']; ?>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="left" style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding: 10px;" valign="top">
                                                                                                Event Venue:
                                                                                            </td>
                                                                                            <td align="left" style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding: 10px;" valign="top">
                                                                                                <?php echo $others['venue']; ?>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <br/><br/>
                                                                            </div>
                                                                            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                                                                                Note: This email is automatically generated by system. If any enquiry on above information please ask to us.
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="center" valign="top">
                                        <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
                                            <tr>
                                                <td align="center" valign="top" width="500" class="flexibleContainerCell">
                                                    <table border="0" cellpadding="30" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td align="center" valign="top">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td valign="top" class="textContent">
                                                                            <div style="text-align:center;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;margin-top:3px;color:#5F5F5F;line-height:135%;"></div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>