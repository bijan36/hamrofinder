<body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">

    <!-- CENTER THE EMAIL // -->
    <!--
    1.  The center tag should normally put all the
            content in the middle of the email page.
            I added "table-layout: fixed;" style to force
            yahoomail which by default put the content left.

    2.  For hotmail and yahoomail, the contents of
            the email starts from this center, so we try to
            apply necessary styling e.g. background-color.
        -->
        <center style="background-color:#E1E1E1;">
            <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="table-layout: fixed;max-width:100% !important;width: 100% !important;min-width: 100% !important;">
                <tr>
                    <td align="center" valign="top" id="bodyCell">

                        <!-- EMAIL HEADER // -->
                <!--
                        The table "emailBody" is the email's container.
                        Its width can be set to 100% for a color band
                        that spans the width of the page.
                    -->
                    <table bgcolor="#E1E1E1" border="0" cellpadding="0" cellspacing="0" width="500" id="emailHeader">

                        <!-- HEADER ROW // -->
                        <tr>
                            <td align="center" valign="top">
                                <!-- CENTERING TABLE // -->
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="center" valign="top">
                                            <!-- FLEXIBLE CONTAINER // -->
                                            <table border="0" cellpadding="10" cellspacing="0" width="500" class="flexibleContainer">
                                                <tr>
                                                    <td valign="top" width="500" class="flexibleContainerCell">

                                                        <!-- CONTENT TABLE // -->
                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                            <!--
                                                                    The "invisibleIntroduction" is the text used for short preview
                                                                    of the email before the user opens it (50 characters max). Sometimes,
                                                                    you do not want to show this message depending on your design but this
                                                                    text is highly recommended.

                                                                    You do not have to worry if it is hidden, the next <td> will automatically
                                                                    center and apply to the width 100% and also shrink to 50% if the first <td>
                                                                    is visible.
                                                                -->
                                                                <td align="left" valign="middle" id="invisibleIntroduction" class="flexibleContainerBox" style="display:none !important; mso-hide:all;">
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
                                                                        <tr>
                                                                            <td align="left" class="textContent">
                                                                                <div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
                                                                                    Offer made to student ads by college.
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td align="right" valign="middle" class="flexibleContainerBox">

                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!-- // FLEXIBLE CONTAINER -->
                                        </td>
                                    </tr>
                                </table>
                                <!-- // CENTERING TABLE -->
                            </td>
                        </tr>
                        <!-- // END -->

                    </table>
                    <!-- // END -->

                    <!-- EMAIL BODY // -->
                <!--
                        The table "emailBody" is the email's container.
                        Its width can be set to 100% for a color band
                        that spans the width of the page.
                    -->
                    <table bgcolor="#FFFFFF"  border="0" cellpadding="0" cellspacing="0" width="500" id="emailBody">

                        <!-- MODULE ROW // -->
                    <!--
                            To move or duplicate any of the design patterns
                            in this email, simply move or copy the entire
                            MODULE ROW section for each content block.
                        -->
                        <tr>
                            <td align="center" valign="top">
                                <!-- CENTERING TABLE // -->
                            <!--
                                    The centering table keeps the content
                                    tables centered in the emailBody table,
                                    in case its width is set to 100%.
                                -->
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="color:#FFFFFF;" bgcolor="#FFFFFF">
                                    <tr>
                                        <td align="center" valign="top">
                                            <!-- FLEXIBLE CONTAINER // -->
                                        <!--
                                                The flexible container has a set width
                                                that gets overridden by the media query.
                                                Most content tables within can then be
                                                given 100% widths.
                                            -->
                                            <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
                                                <tr>
                                                    <td align="center" valign="top" width="500" class="flexibleContainerCell">

                                                        <!-- CONTENT TABLE // -->
                                                    <!--
                                                    The content table is the first element
                                                            that's entirely separate from the structural
                                                            framework of the email.
                                                        -->
                                                        <table border="0" cellpadding="30" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td align="center" valign="top" class="textContent">
                                                                    <a href='<?php echo site_url() ?>' title="<?php echo COMPANYNAME; ?>">
                                                                        <img src="<?php echo base_url('_public/front/assets/img/logo/logo.png') ?>" alt="<?php echo COMPANYNAME; ?>">
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <!-- // CONTENT TABLE -->

                                                    </td>
                                                </tr>
                                            </table>
                                            <!-- // FLEXIBLE CONTAINER -->
                                        </td>
                                    </tr>
                                </table>
                                <!-- // CENTERING TABLE -->
                            </td>
                        </tr>
                        <!-- // MODULE ROW -->





                        <!-- MODULE ROW // -->
                        <tr>
                            <td align="center" valign="top">
                                <!-- CENTERING TABLE // -->
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#FFFFFF">
                                    <tr>
                                        <td align="center" valign="top">
                                            <!-- FLEXIBLE CONTAINER // -->
                                            <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
                                                <tr>
                                                    <td align="center" valign="top" width="500" class="flexibleContainerCell">
                                                        <table border="0" cellpadding="30" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td align="center" valign="top">

                                                                    <!-- CONTENT TABLE // -->
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td valign="top" class="textContent">
                                                                            <!--
                                                                                    The "mc:edit" is a feature for MailChimp which allows
                                                                                    you to edit certain row. It makes it easy for you to quickly edit row sections.
                                                                                    http://kb.mailchimp.com/templates/code/create-editable-content-areas-with-mailchimps-template-language
                                                                                -->
                                                                                <h3 mc:edit="header" style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">Hi Admin,</h3><br/>
                                                                                <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                                                                                    <br/>
                                                                                    <strong>College made offer in student ads as below:</strong><br/><br/>
                                                                                    <table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; box-sizing: border-box;" width="100%">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td align="left" style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding: 10px; background:#d0e8ff;" valign="top">
                                                                                                    Ad ID:
                                                                                                </td>
                                                                                                <td align="left" style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding: 10px; background:#d0e8ff;" valign="top">
                                                                                                    <?php echo $others['adID']; ?>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td align="left" style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding: 10px;" valign="top">
                                                                                                    College Name:
                                                                                                </td>
                                                                                                <td align="left" style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding: 10px;" valign="top">
                                                                                                    <?php echo $others['collegeName']; ?> (<?php echo $others['collegeEmail'] ?>)
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td align="left" style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding: 10px; background:#d0e8ff;" valign="top">
                                                                                                    Student Name:
                                                                                                </td>
                                                                                                <td align="left" style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding: 10px; background:#d0e8ff;" valign="top">
                                                                                                    <?php echo $others['studentName']; ?> (<?php echo $others['studentEmail'] ?>)
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td align="left" style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding: 10px;" valign="top" colspan="2">
                                                                                                    <strong>Offer Message:</strong>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td align="left" style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding: 10px;" valign="top" colspan="2">
                                                                                                    <?php echo $others['offermsg']; ?>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>        
                                                                                    </table>
                                                                                </div>

                                                                                <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                                                                                    <p>
                                                                                        View details offer in Admin Dashboard ref: by student ads ID.
                                                                                    </p>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <!-- // CONTENT TABLE -->

                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!-- // FLEXIBLE CONTAINER -->
                                        </td>
                                    </tr>
                                </table>
                                <!-- // CENTERING TABLE -->
                            </td>
                        </tr>
                        <!-- // MODULE ROW -->




                        <tr>
                            <td align="center" valign="top">
                                <!-- CENTERING TABLE // -->
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="center" valign="top">
                                            <!-- FLEXIBLE CONTAINER // -->
                                            <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
                                                <tr>
                                                    <td align="center" valign="top" width="500" class="flexibleContainerCell">
                                                        <table border="0" cellpadding="30" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td align="center" valign="top">

                                                                    <!-- CONTENT TABLE // -->
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td valign="top" class="textContent">
                                                                                <div style="text-align:center;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;margin-top:3px;color:#5F5F5F;line-height:135%;"></div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <!-- // CONTENT TABLE -->

                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!-- // FLEXIBLE CONTAINER -->
                                        </td>
                                    </tr>
                                </table>
                                <!-- // CENTERING TABLE -->
                            </td>
                        </tr>



                    </table>
                <!-- // END -->