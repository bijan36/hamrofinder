<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ipn extends Frontendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('Ipn_m', 'fipn');
    }

    //USER DASHBOARD
    public function index() {
        redirect('site');
        exit();
    }

    //DISPLAY FOR THE PLANS
    public function success() {

        $string = $_REQUEST['oid'];
        $parts = explode('-', $string);

        $planID = substr($parts[1], 12);

        if ($parts[0] == 'College') {
            $planRow = Modules::run('college_membership_plans/get_plan_by_ID', $planID);
        } elseif ($parts[0] == 'Institute') {
            $planRow = Modules::run('institute_membership_plans/get_plan_by_ID', $planID);
        } else {
            $planRow = FALSE;
        }


        if ($planRow) {

            //create array of data to be posted
            $post_data['amt'] = $planRow->per_year_fee;
            $post_data['scd'] = $this->config->item('esewa')['merchant_id'];
            $post_data['pid'] = $_REQUEST['oid'];
            $post_data['rid'] = $_REQUEST['refId'];

            foreach ($post_data as $key => $value) {
                $post_items[] = $key . '=' . $value;
            }

            $post_string = implode('&', $post_items);
            $curl_connection = curl_init($this->config->item('esewa')['esewa_verfication_url']);
            if ($curl_connection == TRUE) {

                curl_setopt($curl_connection, CURLOPT_CONNECTTIMEOUT, 30);
                curl_setopt($curl_connection, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
                curl_setopt($curl_connection, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl_connection, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($curl_connection, CURLOPT_FOLLOWLOCATION, 1);

                curl_setopt($curl_connection, CURLOPT_POSTFIELDS, $post_string);

                $result = curl_exec($curl_connection);

                if ($result === FALSE) {
                    die(curl_error($curl_connection));
                }

                curl_close($curl_connection);
                $verification_response = strtoupper(trim(strip_tags($result)));


                if ('SUCCESS' == $verification_response) {
                    //echo '<h2><strong>SUCCESS:</strong> Transaction is successful !!!</h2>';
                    //SAVE DATA WITH WHITE FLAG
                    $dataSave = array(
                        'unique_ID' => $_REQUEST['oid'],
                        'ref_ID' => $_REQUEST['refId'],
                        'parent_ID' => $this->session->userdata('userID'),
                        'type' => $this->session->userdata('userType'),
                        'privileged' => $planRow->privileged,
                        'price' => $planRow->per_year_fee,
                        'free_ad' => $planRow->free_ad,
                        'title' => $planRow->title,
                        'ip_address' => $this->input->ip_address(),
                        'created_at' => get_date_time(),
                        'payment_status' => 'success',
                        'purchase_type' => 'plan',
                        'start_date' => today_date(),
                        'end_date' => oneYearDate()
                    );

                    $formTo = 'from ' . $this->session->userdata('userLevel') . ' to ' . $planRow->title;

                    if ($this->fipn->insert($dataSave)) {

                        //LETS EMAIL TO ADMIN
                        $data['to'] = admin_notification_email();
                        $data['toName'] = admin_notification_email_name();
                        $data['subject'] = 'Membership upgraded via E-Sewa';
                        $data['template'] = 'membership_admin';
                        $data['others'] = array(
                            'userfullname' => $this->session->userdata('userName'),
                            'emailaddress' => $this->session->userdata('userEmail'),
                            'fromTo' => $formTo,
                            'amount' => $planRow->per_year_fee,
                            'refID' => $_REQUEST['refId'],
                            'msg' => 'User ' . $this->session->userdata('userName') . ' upgrade membership subscription by E-Sewa Payment as below.',
                        );
                        Modules::run('emailsystem/doEmail', $data);
                        //LETS DO EMAIL TO ADMIN
                        //LETS EMAIL TO USER
                        $data['to'] = $this->session->userdata('userEmail');
                        $data['toName'] = $this->session->userdata('userName');
                        $data['from'] = admin_notification_email();
                        $data['fromName'] = admin_notification_email_name();
                        $data['subject'] = 'Membership upgraded via E-Sewa';
                        $data['template'] = 'membership_user';
                        $data['others'] = array(
                            'userfullname' => $this->session->userdata('userName'),
                            'emailaddress' => $this->session->userdata('userEmail'),
                            'fromTo' => $formTo,
                            'msg' => 'Your membership subscription has been updated.',
                        );
                        Modules::run('emailsystem/doEmail', $data);
                        //LETS DO EMAIL TO USER
                        //LETS UPGRADE THE USER LEVEL HERE
                        Modules::run('user/esewa_memeber_upgrade_instant', $this->session->userdata('userID'), $planRow->title);
                        $this->session->set_flashdata(array('msgs' => 'Payment Successfull!<br/> Your account has been upgraded ' . $formTo . '<br/> To view your purchase history <a href="' . site_url('myplans') . '">click here</a>'));
                    } else {

                        //LETS EMAIL TO ADMIN
                        $data['to'] = admin_notification_email();
                        $data['toName'] = admin_notification_email_name();
                        $data['subject'] = 'Membership upgraded via E-Sewa';
                        $data['template'] = 'membership_admin';
                        $data['others'] = array(
                            'userfullname' => $this->session->userdata('userName'),
                            'emailaddress' => $this->session->userdata('userEmail'),
                            'fromTo' => $formTo,
                            'amount' => $planRow->per_year_fee,
                            'refID' => $_REQUEST['refId'],
                            'msg' => 'User ' . $this->session->userdata('userName') . ' upgrade membership subscription by E-Sewa Payment as below. But technically system unable to mark his level at the right time. Please do mark his account as per information below in system backend mannually.',
                        );
                        Modules::run('emailsystem/doEmail', $data);
                        //LETS DO EMAIL TO ADMIN
                        //LETS EMAIL TO USER
                        $data['to'] = $this->session->userdata('userEmail');
                        $data['toName'] = $this->session->userdata('userName');
                        $data['from'] = admin_notification_email();
                        $data['fromName'] = admin_notification_email_name();
                        $data['subject'] = 'Membership upgraded via E-Sewa';
                        $data['template'] = 'membership_user';
                        $data['others'] = array(
                            'userfullname' => $this->session->userdata('userName'),
                            'emailaddress' => $this->session->userdata('userEmail'),
                            'fromTo' => $formTo,
                            'msg' => 'Payment Successfull!<br/> We will update you ' . $formTo . ' as soon as possible. Upgrade details as below:',
                        );
                        Modules::run('emailsystem/doEmail', $data);
                        //LETS DO EMAIL TO USER
                        $this->session->set_flashdata(array('msgs' => 'Payment Successfull!<br/> We will update you ' . $formTo . ' as soon as possible.'));
                    }

                    //REDIRECT TO PAYMENT INFO PAGE
                    redirect('payment');
                    exit();
                } else {

                    $formTo = 'from ' . $this->session->userdata('userLevel') . ' to ' . $planRow->title;
                    // echo '<h2><strong>ERROR:</strong> Transaction could not be verified</h2>';
                    //SAVE DATA WITH UNSUCCESSFUL
                    $dataSave = array(
                        'unique_ID' => $_REQUEST['oid'],
                        'ref_ID' => $_REQUEST['refId'],
                        'parent_ID' => $this->session->userdata('userID'),
                        'type' => $this->session->userdata('userType'),
                        'privileged' => $planRow->privileged,
                        'price' => $planRow->per_year_fee,
                        'free_ad' => $planRow->free_ad,
                        'title' => $planRow->title,
                        'ip_address' => $this->input->ip_address(),
                        'created_at' => get_date_time(),
                        'payment_status' => 'fail'
                    );

                    if ($this->fipn->insert($dataSave)) {
                        //LETS EMAIL TO ADMIN
                        $data['to'] = admin_notification_email();
                        $data['toName'] = admin_notification_email_name();
                        $data['subject'] = 'Unsuccessful Membership upgraded via E-Sewa';
                        $data['template'] = 'membership_admin';
                        $data['others'] = array(
                            'userfullname' => $this->session->userdata('userName'),
                            'emailaddress' => $this->session->userdata('userEmail'),
                            'fromTo' => $formTo,
                            'amount' => $planRow->per_year_fee,
                            'refID' => $_REQUEST['refId'],
                            'msg' => 'User ' . $this->session->userdata('userName') . ' try upgrade membership subscription by E-Sewa Payment as below but payment could not be verified by E-Sewa.',
                        );
                        Modules::run('emailsystem/doEmail', $data);
                        //LETS DO EMAIL TO ADMIN
                        //LETS EMAIL TO USER
                        $data['to'] = $this->session->userdata('userEmail');
                        $data['toName'] = $this->session->userdata('userName');
                        $data['from'] = admin_notification_email();
                        $data['fromName'] = admin_notification_email_name();
                        $data['subject'] = 'Membership upgraded via E-Sewa';
                        $data['template'] = 'membership_user';
                        $data['others'] = array(
                            'userfullname' => $this->session->userdata('userName'),
                            'emailaddress' => $this->session->userdata('userEmail'),
                            'fromTo' => $formTo,
                            'msg' => 'Payment Unsuccessfull!<br/> Your payment is not verified by E-Sewa while processing payment.',
                        );
                        Modules::run('emailsystem/doEmail', $data);
                        //LETS DO EMAIL TO USER
                        $this->session->set_flashdata(array('msge' => 'Payment is not successfull!<br/> Please contact website administator as soon as possible.'));
                    } else {
                        //LETS EMAIL TO ADMIN
                        $data['to'] = admin_notification_email();
                        $data['toName'] = admin_notification_email_name();
                        $data['subject'] = 'Unsuccessful Membership upgraded via E-Sewa';
                        $data['template'] = 'membership_admin';
                        $data['others'] = array(
                            'userfullname' => $this->session->userdata('userName'),
                            'emailaddress' => $this->session->userdata('userEmail'),
                            'fromTo' => $formTo,
                            'amount' => $planRow->per_year_fee,
                            'refID' => $_REQUEST['refId'],
                            'msg' => 'User ' . $this->session->userdata('userName') . ' try upgrade membership subscription by E-Sewa Payment as below but payment could not be verified by E-Sewa.',
                        );
                        Modules::run('emailsystem/doEmail', $data);
                        //LETS DO EMAIL TO ADMIN
                        //LETS EMAIL TO USER
                        $data['to'] = $this->session->userdata('userEmail');
                        $data['toName'] = $this->session->userdata('userName');
                        $data['from'] = admin_notification_email();
                        $data['fromName'] = admin_notification_email_name();
                        $data['subject'] = 'Membership upgraded via E-Sewa';
                        $data['template'] = 'membership_user';
                        $data['others'] = array(
                            'userfullname' => $this->session->userdata('userName'),
                            'emailaddress' => $this->session->userdata('userEmail'),
                            'fromTo' => $formTo,
                            'msg' => 'Payment Unsuccessfull!<br/> Your payment is not verified by E-Sewa while processing payment.',
                        );
                        Modules::run('emailsystem/doEmail', $data);
                        //LETS DO EMAIL TO USER
                        $this->session->set_flashdata(array('msge' => 'Payment is not successfull!<br/> Please contact website administator as soon as possible.'));
                    }
                    redirect('payment');
                    exit();
                }
            } else {
                echo 'Somthing not good!';
            }
        } else {
            echo "Error";
        }
    }

    //AD SUBSCRIPTIONS PAYMENT IN SUCCESS
    public function subs_success() {

        $string = $_REQUEST['oid'];
        $parts = explode('-', $string);

        $planID = substr($parts[1], 12);

        if ($parts[0] == 'College') {
            $planRow = Modules::run('college_ad_pricing/get_plan_by_ID', $planID);
        } elseif ($parts[0] == 'Institute') {
            $planRow = Modules::run('institute_ad_pricing/get_plan_by_ID', $planID);
        } else {
            $planRow = FALSE;
        }


        if ($planRow) {

            //create array of data to be posted
            $post_data['amt'] = $planRow->per_year_fee;
            $post_data['scd'] = $this->config->item('esewa')['merchant_id'];
            $post_data['pid'] = $_REQUEST['oid'];
            $post_data['rid'] = $_REQUEST['refId'];

            foreach ($post_data as $key => $value) {
                $post_items[] = $key . '=' . $value;
            }

            $post_string = implode('&', $post_items);
            $curl_connection = curl_init($this->config->item('esewa')['esewa_verfication_url']);
            if ($curl_connection == TRUE) {

                curl_setopt($curl_connection, CURLOPT_CONNECTTIMEOUT, 30);
                curl_setopt($curl_connection, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
                curl_setopt($curl_connection, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl_connection, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($curl_connection, CURLOPT_FOLLOWLOCATION, 1);

                curl_setopt($curl_connection, CURLOPT_POSTFIELDS, $post_string);

                $result = curl_exec($curl_connection);

                if ($result === FALSE) {
                    die(curl_error($curl_connection));
                }

                curl_close($curl_connection);
                $verification_response = strtoupper(trim(strip_tags($result)));


                if ('SUCCESS' == $verification_response) {
                    //echo '<h2><strong>SUCCESS:</strong> Transaction is successful !!!</h2>';
                    //SAVE DATA WITH WHITE FLAG
                    $dataSave = array(
                        'unique_ID' => $_REQUEST['oid'],
                        'ref_ID' => $_REQUEST['refId'],
                        'parent_ID' => $this->session->userdata('userID'),
                        'type' => $this->session->userdata('userType'),
                        'privileged' => '--',
                        'price' => $planRow->per_year_fee,
                        'free_ad' => $planRow->number_ads,
                        'title' => $planRow->number_ads . ' Ad NPR ' . $planRow->per_year_fee,
                        'ip_address' => $this->input->ip_address(),
                        'created_at' => get_date_time(),
                        'payment_status' => 'success',
                        'purchase_type' => 'subscription',
                        'start_date' => today_date(),
                        'end_date' => oneYearDate()
                    );

                    $formTo = $planRow->number_ads . ' Ad NPR ' . $planRow->per_year_fee;

                    if ($this->fipn->insert($dataSave)) {

                        //LETS EMAIL TO ADMIN
                        $data['to'] = admin_notification_email();
                        $data['toName'] = admin_notification_email_name();
                        $data['subject'] = 'Ad suscribed and purchased';
                        $data['template'] = 'ad_subscription_admin';
                        $data['others'] = array(
                            'userfullname' => $this->session->userdata('userName'),
                            'emailaddress' => $this->session->userdata('userEmail'),
                            'fromTo' => $formTo,
                            'amount' => $planRow->per_year_fee,
                            'refID' => $_REQUEST['refId'],
                            'msg' => 'User ' . $this->session->userdata('userName') . ' purchase ad subscription by E-Sewa Payment as below.',
                        );
                        Modules::run('emailsystem/doEmail', $data);
                        //LETS DO EMAIL TO ADMIN
                        //LETS EMAIL TO USER
                        $data['to'] = $this->session->userdata('userEmail');
                        $data['toName'] = $this->session->userdata('userName');
                        $data['from'] = admin_notification_email();
                        $data['fromName'] = admin_notification_email_name();
                        $data['subject'] = 'Ad subscirbed successfully';
                        $data['template'] = 'ad_subscription_user';
                        $data['others'] = array(
                            'userfullname' => $this->session->userdata('userName'),
                            'emailaddress' => $this->session->userdata('userEmail'),
                            'fromTo' => $formTo,
                            'msg' => 'Ad subscription has been completed as below.',
                        );
                        Modules::run('emailsystem/doEmail', $data);
                        //LETS DO EMAIL TO USER
                        $this->session->set_flashdata(array('msgs' => 'Payment Successfull!<br/> Your account has been upgraded ' . $formTo . '<br/> To view your purchase history <a href="' . site_url('myplans') . '">click here</a>'));
                    } else {

                        //LETS EMAIL TO ADMIN
                        $data['to'] = admin_notification_email();
                        $data['toName'] = admin_notification_email_name();
                        $data['subject'] = 'Membership upgraded via E-Sewa';
                        $data['template'] = 'membership_admin';
                        $data['others'] = array(
                            'userfullname' => $this->session->userdata('userName'),
                            'emailaddress' => $this->session->userdata('userEmail'),
                            'fromTo' => $formTo,
                            'amount' => $planRow->per_year_fee,
                            'refID' => $_REQUEST['refId'],
                            'msg' => 'User ' . $this->session->userdata('userName') . ' upgrade membership subscription by E-Sewa Payment as below. But technically system unable to mark his level at the right time. Please do mark his account as per information below in system backend mannually.',
                        );
                        Modules::run('emailsystem/doEmail', $data);
                        //LETS DO EMAIL TO ADMIN
                        //LETS EMAIL TO USER
                        $data['to'] = $this->session->userdata('userEmail');
                        $data['toName'] = $this->session->userdata('userName');
                        $data['from'] = admin_notification_email();
                        $data['fromName'] = admin_notification_email_name();
                        $data['subject'] = 'Membership upgraded via E-Sewa';
                        $data['template'] = 'membership_user';
                        $data['others'] = array(
                            'userfullname' => $this->session->userdata('userName'),
                            'emailaddress' => $this->session->userdata('userEmail'),
                            'fromTo' => $formTo,
                            'msg' => 'Payment Successfull!<br/> We will update you ' . $formTo . ' as soon as possible. Upgrade details as below:',
                        );
                        Modules::run('emailsystem/doEmail', $data);
                        //LETS DO EMAIL TO USER
                        $this->session->set_flashdata(array('msgs' => 'Payment Successfull!<br/> We will update you ' . $formTo . ' as soon as possible.'));
                    }

                    //REDIRECT TO PAYMENT INFO PAGE
                    redirect('payment');
                    exit();
                } else {

                    $formTo = 'from ' . $this->session->userdata('userLevel') . ' to ' . $planRow->title;
                    // echo '<h2><strong>ERROR:</strong> Transaction could not be verified</h2>';
                    //SAVE DATA WITH UNSUCCESSFUL
                    $dataSave = array(
                        'unique_ID' => $_REQUEST['oid'],
                        'ref_ID' => $_REQUEST['refId'],
                        'parent_ID' => $this->session->userdata('userID'),
                        'type' => $this->session->userdata('userType'),
                        'privileged' => $planRow->privileged,
                        'price' => $planRow->per_year_fee,
                        'free_ad' => $planRow->free_ad,
                        'title' => $planRow->title,
                        'ip_address' => $this->input->ip_address(),
                        'created_at' => get_date_time(),
                        'payment_status' => 'fail'
                    );

                    if ($this->fipn->insert($dataSave)) {
                        //LETS EMAIL TO ADMIN
                        $data['to'] = admin_notification_email();
                        $data['toName'] = admin_notification_email_name();
                        $data['subject'] = 'Unsuccessful Membership upgraded via E-Sewa';
                        $data['template'] = 'membership_admin';
                        $data['others'] = array(
                            'userfullname' => $this->session->userdata('userName'),
                            'emailaddress' => $this->session->userdata('userEmail'),
                            'fromTo' => $formTo,
                            'amount' => $planRow->per_year_fee,
                            'refID' => $_REQUEST['refId'],
                            'msg' => 'User ' . $this->session->userdata('userName') . ' try upgrade membership subscription by E-Sewa Payment as below but payment could not be verified by E-Sewa.',
                        );
                        Modules::run('emailsystem/doEmail', $data);
                        //LETS DO EMAIL TO ADMIN
                        //LETS EMAIL TO USER
                        $data['to'] = $this->session->userdata('userEmail');
                        $data['toName'] = $this->session->userdata('userName');
                        $data['from'] = admin_notification_email();
                        $data['fromName'] = admin_notification_email_name();
                        $data['subject'] = 'Membership upgraded via E-Sewa';
                        $data['template'] = 'membership_user';
                        $data['others'] = array(
                            'userfullname' => $this->session->userdata('userName'),
                            'emailaddress' => $this->session->userdata('userEmail'),
                            'fromTo' => $formTo,
                            'msg' => 'Payment Unsuccessfull!<br/> Your payment is not verified by E-Sewa while processing payment.',
                        );
                        Modules::run('emailsystem/doEmail', $data);
                        //LETS DO EMAIL TO USER
                        $this->session->set_flashdata(array('msge' => 'Payment is not successfull!<br/> Please contact website administator as soon as possible.'));
                    } else {
                        //LETS EMAIL TO ADMIN
                        $data['to'] = admin_notification_email();
                        $data['toName'] = admin_notification_email_name();
                        $data['subject'] = 'Unsuccessful Membership upgraded via E-Sewa';
                        $data['template'] = 'membership_admin';
                        $data['others'] = array(
                            'userfullname' => $this->session->userdata('userName'),
                            'emailaddress' => $this->session->userdata('userEmail'),
                            'fromTo' => $formTo,
                            'amount' => $planRow->per_year_fee,
                            'refID' => $_REQUEST['refId'],
                            'msg' => 'User ' . $this->session->userdata('userName') . ' try upgrade membership subscription by E-Sewa Payment as below but payment could not be verified by E-Sewa.',
                        );
                        Modules::run('emailsystem/doEmail', $data);
                        //LETS DO EMAIL TO ADMIN
                        //LETS EMAIL TO USER
                        $data['to'] = $this->session->userdata('userEmail');
                        $data['toName'] = $this->session->userdata('userName');
                        $data['from'] = admin_notification_email();
                        $data['fromName'] = admin_notification_email_name();
                        $data['subject'] = 'Membership upgraded via E-Sewa';
                        $data['template'] = 'membership_user';
                        $data['others'] = array(
                            'userfullname' => $this->session->userdata('userName'),
                            'emailaddress' => $this->session->userdata('userEmail'),
                            'fromTo' => $formTo,
                            'msg' => 'Payment Unsuccessfull!<br/> Your payment is not verified by E-Sewa while processing payment.',
                        );
                        Modules::run('emailsystem/doEmail', $data);
                        //LETS DO EMAIL TO USER
                        $this->session->set_flashdata(array('msge' => 'Payment is not successfull!<br/> Please contact website administator as soon as possible.'));
                    }
                    redirect('payment');
                    exit();
                }
            } else {
                echo 'Somthing not good!';
            }
        } else {
            echo "Error";
        }
    }

    //EVENT PLAN BUY BY ALL OF THE USERS
    public function even_success() {

        $string = $_REQUEST['oid'];
        $parts = explode('-', $string);

        $planID = substr($parts[1], 12);

        $planRow = FALSE; //The plan row is event row actually
        //GET ONLY THE VALID EVENT ROW
        $planRow = Modules::run('events/get_valid_event_row', $planID);

        if ($planRow) {

            //create array of data to be posted
            $post_data['amt'] = $planRow->pirce;
            $post_data['scd'] = $this->config->item('esewa')['merchant_id'];
            $post_data['pid'] = $_REQUEST['oid'];
            $post_data['rid'] = $_REQUEST['refId'];

            foreach ($post_data as $key => $value) {
                $post_items[] = $key . '=' . $value;
            }

            $post_string = implode('&', $post_items);
            $curl_connection = curl_init($this->config->item('esewa')['esewa_verfication_url']);
            if ($curl_connection == TRUE) {

                curl_setopt($curl_connection, CURLOPT_CONNECTTIMEOUT, 30);
                curl_setopt($curl_connection, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
                curl_setopt($curl_connection, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl_connection, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($curl_connection, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($curl_connection, CURLOPT_POSTFIELDS, $post_string);

                $result = curl_exec($curl_connection);
                if ($result === FALSE) {
                    die(curl_error($curl_connection));
                }
                curl_close($curl_connection);
                $verification_response = strtoupper(trim(strip_tags($result)));

                if ('SUCCESS' == $verification_response) {
                    //echo '<h2><strong>SUCCESS:</strong> Transaction is successful !!!</h2>';
                    //SAVE DATA WITH WHITE FLAG
                    $dataSave = array(
                        'unique_ID' => $_REQUEST['oid'],
                        'ref_ID' => $_REQUEST['refId'],
                        'parent_ID' => $this->session->userdata('userID'),
                        'type' => $this->session->userdata('userType'),
                        'privileged' => '--',
                        'price' => $planRow->price,
                        'free_ad' => '--',
                        'title' => $planRow->title . ' NPR ' . $planRow->price,
                        'ip_address' => $this->input->ip_address(),
                        'created_at' => get_date_time(),
                        'payment_status' => 'success',
                        'purchase_type' => 'events',
                        'start_date' => today_date(),
                        'end_date' => $planRow->event_date
                    );

                    $formTo = $planRow->title . ' NPR ' . $planRow->price;

                    if ($this->fipn->insert($dataSave)) {

                        //LETS EMAIL TO ADMIN
                        $data['to'] = admin_notification_email();
                        $data['toName'] = admin_notification_email_name();
                        $data['subject'] = 'Event booking by ' . $this->session->userdata('userName');
                        $data['template'] = 'event_booking_admin';
                        $data['others'] = array(
                            'userfullname' => $this->session->userdata('userName'),
                            'emailaddress' => $this->session->userdata('userEmail'),
                            'fromTo' => $formTo,
                            'amount' => $planRow->per_year_fee,
                            'event_start' => $planRow->event_date . ' Time: ' . $planRow->event_time,
                            'venue' => $planRow->venue,
                            'refID' => $_REQUEST['refId'],
                            'msg' => 'User ' . $this->session->userdata('userName') . ' purchase ad subscription by E-Sewa Payment as below.',
                        );
                        Modules::run('emailsystem/doEmail', $data);
                        //LETS DO EMAIL TO ADMIN
                        //LETS EMAIL TO USER
                        $data['to'] = $this->session->userdata('userEmail');
                        $data['toName'] = $this->session->userdata('userName');
                        $data['from'] = admin_notification_email();
                        $data['fromName'] = admin_notification_email_name();
                        $data['subject'] = 'Event Booking Successful';
                        $data['template'] = 'event_booking_user';
                        $data['others'] = array(
                            'userfullname' => $this->session->userdata('userName'),
                            'emailaddress' => $this->session->userdata('userEmail'),
                            'fromTo' => $formTo,
                            'event_start' => $planRow->event_date . ' Time: ' . $planRow->event_time,
                            'venue' => $planRow->venue,
                            'msg' => 'Event booking has been completed as below.',
                        );
                        Modules::run('emailsystem/doEmail', $data);
                        //LETS DO EMAIL TO USER
                        $this->session->set_flashdata(array('msgs' => 'Payment Successfull!<br/> Thank you for your payment.<br/> To view your purchase history <a href="' . site_url('myplans') . '">click here</a>'));
                    } else {

                        //LETS EMAIL TO ADMIN
                        $data['to'] = admin_notification_email();
                        $data['toName'] = admin_notification_email_name();
                        $data['subject'] = 'Event booking by ' . $this->session->userdata('userName');
                        $data['template'] = 'event_booking_admin';
                        $data['others'] = array(
                            'userfullname' => $this->session->userdata('userName'),
                            'emailaddress' => $this->session->userdata('userEmail'),
                            'fromTo' => $formTo,
                            'amount' => $planRow->per_year_fee,
                            'event_start' => $planRow->event_date . ' Time: ' . $planRow->event_time,
                            'venue' => $planRow->venue,
                            'refID' => $_REQUEST['refId'],
                            'msg' => 'User ' . $this->session->userdata('userName') . ' booked Event and activities but system unable to complete the process.',
                        );
                        Modules::run('emailsystem/doEmail', $data);
                        //LETS DO EMAIL TO ADMIN
                        //LETS EMAIL TO USER
                        $data['to'] = $this->session->userdata('userEmail');
                        $data['toName'] = $this->session->userdata('userName');
                        $data['from'] = admin_notification_email();
                        $data['fromName'] = admin_notification_email_name();
                        $data['subject'] = 'Event Booking Successful';
                        $data['template'] = 'event_booking_user';
                        $data['others'] = array(
                            'userfullname' => $this->session->userdata('userName'),
                            'emailaddress' => $this->session->userdata('userEmail'),
                            'fromTo' => $formTo,
                            'event_start' => $planRow->event_date . ' Time: ' . $planRow->event_time,
                            'venue' => $planRow->venue,
                            'msg' => 'Event booking has been completed as below.',
                        );
                        Modules::run('emailsystem/doEmail', $data);
                        //LETS DO EMAIL TO USER
                        $this->session->set_flashdata(array('msgs' => 'Payment Successfull!<br/> Thank you for your payment.'));
                    }

                    //REDIRECT TO PAYMENT INFO PAGE
                    redirect('payment');
                    exit();
                } else {

                    $formTo = 'from ' . $this->session->userdata('userLevel') . ' to ' . $planRow->title;
                    // echo '<h2><strong>ERROR:</strong> Transaction could not be verified</h2>';
                    //SAVE DATA WITH UNSUCCESSFUL

                    $dataSave = array(
                        'unique_ID' => $_REQUEST['oid'],
                        'ref_ID' => $_REQUEST['refId'],
                        'parent_ID' => $this->session->userdata('userID'),
                        'type' => $this->session->userdata('userType'),
                        'privileged' => '--',
                        'price' => $planRow->price,
                        'free_ad' => '--',
                        'title' => $planRow->title . ' NPR ' . $planRow->price,
                        'ip_address' => $this->input->ip_address(),
                        'created_at' => get_date_time(),
                        'payment_status' => 'fail',
                        'purchase_type' => 'events',
                        'start_date' => today_date(),
                        'end_date' => $planRow->event_date
                    );

                    if ($this->fipn->insert($dataSave)) {
                        //LETS EMAIL TO ADMIN
                        $data['to'] = admin_notification_email();
                        $data['toName'] = admin_notification_email_name();
                        $data['subject'] = 'Unsuccessful Membership upgraded via E-Sewa';
                        $data['template'] = 'membership_admin';
                        $data['others'] = array(
                            'userfullname' => $this->session->userdata('userName'),
                            'emailaddress' => $this->session->userdata('userEmail'),
                            'fromTo' => $formTo,
                            'amount' => $planRow->per_year_fee,
                            'refID' => $_REQUEST['refId'],
                            'msg' => 'User ' . $this->session->userdata('userName') . ' try upgrade membership subscription by E-Sewa Payment as below but payment could not be verified by E-Sewa.',
                        );
                        Modules::run('emailsystem/doEmail', $data);
                        //LETS DO EMAIL TO ADMIN
                        //LETS EMAIL TO USER
                        $data['to'] = $this->session->userdata('userEmail');
                        $data['toName'] = $this->session->userdata('userName');
                        $data['from'] = admin_notification_email();
                        $data['fromName'] = admin_notification_email_name();
                        $data['subject'] = 'Membership upgraded via E-Sewa';
                        $data['template'] = 'membership_user';
                        $data['others'] = array(
                            'userfullname' => $this->session->userdata('userName'),
                            'emailaddress' => $this->session->userdata('userEmail'),
                            'fromTo' => $formTo,
                            'msg' => 'Payment Unsuccessfull!<br/> Your payment is not verified by E-Sewa while processing payment.',
                        );
                        Modules::run('emailsystem/doEmail', $data);
                        //LETS DO EMAIL TO USER
                        $this->session->set_flashdata(array('msge' => 'Payment is not successfull!<br/> Please contact website administator as soon as possible.'));
                    } else {
                        //LETS EMAIL TO ADMIN
                        $data['to'] = admin_notification_email();
                        $data['toName'] = admin_notification_email_name();
                        $data['subject'] = 'Unsuccessful Membership upgraded via E-Sewa';
                        $data['template'] = 'membership_admin';
                        $data['others'] = array(
                            'userfullname' => $this->session->userdata('userName'),
                            'emailaddress' => $this->session->userdata('userEmail'),
                            'fromTo' => $formTo,
                            'amount' => $planRow->per_year_fee,
                            'refID' => $_REQUEST['refId'],
                            'msg' => 'User ' . $this->session->userdata('userName') . ' try upgrade membership subscription by E-Sewa Payment as below but payment could not be verified by E-Sewa.',
                        );
                        Modules::run('emailsystem/doEmail', $data);
                        //LETS DO EMAIL TO ADMIN
                        //LETS EMAIL TO USER
                        $data['to'] = $this->session->userdata('userEmail');
                        $data['toName'] = $this->session->userdata('userName');
                        $data['from'] = admin_notification_email();
                        $data['fromName'] = admin_notification_email_name();
                        $data['subject'] = 'Membership upgraded via E-Sewa';
                        $data['template'] = 'membership_user';
                        $data['others'] = array(
                            'userfullname' => $this->session->userdata('userName'),
                            'emailaddress' => $this->session->userdata('userEmail'),
                            'fromTo' => $formTo,
                            'msg' => 'Payment Unsuccessfull!<br/> Your payment is not verified by E-Sewa while processing payment.',
                        );
                        Modules::run('emailsystem/doEmail', $data);
                        //LETS DO EMAIL TO USER
                        $this->session->set_flashdata(array('msge' => 'Payment is not successfull!<br/> Please contact website administator as soon as possible.'));
                    }
                    redirect('payment');
                    exit();
                }
            } else {
                echo 'Sorry server is busy right now please contact us for further process!';
            }
        } else {
            echo "No such event found on the system. Plase contact us for further process";
        }
    }

    public function even_fail() {
        echo 'Payment process has been stoped. You may visit our website.';
    }

    public function fail() {
        echo 'Processing fail';
    }

    //GET THE LATEST PAYMENT BY USER
    public function latest_payment_by($parentID) {
        if (isset($parentID)) {
            $row = $this->fipn->order_by('ID', 'DESC')->get_by(array('parent_ID' => $parentID));
            return $row ? $row : false;
        } else {
            return false;
        }
    }

    //GET THE LATEST PLAN PAYMENT BY USER
    public function latest_plan_payment_by($parentID = null) {
        if (isset($parentID)) {
            $row = $this->fipn->order_by('ID', 'DESC')->get_by(array('parent_ID' => $parentID, 'purchase_type' => 'plan'));
            return $row ? $row : false;
        } else {
            $parentID = $this->session->userdata('userID');
            $row = $this->fipn->order_by('ID', 'DESC')->get_by(array('parent_ID' => $parentID, 'purchase_type' => 'plan'));
            return $row ? $row : false;
        }
    }

    //GET THE LATEST SUBSCRIPTION PAYMENT BY USER
    public function latest_subscription_payment_by($parentID) {
        if (isset($parentID)) {
            $row = $this->fipn->order_by('ID', 'DESC')->get_by(array('parent_ID' => $parentID));
            return $row ? $row : false;
        } else {
            return false;
        }
    }

    //GET ALL THE PAYMENT BY PARENT ID
    public function get_all_the_payment_by($parentID = null) {
        if (isset($parentID)) {
            $row = $this->fipn->order_by('ID', 'DESC')->get_many_by(array('parent_ID' => $parentID));
            return $row ? $row : false;
        } else {
            $parentID = $this->session->userdata('userID');
            $row = $this->fipn->order_by('ID', 'DESC')->get_many_by(array('parent_ID' => $parentID));
            return $row ? $row : false;
        }
    }

    //GET TOTAL VALID ADS IN NUMBER OF COLLEGE
    public function get_total_ads() {
        $paymentRows = $this->get_all_the_payment_by();
        $latestPlanPayment = $this->latest_plan_payment_by();
        $cuPlanID = $latestPlanPayment ? $latestPlanPayment->ID : '0';

        $pTotalAd = 0;
        if ($paymentRows) {
            foreach ($paymentRows as $pRow):
                if ($pRow->purchase_type == 'plan') {
                    if ($cuPlanID == $pRow->ID) {
                        $pTotalAd = $pTotalAd + $pRow->free_ad;
                    }
                } else {
                    $pTotalAd = $pTotalAd + $pRow->free_ad;
                }
            endforeach;
            return $pTotalAd;
        } else {
            return $pTotalAd;
        }
    }

    //ad reming and link
    public function college_ad_remaining_link() {
        $totalAllowedAds = $this->get_total_ads();
        $totalAds = Modules::run('collegead/get_total_my_ads');
        $outPut = '';
        if ($totalAllowedAds > $totalAds) {
            $remaing = $totalAllowedAds - $totalAds;
            $outPut .= 'you have ' . $remaing . ' ad. Please choose suitable plan to purchase more ads';
        } elseif ($totalAllowedAds == $totalAds) {
            $outPut .= 'you have crossed the limit of free ad placement. Please, <a href="' . site_url("plans") . '">Choose a suitable payment plan to purchase more ads</a>';
        } elseif ($totalAllowedAds < $totalAds) {
            $outPut .= 'you have crossed the limit of free ad placement. Please, <a href="' . site_url("plans") . '">Choose a suitable payment plan to purchase more ads</a>';
        } else {
            
        }
        return $outPut;
    }

    //ad remaing link for institute
    public function institute_ad_remaining_link() {
        $totalAllowedAds = $this->get_total_ads();
        $totalAds = Modules::run('institutead/get_total_my_ads');
        $outPut = '';
        if ($totalAllowedAds > $totalAds) {
            $remaing = $totalAllowedAds - $totalAds;
            $outPut .= 'you have ' . $remaing . ' ad. Please choose suitable plan to purchase more ads';
        } elseif ($totalAllowedAds == $totalAds) {
            $outPut .= 'you have crossed the limit of free ad placement. Please, <a href="' . site_url("plans") . '">Choose a suitable payment plan to purchase more ads</a>';
        } elseif ($totalAllowedAds < $totalAds) {
            $outPut .= 'you have crossed the limit of free ad placement. Please, <a href="' . site_url("plans") . '">Choose a suitable payment plan to purchase more ads</a>';
        } else {
            
        }
        return $outPut;
    }
}
?>