<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Imagehub_m extends MY_Model {

    /**
     * @ Author: Suraj Bajracharya
     * @ Author URL : www.indesignmedia.net
     * @see http://indesignmedia.net
     */
    protected $_table = SYSTEMIMAGES;

    public function __construct() {
        parent::__construct();
        $this->_database = $this->db;
        $this->primary_key = 'ID';
    }

    public function index() {
        echo "No good stuff";
    }

    /*
     * **************************************************************
     * @ TAKING CARE ABOUT PRODUCT CATEGORY
     * ************************************************************* 
     */

    //get feature image
    public function get_feature_image($id) {
        if (isset($id)) {
            $row = $this->get($id);
            if ($row) {
                if (file_exists('./media/' . $row->fname)) {
                    $fileName = $row->fname;
                } else {
                    $fileName = 'sample.jpg';
                }
            } else {
                $fileName = 'sample.jpg';
            }
        } else {
            $fileName = 'sample.jpg';
        }
        return $fileName;
    }

    //RETURN FILE NAME WITH SIZE
    public function filename_sized($id, $size) {
        if (isset($id)) {
            $row = $this->get($id);
            if ($row) {
                if (file_exists('./media/' . $row->fname)) {
                    $fileName = $row->fname;
                    $fileName = image(base_url() . 'media/' . $fileName, $size);
                } else {
                    $fileName = base_url() . 'media/sample.jpg';
                }
            } else {
                $fileName = base_url() . 'media/sample.jpg';
            }
        } else {
            $fileName = base_url() . 'media/sample.jpg';
        }
        return $fileName;
    }

    //GET ICONS AS PER FILE TYPE
    public function get_file_icons($id) {
        if (isset($id)) {
            $row = $this->get($id);
            if ($row) {
                if (file_exists('./media/' . $row->fname)) {
                    $fileName = $row->fname;
                    $array = explode('.', $fileName);
                    $extension = end($array);
                    $icon = get_placeholder_img($extension);
                    $fileName = base_url() . 'media/' . $icon;
                } else {
                    $fileName = base_url() . 'media/file.png';
                }
            } else {
                $fileName = base_url() . 'media/file.png';
            }
        } else {
            $fileName = base_url() . 'media/file.png';
        }
        return $fileName;
    }

    //size specific
    public function get_feature_image_size($id, $size) {
        $title = COMPANYNAME;
        if (isset($id)) {
            $row = $this->get($id);
            if ($row) {
                if (file_exists('./media/' . $row->fname)) {
                    $fileName = $row->fname;
                    $title = $row->title;
                } else {
                    $fileName = 'sample.jpg';
                }
            } else {
                $fileName = 'sample.jpg';
            }
        } else {
            $fileName = 'sample.jpg';
        }
        $img = image(base_url() . 'media/' . $fileName, $size);

        if ($fileName == 'sample.jpg') {
            return '';
        } else {
            return '<img src="' . $img . '" alt="' . $title . '" title="' . $title . '" class="img-responsive">';
        }
    }

    public function get_profile_image($id) {
        $title = COMPANYNAME;
        if (isset($id)) {
            $row = $this->get($id);
            if ($row) {
                if (file_exists('./media/' . $row->fname)) {
                    $fileName = $row->fname;
                    $title = $row->title;
                } else {
                    $fileName = 'sample.jpg';
                }
            } else {
                $fileName = 'sample.jpg';
            }
        } else {
            $fileName = 'sample.jpg';
        }

        $img = image(base_url() . 'media/' . $fileName, 'profilethumb');
        //$img = base_url() . 'media/' . $fileName;

        if ($fileName == 'sample.jpg') {
            return '<img src="' . $img . '" alt="' . $title . '" title="' . $title . '" class="img-responsive">';
        } else {
            return '<img src="' . $img . '" alt="' . $title . '" title="' . $title . '" class="img-responsive">';
        }
    }

    public function get_profile_image_real($id) {
        $title = COMPANYNAME;
        if (isset($id)) {
            $row = $this->get($id);
            if ($row) {
                if (file_exists('./media/' . $row->fname)) {
                    $fileName = $row->fname;
                    $title = $row->title;
                } else {
                    $fileName = 'sample.jpg';
                }
            } else {
                $fileName = 'sample.jpg';
            }
        } else {
            $fileName = 'sample.jpg';
        }

        $img = image(base_url() . 'media/' . $fileName, 'profilethumbReal');
        //$img = base_url() . 'media/' . $fileName;

        if ($fileName == 'sample.jpg') {
            return '<img src="' . $img . '" alt="' . $title . '" title="' . $title . '" class="img-responsive">';
        } else {
            return '<img src="' . $img . '" alt="' . $title . '" title="' . $title . '" class="img-responsive">';
        }
    }

    public function profile_img($id, $size = null) {
        $title = COMPANYNAME;
        if (isset($id)) {
            $row = $this->get($id);
            if ($row) {
                if (file_exists('./media/' . $row->fname)) {
                    $fileName = $row->fname;
                    $title = $row->title;
                } else {
                    $fileName = 'sample.jpg';
                }
            } else {
                $fileName = 'sample.jpg';
            }
        } else {
            $fileName = 'sample.jpg';
        }
        $img = base_url() . 'media/' . $fileName;

        if ($fileName == 'sample.jpg') {
            return '<img src="' . $img . '" alt="' . $title . '" title="' . $title . '" class="img-responsive">';
        } else {
            return '<img src="' . $img . '" alt="' . $title . '" title="' . $title . '" class="img-responsive">';
        }
    }

    //size specific
    public function get_img_size($id, $size) {
        $title = COMPANYNAME;
        if (isset($id)) {
            $row = $this->get($id);
            if ($row) {
                if (file_exists('./media/' . $row->fname)) {
                    $fileName = $row->fname;
                    $title = $row->title;
                } else {
                    $fileName = 'sample.jpg';
                }
            } else {
                $fileName = 'sample.jpg';
            }
        } else {
            $fileName = 'sample.jpg';
        }
        $img = image(base_url() . 'media/' . $fileName, $size);


        if ($fileName == 'sample.jpg') {
            return '';
        } else {
            return '<img src="' . $img . '" alt="' . $title . '" title="' . $title . '" class="img-responsive">';
        }
    }

    public function get_circle($id, $size) {
        $title = COMPANYNAME;
        if (isset($id)) {
            $row = $this->get($id);
            if ($row) {
                if (file_exists('./media/' . $row->fname)) {
                    $fileName = $row->fname;
                    $title = $row->title;
                } else {
                    $fileName = 'sample.jpg';
                }
            } else {
                $fileName = 'sample.jpg';
            }
        } else {
            $fileName = 'sample.jpg';
        }
        $img = image(base_url() . 'media/' . $fileName, $size);
        if ($fileName == 'sample.jpg') {
            //return '';
            return '<img src="' . $img . '" alt="' . $title . '" title="' . $title . '" class="img-responsive img-circle center-block" style="background:#fff; border:1px solid #f0f0f0; padding:2px;">';
        } else {
            return '<img src="' . $img . '" alt="' . $title . '" title="' . $title . '" class="img-responsive img-circle center-block" style="background:#fff; border:1px solid #f0f0f0; padding:2px;">';
        }
    }

    public function do_upload_ajax($filename, $class = null) {
        $config = array(
            'upload_path' => "./media/",
            'allowed_types' => "gif|jpg|png|jpeg|pdf",
            'overwrite' => FALSE,
            'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
            'max_height' => IMGMAXH,
            'max_width' => IMGMAXW
        );
        $this->load->library('upload', $config);
        if ($this->upload->do_upload($filename)) {

            $data = array(
                'upload_data' => $this->upload->data()
            );

            //MAKING TITLE FROM THE FILE NAME
            $imgTitle = make_image_title($data['upload_data']['file_name']);

            //LETS SAVE THE FILE
            if ($this->insert(array('title' => $imgTitle, 'fname' => $data['upload_data']['file_name']))) {

                $imgID = $this->get_last_id();

                //LETS UPDATE STUDENT ROW
                Modules::run('user/update_profile_image', $imgID);

                $dataview['fileNameTo'] = $data['upload_data']['file_name'];
                $dataview['fileID'] = $imgID;
                //$dataview['class'] = $class ? $class : '';

                $imgPath = base_url() . 'media/' . $data['upload_data']['file_name'];

                $imgSrc = '<img src="' . $imgPath . '"  class="img-responsive">';

                $returnData = array(
                    'status' => 'success',
                    'msg' => 'Image has been uploaded',
                    'img_id' => $this->get_last_id(),
                    'img_src' => $imgSrc
                );
            } else {
                $returnData = array(
                    'status' => 'error',
                    'msg' => 'Unable to upload image'
                );
            }
        } else {
            $returnData = array(
                'status' => 'error',
                'msg' => $this->upload->display_errors()
            );
        }
        return $returnData;
    }

    //GALLEY IMAGE
    public function do_upload_ajax_gallery($filename, $class = null) {
        $config = array(
            'upload_path' => "./media/",
            'allowed_types' => "gif|jpg|png|jpeg|pdf",
            'overwrite' => FALSE,
            'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
            'max_height' => IMGMAXH,
            'max_width' => IMGMAXW
        );
        $this->load->library('upload', $config);
        if ($this->upload->do_upload($filename)) {

            $data = array(
                'upload_data' => $this->upload->data()
            );

            //MAKING TITLE FROM THE FILE NAME
            $imgTitle = make_image_title($data['upload_data']['file_name']);

            //LETS SAVE THE FILE
            if ($this->insert(array('title' => $imgTitle, 'fname' => $data['upload_data']['file_name']))) {

                $imgID = $this->get_last_id();

                //LETS UPDATE STUDENT ROW
                Modules::run('user/update_gallery_image', $imgID);

                $dataview['fileNameTo'] = $data['upload_data']['file_name'];
                $dataview['fileID'] = $imgID;
                //$dataview['class'] = $class ? $class : '';

                $imgPath = base_url() . 'media/' . $data['upload_data']['file_name'];

                $iconPath = Modules::run('imagehub/get_file_icons', $imgID);
                $imgSrc = '<div class="col-md-2 galind-' . $imgID . '"><div class="gal_img_cover"><img src="' . $iconPath . '"  class="img-responsive"><a href="#" class="gal_del" data-id="' . $imgID . '"><i class="fa fa-times"></i></a><a href="' . $imgPath . '" target="_blank" class="viewfile"><i class="fa fa-eye"></i></a></div></div>';

                $returnData = array(
                    'status' => 'success',
                    'msg' => 'Image has been uploaded',
                    'img_id' => $this->get_last_id(),
                    'img_src' => $imgSrc
                );
            } else {
                $returnData = array(
                    'status' => 'error',
                    'msg' => 'Unable to upload image'
                );
            }
        } else {
            $returnData = array(
                'status' => 'error',
                'msg' => $this->upload->display_errors()
            );
        }
        return $returnData;
    }

    //GET LAT ID
    public function get_last_id() {
        $this->db->select_max('ID');
        $result = $this->db->get(SYSTEMIMAGES)->result_array();
        return $result[0]['ID'];
    }

}
