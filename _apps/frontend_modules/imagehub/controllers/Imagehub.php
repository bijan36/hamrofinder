<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Imagehub extends Frontendcontroller {

    /**
     * @ Author: Suraj Bajracharya
     * @ Author URL : www.indesignmedia.net
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     *
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('imagehub_m', 'imagehub');
    }

    /*
     * **************************************************************
     * @ IMAGE CONTROLLERS
     * ************************************************************* 
     */

    public function index() {
        redirect('site');
        exit();
    }

    public function get_row($id) {
        $row = $this->imagehub->get($id);
        return $row ? $row : false;
    }

    public function filename_sized($id, $size) {
        return $this->imagehub->filename_sized($id, $size);
    }

    public function get_file_icons($id) {
        return $this->imagehub->get_file_icons($id);
    }

    public function get_image_by_id($id) {
        return $this->imagehub->get_feature_image($id);
    }

    //BY ID AND SIZE
    public function get_feature_image($id, $size) {
        return $this->imagehub->get_feature_image_size($id, $size);
    }

    //BY ID AND SIZE
    public function get_img_size($id, $size) {
        return $this->imagehub->get_feature_image_size($id, $size);
    }

    //GETTING ALL THE IMAGE
    public function get_all() {
        $rows = $this->imagehub->order_by('ID', 'DESC')->get_all();
        return $rows ? $rows : false;
    }

    //BY ID AND SIZE
    public function get_circle_image($id, $size) {
        return $this->imagehub->get_circle($id, $size);
    }

    //BY ID AND SIZE
    public function get_profile_image($id) {
        return $this->imagehub->get_profile_image($id);
    }

    //BY ID AND SIZE
    public function get_profile_image_real($id) {
        return $this->imagehub->get_profile_image_real($id);
    }

    //ADDING BY AJAX
    public function studentpicupdate() {
        if ($this->input->post('addtolib') == 'yes') {
            $imgName = $_FILES['student_profile_pic']['name'];
            if (!empty($imgName)) {
                $class = $this->input->post('classname');
                $datas = $this->imagehub->do_upload_ajax('student_profile_pic', $class);
            } else {
                $datas = array(
                    'status' => 'error',
                    'msg' => 'Not uploaded'
                );
            }
        } else {
            $datas = array(
                'status' => 'error',
                'msg' => 'Not allowed'
            );
        }
        echo json_encode($datas);
        exit();
    }

    //GALLERY IMAGE UPLOAD
    public function collegegallery() {
        if ($this->input->post('addtolibgal') == 'yes') {
            $imageFile = $_FILES['gal_pic']['name'];
            if (!empty($imageFile)) {
                $datas = $this->imagehub->do_upload_ajax_gallery('gal_pic');
            } else {
                $datas = array(
                    'status' => 'error',
                    'msg' => 'Image not supported'
                );
            }
        } else {
            $datas = array(
                'status' => 'error',
                'msg' => 'Missing fields'
            );
        }
        echo json_encode($datas);
        exit();
    }

    //removing the gallery image
    public function del_gallery() {
        $galID = $this->input->post('id');
        $userID = $this->session->userdata('userID');
        $userRow = Modules::run('user/get_row', $userID);

        if (!empty($userRow->gallery_picture)) {
            if (strpos($userRow->gallery_picture, ',') !== false) {
                //more than one value
                $idArray = explode(',', $userRow->gallery_picture);
                $newIds = array();
                foreach ($idArray as $gp):
                    if ($gp != $galID) {
                        $newIds[] = $gp;
                    }
                endforeach;

                if (count($newIds) == 0) {
                    $finalIds = '';
                } elseif (count($newIds) == 1) {
                    $finalIds = $newIds[0];
                } else {
                    $comma = '';
                    $finalIds = '';
                    foreach ($newIds as $nid):
                        $finalIds .= $comma . $nid;
                        $comma = ',';
                    endforeach;
                }
                if (Modules::run('user/deletegalleyimage', $finalIds)) {
                    $datas = array(
                        'status' => 'success',
                        'msg' => 'Galley image has been deleted'
                    );
                    //lets delete the image from database permanently
                    $this->imagehub->delete($galID);
                    echo json_encode($datas);
                    exit();
                } else {
                    $datas = array(
                        'status' => 'error',
                        'msg' => 'Unable to update gallery'
                    );
                    echo json_encode($datas);
                    exit();
                }
            } else {
                //just one value
                $finalIds = '';
                if (Modules::run('user/deletegalleyimage', $finalIds)) {
                    $datas = array(
                        'status' => 'success',
                        'msg' => 'Galley image has been deleted'
                    );
                    //lets delete the image from database permanently
                    $this->imagehub->delete($galID);
                    echo json_encode($datas);
                    exit();
                } else {
                    $datas = array(
                        'status' => 'error',
                        'msg' => 'Unable to update gallery'
                    );
                    echo json_encode($datas);
                    exit();
                }
            }
        } else {
            $datas = array(
                'status' => 'error',
                'msg' => 'No image found to be deleted'
            );
            echo json_encode($datas);
            exit();
        }
    }

    //get thumb by image absolute url
    public function get_thumb_by_url($url, $size) {
        if (isset($url)) {
            $parts = explode('/', $url);
            $filename = end($parts);
            $row = $this->imagehub->get_by(array('fname' => $filename));
            if ($row) {
                if (file_exists('./media/' . $row->fname)) {
                    $filename = $row->fname;
                    $title = $row->title;
                    $img = image(base_url() . 'media/' . $filename, $size);

                    return $img;
                } else {
                    return base_url('_public/front/assets/img/sample-300-200.jpg');
                }
            } else {
                return base_url('_public/front/assets/img/sample-300-200.jpg');
            }
        } else {
            return base_url('_public/front/assets/img/sample-300-200.jpg');
        }
    }

}
