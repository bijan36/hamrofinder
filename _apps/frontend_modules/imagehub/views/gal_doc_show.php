<?php
if (isset($imgID) && !empty($imgID)) {
    $filepath = Modules::run('imagehub/get_file_icons', $imgID);
    $imgRow = Modules::run('imagehub/get_row', $imgID);
    $imgURL = base_url('media/' . $imgRow->fname);
    ?>
    <div class="col-md-2 galind-<?php echo $imgID; ?>">
        <a href="<?php echo $imgURL ?>" target="_blank">
            <img src="<?php echo $filepath; ?>"  class="img-responsive">
        </a>
    </div>
    <?php
}
?>