<?php
if (isset($imgID) && !empty($imgID)) {
    $filepath = Modules::run('imagehub/get_file_icons', $imgID);
    $imgRow = Modules::run('imagehub/get_row', $imgID);
    $imgURL = base_url('media/' . $imgRow->fname);
    ?>
    <div class="col-md-2 galind-<?php echo $imgID; ?>">
        <div class="gal_img_cover">
            <img src="<?php echo $filepath; ?>"  class="img-responsive">
            <a href="#" class="gal_del" data-id="<?php echo $imgID; ?>"><i class="fa fa-times"></i></a>
            <a href="<?php echo $imgURL ?>" class="viewfile" target="_blank"><i class="fa fa-eye"></i></a>
        </div>
    </div>
    <?php
}
?>