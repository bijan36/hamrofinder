<?php
if (isset($imgID) && !empty($imgID)) {
    $filepath = Modules::run('imagehub/filename_sized', $imgID, 'thumb100x100');
    ?>
    <div class="col-md-2 galind-<?php echo $imgID; ?>"><div class="gal_img_cover"><img src="<?php echo $filepath; ?>"  class="img-responsive"><a href="#" class="gal_del" data-id="<?php echo $imgID; ?>"><i class="fa fa-times"></i></a></div></div>
    <?php
}
?>