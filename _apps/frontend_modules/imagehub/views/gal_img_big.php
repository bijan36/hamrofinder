<?php
if (isset($imgID) && !empty($imgID)) {
    $filepath = Modules::run('imagehub/filename_sized', $imgID, 'thumb350x350');
    $imageFile = Modules::run('imagehub/get_image_by_id', $imgID);
    ?>
    <div class="col-md-3"><div class="gal_img_cover"><a class="group1" title="<?php echo $college_name; ?>" href="<?php echo base_url() ?>media/<?php echo $imageFile; ?>" ><img src="<?php echo $filepath; ?>"  class="img-responsive"></a></div></div>
    <?php
}
?>