<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Institute extends Frontendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('Institute_m', 'finstitute');
    }

    //USER DASHBOARD
    public function index() {
        redirect('site');
        exit();
    }

    //GET SAFE DATA BY USER ID
    public function get_row($ID) {
        $userRow = $this->finstitute->get($ID);
        return $userRow ? $userRow : false;
    }
    
    //GET THE SECURITY VALUES BY ID
    public function get_securities_values($parent_ID) {
        if (!empty($parent_ID)) {
            $row = $this->finstitute->get_by(array('parent_ID' => $parent_ID));
            if ($row) {
                return strtolower($row->security_issues);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    //GET THE FACILITY VALUES BY ID
    public function get_facility_values($parent_ID) {
        if (!empty($parent_ID)) {
            $row = $this->finstitute->get_by(array('parent_ID' => $parent_ID));
            if ($row) {
                if (!empty($row->facilities_in_college)) {
                    return strtolower($row->facilities_in_college);
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    //GET SAFE DATA BY USER PARENT
    public function get_row_by_parent($parent_ID) {
        $userRow = $this->finstitute->get_by(array('parent_ID' => $parent_ID));
        return $userRow ? $userRow : false;
    }

    //UPDAET META
    public function update_institute_meta($ID, $data) {
        $row = $this->finstitute->get_by(array('parent_ID' => $ID));
        if ($row) {
            //FOND ROW UPDATE HERE
            if ($this->finstitute->update($row->ID, $data)) {
                $data = array('status' => 'success', 'msg' => 'Profile updated successfully');
                return $data;
            } else {
                $data = array('status' => 'error', 'msg' => 'Unable to update profile.');
                return $data;
            }
        } else {
            //NOT FOUND ROW INSERT HERE
            if ($this->finstitute->insert($data)) {
                $data = array('status' => 'success', 'msg' => 'Profile updated successfully');
                return $data;
            } else {
                $data = array('status' => 'error', 'msg' => 'Unable to update profile.');
                return $data;
            }
        }
    }
    
    //GET THE ADDITIONALS VALUES BY ID
    public function get_additionals_values($parent_ID) {
        if (!empty($parent_ID)) {
            $row = $this->finstitute->get_by(array('parent_ID' => $parent_ID));
            if ($row) {
                return strtolower($row->additional_features);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}
