<div class="row">
    <div class="col-md-3">
        <div class="login-cover wow bounceInLeft scrolDivChat">
            <h2>Conversation Archive:</h2>
            <?php
//echo '<pre>';
//print_r($rows);
//echo '</pre>';
            if ($rows != '' && count($rows) > 0) {
                //print_r($rows);
                $ids = array();
                foreach ($rows as $row):
                    $ids[] = $row->to;
                endforeach;
                if (!empty($ids)) {
                    $ids = array_unique($ids);
                    foreach ($ids as $toid):
                        if ($toid != $this->session->userdata('userID')) {
                            $userRow = Modules::run('user/get_row', $toid);
                            if ($userRow) {
                                ?>
                                <div class="singleConversationBox">
                                    <a href='#' data-id='<?php echo $toid; ?>' class="showchatconversation">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <?php
                                                $thumb = Modules::run('imagehub/filename_sized', $userRow->profile_picture, 'thumb40x40');
                                                if ($thumb) {
                                                    ?>
                                                    <img src="<?php echo $thumb; ?>" class="img-responsive">
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <div class="col-md-9"><?php echo $userRow->name; ?></div>
                                        </div>
                                    </a>
                                </div>
                                <?php
                            }
                        }
                    endforeach;
                } else {
                    echo 'No conversation found!';
                }
            } else {
                echo 'No conversation found!';
            }
            ?>
        </div>
    </div>

    <div class="col-md-9">
        <div class="wow pulse">
            <h1><i class="fa fa-sign-in fa-fw"></i> <span class="chattingHeading">Conversation</span></h1>
            <div class="row">
                <div id="chatDivResult" class="col-md-12 conversationDetails scrolDivChat">

                </div>
            </div>
        </div>
    </div>
</div>