<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Chat extends Frontendcontroller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Chat_m', 'fchat');
        if (!isset($_SESSION['chatHistory'])) {
            $_SESSION['chatHistory'] = array();
        }
        if (!isset($_SESSION['openChatBoxes'])) {
            $_SESSION['openChatBoxes'] = array();
        }
    }

    public function index() {
        //LOADING UP ALL THE CONVERSATON ARCHIVE
        $this->db->select("*");
        $this->db->from("chat");
        $this->db->where('to', $this->session->userdata('userID'));
        $this->db->or_where('from', $this->session->userdata('userID'));
        $q = $this->db->get();
        $rows = $q->result();

        $data['rows'] = $rows;
        $this->template->load(get_template('default'), 'chat_box', $data);
    }

    public function startchatsession() {
        $items = '';
        if (!empty($_SESSION['openChatBoxes'])) {
            foreach ($_SESSION['openChatBoxes'] as $chatbox => $void) {
                $items .= $this->chatBoxSession($chatbox);
            }
        }
        if ($items != '') {
            $items = substr(trim($items), 0, -1);
        }
        header('Content-type: application/json');
        ?>
        {
        "username": "<?php echo $_SESSION['username']; ?>",
        "from_username": "<?php echo $_SESSION['chatusername']; ?>",
        "items": [
        <?php echo $items; ?>
        ]
        }
        <?php
        exit();
    }

    public function chatheartbeat() {
        $this->db->select('*');
        $this->db->where('to', $_SESSION['username']);
        $this->db->where('recd', 0);
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get('chat');
        $items = '';
        $chatBoxes = array();
        foreach ($query->result_array() as $chat) {
            # code...
            if (!isset($_SESSION['openChatBoxes'][$chat['from']]) && isset($_SESSION['chatHistory'][$chat['from']])) {
                $items = $_SESSION['chatHistory'][$chat['from']];
            }

            $chat['message'] = $this->sanitize($chat['message']);

            $d = array();
            $d['s'] = '0';
            $d['f'] = "$chat[from]";
            $d['fname'] = "$chat[from_name]";
            $d['m'] = "$chat[message]";
            /* $items .= <<<EOD
              {
              "s": "0",
              "f": "{$chat['from']}",
              "m": "{$chat['message']}"
              },
              EOD; */
            $items .= json_encode($d) . ',';

            if (!isset($_SESSION['chatHistory'][$chat['from']])) {
                $_SESSION['chatHistory'][$chat['from']] = '';
            }
            $ch = array();
            $ch["s"] = "0";
            $ch["f"] = "$chat[from]";
            $ch["fname"] = "$chat[from_name]";
            $ch["m"] = "$chat[message]";
            $_SESSION['chatHistory'][$chat['from']] .= json_encode($ch) . ",\r\n";
            ;
            /* $_SESSION['chatHistory'][$chat['from']] .= <<<EOD
              {
              "s": "0",
              "f": "{$chat['from']}",
              "m": "{$chat['message']}"
              },
              EOD; */

            //unset($_SESSION['tsChatBoxes'][$chat['from']]);
            $unID = $chat['from'];
            $dataToBeUnset = $this->session->userdata();
            unset($dataToBeUnset['openChatBoxes'][$unID]);
            $this->session->set_userdata($dataToBeUnset);
            $_SESSION['openChatBoxes'][$chat['from']] = $chat['sent'];
        }

        if (!empty($_SESSION['openChatBoxes'])) {
            foreach ($_SESSION['openChatBoxes'] as $chatbox => $time) {
                if (!isset($_SESSION['tsChatBoxes'][$chatbox])) {
                    $now = time() - strtotime($time);
                    $time = date('g:iA M dS', strtotime($time));

                    $message = "Sent at $time";
                    if ($now > 180) {

                        $d = array();
                        $d['s'] = '2';
                        $d['f'] = "$chatbox";
                        $d['m'] = "$message";

                        $items .= json_encode($d) . ",\r\n";
                        // $items .= <<<EOD
                        // {
                        // "s": "2",
                        // "f": "$chatbox",
                        // "m": "{$message}"
                        // },
                        // EOD;

                        if (!isset($_SESSION['chatHistory'][$chatbox])) {
                            $_SESSION['chatHistory'][$chatbox] = '';
                        }
                        $d = array();
                        $d['s'] = '2';
                        $d['f'] = "$chatbox";
                        $d['m'] = "$message";
                        $_SESSION['chatHistory'][$chatbox] .= json_encode($d) . ",\r\n";
                        /* $_SESSION['chatHistory'][$chatbox] .= <<<EOD
                          {
                          "s": "2",
                          "f": "$chatbox",
                          "m": "{$message}"
                          },
                          EOD; */
                        $_SESSION['tsChatBoxes'][$chatbox] = 1;
                    }
                }
            }
        }

        $sql = "update chat set recd = 1 where chat.to = '" . $_SESSION['username'] . "' and recd = 0";
        $query = $this->db->query($sql);

        if ($items != '') {
            $items = substr($items, 0, -1);
        }
        header('Content-type: application/json');
        ?>
        {
        "items": [
        <?php echo $items; ?>
        ]
        }

        <?php
        exit(0);
    }

    public function sendchat() {
        $from = $_SESSION['username'];
        $from_name = $_SESSION['chatusername'];
        $to = $_POST['to'];
        $to_name = $_POST['to_name'];
        $message = $_POST['message'];
        $_SESSION['openChatBoxes'][$_POST['to']] = date('Y-m-d H:i:s', time());

        $messagesan = $this->sanitize($message);

        if (!isset($_SESSION['chatHistory'][$_POST['to']])) {
            $_SESSION['chatHistory'][$_POST['to']] = '';
        }
        $d['s'] = '1';
        $d['fname'] = "$to_name";
        $d['f'] = "$to";
        $d['m'] = "$messagesan";

        $_SESSION['chatHistory'][$_POST['to']] .= json_encode($d) . ",\r\n";
        /* $_SESSION['chatHistory'][$_POST['to']] .= <<<EOD
          {
          "s": "1",
          "f": "{$to}",
          "m": "{$messagesan}"
          },
          EOD; */
        unset($_SESSION['tsChatBoxes'][$_POST['to']]);

        $data = array(
            'from' => $from,
            'from_name' => $from_name,
            'to_name' => $to_name,
            'to' => $to,
            'message' => $message,
        );
        $this->db->set('sent', 'NOW()', FALSE);
        $this->db->insert('chat', $data);
        echo "1";
        exit(0);
    }

    public function closechat() {
        $IDtoBeUnset = $this->input->post('chatbox');
        //unset($_SESSION['openChatBoxes'][$_POST['chatbox']]);
        $dataToBeUnset = $this->session->userdata();
        unset($dataToBeUnset['openChatBoxes'][$IDtoBeUnset]);
        $this->session->set_userdata($dataToBeUnset);
        echo "1";
        exit(0);
    }

    public function chathistory() {
        $this->db->select('*');
        $this->db->where("('from' = $_SESSION[username]  AND  'to' = " . $this->input->get('to') . " )");
        $this->db->or_where("('to' = $_SESSION[username]  AND  'from' = " . $this->input->get('to') . " )");
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get('chat');
        $items = '';
        $chatBoxes = array();
        foreach ($query->result_array() as $chat) {
            # code...
            $chat['message'] = $this->sanitize($chat['message']);

            $d = array();
            $d['s'] = '0';
            $d['f'] = "$chat[from]";
            $d['fname'] = "$chat[from_name]";
            $d['m'] = "$chat[message]";
            $items .= json_encode($d) . ',';
        }
        header('Content-type: application/json');
        ?>
        {
        "items": [
        <?php echo $items; ?>
        ]
        }

        <?php
        exit(0);
    }

    public function sanitize($text) {
        $text = htmlspecialchars($text, ENT_QUOTES);
        $text = str_replace("\n\r", "\n", $text);
        $text = str_replace("\r\n", "\n", $text);
        $text = str_replace("\n", "<br>", $text);
        return $text;
    }

    public function chatBoxSession($chatbox) {
        $items = '';
        if (isset($_SESSION['chatHistory'][$chatbox])) {
            $items = $_SESSION['chatHistory'][$chatbox];
        } else {
            // fetch from database last few records
        }
        return $items;
    }

    //LOAD THE CONVERSATION 
    public function showconversation_details() {

        $ID = $this->input->post('id');

        if ($ID) {

//SELF ROW
            $userID = $this->session->userdata('userID');
            $userIDRow = Modules::run('user/get_row', $userID);
            $userIDRowthumb = Modules::run('imagehub/filename_sized', $userIDRow->profile_picture, 'thumb40x40');
            $userIDRowURL = profile_url($userID);

//FRIEND ROW
            $IDRow = Modules::run('user/get_row', $ID);
            $IDRowthumb = Modules::run('imagehub/filename_sized', $IDRow->profile_picture, 'thumb40x40');
            $IDRowURL = profile_url($ID);


            //$rows = $this->fchat->get_many_by(array('from' => $userID, 'to' => $ID));
            $this->db->select("*");
            $this->db->from("chat");
            $this->db->where("(to = $ID AND from = $userID)");
            $this->db->or_where("(from = $ID AND to = $userID)");
            $q = $this->db->get();
            $rows = $q->result();

            $outHead = 'Conversation with ' . $IDRow->name;


            $outPut = '';
            if ($rows) {
                foreach ($rows as $row):
                    if ($row->from == $userID) {


                        $outPut .= '<div class="AreaR">';
                        $outPut .= '<div class="R">';
                        $outPut .= '<a href="' . $userIDRowURL . '">';
                        $outPut .= '<img src="' . $userIDRowthumb . '"  class="img-responsive center-block"/>';
                        $outPut .= '</a>';
                        $outPut .= '</div>';
                        $outPut .= '<div class="text L textL">' . $row->message;
                        $outPut .= '<span class="chatTimeWhite">';
                        $outPut .= makeFancyDate($row->sent);
                        $outPut .= '</span>';
                        $outPut .= '</div>';
                        $outPut .= '</div>';

                        //$outPut .= '<div class="coverConversation"><div class="myTalk pull-left">' . $row->message . '</div></div>';
                    } else {
                        $outPut .= '<div class="AreaL">';
                        $outPut .= '<div class="L">';
                        $outPut .= '<a href="' . $IDRowURL . '">';
                        $outPut .= '<img src="' . $IDRowthumb . '" class="img-responsive center-block"/>';
                        $outPut .= '</a>';
                        $outPut .= '</div>';
                        $outPut .= '<div class="text R textR">' . $row->message;
                        $outPut .= '<span class="chatTime">';
                        $outPut .= makeFancyDate($row->sent);
                        $outPut .= '</span>';
                        $outPut .= '</div>';
                        $outPut .= '</div>';
                        //$outPut .= '<div class="coverConversation"><div style="otherTalk pull-right">' . $row->message . '</div></div>';
                    }
                endforeach;

                //$outPut = $outPut.'<div id="chatBottom"></div>';
            } else {
                //IF NO CONVERSATION FOUND
                $outPut = "No conversation found!";
                $outHead = "--";
            }
            $dataRes = array(
                'status' => 'success',
                'message' => $outPut,
                'messageHead' => $outHead,
            );

            echo json_encode($dataRes);
            exit();
        }
    }

}
