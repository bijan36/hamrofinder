<a href="<?php echo site_url('plans') ?>" class="btn blueButton" style="margin-bottom: 20px;">Upgrade/Membership/Purchase ads</a>

<div class="line-well-heder">
    <div class="row">
        <div class="col-md-6">
            <span class="line-well-title"><?php echo $this->session->userdata('userName'); ?></span>
        </div>
        <div class="col-md-6">
            <a class="btn yelloButton pull-right" href="<?php echo profile_url($this->session->userdata('userID')); ?>">View Profile</a>
            <a class="btn yelloButton pull-right" href="<?php echo site_url('myplans'); ?>" style="margin-right:7px;">My Plans</a>
        </div>
    </div>
</div>

<div class="line-well-body">
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#myAd" aria-expanded="true" aria-controls="collapseOne">
                        My Ad
                        <span class="glyphicon pull-right glyphicon-chevron-right"></span>
                    </a>
                </h4>
            </div>
            <div id="myAd" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                    <?php
                    $myAds = Modules::run('collegead/get_my_ads');
                    if ($myAds) {
                        foreach ($myAds as $myAd):
                            $data['row'] = $myAd;
                            $this->load->view('collegead/loop_college_ad_self', $data);
                        endforeach;
                    } else {
                        echo 'No ad yet! :) ';
                    }
                    ?>

                    <div class="bold-uppercase-highlight">
                        <?php echo Modules::run('ipn/college_ad_remaining_link');?>
                    </div>

                </div>
            </div>

        </div>


        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#savedAd" aria-expanded="false" aria-controls="collapseTwo">
                        Saved Ad
                        <span class="glyphicon pull-right glyphicon-chevron-right"></span>
                    </a>
                </h4>
            </div>
            <div id="savedAd" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-condensed mytable">
                                <?php
                                //SAVED AD OF USERS
                                $savedAd = Modules::run('savedad/get_rows_by_user', $userID);
                                if ($savedAd) {
                                    ?>
                                    <tr>
                                        <th>Student Name</th>
                                        <th>Looking For</th>
                                        <th>Price Range</th>
                                        <th></th>
                                    </tr>
                                    <?php
                                    foreach ($savedAd as $sa):
                                        $adRow = Modules::run('studentad/get_row', $sa->ad_ID);
                                        if ($adRow) {
                                            //IF THE AD VALID AND ACTIVE
                                            if ($adRow->status == ACTIVE && today_date() < $adRow->ad_end_date) {
                                                //GET THE COLLEGE ROW
                                                $studentRow = Modules::run('user/get_row', $adRow->parent_ID);
                                                if ($studentRow) {
                                                    if ($studentRow->status == ACTIVE) {
                                                        ?>
                                                        <tr class="rowid-<?php echo $sa->ID; ?>">
                                                            <td>
                                                                <a href="<?php echo get_ads_permalink($adRow->ID, $studentRow->ID); ?>" title="<?php echo $studentRow->name; ?>">
                                                                    <?php echo $studentRow->name; ?>
                                                                </a>

                                                            </td>
                                                            <td><?php echo $adRow->looking_for ?></td>
                                                            <td><?php echo $adRow->tution_fee_range ?></td>
                                                            <td><a href="#" class="remove-saved-ad" data-id='<?php echo $sa->ID; ?>'><i class="fa fa-times-circle fa-fw"></i></a></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                            }
                                        }
                                    endforeach;
                                    ?>
                                    <?php
                                } else {
                                    ?>
                                    <tr>
                                        <td>You don't have any saved Ads yet!</td>    
                                    </tr>
                                    <?php
                                }
                                ?>

                            </table>
                        </div>

                    </div>

                </div>
            </div>
        </div>


        <!--ADDING COLLEGE ADS-->
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingThree">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#placeAd" aria-expanded="false" aria-controls="collapseThree">
                        Place Your Ad
                        <span class="glyphicon pull-right glyphicon-chevron-right"></span>
                    </a>
                </h4>
            </div>
            <div id="placeAd" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">

                <?php echo form_open('collegead/add', array('id' => 'collegeAdAdding')); ?>
                <div class="panel-body">
                    <div class="line-well grey-bg round-border">
                        <div class="row">
                            <div class="collegeadmsg"></div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="looking_for">Looking For</label>
                                    <div class="drop_list">
                                        <select class="form-control" name="looking_for">
                                            <?php echo get_select_box('college-ad-course-looking-for'); ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="affiliation">Affiliation</label>
                                    <div class="drop_list">
                                        <select class="form-control" name="affiliation">
                                            <?php echo get_select_box('college-ad-affiliation-options'); ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="grade_accepted">Eligible Percentage</label>
                                    <div class="drop_list">
                                        <select class="form-control" name="grade_accepted">
                                            <?php echo get_select_box('college-ad-grade-accepted') ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="entrance_required">Entrance Required</label>
                                    <div class="drop_list">
                                        <select class="form-control" name="entrance_required">
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="tuition_fee_range">Tuition Fee (NRs. In Lakh)</label>
                                    <div class="drop_list">
                                        <select class="form-control" name="tuition_fee_range">
                                            <?php echo get_select_box('college-ad-tuition-fee-range') ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="college_bus">College Bus</label>
                                    <div class="drop_list">
                                        <select class="form-control" name="college_bus">
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="shift">Shift</label>
                                    <div class="drop_list">
                                        <select class="form-control" name="shift">
                                            <?php echo get_select_box('college-ad-shift') ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="admission_within">Can admit in (Days)</label>
                                    <div class="drop_list">
                                        <select class="form-control" name="admission_within">
                                            <?php echo get_select_box('college-ad-admission-within-days') ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="starting_date">Starting Date</label>
                                    <div class="drop_list">
                                        <input type="text" name="starting_date" class="form-control datepicker">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="student_add_message">Message if any</label>
                                    <textarea class="form-control" name="student_add_message"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn yelloButton">Submit</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>



        <!--CONFIRM PASSWORD MODAL-->
        <div class="modal fade modal-wide" id="check-confirm-password-college" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-key fa-fw"></i> Confirm Password</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Confirm Your Password:</label>
                            <input type="password" class="form-control" name="con-pass" id="con-pass">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn yelloButton" id="con-pass-btn-college">Submit</button>
                    </div>
                </div>
            </div>
        </div>
        <!--CONFIRM PASSWORD MODAL-->



        <!--ADDING STUDENT ADS-->
        <div class="panel panel-default" style="margin-top: 5px;">

            <div class="panel-heading" role="tab" id="headingThree">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#profileInfo" aria-expanded="false" aria-controls="collapseThree">
                        Add/Edit Profile Info
                        <span class="glyphicon pull-right glyphicon-chevron-right"></span>
                    </a>
                </h4>
            </div>

            <div id="profileInfo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="panel-body">

                    <div class="profileerror"></div>

                    <div class="line-well grey-bg round-border">
                        <h4>PROFILE PICTURE</h4>
                        <?php echo form_open_multipart('imagehub/studentpicupdate', array('id' => 'userProfilePic')); ?>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="propic_error"></div>
                            </div>

                            <div class="col-md-2">
                                <div class="propic">
                                    <?php
                                    $proImgID = $basicInfo->profile_picture ? $basicInfo->profile_picture : '0';
                                    $profile_image = Modules::run('imagehub/get_profile_image', $proImgID);
                                    echo $profile_image;
                                    ?>
                                </div>
                            </div>

                            <div class="col-md-10">
                                <div class="form-group">
                                    <span class="special-note">
                                        Allowed file types: jpg, jpeg, png<br/>
                                        Best file dimension: 400px X 512px
                                    </span>                                   
                                    <input type="file" name="student_profile_pic" id="student_profile_pic"><br/>
                                    <input type="submit" name="profilepicture" class="btn yelloButton" value="Change Profile Picture">
                                    <input type="hidden" name="addtolib" value="yes">
                                </div>
                            </div>                            

                        </div>
                        <?php echo form_close(); ?>
                    </div>




                    <?php echo form_open('user/updateprofile_college', array('id' => 'userProfileEdit')); ?>
                    <div class="line-well grey-bg round-border">
                        <h4>BASIC INFO</h4>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">Name:</label>
                                    <?php $name = $basicInfo->name ? $basicInfo->name : ''; ?>
                                    <input type="text" name="name" class="form-control" value="<?php echo $name; ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="year_established">Establish Year:</label>
                                    <?php $year_established = isset($metaInfo->year_established) ? $metaInfo->year_established : ''; ?>
                                    <input type="text" name="year_established" class="form-control" value="<?php echo $year_established; ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="mobile">Phone No.:</label>
                                    <?php $contact = $basicInfo->contact ? $basicInfo->contact : ''; ?>
                                    <input type="text" name="mobile" class="form-control" value="<?php echo $contact; ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="email">Email:</label>
                                    <?php $email = $basicInfo->email ? $basicInfo->email : ''; ?>
                                    <input type="text" name="email" class="form-control" value="<?php echo $email; ?>" disabled>
                                </div>
                            </div>
                            
                            
                            
                            <!--NEW ADDRESS-->
                            <div class="col-md-12"></div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?php $country = isset($metaInfo->country) ? $metaInfo->country : ''; ?>
                                    <?php echo Modules::run('address/get_select_box', 0, 'Country', 'name', 'country', $country); ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group" id="provienceDiv">
                                    <?php
                                    $province = isset($basicInfo->province) ? $basicInfo->province : '';
                                    ?>
                                    <?php echo Modules::run('address/get_select_box_find_parent', $country, 'Province', 'name', 'province', $province); ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group" id="districtDiv">
                                    <?php
                                    $district = isset($basicInfo->district) ? $basicInfo->district : '';
                                    ?>
                                    <?php echo Modules::run('address/get_select_box_find_parent', $province, 'District', 'name', 'district', $district); ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group" id="localLevelDiv">
                                    <?php
                                    $local_level = isset($basicInfo->local_level) ? $basicInfo->local_level : '';
                                    ?>
                                    <?php echo Modules::run('address/get_select_box_find_parent', $district, 'Local Level', 'name', 'local_level', $local_level); ?>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group" id="wardnoDiv">
                                    <?php
                                    $ward_no = isset($basicInfo->ward_no) ? $basicInfo->ward_no : '';
                                    ?>
                                    <?php echo Modules::run('address/get_select_box_find_parent', $local_level, 'Ward No', 'name', 'ward_no', $ward_no); ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="address">Address:</label>
                                    <?php $address = $basicInfo->address ? $basicInfo->address : ''; ?>
                                    <input type="text" name="address" class="form-control" value="<?php echo $address; ?>"> 
                                </div>
                            </div>
                            <!--NEW ADDRESS-->
                            
                            
                            
                            
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="average_student_per_class">Average Student Per Class:</label>
                                    <?php $average_student_per_class = isset($metaInfo->average_student_per_class) ? $metaInfo->average_student_per_class : ''; ?>
                                    <input type="text" name="average_student_per_class" class="form-control" value="<?php echo $average_student_per_class; ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="website">Website:</label>
                                    <?php $website = isset($metaInfo->website) ? $metaInfo->website : ''; ?>
                                    <input type="text" name="website" class="form-control" value="<?php echo $website; ?>">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="area_occupied">Area Occupied:</label>
                                    <?php $area_occupied = isset($metaInfo->area_occupied) ? $metaInfo->area_occupied : ''; ?>
                                    <input type="text" name="area_occupied" class="form-control" value="<?php echo $area_occupied; ?>"> 
                                </div>
                            </div>
                          

                        </div>
                    </div>

                    <div class="line-well grey-bg round-border">
                        <h4>PROGRAM OFFERED</h4>

                        <div id="slc" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">

                                <div class="row">
                                    <div class="col-md-12 removeerror"></div>
                                    <div class="col-md-12 listarea">
                                        <?php
                                        $currentProgramms = Modules::run('college_programe/get_by_parent_level', $userID);
                                        //print_r($currentProgramms);
                                        if ($currentProgramms) {
                                            $ids = array();
                                            foreach ($currentProgramms as $cp):
                                                $ids[] = $cp->ID;
                                            endforeach;
                                            $idArray['idArray'] = $ids;
                                            $this->load->view('college_programe/college_program_loop', $idArray);
                                        }
                                        ?>

                                    </div>
                                </div>

                                <h3>Add new program offered below:</h3>
                                <div class="blue-bg line-well round-border">
                                    <div class="programeerror"></div>
                                    <div class="row">

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="offered_programe">Offered Program:</label>
                                                <div class="drop_list">
                                                    <select class="form-control drop_list" name="offered_programe" id="offered_programe">
                                                        <option value="Government Recognized Vocational">Government Recognized Vocational</option>
                                                        <option value="+2/PCL/Higher Secondary">+2/PCL/Higher Secondary</option>
                                                        <option value="Diploma">Diploma</option>
                                                        <option value="Bachelor">Bachelor</option>
                                                        <option value="Master">Master</option>
                                                        <option value="Others">Others</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="program_title">Program Title:</label>
                                                <select class="form-control" name="program_title" id="program_title">
                                                    <?php echo get_select_box('college-programe-offered-select-options'); ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="program_title_alt">Program if others, Please mention:</label>
                                                <input type="text" name="program_title_alt" class="form-control" id="program_title_alt">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="affiliation">Affiliation:</label>
                                                <select class="form-control" name="affiliation" id="affiliation">
                                                    <?php echo get_select_box('college-programe-affiliation-select-options'); ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="affiliation_alt">Affiliation if others, Please mention:</label>
                                                <input type="text" name="affiliation_alt" id="affiliation_alt" class="form-control">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="program_duration">Program Duration:</label>
                                                <input type="text" name="program_duration" id="program_duration" placeholder="eg: 3 Months" class="form-control">
                                                </select>
                                            </div>
                                        </div>


                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="class_held_at">Class Held at:</label>
                                                <input type="text" name="class_held_at" id="class_held_at" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="admission_fee">Admission Fee(Rs.):</label>
                                                <input type="text" name="admission_fee" id="admission_fee" placeholder="eg: 150000" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="total_course_fee">Total Course Fee(Rs.):</label>
                                                <input type="text" name="total_course_fee" id="total_course_fee" placeholder="eg: 150000" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="other_fee">Others Fee(Rs.):</label>
                                                <input type="text" name="other_fee" id="other_fee" placeholder="eg: 150000"  class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="fee_collect">How do you collect fee for this program?</label>
                                                <div class="drop_list">
                                                    <select class="form-control" name="fee_collect"  id="fee_collect">
                                                        <?php echo get_select_box('college-programe-fee-collection-select-options'); ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <button class="btn yelloButton btn-block addProgramOffered" data-parentid="<?php echo $userID ?>" data-level="Bachelor" data-prefix="bachelor">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>


                    <div class="line-well grey-bg round-border">
                        <h4>FACILITIES IN SCHOOL/COLLEGE</h4>
                        <div class="row">
                            <div class="col-md-12" style="padding-bottom: 15px;">
                                <?php
                                $faciRow = Modules::run('college/get_row_by_parent', $userID);
                                $arraychk = array();
                                if ($faciRow) {
                                    $arraychk = explode(',', $faciRow->facilities_in_college);
                                }
                                ?>
                                <?php echo get_check_box('college-facilities-checked-options', 'facilities_in_college', $arraychk); ?>
                            </div>
                        </div>
                    </div>


                    <div class="line-well grey-bg round-border">
                        <h4>SECURITY ISSUES</h4>
                        <div class="row">
                            <div class="col-md-12" style="padding-bottom: 15px;">
                                <?php
                                $secRow = Modules::run('college/get_row_by_parent', $userID);
                                $arraychk = array();
                                if ($secRow) {
                                    $arraychk = explode(',', $secRow->secuirty_issues);
                                }
                                ?>
                                <?php echo get_check_box('college-security-checked-options', 'secuirty_issues', $arraychk); ?>
                            </div>
                        </div>
                    </div>

                    <div class="line-well grey-bg round-border">
                        <h4>ADDITIONAL FEATURES</h4>
                        <div class="row">
                            <div class="col-md-12" style="padding-bottom: 15px;">
                                <?php
                                $secRow = Modules::run('college/get_row_by_parent', $userID);
                                $arraychk = array();
                                if ($secRow) {
                                    $arraychk = explode(',', $secRow->additional_features);
                                }
                                ?>
                                <?php echo get_check_box('college-additional-features-checked-options', 'additional_features', $arraychk); ?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <button type="submit" name="updateUsers" class="btn yelloButton">Update Your Profile Info</button>
                    </div>

                    <?php echo form_close(); ?>

                    <hr/>

                    <div class="line-well grey-bg round-border">
                        <h4>GALLERY IMAGES</h4>

                        <div class="row">
                            <div class="col-md-12 gal-img">
                                <div class="row no-gutter gallery_img_div">
                                    <?php
                                    if (!empty($basicInfo->gallery_picture)) {
                                        $galImages = explode(',', $basicInfo->gallery_picture);
                                        foreach ($galImages as $gm):
                                            $galData['imgID'] = $gm;
                                            $this->load->view('imagehub/gal_img', $galData);
                                        endforeach;
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="gal_pic_error"></div>
                            </div>
                            <?php echo form_open_multipart('imagehub/collegegallery', array('id' => 'galleryPic')); ?>
                            <div class="col-md-12">
                                <span class="special-note">
                                    Allowed file types: jpg, jpeg, png, gif<br/>
                                    Best file dimension: 800px X 500px
                                </span>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">                                    
                                    <input type="file" name="gal_pic" id="college_gallery">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">
                                <input type="submit" name="galpic" class="btn yelloButton btn-block galBtn" value="Add New Picture">
                                <input type="hidden" name="addtolibgal" value="yes">
                            </div>
                            <?php echo form_close(); ?>
                        </div>

                    </div>


                    <div class="line-well grey-bg round-border">
                        <h4>VIDEO GALLERY</h4>

                        <div class="row">
                            <div class="col-md-12 gal-img">
                                <div class="row no-gutter gallery_vid_div">
                                    <?php
                                    $allVideos = Modules::run('videos/get_videos', $userID);
                                    if ($allVideos) {
                                        foreach ($allVideos as $av):
                                            $this->load->view('videos/video_thumb', array('video_code' => $av->video_code, 'vid_id' => $av->ID));
                                        endforeach;
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="gal_vid_error"></div>
                            </div>
                            <?php echo form_open('videos/add', array('id' => 'galleryVideo')); ?>
                            <div class="col-md-12">
                                <span class="special-note" style="margin-top: 15px;">
                                    ex: https://www.youtube.com/watch?v=<span style="color:red">ICMn3bbccPo</span>
                                </span>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="vid_code">Youtube video code:</label>
                                    <input type="text" class="form-control" placeholder="ICMn3bbccPo" name="vid_code" id="vid_code">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">
                                <input type="submit" name="galvideo" class="btn yelloButton btn-block vidbtn" value="Add Video">
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>


                    <div class="line-well grey-bg round-border">
                        <h4>Upload Prospectus/Brochure/Application Form/Leaflet/Flyer and Other Information</h4>

                        <div class="row">
                            <div class="col-md-12 gal-img">
                                <div class="row no-gutter doc_img_div">
                                    <?php
                                    $docus = Modules::run('documents/get_docs');
                                    if ($docus) {
                                        foreach ($docus as $dc):
                                            ?>
                                            <div class="col-md-2  docholder-<?php echo $dc->ID; ?>">
                                                <div class="doc_img_cover">
                                                    <a href="<?php echo base_url() ?>documents/<?php echo $dc->filename ?>" target="_blank" title="<?php echo $dc->title; ?>">
                                                        <img src="<?php echo base_url(); ?>documents/sample.jpg"  class="img-responsive">
                                                        <div class="docname"><?php echo $dc->title; ?></div>
                                                    </a>
                                                    <a href="#" class="doc_del" data-id="<?php echo $dc->ID; ?>">
                                                        <i class="fa fa-times"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <?php
                                        endforeach;
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="docError"></div>
                            </div>
                            <?php echo form_open_multipart('documents/documents_upload', array('id' => 'docUploadFile')); ?>
                            <div class="col-md-12">
                                <span class="special-note">
                                    Max file upload: 6 files<br/>
                                    Allowed file type: pdf<br/>
                                    Max size each: 5MB
                                </span>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">                                    
                                    <input type="file" name="docfiles" id="docfiles">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-6">
                                <input type="submit" name="docFileUPload" class="btn yelloButton btn-block galBtn docBtn" value="Add New PDF">
                                <input type="hidden" name="addtodoc" value="yes">
                            </div>
                            <?php echo form_close(); ?>
                        </div>

                    </div>


                </div>
            </div>

        </div>

    </div>
</div>

