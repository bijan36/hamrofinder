<div class="line-well-heder">
    <div class="row">
        <div class="col-md-8">
            <span class="line-well-title"><?php echo $this->session->userdata('userName'); ?> </span>
        </div>
        <div class="col-md-4">
            <a class="btn yelloButton pull-right" href="<?php echo profile_url($this->session->userdata('userID')); ?>">View Profile</a>
        </div>
    </div>
</div>

<div class="line-well-body">
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#myAd" aria-expanded="true" aria-controls="collapseOne">
                        My Ad
                        <span class="glyphicon pull-right glyphicon-chevron-right"></span>
                    </a>
                </h4>
            </div>
            <div id="myAd" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">

                    <?php
                    $myAds = Modules::run('studentad/get_my_ads');
                    if ($myAds) {
                        foreach ($myAds as $myAd):
                            $data['row'] = $myAd;
                            $this->load->view('studentad/loop_student_ad_self', $data);
                        endforeach;
                    } else {
                        echo 'No ad yet! :) ';
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#savedAd" aria-expanded="false" aria-controls="collapseTwo">
                        Saved Ad
                        <span class="glyphicon pull-right glyphicon-chevron-right"></span>
                    </a>
                </h4>
            </div>
            <div id="savedAd" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-condensed mytable">
                                <?php
                                //SAVED AD OF USERS
                                $savedAd = Modules::run('savedad/get_rows_by_user', $userID);
                                if ($savedAd) {
                                    ?>
                                    <tr>
                                        <th>College/Institute Name</th>
                                        <th>Looking For</th>
                                        <th>Fee Range</th>
                                        <th></th>
                                    </tr>
                                    <?php
                                    foreach ($savedAd as $sa):
                                        if ($sa->type == 'College') {
                                            $adRow = Modules::run('collegead/get_row', $sa->ad_ID);
                                        } elseif ($sa->type == 'Institute') {
                                            $adRow = Modules::run('institutead/get_row', $sa->ad_ID);
                                        }


                                        if ($adRow) {
                                            //IF THE AD VALID AND ACTIVE
                                            if ($adRow->status == ACTIVE && today_date() < $adRow->ad_end_date) {
                                                //GET THE COLLEGE ROW
                                                $collegeRow = Modules::run('user/get_row', $adRow->parent_ID);
                                                if ($collegeRow) {
                                                    if ($collegeRow->status == ACTIVE) {
                                                        ?>
                                                        <tr class="rowid-<?php echo $sa->ID; ?>">
                                                            <td>
                                                                <a href="<?php echo get_ads_permalink($adRow->ID, $collegeRow->ID); ?>" title="<?php echo $collegeRow->name; ?>">
                                                                    <?php echo $collegeRow->name; ?> <small>(<?php echo $sa->type; ?>)</small>
                                                                </a>
                                                            </td>
                                                            <td><?php echo $adRow->looking_for ?></td>
                                                            <td><?php echo $adRow->tuition_fee_range ?></td>
                                                            <td><a href="#" class="remove-saved-ad" data-id='<?php echo $sa->ID; ?>'><i class="fa fa-times-circle fa-fw"></i></a></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                            }
                                        }
                                    endforeach;
                                    ?>
                                    <?php
                                } else {
                                    ?>
                                    <tr>
                                        <td>You don't have any saved Ads yet!</td>    
                                    </tr>
                                    <?php
                                }
                                ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <!--ADDING STUDENT ADS-->


        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingThree">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#placeAd" aria-expanded="false" aria-controls="collapseThree">
                        Place Your Ad
                        <span class="glyphicon pull-right glyphicon-chevron-right"></span>
                    </a>
                </h4>
            </div>
            <div id="placeAd" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">

                <?php echo form_open('studentad/add', array('id' => 'studentAdAdding')); ?>
                <div class="panel-body">
                    <div class="line-well grey-bg round-border">
                        <div class="row">
                            <div class="studentadmsg"></div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="looking_for_type">Looking For</label>
                                    <div class="drop_list">
                                        <select class="form-control" id="looking_for_type" name="looking_for_type">
                                            <option value="<?php echo COLLEGE; ?>" selected>College</option>
                                            <option value="<?php echo INSTITUTE; ?>">Institute</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div id="program-class-selector">
                                    <div class="form-group">
                                        <label for="looking_for">Looking For Program</label>
                                        <div class="drop_list">
                                            <select class="form-control" name="looking_for">
                                                <?php echo get_select_box('student-ad-course-looking-for'); ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div id="affiliation-certified">
                                    <div class="form-group">
                                        <label for="affiliation">Affiliation</label>
                                        <div class="drop_list">
                                            <select class="form-control" name="affiliation">
                                                <?php echo get_select_box('student-ad-affiliation'); ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="admission_within">Can admit in(Days)</label>
                                    <div class="drop_list">
                                        <select class="form-control" name="admission_within">
                                            <?php echo get_select_box('student-ad-admission-within-days') ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="achieved_grade">Recent grade/cgpa (converted to %)</label>
                                    <div class="drop_list">
                                        <select class="form-control" name="achieved_grade">
                                            <?php echo get_select_box('student-ad-achieved-grade') ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="fee_range_looking_for">Course fee range (NRs. in Lakh)</label>
                                    <div class="drop_list">
                                        <select class="form-control" name="fee_range_looking_for">
                                            <?php echo get_select_box('student-ad-tuition-fee-range'); ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="student_add_message">Message to your future college</label>
                                    <textarea class="form-control" name="student_add_message"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button type="submit" class="btn yelloButton">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <?php echo form_close(); ?>
            </div>
        </div>



        <!--CONFIRM PASSWORD MODAL-->
        <div class="modal fade modal-wide" id="check-confirm-password" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-key fa-fw"></i> Confirm Password</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Confirm Your Password:</label>
                            <input type="password" class="form-control" name="con-pass" id="con-pass">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="con-pass-btn">Submit</button>
                    </div>
                </div>
            </div>
        </div>
        <!--CONFIRM PASSWORD MODAL-->


        <!--ADDING STUDENT ADS-->





        <div class="panel panel-default" style="margin-top:5px;">

            <div class="panel-heading" role="tab" id="headingFour">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#profileInfo" aria-expanded="false" aria-controls="collapseThree">
                        Add/Edit Profile Info
                        <span class="glyphicon pull-right glyphicon-chevron-right"></span>
                    </a>
                </h4>
            </div>


            <div id="profileInfo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="panel-body">

                    <div class="profileerror"></div>

                    <div class="line-well grey-bg round-border">
                        <h4>PROFILE PICTURE</h4>
                        <?php echo form_open_multipart('imagehub/studentpicupdate', array('id' => 'userProfilePic')); ?>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="propic_error"></div>
                            </div>
                            <div class="col-md-2">
                                <div class="propic">
                                    <?php
                                    $proImgID = $basicInfo->profile_picture ? $basicInfo->profile_picture : '0';
                                    $profile_image = Modules::run('imagehub/get_profile_image', $proImgID);
                                    echo $profile_image;
                                    ?>
                                </div>
                            </div>


                            <div class="col-md-10">
                                <div class="form-group"> 
                                    <span class="special-note">
                                        Allowed file types: jpg, jpeg, png<br/>
                                        Best file dimension: 400px X 512px
                                    </span>                                       
                                    <input type="file" name="student_profile_pic" id="student_profile_pic"><br/>
                                    <input type="submit" name="profilepicture" class="btn yelloButton" value="Update Profle Picture">
                                    <input type="hidden" name="addtolib" value="yes">
                                </div>
                            </div>
                        </div>
                        <?php echo form_close() ?>
                    </div>

                    <hr/>


                    <?php echo form_open('user/updateprofile', array('id' => 'userProfileEdit')); ?>
                    <?php //echo form_open('', array('id' => 'userProfileEdit')); ?>

                    <div class="line-well grey-bg round-border">
                        <h4>BASIC INFO</h4>


                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">Name:</label>
                                    <?php $name = $basicInfo->name ? $basicInfo->name : ''; ?>
                                    <input type="text" name="name" class="form-control" value="<?php echo $name; ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="phone_no">Phone No.:</label>
                                    <?php $phone_no = isset($metaInfo->phone_no) ? $metaInfo->phone_no : ''; ?>
                                    <input type="text" name="phone_no" class="form-control" value="<?php echo $phone_no; ?>">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="mobile">Mobile No.:</label>
                                    <?php $contact = $basicInfo->contact ? $basicInfo->contact : ''; ?>
                                    <input type="text" name="mobile" class="form-control" value="<?php echo $contact; ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="email">Email:</label>
                                    <?php $email = $basicInfo->email ? $basicInfo->email : ''; ?>
                                    <input type="text" name="email" class="form-control" value="<?php echo $email; ?>" disabled>
                                </div>
                            </div>


                            <!--NEW ADDRESS-->
                            <div class="col-md-12"></div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?php $country = isset($metaInfo->country) ? $metaInfo->country : ''; ?>
                                    <?php echo Modules::run('address/get_select_box', 0, 'Country', 'name', 'country', $country); ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group" id="provienceDiv">
                                    <?php
                                    $province = isset($basicInfo->province) ? $basicInfo->province : '';
                                    ?>
                                    <?php echo Modules::run('address/get_select_box_find_parent', $country, 'Province', 'name', 'province', $province); ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group" id="districtDiv">
                                    <?php
                                    $district = isset($basicInfo->district) ? $basicInfo->district : '';
                                    ?>
                                    <?php echo Modules::run('address/get_select_box_find_parent', $province, 'District', 'name', 'district', $district); ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group" id="localLevelDiv">
                                    <?php
                                    $local_level = isset($basicInfo->local_level) ? $basicInfo->local_level : '';
                                    ?>
                                    <?php echo Modules::run('address/get_select_box_find_parent', $district, 'Local Level', 'name', 'local_level', $local_level); ?>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group" id="wardnoDiv">
                                    <?php
                                    $ward_no = isset($basicInfo->ward_no) ? $basicInfo->ward_no : '';
                                    ?>
                                    <?php echo Modules::run('address/get_select_box_find_parent', $local_level, 'Ward No', 'name', 'ward_no', $ward_no); ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="address">Address:</label>
                                    <?php $address = $basicInfo->address ? $basicInfo->address : ''; ?>
                                    <input type="text" name="address" class="form-control" value="<?php echo $address; ?>"> 
                                </div>
                            </div>
                            <!--NEW ADDRESS-->


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="intersted_sports">Sports you play:</label>
                                    <?php $intersted_sports = isset($metaInfo->intersted_sports) ? $metaInfo->intersted_sports : ''; ?>
                                    <input type="text" name="intersted_sports" class="form-control" value="<?php echo $intersted_sports; ?>"> 
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="intersted_art">Art you do:</label>
                                    <?php $intersted_art = isset($metaInfo->intersted_art) ? $metaInfo->intersted_art : ''; ?>
                                    <input type="text" name="intersted_art" class="form-control" value="<?php echo $intersted_art; ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="interest">Interest:</label>
                                    <?php $interest = isset($metaInfo->interest) ? $metaInfo->interest : ''; ?>
                                    <input type="text" name="interest" class="form-control" value="<?php echo $interest; ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="gender">Gender:</label>
                                    <div class="drop_list">
                                        <select class="form-control" name='gender'>
                                            <?php $gender = isset($metaInfo->gender) ? $metaInfo->gender : ''; ?>
                                            <?php echo get_select_box('gender-setting', $gender); ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="line-well grey-bg round-border">
                        <h4>YOUR QUALIFICATION</h4>

                        <div class="panel-group" id="accordion-inner" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">


                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion-inner" href="#slc" aria-expanded="true" aria-controls="collapseOne">
                                            SLC
                                            <span class="glyphicon pull-right glyphicon-chevron-right"></span>
                                        </a>
                                    </h4>
                                </div>

                                <div id="slc" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="slc_affiliation">Affiliation:</label>
                                                    <?php $slc_affiliation = isset($metaInfo->slc_affiliation) ? $metaInfo->slc_affiliation : ''; ?>
                                                    <div class="drop_list">
                                                        <select class="form-control" name="slc_affiliation">
                                                            <?php echo get_select_box('slc-affiliation', $slc_affiliation); ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="slc_score">Your Total Score(%):</label>
                                                    <?php $slc_score = isset($metaInfo->slc_score) ? $metaInfo->slc_score : ''; ?>
                                                    <input type="text" name="slc_score" class="form-control" value="<?php echo $slc_score; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="slc_school">Your School/College Name:</label>
                                                    <?php $slc_school = isset($metaInfo->slc_school) ? $metaInfo->slc_school : ''; ?>
                                                    <input type="text" name="slc_school" class="form-control" value="<?php echo $slc_school; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="slc_school_location">Your School/Location:</label>
                                                    <?php $slc_school_location = isset($metaInfo->slc_school_location) ? $metaInfo->slc_school_location : ''; ?>
                                                    <input type="text" name="slc_school_location" class="form-control" value="<?php echo $slc_school_location; ?>">
                                                </div>
                                            </div>
                                        </div>

                                        <h3>Your main subjects and scores:</h3>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <table class="table table-condensed mytable" id="slctable">
                                                    <tr>
                                                        <th>Subject</th>
                                                        <th>Score</th>
                                                        <th></th>
                                                    </tr>
                                                    <?php
                                                    $slcMarks = Modules::run('student_marks/get_by_parent_level', $userID, 'SLC');
                                                    if ($slcMarks) {
                                                        foreach ($slcMarks as $smarks):
                                                            ?>
                                                            <tr>
                                                                <td><?php echo $smarks->subject; ?></td>
                                                                <td><?php echo $smarks->score; ?></td>
                                                                <td><a href="#" data-id="<?php echo $smarks->ID; ?>" class="removeScore remove-btn" data-prefix="slc"><i class="fa fa-times fa-fw"></i></a></td>
                                                            </tr>
                                                            <?php
                                                        endforeach;
                                                    }
                                                    ?>
                                                </table>
                                            </div>
                                        </div>

                                        <div class="blue-bg standard-padding-little-height round-border">
                                            <div class="slcerror"></div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <input type="text" id="slc-subject" name="subject" class="form-control" placeholder="Subject">
                                                </div>
                                                <div class="col-md-4">
                                                    <input type="text" id="slc-score" name="score" class="form-control" placeholder="Score">
                                                </div>
                                                <div class="col-md-4">
                                                    <button class="btn yelloButton btn-block addScoreMarks" data-parentid="<?php echo $userID ?>" data-level="SLC" data-prefix="slc"> Add Subject</button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-inner" href="#plusTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            +2
                                            <span class="glyphicon pull-right glyphicon-chevron-right"></span>
                                        </a>
                                    </h4>
                                </div>
                                <div id="plusTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="panel-body">

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="plus2_affiliation">Affiliation:</label>
                                                    <?php $plus2_affiliation = isset($metaInfo->plus2_affiliation) ? $metaInfo->plus2_affiliation : ''; ?>
                                                    <div class="drop_list">
                                                        <select class="form-control" name="plus2_affiliation">
                                                            <?php echo get_select_box('plus2-affiliation', $plus2_affiliation); ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="plus2_score">Your Total Score(%):</label>
                                                    <?php $plus2_score = isset($metaInfo->plus2_score) ? $metaInfo->plus2_score : ''; ?>
                                                    <input type="text" name="plus2_score" class="form-control" value="<?php echo $plus2_score; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="plus2_school">Your School/College Name:</label>
                                                    <?php $plus2_school = isset($metaInfo->plus2_school) ? $metaInfo->plus2_school : ''; ?>
                                                    <input type="text" name="plus2_school" class="form-control" value="<?php echo $plus2_school; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="plus2_school_location">Your School/Location:</label>
                                                    <?php $plus2_school_location = isset($metaInfo->plus2_school_location) ? $metaInfo->plus2_school_location : ''; ?>
                                                    <input type="text" name="plus2_school_location" class="form-control" value="<?php echo $plus2_school_location; ?>">
                                                </div>
                                            </div>
                                        </div>

                                        <h3>Your main subjects and scores:</h3>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <table class="table table-condensed mytable" id="plus2table">
                                                    <tr>
                                                        <th>Subject</th>
                                                        <th>Score</th>
                                                        <th></th>
                                                    </tr>
                                                    <?php
                                                    $plus2Marks = Modules::run('student_marks/get_by_parent_level', $userID, 'Plus Two');
                                                    if ($plus2Marks) {
                                                        foreach ($plus2Marks as $pmarks):
                                                            ?>
                                                            <tr>
                                                                <td><?php echo $pmarks->subject; ?></td>
                                                                <td><?php echo $pmarks->score; ?></td>
                                                                <td><a href="#" data-id="<?php echo $pmarks->ID; ?>" class="removeScore remove-btn" data-prefix="plus2"><i class="fa fa-times fa-fw"></i></a></td>
                                                            </tr>
                                                            <?php
                                                        endforeach;
                                                    }
                                                    ?>
                                                </table>
                                            </div>
                                        </div>


                                        <div class="blue-bg standard-padding-little-height round-border">
                                            <div class="plus2error"></div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <input type="text" id="plus2-subject" name="subject" class="form-control" placeholder="Subject">
                                                </div>
                                                <div class="col-md-4">
                                                    <input type="text" id="plus2-score" name="score" class="form-control" placeholder="Score">
                                                </div>
                                                <div class="col-md-4">
                                                    <button class="btn yelloButton btn-block addScoreMarks" data-parentid="<?php echo $userID ?>" data-level="Plus Two" data-prefix="plus2"> Add Subject</button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-inner" href="#bachelor" aria-expanded="false" aria-controls="collapseThree">
                                            Bachelor
                                            <span class="glyphicon pull-right glyphicon-chevron-right"></span>
                                        </a>
                                    </h4>
                                </div>
                                <div id="bachelor" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body">

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="bachelor_affiliation">Affiliation:</label>
                                                    <?php $bachelor_affiliation = isset($metaInfo->bachelor_affiliation) ? $metaInfo->bachelor_affiliation : ''; ?>
                                                    <div class="drop_list">
                                                        <select class="form-control" name="bachelor_affiliation">
                                                            <?php echo get_select_box('bachelor-affiliation', $bachelor_affiliation); ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="bachelor_score">Your Total Score(%):</label>
                                                    <?php $bachelor_score = isset($metaInfo->bachelor_score) ? $metaInfo->bachelor_score : ''; ?>
                                                    <input type="text" name="bachelor_score" class="form-control" value="<?php echo $bachelor_score; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="bachelor_school">Your School/College Name:</label>
                                                    <?php $bachelor_school = isset($metaInfo->bachelor_school) ? $metaInfo->bachelor_school : ''; ?>
                                                    <input type="text" name="bachelor_school" class="form-control" value="<?php echo $bachelor_school; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="bachelor_school_location">Your College Location:</label>
                                                    <?php $bachelor_school_location = isset($metaInfo->bachelor_school_location) ? $metaInfo->bachelor_school_location : ''; ?>
                                                    <input type="text" name="bachelor_school_location" class="form-control" value="<?php echo $bachelor_school_location; ?>">
                                                </div>
                                            </div>
                                        </div>

                                        <h3>Your main subjects and scores:</h3>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <table class="table table-condensed mytable" id="bachelortable">
                                                    <tr>
                                                        <th>Subject</th>
                                                        <th>Score</th>
                                                        <th></th>
                                                    </tr>
                                                    <?php
                                                    $bachMarks = Modules::run('student_marks/get_by_parent_level', $userID, 'Bachelor');
                                                    if ($bachMarks) {
                                                        foreach ($bachMarks as $bmarks):
                                                            ?>
                                                            <tr>
                                                                <td><?php echo $bmarks->subject; ?></td>
                                                                <td><?php echo $bmarks->score; ?></td>
                                                                <td><a href="#" data-id="<?php echo $bmarks->ID; ?>" class="removeScore remove-btn" data-prefix="bachelor"><i class="fa fa-times fa-fw"></i></a></td>
                                                            </tr>
                                                            <?php
                                                        endforeach;
                                                    }
                                                    ?>
                                                </table>
                                            </div>
                                        </div>


                                        <div class="blue-bg standard-padding-little-height round-border">
                                            <div class="bachelorerror"></div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <input type="text" id="bachelor-subject" name="subject" class="form-control" placeholder="Subject">
                                                </div>
                                                <div class="col-md-4">
                                                    <input type="text" id="bachelor-score" name="score" class="form-control" placeholder="Score">
                                                </div>
                                                <div class="col-md-4">
                                                    <button class="btn yelloButton btn-block addScoreMarks" data-parentid="<?php echo $userID ?>" data-level="Bachelor" data-prefix="bachelor"> Add Subject</button>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="line-well grey-bg round-border">
                        <h4>YOURS ENTRANCE TEST SCORE</h4>
                        <div class="row">
                            <div class="col-md-12" style="padding-bottom: 15px;">
                                <label for="plus_2_college_entrance">
                                    <?php
                                    $plus_2_college_entrance = isset($metaInfo->plus_2_college_entrance) ? $metaInfo->plus_2_college_entrance : 'Appeared';
                                    ?>
                                    <input type="checkbox" <?php echo $plus_2_college_entrance == 'Not Appeared' ? 'checked' : ''; ?> name="plus_2_college_entrance" value="Not Appeared" id="test-not-appeared">
                                    Test Not Appeared
                                </label>
                            </div>
                            <div id="test-not-appeared-box" <?php echo $plus_2_college_entrance == 'Not Appeared' ? 'style="display:none;"' : ''; ?>>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="plus_2_college_entrance_marks">+2 College Entrance Marks:</label>
                                        <input type="text" class="form-control"  value="<?php echo isset($metaInfo->plus_2_college_entrance_marks) ? $metaInfo->plus_2_college_entrance_marks : ''; ?>"  name="plus_2_college_entrance_marks">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="CMAT">CMAT Marks:</label>
                                        <input type="text" class="form-control" value="<?php echo isset($metaInfo->CMAT) ? $metaInfo->CMAT : ''; ?>" name="CMAT">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="KUUMAT">KUUMAT Marks:</label>
                                        <input type="text" class="form-control"   value="<?php echo isset($metaInfo->KUUMAT) ? $metaInfo->KUUMAT : ''; ?>"   name="KUUMAT">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="Engineering">Engineering Marks:</label>
                                        <input type="text" class="form-control"  value="<?php echo isset($metaInfo->engineering) ? $metaInfo->engineering : ''; ?>"   name="Engineering">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="Medicine">Medicine Marks:</label>
                                        <input type="text" class="form-control"  value="<?php echo isset($metaInfo->medicine) ? $metaInfo->medicine : ''; ?>"   name="Medicine">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <button type="submit" name="updateUsers" class="btn yelloButton">Update Profile</button>
                    </div>

                    <?php echo form_close(); ?>



                    <div class="line-well grey-bg round-border">
                        <h4>DOCUMENTS & CERTIFICATES</h4>

                        <div class="row">
                            <div class="col-md-12 gal-img">
                                <div class="row no-gutter gallery_img_div">
                                    <?php
                                    if (!empty($basicInfo->gallery_picture)) {
                                        $galImages = explode(',', $basicInfo->gallery_picture);
                                        foreach ($galImages as $gm):
                                            $galData['imgID'] = $gm;
                                            $this->load->view('imagehub/gal_img_del', $galData);
                                        endforeach;
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="gal_pic_error"></div>
                            </div>
                            <?php echo form_open_multipart('imagehub/collegegallery', array('id' => 'galleryPic')); ?>
                            <div class="col-md-12">
                                <span class="special-note">
                                    Allowed file types: jpg, jpeg, png, pdf
                                </span>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">                                    
                                    <input type="file" name="gal_pic" id="college_gallery">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">
                                <input type="submit" name="galpic" class="btn yelloButton btn-block galBtn" value="Add Document">
                                <input type="hidden" name="addtolibgal" value="yes">
                            </div>
                            <?php echo form_close(); ?>
                        </div>

                    </div>


                </div>
            </div>

        </div>


    </div>
</div>
