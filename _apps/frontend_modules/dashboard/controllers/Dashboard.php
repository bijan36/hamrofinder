<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Frontendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER DASHBOARD CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        
        //IF NO USER LOGIN SESSION JUST REDIRECT TO LOGIN
        //ASKING THE VALIDATOR 
        if (!Modules::run('validator/is_user_login')) {
            redirect('login');
            exit();
        }
    }

    //USER DASHBOARD
    public function index() {
        $userID = (int) $this->session->userdata('userID');
        $userRow = Modules::run('user/get_row', $userID);
        if ($userRow) {
            $prefix = strtolower($userRow->type);
            $data['basicInfo'] = $userRow;
            $data['userRow'] = $userRow;
            $data['metaInfo'] = Modules::run($prefix . '/get_row_by_parent', $userID);
            $data['userID'] = $userID;
            $this->template->load(get_template('common-right-bar-' . $prefix), $prefix . '/dashboard', $data);
        } else {
            redirect('site');
            exit();
        }
    }

}
