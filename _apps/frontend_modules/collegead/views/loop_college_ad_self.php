<?php
if (isset($row) && !empty($row)) {
    if ($row->parent_ID == $this->session->userdata('userID')) {


        $userInfo = Modules::run('user/get_row', $row->parent_ID);
        //$collegeInfo = Modules::run('student/get_row_by_parent', $userInfo->ID);
        ?>
        <?php
        //IF THE ADS ARE NOT DELETED
        if ($row->status != 'Deleted') {
            ?>
            <div class="student-box round-border-with-border grey-bg adbox<?php echo $row->ID; ?>"  style="margin-bottom: 10px;">
                <div class="row no-gutter">
                    <div class="col-md-3">
                        <?php
                        $profile_image = Modules::run('imagehub/get_profile_image_real', $userInfo->profile_picture);
                        echo $profile_image;
                        ?>
                    </div>

                    <div class="col-md-9" style="padding-left: 15px;">


                        <table class="table">
                            <tr>
                                <td style="border-top: none;">
                                    <span class="bold-uppercase-blue"><?php echo $userInfo->name; ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="text-uppercase"><?php echo $userInfo->address; ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button class="blueCompact-small who-intrested"  data-id="<?php echo $row->ID . '-College'; ?>">Interest: <span class="interest-span<?php echo $row->ID; ?>"><?php echo total_interest($row->ID, 'College'); ?></span></button>
                                            <?php
                                            //FIND THE CURRENT STATUS OF AD
                                            echo Modules::run('collegead/get_ad_status_label', $row->ID);
                                            ?>
                                            <?php
                                            //FEATUE AD
                                            if ($row->feature == 'Yes') {
                                                echo '<span class="featured-span">Featured Ad</span>';
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>



                        <table class="table">
                            <tr>
                                <td style="width: 120px;">Looking for:</td>
                                <td><span class="bold-uppercase-highlight"><?php echo $row->looking_for; ?></span></td>
                            </tr>
                            <tr>
                                <td>Affiliation:</td>
                                <td><?php echo $row->affiliation; ?></td>
                            </tr>
                            <tr>
                                <td>Entrance Req.:</td>
                                <td><?php echo $row->entrance_required; ?></td>
                            </tr>
                            <tr>
                                <td>Shift:</td>
                                <td><?php echo $row->shift; ?></td>
                            </tr>
                            <tr>
                            <td>College Bus: <span class="bold-uppercase"><?php echo $row->college_bus; ?></span></td>
                        </tr>
                            <tr>
                                <td>Grade Accepted(%):</td>
                                <td><?php echo $row->grade_accepted; ?></td>
                            </tr>
                            <tr>
                                <td>College Bus:</td>
                                <td><?php echo $row->college_bus; ?></td>
                            </tr>
                            <tr>
                                <td>Can admit in: </td>
                                <td><?php echo $row->admission_within; ?> Days</td>
                            </tr>
                            <tr>
                                <td>Starting Date:</td>
                                <td><?php echo convert_date($row->starting_date); ?></td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-12">
                        <div class="student-box-message">
                            <?php echo $row->message; ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <?php
                        $apply_list = Modules::run('studentapply/get_apply_list_by_adID', $row->ID);
                        if ($apply_list):
                            ?>
                            <div class="bold-uppercase-blue" style="padding-bottom:10px; padding-top:10px; border-bottom: 1px solid #ccc;">Applications on this ad</div>
                            <?php
                            foreach ($apply_list as $apply):
                                ?>

                                <?php
                                $datas['row'] = $apply;
                                $this->load->view('studentapply/list', $datas);
                                ?>

                                <?php
                            endforeach;
                        endif;
                        ?>
                    </div>
                </div>
                <div class="remove-btn remove-college-ad" data-id="<?php echo $row->ID; ?>"><i class="fa fa-times fa-fw"></i></div>
            </div>

            <?php
        }else {
            ?>
            <div class="student-box round-border-with-border grey-bg adbox<?php echo $row->ID; ?>"  style="margin-bottom: 10px;">
                <div class="row no-gutter">
                    <div class="col-md-12">Looking for: <span class="bold-uppercase-highlight"><?php echo $row->looking_for; ?></span> <span class="pull-right">Removed</span></div>
                </div>
            </div>
            <?php
        }
    }
}
?>