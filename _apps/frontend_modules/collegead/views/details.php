<?php
if (isset($userRow) && !empty($userRow)) {
    //STUDENT META
    $meta = Modules::run('college/get_row_by_parent', $userRow->ID);
    ?>
    <h1><?php echo!empty($adRow->looking_for) ? '<span class="blueHead"><span class="uppercaseTextYellow">Looking For: </span>' . $adRow->looking_for . '</span>' : '<span class="blueHead">Ads Details</span>'; ?>
        <?php
        //show edit link if the login user is same
        if (is_user_login() && $this->session->userdata('userID') == $userRow->ID) {
            ?>
            <span class="pull-right">
                <a href="<?php echo site_url('dashboard') ?>" class="btn yelloButton pull-right" title="View/Edit Your Profile">
                    <i class="fa fa-edit fa-fw"></i> Edit Profile
                </a>
            </span>
            <?php
        }
        ?>
    </h1>

    <?php
    $data['row'] = $adRow;
    $this->load->view('details_ads_box', $data);
}
?>


<?php
$type = 'College';
$intr = Modules::run('interest/list_who_intrested', $adRow->ID, $type);
if ($intr) {
    ?>
    <h3>Students showing interest</h3>
    <?php
    echo $intr ? $intr : '';
}
?>

<?php
//ISSUED AD
$myadrows = Modules::run('collegead/get_rows_by_parent', $userRow->ID);
if ($myadrows && count($myadrows) > 1) {
    ?>
    <div class="ad-box-round-corner standard-padding round-border normal-list">
        <h4>Published Ad</h4>
        <ul>
            <?php
            foreach ($myadrows as $row):
                if (can_show_ad($row->ID, $row->parent_ID)) {
                    if ($adRow->ID != $row->ID) {
                        ?>
                        <li>
                            <span class="which-ads" data-id="<?php echo $row->ID ?>-<?php echo $type ?>" data-type="<?php echo $type ?>" data-url="<?php echo base_url(); ?>">
                                Looking for: <strong><?php echo $row->looking_for; ?></strong>
                            </span>

                            <?php
                            $totalInterest = total_interest($row->ID, $type);
                            if ($totalInterest > 0) {
                                ?>
                                <small>
                                    <div class="red-batch who-intrested"  data-toggle="tooltip" data-placement="top" title="Total interest <?php echo $totalInterest; ?>" data-id="<?php echo $row->ID . '-' . $type; ?>" data-url="<?php echo base_url(); ?>"><?php echo $totalInterest; ?></div>
                                </small>
                                <?php
                            }
                            ?>
                        </li>
                        <?php
                    }
                }
            endforeach;
            ?>
        </ul>
    </div>
    <?php
}
?>
<?php
//DOCUMENTS
$allDocs = Modules::run('documents/get_docs_by', $userRow->ID, COLLEGE);
if ($allDocs) {
    $totalDoc = count($allDocs);
    ?>
    <div class="ad-box-round-corner standard-padding round-border normal-list">
        <h4>Documents of <?php echo $userRow->name; ?></h4>
        <div class="row no-gutter">
            <?php
            foreach ($allDocs as $doc):
                $docData['doc'] = $doc;
                $docData['total'] = $totalDoc;
                $this->load->view('documents/loop', $docData);
            endforeach;
            ?>
        </div>
    </div>
    <?php
}
?>
<?php
//PHOTO GALLERY
if ($userRow->gallery_picture) {
    ?>
    <div class="ad-box-round-corner standard-padding round-border normal-list">
        <h4>Gallery Images of <?php echo $userRow->name; ?></h4>
        <div class="row">
            <?php
            if (strpos($userRow->gallery_picture, ',') !== false) {
                //yes comma exist
                $galImages = explode(',', $userRow->gallery_picture);
                foreach ($galImages as $gim):
                    $this->load->view('imagehub/gal_img_big', array('imgID' => $gim, 'college_name' => $userRow->name));
                endforeach;
            } else {
                //no comma that means just one image exist
                $this->load->view('imagehub/gal_img_big', array('imgID' => $userRow->gallery_picture, 'college_name' => $userRow->name));
            }
            ?>
        </div>
    </div>
    <?php
}
?>



