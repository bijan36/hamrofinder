<?php
if (is_array($idArray)) {
    foreach ($idArray as $id):
        $row = Modules::run('college_programe/get_row', $id);
        if ($row) {
            ?>

            <div class="line-well unique-<?php echo $row->ID; ?>">
                <table class="table">
                    <tr>
                        <td colspan="4"><strong><?php echo $row->offered_programe; ?></strong></td>
                    </tr>
                    <tr>
                        <td>Program Title:</td>
                        <td><?php echo $row->program_title; ?></td>
                        <td>Program Affiliation:</td>
                        <td><?php echo $row->affiliation; ?></td>
                    </tr>
                    <tr>
                        <td>Program Duration:</td>
                        <td><?php echo $row->program_duration; ?></td>
                        <td>Class held at:</td>
                        <td><?php echo $row->class_held_at; ?></td>
                    </tr>
                    <tr>
                        <td>Admission Fee:</td>
                        <td>Rs. <?php echo $row->admission_fee; ?></td>
                        <td>Total Fee:</td>
                        <td>Rs. <?php echo $row->total_course_fee; ?></td>
                    </tr>
                    <tr>
                        <td>Other Fee:</td>
                        <td><?php echo $row->other_fee == '0' ? '--' : 'Rs. '.$row->other_fee; ?></td>
                        <td>Fee Collection:</td>
                        <td><?php echo $row->fee_collect; ?></td>
                    </tr>
                </table>
                <?php
                //NO DELETE BUTTON IF NEEDED
                if (!isset($no_del)) {
                    ?>
                    <div class="removeOffer remove-btn" data-id="<?php echo $row->ID; ?>" data-parent="<?php echo $row->parent_ID; ?>"><i class="fa fa-times fa-fw"></i></div>
                        <?php
                    }
                    ?>
            </div>

            <?php
        }
    endforeach;
}
?>
