<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Interest extends Frontendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('Interest_m', 'finterest');
    }

    //USER DASHBOARD
    public function index() {
        redirect('site');
        exit();
    }

    //ADDING INTEREST
    public function interest_clicked() {
        $adID = $this->input->post('id');
        $parts = explode('-', $adID);
        $adID = $parts[0];
        $adType = $parts[1];
        //CHECK USER LOGIN
        if (is_user_login()) {
            $userID = get_user_ID();

            //FIND THE UESR LEVEL
            $userLevel = $this->session->userdata('userLevel');
            //FIND THE USER TYPE
            $userType = $this->session->userdata('userType');

            //LEVEL ATHONTICATION FOR THE SHOW AN INTEREST ACTION FOR USERS
            if ($userType == 'College') {
                $interestAuth = Modules::run('college_membership_plans/find_authorise', $userLevel, 'interest');
                if ($interestAuth == 'No') {
                    $data = array(
                        'status' => 'error',
                        'msg' => INVALIDMSG,
                        'do' => 'sametype'
                    );
                    echo json_encode($data);
                    exit();
                }
            }

            if ($userType == 'Institute') {
                $interestAuth = Modules::run('institute_membership_plans/find_authorise', $userLevel, 'interest');
                if ($interestAuth == 'No') {
                    $data = array(
                        'status' => 'error',
                        'msg' => INVALIDMSG,
                        'do' => 'sametype'
                    );
                    echo json_encode($data);
                    exit();
                }
            }
            //LEVEL ATHONTICATION FOR THE SHOW AN INTEREST ACTION FOR USERS
            //IF THE USER TYPE IS MATCHED NOT ALLOW TO INTEREST THEM
            if ($userType == $adType) {
                $data = array(
                    'status' => 'error',
                    'msg' => INVALIDMSG,
                    'do' => 'sametype'
                );
                echo json_encode($data);
                exit();
            }
            //IF COLLEGE AD OR INSTITUTE AD ONLY ALLOW STUDENT TO SHOW INTEREST
            if ($adType == 'College' || $adType == 'Institute') {
                if ($userType != 'Student') {
                    $data = array(
                        'status' => 'error',
                        'msg' => INVALIDMSG,
                        //'msg' => '1',
                        'do' => 'sametype'
                    );
                    echo json_encode($data);
                    exit();
                }
            }

            //IF STUDENT AD ONLY ALLOW COLLEGE AND INSTITUTE TO SHOW INTEREST
            /* if ($adType == 'Student') {
              if ($userType != 'College' || $userType != 'Institute') {
              $data = array(
              'status' => 'error',
              //'msg' => '2',
              'msg' => INVALIDMSG,
              'do' => 'sametype'
              );
              echo json_encode($data);
              exit();
              }
              } */


            //FIND IF ALREADY DONE INTEREST BEFORE
            $row = $this->finterest->get_by(array('ad_ID' => $adID, 'interest_ID' => $userID));
            if ($row) {
                $data = array(
                    'status' => 'error',
                    'msg' => 'You have already shown interest!',
                    'do' => 'nothing'
                );
                echo json_encode($data);
                exit();
            } else {
                $dataIns = array(
                    'ad_ID' => $adID,
                    'interest_ID' => $userID,
                    'date_time' => get_date_time(),
                    'ip_address' => $this->input->ip_address(),
                    'type' => $adType
                );
                if ($this->finterest->insert($dataIns)) {
                    $data = array(
                        'status' => 'success',
                        'msg' => 'Thank you for your interest.',
                        'total' => $this->get_total_by_ad($adID, $adType)
                    );
                    echo json_encode($data);
                    exit();
                } else {
                    $data = array(
                        'status' => 'error',
                        'msg' => 'Unable to do it right now, please try again later',
                        'do' => 'nothing'
                    );
                    echo json_encode($data);
                    exit();
                }
            }
        } else {
            //REDIRECT TO LOGIN PAGE
            $data = array(
                'status' => 'error',
                'msg' => 'To show interest, please register or login.',
                'do' => 'login'
            );
            echo json_encode($data);
            exit();
        }
    }

    //INSTERSTED PERSON LIST
    public function who_intrested() {
        $adID = $this->input->post('id');
        $parts = explode('-', $adID);
        $adID = $parts[0];
        $adType = $parts[1];
        if (isset($adID) && !empty($adID) && $adID > 0) {


            //FIND THE UESR LEVEL
            $userLevel = $this->session->userdata('userLevel');
            //FIND THE USER TYPE
            $userType = $this->session->userdata('userType');

            //LEVEL ATHONTICATION FOR THE SHOW AN INTEREST ACTION FOR USERS
            if ($userType == 'College') {
                $interestAuth = Modules::run('college_membership_plans/find_authorise', $userLevel, 'profile');
                if ($interestAuth == 'No') {
                    $data = array(
                        'status' => 'error',
                        'msg' => INVALIDMSG,
                        'do' => 'sametype'
                    );
                    echo json_encode($data);
                    exit();
                }
            }

            if ($userType == 'Institute') {
                $interestAuth = Modules::run('institute_membership_plans/find_authorise', $userLevel, 'profile');
                if ($interestAuth == 'No') {
                    $data = array(
                        'status' => 'error',
                        'msg' => INVALIDMSG,
                        'do' => 'sametype'
                    );
                    echo json_encode($data);
                    exit();
                }
            }
            //LEVEL ATHONTICATION FOR THE SHOW AN INTEREST ACTION FOR USERS
            
            $rows = $this->finterest->get_many_by(array('ad_ID' => $adID, 'type' => $adType));
            if ($rows) {

                $list = '<div class="intersted_user_list"><ul>';
                $toInterest = 0;
                foreach ($rows as $row):

                    $userID = $row->interest_ID;
                    $userRow = Modules::run('user/get_row', $userID);
                    if ($userRow->status == ACTIVE) {
                        if ($userRow) {
                            $data['userRowi'] = $userRow;
                            $data['intRow'] = $row;
                            $list .= $this->load->view("user/interest_list_loop", $data, true);
                            $toInterest = $toInterest + 1;
                        }
                    }
                endforeach;
                $list .= "</ul></div>";

                $data = array(
                    'status' => 'success',
                    'toInterest' => $toInterest,
                    'intersted_list' => $list,
                );
                echo json_encode($data);
                exit();
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => 'No one shown interst yet.',
                );
                echo json_encode($data);
                exit();
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => 'Your are not allowed to view this list.',
            );
            echo json_encode($data);
            exit();
        }
    }

    public function list_who_intrested($adID, $adType) {
        if (isset($adID) && !empty($adID) && $adID > 0) {
            $rows = $this->finterest->get_many_by(array('ad_ID' => $adID, 'type' => $adType));
            if ($rows) {
                $list = '<div class="intersted_user_list"><div class="row">';
                $showLimit = 0;
                $toInterest = 0;
                foreach ($rows as $row):
                    if ($showLimit <= 4) {
                        $userID = $row->interest_ID;
                        $userRows = Modules::run('user/get_row', $userID);
                        if ($userRows->status == ACTIVE) {
                            if ($userRows) {
                                $data['userRowi'] = $userRows;
                                $data['intRow'] = $row;
                                $list .= '<div class="col-md-6">';
                                $list .= '<div class="list_who_interested">';
                                $list .= $this->load->view("user/interest_list_loop_more", $data, true);
                                $list .= '</div>';
                                $list .= '</div>';
                                $showLimit = $showLimit + 1;
                            }
                        }
                    }
                    $toInterest = $toInterest + 1;
                endforeach;
                $rest = $toInterest - $showLimit;
                $pre = ($rest > 1) ? "Students" : "Student";
                if ($rest > 0) {
                    $list .= '<div class="col-md-6">';
                    $list .= '<div class="list_who_interested_focus">';
                    $list .= '<a href="#" class="who-intrested"  data-url="' . base_url() . '"  data-id="' . $adID . '-' . $adType . '">+' . $rest . '<span style="display:inline-block; font-size:14px; color: #666;">' . $pre . '</span></a>';
                    $list .= '</div>';
                    $list .= '</div>';
                }
                $list .= "</div></div>";
                return $list;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    //GETTING TOTAL BY AD
    public function get_total_by_ad($adID, $type) {
        return $this->finterest->count_by(array('ad_ID' => $adID, 'type' => $type));
    }

    //GET ROWS BY USER
    public function get_rows_by_user($userID) {
        $rows = $this->finterest->order_by('ID', 'DESC')->get_many_by(array('interest_ID' => $userID));
        return $rows ? $rows : false;
    }

}
