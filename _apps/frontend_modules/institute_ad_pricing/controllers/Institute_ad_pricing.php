<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Institute_ad_pricing extends Frontendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('Institute_ad_pricing_m', 'finsadprice');
    }

    //STUDENT LIST
    public function index() {
        redirect('site');
        exit();
    }

    public function get_price_list() {
        $rows = $this->finsadprice->order_by('position', 'ASC')->get_all();
        return $rows ? $rows : false;
    }

    public function get_plan_by_ID($ID) {
        if (!is_user_login() && is_user_institute()) {
            redirect('login');
            exit();
        }

        $row = $this->finsadprice->get_by(array('ID' => $ID, 'Active'));
        if ($row) {
            return $row;
        } else {
            return false;
        }
    }

}
