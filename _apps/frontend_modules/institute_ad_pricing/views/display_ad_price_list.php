<?php
$allAdPrices = Modules::run('institute_ad_pricing/get_price_list');
if (!empty($allAdPrices)) {
    ?>
    <div class="col-md-12">
        <div class="row">
            <?php foreach ($allAdPrices as $adPrice): ?>
                <div class="col-md-3">
                    <div class="planBox">
                        <div class="planBox-header"><span>Buy</span> <?php echo $adPrice->number_ads; ?> Ad</div>
                        <div><strong>NPR <?php echo $adPrice->per_year_fee; ?></strong><span class="small">/per year</span></div>
                    </div>        
                </div>        
            <?php endforeach; ?>
        </div>
    </div>
    <?php
}
?>