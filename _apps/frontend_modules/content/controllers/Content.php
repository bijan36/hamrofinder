<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Content extends Frontendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('Content_m', 'fcontent');
    }

    //CONTENT
    public function index() {
        redirect('site');
        exit();
    }

    //show the singel content here
    public function show($slug) {
        $row = $this->fcontent->get_by(array('slug' => $slug));
        if ($row) {
            $template = strtolower(str_replace(' ', '-', $row->template));
            $data['content'] = $row;
            $this->template->load('front/templates/pages/' . $template, 'content', $data);
        } else {
            redirect('site');
            exit();
        }
    }

    public function contact() {
        $this->template->load('front/templates/default', 'contact');
    }

}
