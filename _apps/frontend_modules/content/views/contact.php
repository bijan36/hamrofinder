<div class="content">
<pre>
<?php print_r($this->session->userdata());?>
</pre>

    <h1>Contact Us</h1>
    <div class="row">
        <div class="col-md-4">
            <?php echo Modules::run('settings/get_value', 'website-contact-address'); ?>
        </div>
        <div class="col-md-8">


            <?php echo form_open('content/do_contact', array('id' => 'doContact')) ?>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Name:</label>
                        <input type="text" name="name" class="form-control">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Email:</label>
                        <input type="email" name="email" class="form-control">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Phone No.:</label>
                        <input type="text" name="phone" class="form-control">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Address:</label>
                        <input type="text" name="address" class="form-control">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Subject:</label>
                        <input type="text" name="subject" class="form-control">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Message / Suggestion:</label>
                        <textarea class="form-control" name="message"></textarea>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="g-recaptcha" data-sitekey="<?php echo get_captcha(); ?>"></div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary" name="letscontact"/>Submit</button>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>


        </div>
    </div>
</div>