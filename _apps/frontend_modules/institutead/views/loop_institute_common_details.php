<?php
if (isset($row) && !empty($row)) {
    $userInfo = Modules::run('user/get_row', $row->parent_ID);
    //$collegeInfo = Modules::run('student/get_row_by_parent', $userInfo->ID);
    ?>
    <div class="student-box round-border-with-border grey-bg"  style="margin-bottom: 10px;">
        <div class="row no-gutter">
            <div class="col-md-3">
                <?php
                $profile_image = Modules::run('imagehub/get_profile_image_real', $userInfo->profile_picture);
                ?>
                <a href="<?php echo profile_url($userInfo->ID) ?>">
                    <div class="relativeBox">
                        <?php echo $profile_image; ?>
                        <?php echo Modules::run('user/get_online_status', $userInfo->ID); ?>
                    </div>
                </a>
                <span class="bold-uppercase-highlight center-block" style="margin-top: 10px;"><?php echo $userInfo->type; ?></span>
            </div>
            <div class="col-md-9" style="padding-left: 15px;">
                <table class="table">
                    <tr>
                        <td colspan="2" style="border-top: none;">
                            <span class="bold-uppercase-blue">
                                <a href="<?php echo profile_url($userInfo->ID) ?>">
                                    <?php echo $userInfo->name; ?>
                                </a>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="text-uppercase"><?php echo $userInfo->address; ?></span>
                        </td>
                    </tr>
                </table>

                <table class="table">
                    <tr>
                        <td style="width: 130px;">Looking for:</td>
                        <td><span class="bold-uppercase-highlight"><?php echo $row->looking_for; ?></span></td>
                    </tr>
                    <tr>
                        <td>Certified By:</td>
                        <td><?php echo $row->affiliation; ?></td>
                    </tr>

                    <tr>
                        <td>Certified By:</td>
                        <td><?php echo $row->affiliation; ?></td>
                    </tr>
                    <tr>
                        <td>Group Discount:</td>
                        <td><?php echo $row->group_discount; ?></td>
                    </tr>
                    <tr>
                        <td>Shift:</td>
                        <td><?php echo $row->shift; ?></td>
                    </tr>
                    <tr>
                        <td>Audio/Video Class: <span class="bold-uppercase"><?php echo $row->audio_visual_class; ?></span></td>
                    </tr>
                    <tr>
                        <td>Scholarship Upto(%):</td>
                        <td><?php echo $row->grade_accepted; ?></td>
                    </tr>
                    <tr>
                        <td>Audio-Visual Class:</td>
                        <td><?php echo $row->audio_visual_class; ?></td>
                    </tr>
                    <tr>
                        <td>Class Duration:</td>
                        <td><?php echo $row->class_duration; ?></td>
                    </tr>
                    <tr>
                        <td>Starting Date:</td>
                        <td><?php echo convert_date($row->starting_date); ?></td>
                    </tr>

                </table>
            </div>
            <div class="col-md-12">
                <div class="student-box-message">
                    <?php echo $row->message; ?>
                </div>
            </div>
        </div>
    </div>
    <?php
}
?>