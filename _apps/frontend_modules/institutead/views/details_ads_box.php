<?php
if (isset($row) && !empty($row)) {
    $userInfo = Modules::run('user/get_row', $row->parent_ID);
    ?>

    <div class="student-box round-border-with-border grey-bg hvr-float"  style="margin-bottom: 10px;">
        <div class="row no-gutter">
            <div class="col-md-3">
                <?php
                $profile_image = Modules::run('imagehub/get_profile_image_real', $userInfo->profile_picture);
                ?>
                <div class="relativeBox">
                    <?php echo $profile_image; ?>
                    <?php echo Modules::run('user/get_online_status', $userInfo->ID); ?>
                </div>
                <span class="bold-uppercase-highlight center-block" style="margin-top: 10px;"><?php echo $userInfo->type; ?></span>
            </div>
            <div class="col-md-9" style="padding-left:15px;">
                <table class="table">
                    <tr>
                        <td><span class="bold-uppercase-blue" style="font-size: 16px;"><?php echo $userInfo->name; ?></span></td>
                    </tr>
                    <tr>
                        <td><span class="text-uppercase"><?php echo Modules::run('user/get_full_address', $userInfo->ID); ?></td>
                    </tr>
                </table>

                <table class="table">
                    <tr>
                        <td style="width: 130px;">Looking for:</td>
                        <td><span class="bold-uppercase-highlight"><?php echo $row->looking_for; ?></span></td>
                    </tr>
                    <tr>
                        <td>Certified By:</td>
                        <td><?php echo $row->affiliation; ?></td>
                    </tr>

                    <tr>
                        <td>Certified By:</td>
                        <td><?php echo $row->affiliation; ?></td>
                    </tr>
                    <tr>
                        <td>Group Discount:</td>
                        <td><?php echo $row->group_discount; ?></td>
                    </tr>
                    <tr>
                        <td>Shift:</td>
                        <td><?php echo $row->shift; ?></td>
                    </tr>
                    <tr>
                        <td>Audio/Video Class: <span class="bold-uppercase"><?php echo $row->audio_visual_class; ?></span></td>
                    </tr>
                    <tr>
                        <td>Scholarship Upto(%):</td>
                        <td><?php echo $row->grade_accepted; ?></td>
                    </tr>
                    <tr>
                        <td>Audio-Visual Class:</td>
                        <td><?php echo $row->audio_visual_class; ?></td>
                    </tr>
                    <tr>
                        <td>Class Duration:</td>
                        <td><?php echo $row->class_duration; ?></td>
                    </tr>
                    <tr>
                        <td>Starting Date:</td>
                        <td><?php echo convert_date($row->starting_date); ?></td>
                    </tr>
                </table>
            </div>
            <div class="col-md-12">
                <div class="student-box-message">
                    <?php
                    echo $row->message;
                    $type = 'Institute';
                    ?>
                </div>
            </div>
            <div class="col-md-3">
                <button class="btn yellowCompact btn-block top-5 interest-btn"  data-url="<?php echo base_url(); ?>" data-id="<?php echo $row->ID . '-' . $type; ?>">Show Interest</button>
            </div>
            <div class="col-md-3">
                <button class="btn yellowCompact btn-block top-5 apply-now-btn" data-url="<?php echo base_url(); ?>"  data-id="<?php echo $row->ID . '-' . $type; ?>">Apply Now</button>
            </div>
            <div class="col-md-3">
                <button class="btn blueCompact btn-block top-5 who-intrested" data-url="<?php echo base_url(); ?>"  data-id="<?php echo $row->ID . '-' . $type; ?>">Interest: <span class="interest-span<?php echo $row->ID . '-' . $type; ?>"><?php echo total_interest($row->ID, $type); ?></span></button>
            </div>
            <div class="col-md-3">
                <button class="btn blueCompact btn-block top-5 save-ad-btn" data-url="<?php echo base_url(); ?>"  data-id="<?php echo $row->ID . '-' . $type; ?>">Save This Ad</button>
            </div>
        </div>
    </div>

    <?php
}
?>