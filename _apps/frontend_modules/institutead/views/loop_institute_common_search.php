<?php
$type = 'Institute';
if (isset($row) && !empty($row)) {
    $userInfo = Modules::run('user/get_row', $row->parent_ID);
    $dyClass = '';
    //echo '<pre>';
    //print_r($dynamicTag);
    //echo '</pre>';
    if (isset($dynamicTag) && !empty($dynamicTag)) {
        foreach ($dynamicTag as $dtk => $dt):
            if ($dtk == 'institute_facilities_element') {
                foreach ($dt as $ddkkk => $ddkkv):
                    if ($ddkkv == $row->ID) {
                        $newv_one = str_replace('-' . $ddkkv, '', $ddkkk);
                        $newv_two = str_replace(' ', '_', $newv_one);
                        $dyClass .= $newv_two . ' ';
                    }
                endforeach;
            } elseif ($dtk == 'institute_securities_element') {
                foreach ($dt as $ddkkk => $ddkkv):
                    if ($ddkkv == $row->ID) {
                        $newv_one = str_replace('-' . $ddkkv, '', $ddkkk);
                        $newv_two = str_replace(' ', '_', $newv_one);
                        $dyClass .= $newv_two . ' ';
                    }
                endforeach;
            } elseif ($dtk == 'additional_features_element') {
                foreach ($dt as $ddkkk => $ddkkv):
                    if ($ddkkv == $row->ID) {
                        $newv_one = str_replace('-' . $ddkkv, '', $ddkkk);
                        $newv_two = str_replace(' ', '_', $newv_one);
                        $dyClass .= $newv_two . ' ';
                    }
                endforeach;
            } else {
                if (in_array($row->ID, $dt)) {
                    $dyClass .= $dtk . ' ';
                }
            }
        endforeach;

        //ADDING EXTRA ADDRESS TAGS IF FOUND
        if (!empty($addressValues) && isset($addressValues)) {
            if (isset($addressValues['country']) && $addressValues['country'] == $userInfo->country) {
                $dyClass .= 'advcountry-institute ';
            }
            if (isset($addressValues['province']) && $addressValues['province'] == $userInfo->province) {
                $dyClass .= 'advprovince-institute ';
            }
            if (isset($addressValues['district']) && $addressValues['district'] == $userInfo->district) {
                $dyClass .= 'advdistrict-institute ';
            }
            if (isset($addressValues['local_level']) && $addressValues['local_level'] == $userInfo->local_level) {
                $dyClass .= 'advlocal_level-institute ';
            }
            if (isset($addressValues['ward_no']) && $addressValues['ward_no'] == $userInfo->ward_no) {
                $dyClass .= 'advward_no-institute ';
            }
            if (isset($addressValues['address']) && $addressValues['address'] == $userInfo->address) {
                $dyClass .= 'advaddress-institute ';
            }
        }
    }
    ?>
    <div class="col-md-3 col-sm-6 col-xs-12 equal-item <?php echo $dyClass; ?>">
        <div class="student-box round-border-with-border grey-bg hvr-float"  style="margin-bottom: 10px;">
            <div class="row no-gutter">
                <div class="col-md-5">
                    <div class="main-profile-holder relativeBox">
                        <?php
                        $profile_image = Modules::run('imagehub/get_profile_image', $userInfo->profile_picture);
                        ?>
                        <a href="<?php echo profile_url($userInfo->ID) ?>">
                            <?php echo $profile_image; ?>
                            <?php echo Modules::run('user/get_online_status', $userInfo->ID); ?>
                        </a>
                        <div class="userTypeShow">
                            <span class="expired-span badge"><?php echo $userInfo->type; ?></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="row bigger-gutter">
                        <div class="col-md-12">
                            <button class="btn yellowCompact btn-block bottom-5 interest-btn"  data-url="<?php echo base_url(); ?>" data-id="<?php echo $row->ID . '-Institute'; ?>">Show Interest</button>
                        </div>
                        <div class="col-md-12">
                            <button class="btn yellowCompact btn-block bottom-5 apply-now-btn"  data-url="<?php echo base_url(); ?>" data-id="<?php echo $row->ID . '-Institute'; ?>">Apply Now</button>
                        </div>
                        <div class="col-md-6">
                            <button class="btn yellowCompact btn-block bottom-5 more-info-btn"  data-url="<?php echo base_url(); ?>" data-id="<?php echo $row->ID . '-' . $userInfo->ID . '-Institute'; ?>">More Info</button>
                        </div>
                        <div class="col-md-6">
                            <button class="btn blueCompact btn-block bottom-5 who-intrested"  data-url="<?php echo base_url(); ?>" data-id="<?php echo $row->ID . '-Institute'; ?>">Interest: <span class="interest-span<?php echo $row->ID; ?>-Institute"><?php echo total_interest($row->ID, 'Institute'); ?></span></button>
                        </div>
                        <div class="col-md-12">
                            <button class="btn blueCompact btn-block bottom-5 save-ad-btn"  data-url="<?php echo base_url(); ?>" data-id="<?php echo $row->ID . '-Institute'; ?>">Save This Ad</button>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <table class="table">
                        <tr>
                            <td style="border-top: none;">
                                <span class="bold-uppercase-blue">
                                    <a href="<?php echo profile_url($userInfo->ID) ?>">
                                        <?php echo $userInfo->name; ?>
                                    </a>
                                </span>
                                <br/>
                                <span class="single-line-address"><small><?php echo Modules::run('user/get_full_address', $userInfo->ID); ?></small></span>
                            </td>
                        </tr>
                        <tr>
                            <td>Looking for: <span class="bold-uppercase-highlight"><?php echo $row->looking_for; ?></span></td>
                        </tr>
                        <tr>
                            <td>Certified by: <span class="bold-uppercase"><span style="font-size:10px;"><?php echo $row->affiliation; ?></span></span></td>
                        </tr>
                        <tr>
                            <td>Total Fee: <span class="bold-uppercase"><?php echo $row->tuition_fee_range; ?> Lakh</span> </td>
                        </tr>
                        <tr>
                            <td>Group Discount: <span class="bold-uppercase"><?php echo $row->group_discount; ?></span></td>
                        </tr>
                        <tr>
                            <td>Shift: <span class="bold-uppercase"><?php echo $row->shift; ?></span></td>
                        </tr>
                        <tr>
                            <td>Audio/Video Class: <span class="bold-uppercase"><?php echo $row->audio_visual_class; ?></span></td>
                        </tr>
                        <tr>
                            <td>Scholarships Upto(%): <span class="bold-uppercase"><?php echo $row->grade_accepted; ?></span></td>
                        </tr>
                        <tr>
                            <td>Starting Date: <span class="bold-uppercase"><?php echo convert_date($row->starting_date); ?></span></td>
                        </tr>
                        <tr>
                            <td>Class Duration: <span class="bold-uppercase"><?php echo $row->class_duration; ?> Days</span></td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-12">
                    <div class="student-box-message multi-lines-fixed-height">
                        <?php echo get_limited_words($row->message, 160); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
}
?>