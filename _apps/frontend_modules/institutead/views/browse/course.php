<?php
$type = 'Institute';
if (isset($row) && !empty($row)) {
    $userInfo = Modules::run('user/get_row', $row->parent_ID);
    $studentInfo = Modules::run('student/get_row_by_parent', $userInfo->ID);
    ?>
    <?php
    if ($snc == 1) {
        ?>
        <tr>
            <th>Course offered</th>
            <th colspan="2">Institute</th>
            <th>Affiliation</th>
            <th>Grade accepted</th>
            <th>Course fee</th>
        </tr>
        <?php
    }
    ?>
    <tr>
        <td>
            <span class="bold-uppercase">
                <a href="<?php echo get_ads_permalink($row->ID, $userInfo->ID); ?>" data-toggle="tooltip" data-placement="top" title="Click to view ad">
                    <?php echo $row->looking_for; ?>
                </a>
            </span>
        </td>
        <td>
            <?php
            $profile_image = Modules::run('imagehub/get_img_size', $userInfo->profile_picture, 'thumb50x50');
            ?>
            <a href="<?php echo profile_url($userInfo->ID) ?>">
                <?php
                echo $profile_image;
                ?>
            </a>
        </td>
        <td>
            <span class="bold-uppercase-blue">
                <a href="<?php echo profile_url($userInfo->ID) ?>">
                    <?php echo $userInfo->name; ?>
                </a>
            </span>
            <br/>
            <span class="text-uppercase single-line-address"><?php echo $userInfo->address; ?></span>
        </td>
        <td><span class="bold-uppercase"><?php echo $row->affiliation; ?></span></td>
        <td><span class="bold-uppercase"><?php echo $row->grade_accepted; ?></span></td>
        <td><span class="bold-uppercase"><?php echo $row->tuition_fee_range; ?></span></td>
    </tr>
    <?php
}
?>