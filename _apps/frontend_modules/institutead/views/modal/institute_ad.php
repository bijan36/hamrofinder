<!--intersted person modal-->
<div class="modal fade modal-wide" id="which-ad-modal-Institute" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-check-square-o fa-fw"></i> Ads Summary</h4>
            </div>
            <div class="modal-body intersted-list-Institute">
            </div>
            
            <div class="modal-footer">
                <a href="" class="pull-left btn btn-primary" id="adURL-Institute">View Details</a>
                <button type="button" class="btn yelloButton" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>