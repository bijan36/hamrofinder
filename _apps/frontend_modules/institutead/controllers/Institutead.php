<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Institutead extends Frontendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('Institutead_m', 'finstitutead');
    }

    //USER DASHBOARD
    public function index() {
        redirect('site');
        exit();
    }

    //SHOWING ADS/ AD DETAILS PAGE
    public function ads($ID) {
        if (isset($ID)) {

            if (is_user_login()) {
                $adRow = $this->finstitutead->get_by(array('ID' => $ID, 'status' => ACTIVE));
                if ($adRow) {
                    $userID = $adRow->parent_ID;
                    //CAN THIS AD BE SHOWN CHEKED HERE
                    if (can_show_ad($ID, $userID)) {
                        $CurrentUserType = $this->session->userdata('userType');
                        $CurrentUserID = $this->session->userdata('userID');
                        //only STUDENT can view the ads
                        if ($CurrentUserType == 'Student' || $CurrentUserID == $userID) {
                            $userRow = Modules::run('user/get_row', $userID);
                            $data['adRow'] = $adRow;
                            $data['userRow'] = $userRow;
                            $this->template->load(get_template('Institute-ads'), 'details', $data);
                        } else {
                            redirect('notfound/invalid');
                            exit();
                        }
                    } else {
                        redirect('notfound/invalid');
                        exit();
                    }
                } else {
                    redirect('notfound');
                    exit();
                }
            } else {
                redirect('login');
                exit();
            }
        } else {
            redirect('notfound');
            exit();
        }
    }

    public function search_ad_by_address_extended($addressArray, $gender) {
        $resArray = array();
        //GET THE ALL USER IDS BY ADDRESS
        $userIDsByAddress = Modules::run('user/get_institute_by_address_extended', $addressArray);

        if (!empty($userIDsByAddress)) {
            foreach ($userIDsByAddress as $parent_ID) {
                $arg = array(
                    'parent_ID' => $parent_ID,
                    'status' => ACTIVE
                );
                $ads = $this->finstitutead->get_many_by($arg);
                if ($ads) {
                    foreach ($ads as $ad):
                        if (can_show_ad($ad->ID, $ad->parent_ID)) {
                            //FILTER BY GENDER HERE
                            if ($gender == 'Both') {
                                $resArray[] = $ad->ID;
                            } else {
                                $studentMeta = Modules::run('institute/get_row_by_parent', $ad->parent_ID);
                                if ($studentMeta) {
                                    if ($studentMeta->gender == $gender) {
                                        $resArray[] = $ad->ID;
                                    }
                                }
                            }
                        }
                    endforeach;
                }
            }
        }
        return $resArray;
    }

    //GET SAFE DATA BY USER ID
    public function get_row($ID) {
        $userRow = $this->finstitutead->get($ID);
        return $userRow ? $userRow : false;
    }

    //GET SAFE DATA BY USER PARENT
    public function get_row_by_parent($parent_ID) {
        $userRow = $this->finstitutead->get_by(array('parent_ID' => $parent_ID));
        return $userRow ? $userRow : false;
    }

    //GET SAFE DATA BY USER PARENT
    public function get_rows_by_parent($parent_ID) {
        $userRow = $this->finstitutead->get_many_by(array('parent_ID' => $parent_ID));
        return $userRow ? $userRow : false;
    }

    //GET SAFE DATA BY COLLEGE ADS
    public function get_rows_order($field1, $field2, $where) {
        $userRows = $this->finstitutead->order_by($field1, $field2)->get_many_by($where);
        $res = array();
        if ($userRows) {
            foreach ($userRows as $row):
                $res[] = $row;
            endforeach;
            return $res;
        } else {
            return false;
        }
    }

    //WHICH ADS
    public function which_ads() {
        $adID = $this->input->post('id');
        $parts = explode('-', $adID);
        $adID = $parts[0];
        $adType = $parts[1];
        if (isset($adID) && !empty($adID) && $adID > 0) {

            $row = $this->finstitutead->get($adID);
            if ($row) {

                $list = '';
                $data['row'] = $row;
                $list .= $this->load->view('institutead/loop_institute_common_details', $data, true);

                //finding he ad url
                $adURL = get_ads_permalink($adID, $row->parent_ID);

                $data = array(
                    'status' => 'success',
                    'type' => $adType,
                    'adURL' => $adURL,
                    'intersted_list' => $list
                );
                echo json_encode($data);
                exit();
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => 'Not allowed.',
                );
                echo json_encode($data);
                exit();
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => 'Your are not allowed to view this list.',
            );
            echo json_encode($data);
            exit();
        }
    }

    //GET LOGIN USER'S AD
    public function get_my_ads() {
        if (is_user_login()) {
            if (is_user_institute()) {
                $ads = $this->finstitutead->get_many_by(array('parent_ID' => $this->session->userdata('userID')));
                if ($ads) {
                    return $ads;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    //COUNT THE TOTAL AD OF SESSION LOGIN
    public function get_total_my_ads() {
        return $this->finstitutead->count_by(array('parent_ID' => $this->session->userdata('userID')));
    }

    //FIND THE REMAING POST ADS HERE
    public function can_submit_ad() {
        $totalAllowedAds = Modules::run('ipn/get_total_ads');
        $totalAds = $this->finstitutead->count_by(array('parent_ID' => $this->session->userdata('userID')));
        if ($totalAds >= $totalAllowedAds) {
            return false;
        } else {
            return true;
        }
    }

    //ADDING NEW AD
    public function add() {

        //CHECK SESSION IS ON
        if (!is_user_login()) {
            $data = array(
                'status' => 'error',
                'msg' => 'You are not authorised to add this ad'
            );
            echo json_encode($data);
            exit();
        }
        //CONFIRM THIS SESSION IS STUDENT
        if (is_user_institute() == false) {
            $data = array(
                'status' => 'error',
                'msg' => 'You are not authorised to add this ad'
            );
            echo json_encode($data);
            exit();
        }

        //CONFIRM THE INSTITUTE CAN POST THE AD
        $ability = is_user_institute_able();
        if ($ability['status'] == 'error') {
            echo json_encode($ability);
            exit();
        }

        //CHECK THE USER HAVE GOOD ENOUGH ADS COUNTS FOR POSTING AD OR NOT
        if (!$this->can_submit_ad()) {
            $errArray = array(
                'status' => 'error',
                'msg' => 'Your ad placement limit is finished. Please, choose a suitable payment plan to place more ads.'
            );
            echo json_encode($errArray);
            exit();
        }

        //CONFIRM COLLEGE ALREADY SET THE PROFILE
        $metaRow = Modules::run('institute/get_row_by_parent', $this->session->userdata('userID'));
        if (!$metaRow) {
            $data = array(
                'status' => 'error',
                'msg' => 'Please complete your profile before posting an ad. <a href="' . site_url("dashboard") . '">Completer profile now.</a>'
            );
            echo json_encode($data);
            exit();
        } else {
            //IF ROW BUT CHECK THE REQUIRED FIELDS
            if (empty($metaRow->country)) {
                $data = array(
                    'status' => 'error',
                    'msg' => 'Please complete your profile before posting an ad. <a href="' . site_url("dashboard") . '">Completer profile now.</a>'
                );
                echo json_encode($data);
                exit();
            }
        }


        //FROM VALIDATION
        $this->form_validation->set_rules('looking_for', 'Looking for', 'required|trim');
        $this->form_validation->set_rules('affiliation', 'Affiliation', 'required|trim');
        $this->form_validation->set_rules('class_duration', 'Class duration', 'required|trim');
        $this->form_validation->set_rules('grade_accepted', 'Scholarships Upto %', 'required|trim');
        $this->form_validation->set_rules('group_discount', 'Group Discount', 'required|trim');
        $this->form_validation->set_rules('tuition_fee_range', 'Total fee range', 'required|trim');
        $this->form_validation->set_rules('shift', 'shift', 'required|trim');
        $this->form_validation->set_rules('starting_date', 'starting date', 'required|trim');

        if ($this->form_validation->run() == TRUE) {

            $looking_for = $this->input->post('looking_for');
            $affiliation_for = $this->input->post('affiliation');

            //CHECK THE LOOKING FOR and AFFILIATION ALREADY SUBMITTED IN DATA BASE BY SAME INSTITUTE OR NOT IF YES PREVIENT IT
            $dub_row = $this->finstitutead->get_by(array('parent_ID' => get_institute_ID(), 'looking_for' => $looking_for, 'affiliation' => $affiliation_for));
            if ($dub_row) {
                $data = array(
                    'status' => 'error',
                    'msg' => 'You have already created the same looking for and certified by ad. Sorry dublicate ad is not allowed.'
                );
                echo json_encode($data);
                exit();
            }

            $affiliation = $this->input->post('affiliation');
            $class_duration = $this->input->post('class_duration');
            $grade_accepted = $this->input->post('grade_accepted');
            $group_discount = $this->input->post('group_discount');
            $tuition_fee_range = $this->input->post('tuition_fee_range');
            $audio_visual_class = $this->input->post('audio_visual_class');
            $shift = $this->input->post('shift');
            $starting_date = convert_date($this->input->post('starting_date'));
            $message = $this->input->post('student_add_message');
            $ad_submit_date = get_date_time();
            $ad_submit_ip = $this->input->ip_address();

            $dataIns = array(
                'parent_ID' => get_institute_ID(),
                'class_duration' => $class_duration,
                'looking_for' => $looking_for,
                'affiliation' => $affiliation,
                'grade_accepted' => $grade_accepted,
                'audio_visual_class' => $audio_visual_class,
                'group_discount' => $group_discount,
                'tuition_fee_range' => $tuition_fee_range,
                'shift' => $shift,
                'starting_date' => $starting_date,
                'message' => $message,
                'ad_submit_date' => $ad_submit_date,
                'ad_submit_ip' => $ad_submit_ip
            );

            //INSERT HERE
            if ($this->finstitutead->insert($dataIns)) {

                $adID = $this->finstitutead->get_last_id();

                //LETS EMAIL TO ADMIN
                $data['to'] = admin_notification_email();
                $data['toName'] = admin_notification_email_name();
                $data['subject'] = 'Institute ad waiting for approval.';
                $data['template'] = 'institute_college_ad_notification';
                $data['others'] = array(
                    'userfullname' => $this->session->userdata('userName'),
                    'emailaddress' => $this->session->userdata('userEmail'),
                    'adID' => $adID
                );
                Modules::run('emailsystem/doEmail', $data);
                //LETS DO EMAIL TO ADMIN
                //LETS EMAIL TO INSTITIUTE
                $data['to'] = $this->session->userdata('userEmail');
                $data['toName'] = $this->session->userdata('userName');
                $data['from'] = admin_notification_email();
                $data['fromName'] = admin_notification_email_name();
                $data['subject'] = 'Ad has been submitted successfully and waiting for approval.';
                $data['template'] = 'institute_institute_ad_notification';
                $data['others'] = array(
                    'userfullname' => $this->session->userdata('userName'),
                    'emailaddress' => $this->session->userdata('userEmail'),
                    'adID' => $adID
                );
                Modules::run('emailsystem/doEmail', $data);
                //LETS DO EMAIL TO INSTITIUTE

                $data = array(
                    'status' => 'success',
                    'msg' => 'Your ad has been submitted. Once it is verified, you will be notified by email.'
                );
                echo json_encode($data);
                exit();
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => 'Unable to process your ad ' . COMMONERROR
                );
                echo json_encode($data);
                exit();
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => validation_errors()
            );
            echo json_encode($data);
            exit();
        }
    }

    //REMOVING INSTITUTE AD
    public function remove_college_ad() {
        $adID = $this->input->post('id');
        $adRow = $this->finstitutead->get($adID);
        if ($adRow) {
            if ($adRow->parent_ID == get_institute_ID()) {
                if ($this->finstitutead->update($adID, array('status' => 'Deleted'))) {
                    $data = array(
                        'status' => 'success',
                        'msg' => 'Ad has been removed successfully.'
                    );
                    echo json_encode($data);
                    exit();
                } else {
                    $data = array(
                        'status' => 'error',
                        'msg' => 'Unable to remove your ad ' . COMMONERROR
                    );
                    echo json_encode($data);
                    exit();
                }
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => 'You are not authorised to perform such action ' . COMMONERROR
                );
                echo json_encode($data);
                exit();
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => 'Unable to remove your ad ' . COMMONERROR
            );
            echo json_encode($data);
            exit();
        }
    }

    public function get_feature_institute_ads_random() {
        $rows = $this->finstitutead->order_by('ID', 'Random')->get_many_by(array('status' => ACTIVE, 'feature' => 'Yes'));

        if ($rows) {
            $qualified = array();
            foreach ($rows as $row):
                //CHECK THE USER IS VALID OR NOT
                if (can_show_ad($row->ID, $row->parent_ID)) {
                    $qualified[] = $row;
                }
            endforeach;
            return $qualified;
        } else {
            return false;
        }
    }

    //IS BELONGS TO
    public function is_belongs_to($adID, $userID) {
        $row = $this->finstitutead->get_by(array('ID' => $adID, 'parent_ID' => $userID));
        if ($row) {
            return true;
        } else {
            return false;
        }
    }

    //GET AD STATUS
    public function get_ad_status_label($ID) {
        if (isset($ID) && !empty($ID)) {
            //get ad row
            $adRow = $this->finstitutead->get($ID);
            if ($adRow) {
                if ($adRow->ad_end_date < today_date()) {
                    if ($adRow->ad_start_date == 0 && $adRow->ad_start_date == 0) {
                        echo '<span class="expired-span">Review</span>';
                    } else {
                        echo '<span class="expired-span">Expired</span>';
                    }
                } elseif ($adRow->status == 'Active') {
                    echo '<span class="active-span">Active</span>';
                } else {
                    echo '<span class="inactive-span">Inactive</span>';
                }
            } else {
                echo '<span class="unknown-span">Unknown</span>';
            }
        } else {
            echo '<span class="unknown-span">Unknown</span>';
        }
    }

    public function get_course_list($selected = null) {
        $allCourse = $this->finstitutead->get_many_by(array('status' => ACTIVE));
        $output = '<option value="">Select One</option>';
        if ($allCourse) {

            $ads = array();
            foreach ($allCourse as $st):
                $ads[] = $st->looking_for;
            endforeach;

            $ads = array_unique($ads);

            sort($ads);

            foreach ($ads as $ro):
                if ($selected) {
                    $sel = $selected == $ro ? 'selected' : '';
                    $output .= '<option value="' . trim($ro) . '" ' . $sel . '>' . $ro . '</option>';
                } else {
                    $output .= '<option value="' . trim($ro) . '">' . $ro . '</option>';
                }
            endforeach;
        } else {
            $output .= '<option value="0">Not Found!</option>';
        }
        return $output;
    }

    //AFFILIATION
    public function get_affiliation_list($selected = null) {
        $allCourse = $this->finstitutead->get_many_by(array('status' => ACTIVE));
        $output = '<option value="">Select One</option>';
        if ($allCourse) {

            $ads = array();
            foreach ($allCourse as $st):
                $ads[] = $st->affiliation;
            endforeach;

            $ads = array_unique($ads);

            sort($ads);

            foreach ($ads as $ro):
                if ($selected) {
                    $sel = $selected == $ro ? 'selected' : '';
                    $output .= '<option value="' . trim($ro) . '" ' . $sel . '>' . $ro . '</option>';
                } else {
                    $output .= '<option value="' . trim($ro) . '">' . $ro . '</option>';
                }
            endforeach;
        } else {
            $output .= '<option value="0">Not Found!</option>';
        }
        return $output;
    }

    //FREE
    public function get_fee_list($selected = null) {
        $allCourse = $this->finstitutead->get_many_by(array('status' => ACTIVE));
        $output = '<option value="">Select One</option>';
        if ($allCourse) {

            $ads = array();
            foreach ($allCourse as $st):
                $ads[] = $st->tuition_fee_range;
            endforeach;

            $ads = array_unique($ads);

            sort($ads);

            foreach ($ads as $ro):
                if ($selected) {
                    $sel = $selected == $ro ? 'selected' : '';
                    $output .= '<option value="' . trim($ro) . '" ' . $sel . '>' . $ro . '</option>';
                } else {
                    $output .= '<option value="' . trim($ro) . '">' . $ro . '</option>';
                }
            endforeach;
        } else {
            $output .= '<option value="0">Not Found!</option>';
        }
        return $output;
    }

    //GET institute ADS IDS BY TUTION FEE RANGE
    public function search_ad_by_fee_range($fee_range) {

        $resArray = array();
        $arg = array(
            'tuition_fee_range' => $fee_range,
            'status' => ACTIVE
        );
        $ads = $this->finstitutead->get_many_by($arg);
        if ($ads) {
            foreach ($ads as $ad):
                if (can_show_ad($ad->ID, $ad->parent_ID)) {
                    $resArray[] = $ad->ID;
                }
            endforeach;
        }
        return $resArray;
    }

    //SEARCH ADS BY COURSE
    public function search_ad_by_course($course) {
        $resArray = array();
        $arg = array(
            'looking_for' => $course,
            'status' => ACTIVE
        );
        $ads = $this->finstitutead->get_many_by($arg);
        if ($ads) {
            foreach ($ads as $ad):
                if (can_show_ad($ad->ID, $ad->parent_ID)) {
                    $resArray[] = $ad->ID;
                }
            endforeach;
        }
        return $resArray;
    }

    //SEARCH ADS BY AFFILIATION
    public function search_ad_by_affiliation($affiliation) {
        $resArray = array();
        $arg = array(
            'affiliation' => $affiliation,
            'status' => ACTIVE
        );
        $ads = $this->finstitutead->get_many_by($arg);
        if ($ads) {
            foreach ($ads as $ad):
                if (can_show_ad($ad->ID, $ad->parent_ID)) {
                    $resArray[] = $ad->ID;
                }
            endforeach;
        }
        return $resArray;
    }

    public function search_ad_by_audio_video($yesno) {
        $resArray = array();
        $arg = array(
            'audio_visual_class' => $yesno,
            'status' => ACTIVE
        );
        $ads = $this->finstitutead->get_many_by($arg);
        if ($ads) {
            foreach ($ads as $ad):
                if (can_show_ad($ad->ID, $ad->parent_ID)) {
                    $resArray[] = $ad->ID;
                }
            endforeach;
        }
        return $resArray;
    }
    public function search_ad_by_class_duration($duration) {
        $resArray = array();
        $arg = array(
            'class_duration' => $duration,
            'status' => ACTIVE
        );
        $ads = $this->finstitutead->get_many_by($arg);
        if ($ads) {
            foreach ($ads as $ad):
                if (can_show_ad($ad->ID, $ad->parent_ID)) {
                    $resArray[] = $ad->ID;
                }
            endforeach;
        }
        return $resArray;
    }
    
    public function search_ad_group_discount($yesno) {
        $resArray = array();
        $arg = array(
            'group_discount' => $yesno,
            'status' => ACTIVE
        );
        $ads = $this->finstitutead->get_many_by($arg);
        if ($ads) {
            foreach ($ads as $ad):
                if (can_show_ad($ad->ID, $ad->parent_ID)) {
                    $resArray[] = $ad->ID;
                }
            endforeach;
        }
        return $resArray;
    }

    //SEARCH ADS BY AFFILIATION
    public function search_ad_by_admission_within($admission_within) {
        $resArray = array();
        $arg = array(
            'admission_within' => $admission_within,
            'status' => ACTIVE
        );
        $ads = $this->finstitutead->get_many_by($arg);
        if ($ads) {
            foreach ($ads as $ad):
                if (can_show_ad($ad->ID, $ad->parent_ID)) {
                    $resArray[] = $ad->ID;
                }
            endforeach;
        }
        return $resArray;
    }

    //SEARCH ADS BY ACCETPTED GRADE
    public function search_ad_by_accepted_grade($accepted_grade) {
        $resArray = array();
        $arg = array(
            'grade_accepted' => $accepted_grade,
            'status' => ACTIVE
        );
        $ads = $this->finstitutead->get_many_by($arg);
        if ($ads) {
            foreach ($ads as $ad):
                if (can_show_ad($ad->ID, $ad->parent_ID)) {
                    $resArray[] = $ad->ID;
                }
            endforeach;
        }
        return $resArray;
    }

    //SEARCH ADS BY SHIFT
    public function search_ad_by_shift($shift) {
        $resArray = array();
        $arg = array(
            'shift' => $shift,
            'status' => ACTIVE
        );
        $ads = $this->finstitutead->get_many_by($arg);
        if ($ads) {
            foreach ($ads as $ad):
                if (can_show_ad($ad->ID, $ad->parent_ID)) {
                    $resArray[] = $ad->ID;
                }
            endforeach;
        }
        return $resArray;
    }

    //SEARCH ADS BY STARTING DATE
    public function search_ad_by_starting_date($starting_date) {
        $resArray = array();
        $arg = array(
            'starting_date' => $starting_date,
            'status' => ACTIVE
        );
        $ads = $this->finstitutead->get_many_by($arg);
        if ($ads) {
            foreach ($ads as $ad):
                if (can_show_ad($ad->ID, $ad->parent_ID)) {
                    $resArray[] = $ad->ID;
                }
            endforeach;
        }
        return $resArray;
    }

    //SEARCH ADS BY ENTRANCE REQUIRED
    public function search_ad_by_entrance_required($entrance_required) {
        $resArray = array();
        $arg = array(
            'entrance_required' => $entrance_required,
            'status' => ACTIVE
        );
        $ads = $this->finstitutead->get_many_by($arg);
        if ($ads) {
            foreach ($ads as $ad):
                if (can_show_ad($ad->ID, $ad->parent_ID)) {
                    $resArray[] = $ad->ID;
                }
            endforeach;
        }
        return $resArray;
    }

    //SEARCH ADS BY COLLEGE BUS
    public function search_ad_by_bus($college_bus) {
        $resArray = array();
        $arg = array(
            'college_bus' => $college_bus,
            'status' => ACTIVE
        );
        $ads = $this->finstitutead->get_many_by($arg);
        if ($ads) {
            foreach ($ads as $ad):
                if (can_show_ad($ad->ID, $ad->parent_ID)) {
                    $resArray[] = $ad->ID;
                }
            endforeach;
        }
        return $resArray;
    }

    //GET PARENT ID OF A ROW 
    public function get_parent_by_id($ID) {
        $row = $this->finstitutead->get($ID);
        if ($row) {
            return $row->parent_ID;
        } else {
            return false;
        }
    }

}
