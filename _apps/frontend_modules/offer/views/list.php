<?php
if (!empty($row)) {
    ?>
    <div class="offer-box">
        <div class="row">
            <div class="col-md-1">

                <?php
                $collegeRow = Modules::run('user/get_row', $row->college_ID);
                ?>
                <a href="<?php echo profile_url($collegeRow->ID) ?>" title="<?php echo $collegeRow->name; ?>">
                    <?php
                    $proImgID = $collegeRow->profile_picture ? $collegeRow->profile_picture : '0';
                    $profile_image = Modules::run('imagehub/get_profile_image', $proImgID);
                    echo $profile_image;
                    ?>
                </a>
            </div>
            <div class="col-md-11">
                <a href="<?php echo profile_url($collegeRow->ID) ?>" title="<?php echo $collegeRow->name; ?>">
                    <?php echo $collegeRow->name; ?>
                </a>
                <br/>
                <small><?php echo $collegeRow->address; ?></small>
            </div>
            <div class="col-md-12">
                <?php echo $row->message; ?>
            </div>
        </div>

    </div>
    <?php
}
?>