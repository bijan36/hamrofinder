<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Offer extends Frontendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('offer_m', 'foffer');
    }

    //USER DASHBOARD
    public function index() {
        redirect('site');
        exit();
    }

    //ADDING INTEREST
    public function validate_offer() {
        $adID = $this->input->post('id');
        $parts = explode('-', $adID);
        $adID = $parts[0];
        $studentID = $parts[1];
        $adType = $parts[2];
        //CHECK USER LOGIN
        if (is_user_login()) {
            $userID = get_user_ID();

            //FIND THE USER TYPE
            $userType = $this->session->userdata('userType');
            $userLevel = $this->session->userdata('userLevel');
            
            
            //LEVEL ATHONTICATION FOR THE SHOW AN INTEREST ACTION FOR USERS
            if ($userType == 'College') {
                $interestAuth = Modules::run('college_membership_plans/find_authorise', $userLevel, 'offer');
                if ($interestAuth == 'No') {
                    $data = array(
                        'status' => 'error',
                        'msg' => INVALIDMSG,
                        'do' => 'sametype'
                    );
                    echo json_encode($data);
                    exit();
                }
            }

            if ($userType == 'Institute') {
                $interestAuth = Modules::run('institute_membership_plans/find_authorise', $userLevel, 'offer');
                if ($interestAuth == 'No') {
                    $data = array(
                        'status' => 'error',
                        'msg' => INVALIDMSG,
                        'do' => 'sametype'
                    );
                    echo json_encode($data);
                    exit();
                }
            }
            //LEVEL ATHONTICATION FOR THE SHOW AN INTEREST ACTION FOR USERS
            
            

            //IF THE USER TYPE IS MATCHED NOT ALLOW TO INTEREST THEM
            if ($userType == $adType) {
                $data = array(
                    'status' => 'error',
                    'msg' => INVALIDMSG,
                    'do' => 'sametype'
                );
                echo json_encode($data);
                exit();
            } else {
                $userRow = Modules::run('user/get_row_where', array('ID' => $studentID, 'type' => $adType, 'status' => ACTIVE));
                if ($userRow) {
                    $data = array(
                        'status' => 'success',
                        'toInterest' => $userRow->name,
                        'adID' => $adID
                    );
                    echo json_encode($data);
                    exit();
                } else {
                    $data = array(
                        'status' => 'error',
                        'msg' => INVALIDMSG,
                        'do' => 'nothing'
                    );
                    echo json_encode($data);
                    exit();
                }
            }
        } else {
            //REDIRECT TO LOGIN PAGE
            $data = array(
                'status' => 'error',
                'msg' => 'To make offer,  please register or login.',
                'do' => 'login'
            );
            echo json_encode($data);
            exit();
        }
    }

    //MAKE OFFER STUFF WITH EMAILS NOTIFICTIONS
    public function make_offer() {
        $adID = (int) $this->input->post('adID');
        $message = $this->input->post('message');

        if (isset($adID) && !empty($adID) && $adID > 0) {
            if (isset($message) && !empty($message)) {

                $adRow = Modules::run('studentad/get_row', $adID);

                //IF AD ROW
                if ($adRow && $adRow->status == ACTIVE) {

                    $userRow = Modules::run('user/get_row', $adRow->parent_ID);
                    //SAVAING OFFER TO DATABASE
                    $dataSave = array(
                        'ad_ID' => $adID,
                        'college_ID' => $this->session->userdata('userID'),
                        'student_ID' => $adRow->parent_ID,
                        'message' => $message,
                        'ipaddress' => $this->input->ip_address(),
                        'timedate' => get_date_time()
                    );

                    if ($this->foffer->insert($dataSave)) {
                        $collegeName = $this->session->userdata('userName');
                        $collegeEmail = $this->session->userdata('userEmail');
                        //EMAIL NOTIFICTION TO STUDENT
                        $data['to'] = $userRow->email;
                        $data['toName'] = $userRow->name;
                        $data['subject'] = $collegeName . ' has offer you.';
                        $data['template'] = 'student_offer_notification';
                        $data['others'] = array(
                            'userfullname' => $userRow->name,
                            'emailaddress' => $userRow->email,
                            'offermsg' => $message,
                            'collegename' => $collegeName
                        );
                        Modules::run('emailsystem/doEmail', $data);

                        //EMAIL NOTIFICTION TO WEBSITE ADMINISTRATOR
                        $dataa['to'] = admin_notification_email();
                        $dataa['toName'] = admin_notification_email_name();
                        $dataa['subject'] = $collegeName . ' made offer to ' . $userRow->name;
                        $dataa['template'] = 'admin_offer_notification';
                        $dataa['others'] = array(
                            'studentName' => $userRow->name,
                            'studentEmail' => $userRow->email,
                            'collegeName' => $collegeName,
                            'collegeEmail' => $collegeEmail,
                            'offermsg' => $message,
                            'adID' => $adID
                        );
                        Modules::run('emailsystem/doEmail', $dataa);
                        //EMAIL NOTIFICTION TO COLLEGE
                        $datac['to'] = $collegeEmail;
                        $datac['toName'] = $collegeName;
                        $datac['subject'] = 'You offer has been sent to student (' . $userRow->name . ') successfully.';
                        $datac['template'] = 'college_offer_notification';
                        $datac['others'] = array(
                            'studentName' => $userRow->name,
                            'studentEmail' => $userRow->email,
                            'collegeName' => $collegeName,
                            'collegeEmail' => $collegeEmail,
                            'offermsg' => $message
                        );
                        Modules::run('emailsystem/doEmail', $datac);

                        $data = array(
                            'status' => 'success',
                            'msg' => 'Your offer message has been sent to student. Thank you.',
                        );
                        echo json_encode($data);
                        exit();
                    } else {
                        $data = array(
                            'status' => 'error',
                            'msg' => 'Sorry unable to post your offer message right now.',
                        );
                        echo json_encode($data);
                        exit();
                    }
                } else {
                    $data = array(
                        'status' => 'error',
                        'msg' => 'You are unable to offer right now.',
                    );
                    echo json_encode($data);
                    exit();
                }
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => 'Please type your offer message and try submiting again.',
                );
                echo json_encode($data);
                exit();
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => 'Your are not allowed to offer.',
            );
            echo json_encode($data);
            exit();
        }
    }

    //GET OFFERS LIST BY AD ID
    public function get_offer_list_by_adID($adID) {
        $rows = $this->foffer->get_many_by(array('ad_ID' => $adID));
        return $rows ? $rows : false;
    }

    //GETTING TOTAL BY AD
    public function get_total_by_ad($adID, $type) {
        return $this->foffer->count_by(array('ad_ID' => $adID, 'type' => $type));
    }

    //GET INTERESTED ROWS OF A COLLEGE OR INSTITUTE
    public function get_offers_rows_by_college($collegeID) {
        $rows = $this->foffer->order_by('ID', 'DESC')->get_many_by(array('college_ID' => $collegeID));
        if ($rows) {
            $rows;
        } else {
            return false;
        }
        return $rows ? $rows : false;
    }
    //GET INTERESTED ROWS OF A COLLEGE OR INSTITUTE
    public function get_offers_rows_by_institute($instituteID) {
        $rows = $this->foffer->order_by('ID', 'DESC')->get_many_by(array('college_ID' => $instituteID));
        if ($rows) {
            $rows;
        } else {
            return false;
        }
        return $rows ? $rows : false;
    }

    //GET OFFER ROWS BY STUDENT ID
    public function get_offers_rows_by_student($studentID) {
        $rows = $this->foffer->order_by('ID', 'DESC')->get_many_by(array('student_ID' => $studentID));
        if ($rows) {
            $rows;
        } else {
            return false;
        }
        return $rows ? $rows : false;
    }

    //GET ROWS BY USER
    public function get_rows_by_user($userID) {
        $rows = $this->foffer->get_many_by(array('interest_ID' => $userID));
        return $rows ? $rows : false;
    }

}
