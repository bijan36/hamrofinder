<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends Frontendcontroller {

    public function __construct() {
        parent::__construct();
        if (is_user_login()) {
            redirect('dashboard');
            exit();
        }
    }

    //DEFAULT
    public function index() {
        $this->template->load(get_template('default'), 'login');
    }

}
