<div class="row">
    <div class="col-md-6">
        <div class="login-cover  wow bounceInLeft">
            <div class="login-cover-images">
                <img src="<?php echo base_url() ?>_public/front/assets/img/login-cover.jpg" class="img-responsive">
            </div>
            <div class="login-cover-title">
                <h1>Select your college / students / institutes.</h1>
                <p>
                    Subscribe us to enhance your study and business.
                </p>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div id="loginBox" class="standard-padding wow pulse">
            <h1><i class="fa fa-sign-in fa-fw"></i> User Login</h1>
            <div class="row">

                <?php echo form_open('user/logintry', array('id' => 'userLogin')); ?>
                <?php //print_r($this->session->all_userdata());?>
                <div class="col-md-12">
                    <div class="round-border">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="showMsg"></div>
                                <div class="form-group">
                                    <label for="emailAddress">Email Address</label>
                                    <input type="email" name="emailAddress" class="form-control" value="">
                                </div>
                                <div class="form-group">
                                    <label for="passCode">Password</label>
                                    <input type="password" name="password" class="form-control" value="">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="blueButton">Sign In</button>
                                </div>
                                <div class="form-group" style="margin-top: 50px;">
                                    <a href="<?php echo site_url('recover') ?>"><span style="display: inline-block; font-weight: 700; padding: 2px 10px; background: #fdb316;">Forgot Username or Password.</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>

                <div class="col-md-12">
                    <span style="margin-top: 10px; display: block; font-size: 16px;">
                        <span style="display: inline-block; font-weight: 700; padding: 2px 10px; background: #fdb316;">Don’t have a hamrofinder account?  <a href="<?php echo site_url('register') ?>">Sign up now!</a></span>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>







