<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Videos extends Frontendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('Videos_m', 'fvideo');
    }

    //USER DASHBOARD
    public function index() {
        redirect('site');
        exit();
    }

    //ADDING THE MARKS
    public function add() {
        $video_code = $this->input->post('vid_code');
        if (!empty($video_code)) {
            $parent_ID = $this->session->userdata('userID');

            $userRow = Modules::run('user/get_row', $parent_ID);
            if ($userRow) {

                $dataArray = array(
                    'parent_ID' => $parent_ID,
                    'video_code' => $video_code
                );

                if ($this->fvideo->insert($dataArray)) {
                    $lastID = $this->fvideo->get_last_id();
                    $data = array(
                        'status' => 'success',
                        'msg' => 'Video has been updated.',
                        'vid_frame' => $this->load->view('videos/video_thumb', array('video_code' => $video_code, 'vid_id' => $lastID), true)
                    );
                    echo json_encode($data);
                    exit();
                } else {
                    $data = array(
                        'status' => 'error',
                        'msg' => 'Unable to update video profile.'
                    );
                    echo json_encode($data);
                    exit();
                }
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => 'User varification fails.'
                );
                echo json_encode($data);
                exit();
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => 'Missing video code field!'
            );
            echo json_encode($data);
            exit();
        }
    }

    public function remove() {
        $id = $this->input->post('id');
        if (!empty($id)) {
            $row = $this->fvideo->get($id);
            if ($row) {
                if ($row->parent_ID == $this->session->userdata('userID')) {
                    if ($this->fvideo->delete($id)) {
                        $data = array(
                            'status' => 'success',
                            'msg' => 'Video has been removed successfully',
                        );
                        echo json_encode($data);
                        exit();
                    } else {
                        $data = array(
                            'status' => 'error',
                            'msg' => 'Unable to remove it!'
                        );
                        echo json_encode($data);
                        exit();
                    }
                } else {
                    $data = array(
                        'status' => 'error',
                        'msg' => 'You are not authorised to remove it!'
                    );
                    echo json_encode($data);
                    exit();
                }
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => 'Nothing found to remove!!'
                );
                echo json_encode($data);
                exit();
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => 'Unable to remove it'
            );
            echo json_encode($data);
            exit();
        }
    }

    //GET SAFE DATA BY USER ID
    public function get_row($ID) {
        $userRow = $this->fvideo->get($ID);
        return $userRow ? $userRow : false;
    }

    public function get_videos($parent_ID) {
        $userRow = $this->fvideo->get_many_by(array('parent_ID' => $parent_ID));
        return $userRow ? $userRow : false;
    }

    //GET SAFE DATA BY USER PARENT
    public function get_by_parent($parent_ID) {
        $userRows = $this->fvideo->get_many_by(array('parent_ID' => $parent_ID, 'level' => $level));
        return $userRows ? $userRows : false;
    }

}
