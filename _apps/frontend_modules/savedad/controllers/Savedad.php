<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Savedad extends Frontendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('Savedad_m', 'fsavedad');
    }

    //USER DASHBOARD
    public function index() {
        redirect('site');
        exit();
    }

    //ADDING INTEREST
    public function do_save() {
        $adID = $this->input->post('id');
        $parts = explode('-', $adID);
        $adID = $parts[0];
        $adType = $parts[1];
        //CHECK USER LOGIN
        if (is_user_login()) {
            $userID = get_user_ID();

            //FIND THE USER TYPE
            $userType = $this->session->userdata('userType');
            $userLevel = $this->session->userdata('userLevel');

            //LEVEL ATHONTICATION FOR THE SHOW AN INTEREST ACTION FOR USERS
            if ($userType == 'College') {
                $interestAuth = Modules::run('college_membership_plans/find_authorise', $userLevel, 'save');
                if ($interestAuth == 'No') {
                    $data = array(
                        'status' => 'error',
                        'msg' => INVALIDMSG,
                        'do' => 'sametype'
                    );
                    echo json_encode($data);
                    exit();
                }
            }

            if ($userType == 'Institute') {
                $interestAuth = Modules::run('institute_membership_plans/find_authorise', $userLevel, 'save');
                if ($interestAuth == 'No') {
                    $data = array(
                        'status' => 'error',
                        'msg' => INVALIDMSG,
                        'do' => 'sametype'
                    );
                    echo json_encode($data);
                    exit();
                }
            }
            //LEVEL ATHONTICATION FOR THE SHOW AN INTEREST ACTION FOR USERS
            //IF THE USER TYPE IS MATCHED NOT ALLOW TO INTEREST THEM
            if ($userType == $adType) {
                $data = array(
                    'status' => 'error',
                    'msg' => INVALIDMSG,
                    'do' => 'sametype'
                );
                echo json_encode($data);
                exit();
            }

            if ($userType == 'Institute') {
                if ($adType == 'College') {
                    $data = array(
                        'status' => 'error',
                        'msg' => INVALIDMSG,
                        'do' => 'sametype'
                    );
                    echo json_encode($data);
                    exit();
                }
            }

            if ($userType == 'College') {
                if ($adType == 'Institute') {
                    $data = array(
                        'status' => 'error',
                        'msg' => INVALIDMSG,
                        'do' => 'sametype'
                    );
                    echo json_encode($data);
                    exit();
                }
            }


            //FIND IF ALREADY DONE SAVED BEFORE
            $row = $this->fsavedad->get_by(array('ad_ID' => $adID, 'interest_ID' => $userID));
            if ($row) {
                $data = array(
                    'status' => 'error',
                    'msg' => 'You have already saved this Ad!',
                    'do' => 'nothing'
                );
                echo json_encode($data);
                exit();
            } else {
                $dataIns = array(
                    'ad_ID' => $adID,
                    'interest_ID' => $userID,
                    'date_time' => get_date_time(),
                    'ip_address' => $this->input->ip_address(),
                    'type' => $adType
                );
                if ($this->fsavedad->insert($dataIns)) {
                    $data = array(
                        'status' => 'success',
                        'msg' => 'The Ad has been saved.',
                        'total' => $this->get_total_by_ad($adID, $adType)
                    );
                    echo json_encode($data);
                    exit();
                } else {
                    $data = array(
                        'status' => 'error',
                        'msg' => 'Unable to save it right now, please try again later.',
                        'do' => 'nothing'
                    );
                    echo json_encode($data);
                    exit();
                }
            }
        } else {
            //REDIRECT TO LOGIN PAGE
            $data = array(
                'status' => 'error',
                'msg' => 'To save this ad, please register or login.',
                'do' => 'login'
            );
            echo json_encode($data);
            exit();
        }
    }

    public function remove_ad() {
        $id = $this->input->post('id');
        if (!empty($id)) {
            $row = $this->fsavedad->get($id);
            if ($row) {
                if ($this->fsavedad->delete($id)) {
                    $data = array(
                        'status' => 'success',
                    );
                    echo json_encode($data);
                    exit();
                } else {
                    $data = array(
                        'status' => 'error',
                        'msg' => 'Unable to remove it!'
                    );
                    echo json_encode($data);
                    exit();
                }
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => 'Nothing found to remove!!'
                );
                echo json_encode($data);
                exit();
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => 'Unable to remove it'
            );
            echo json_encode($data);
            exit();
        }
    }

    //GETTING TOTAL BY AD
    public function get_total_by_ad($adID, $type) {
        return $this->fsavedad->count_by(array('ad_ID' => $adID, 'type' => $type));
    }

    //GET ROWS BY USER
    public function get_rows_by_user($userID) {
        $rows = $this->fsavedad->get_many_by(array('interest_ID' => $userID));
        return $rows ? $rows : false;
    }

}
