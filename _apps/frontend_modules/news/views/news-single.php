<div class="content">
    <?php
    if (!empty($row)) {
        ?>
        <h1 class="new-big-title"><?php echo $row->title; ?></h1>
        <div class="feature_image">
            <?php
            if (!empty($row->feature_image)) {
                ?>
                <img src="<?php echo $row->feature_image; ?>" alt="<?php echo $row->title; ?>" class="img-responsive center-block">
                <div class="clearfix"></div>
                <?php
            }
            ?>
            <?php
            if (!empty($row->gallery1) || !empty($row->gallery2) || !empty($row->gallery3) || !empty($row->gallery4) || !empty($row->gallery5) || !empty($row->gallery6)) {
                ?>
                <div class="row no-gutter" style="margin-top: 5px;">
                    <?php
                    for ($i = 1; $i <= 6; $i++):
                        $ros = 'gallery' . $i;
                        if (!empty($row->$ros)) {
                            $imgThumb = Modules::run('imagehub/get_thumb_by_url', $row->$ros, 'thumb300x200');
                            ?>
                            <div class="col-md-2">
                                <a class="group1" href="<?php echo $row->$ros ?>" title="<?php echo $row->title; ?>">
                                    <img src="<?php echo $imgThumb ?>" class="img-responsive">
                                </a>
                            </div>
                            <?php
                        }
                    endfor;
                    ?>
                </div>
                <?php
            }
            ?>
        </div>
        <div class="news-content">
            <?php echo $row->content; ?>
        </div>
        <div class="share-box">
            Share on social media:
            <br/><br/>
            <!-- Go to www.addthis.com/dashboard to customize your tools -->
            <div class="addthis_inline_share_toolbox"></div>
        </div>
        <?php
    } else {
        echo 'No news & updates found!';
    }
    ?>
</div>