
<h1>News & Updates</h1>
<?php
if (!empty($alldata)) {
    ?>
    <div class="news-holder-group">
        <?php
        foreach ($alldata as $row):
            $blogUrl = site_url('news/' . $row->slug);
            $blogTitle = $row->title;
            ?>
            <?php
            if (!empty($row->feature_image)) {
                $feature_image = Modules::run('imagehub/get_thumb_by_url', $row->feature_image, 'thumb400x250');
            } else {
                $feature_image = base_url('_public/front/assets/img/sample-400-250.jpg');
            }
            ?>

                <div class="news-holder-grid">
                    <div class="news-thumb">
                        <a href="<?php echo $blogUrl; ?>" title="<?php echo $blogTitle; ?>">
                            <img src="<?php echo $feature_image; ?>" class="img-responsive">
                        </a>
                        <div class="news-icon-box">
                            <a href="<?php echo $blogUrl; ?>" title="<?php echo $blogTitle; ?>">
                                <i class="fa fa-external-link fa-fw"></i>
                            </a>
                        </div>
                    </div>
                    <div class="news-thum-title">
                        <a href="<?php echo $blogUrl; ?>" title="<?php echo $blogTitle; ?>">
                            <?php echo $row->title; ?>
                        </a>
                    </div>
                </div>

            <?php
        endforeach;
        ?>
    </div>
    <?php
} else {
    echo 'No news & updates found!';
}
?>
