<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class News extends Frontendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('news_m', 'fnews');
    }

    //NEWS ARCHIVE ALL THE NEWS IN ONE PLACE IF NO SLUG IF SLUG
    public function index($param = null) {
        if ($param) {
            $row = $this->fnews->get_by(array('slug' => $param, 'status' => ACTIVE));
            if ($row) {
                $data['row'] = $row;
                $this->template->load(get_template('news/news-single'), 'news-single', $data);
            } else {
                $this->load->view('front/templates/not-found');
            }
        } else {
            $data['alldata'] = $this->fnews->order_by('position', 'DESC')->get_all();
            $this->template->load(get_template('news/news-group'), 'news-group', $data);
        }
    }

    //GET ALL
    public function get_all() {
        $rows = $this->fnews->get_all();
        return $rows ? $rows : false;
    }

    public function get_latest($limit) {
        $rows = $this->fnews->order_by('position', 'DESC')->limit($limit)->get_all();
        return $rows ? $rows : false;
    }

}
