<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Validator extends Frontendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
    }

    //USER DASHBOARD
    public function index() {
        redirect('site');
        exit();
    }

    function is_user_login() {
        if ($this->session->userdata('userLogin')) {
            return true;
        } else {
            return false;
        }
    }

}
