<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Student extends Frontendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('Student_m', 'fstudent');
    }

    //USER DASHBOARD
    public function index() {
        redirect('site');
        exit();
    }

    //GET SAFE DATA BY USER ID
    public function get_row($ID) {
        $userRow = $this->fstudent->get($ID);
        return $userRow ? $userRow : false;
    }

    //GET SAFE DATA BY USER PARENT
    public function get_row_by_parent($parent_ID) {
        $userRow = $this->fstudent->get_by(array('parent_ID' => $parent_ID));
        return $userRow ? $userRow : false;
    }

    //UPDAET META
    public function update_student_meta($ID, $data) {
        $row = $this->fstudent->get_by(array('parent_ID' => $ID));
        if ($row) {
            //FOND ROW UPDATE HERE
            if ($this->fstudent->update($row->ID, $data)) {
                $data = array('status' => 'success', 'msg' => 'Profile updated successfully');
                return $data;
            } else {
                $data = array('status' => 'error', 'msg' => 'Unable to update profile.');
                return $data;
            }
        } else {
            //NOT FOUND ROW INSERT HERE
            if ($this->fstudent->insert($data)) {
                $data = array('status' => 'success', 'msg' => 'Profile updated successfully');
                return $data;
            } else {
                $data = array('status' => 'error', 'msg' => 'Unable to update profile.');
                return $data;
            }
        }
    }

    

}
