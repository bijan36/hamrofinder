<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_m extends MY_Model {

    /**
     * @ Author: Suraj Bajracharya
     * @ Author URL : www.indesignmedia.net
     * @see http://indesignmedia.net
     */
    protected $_table = SYSTEMUSER;

    public function __construct() {
        parent::__construct();
        $this->_database = $this->db;
        $this->primary_key = 'ID';
    }

    public function index() {
        echo "No good stuff";
    }

    //LOIGN CHEKC
    public function logincheck($data) {

        if (!empty($data['email']) && !empty($data['passcode'])) {
            if (isset($data) && is_array($data)) {
                $row = $this->get_by($data);
                if ($row) {
                    if ($row->status == ACTIVE) {
                        $sessArray = array(
                            'userID' => $row->ID,
                            'username' => $row->ID,
                            'userName' => $row->name,
                            'chatusername' => $row->name,
                            'userEmail' => $row->email,
                            'userType' => $row->type,
                            'userLevel' => $row->level,
                            'userLogin' => TRUE
                        );
                        $this->session->set_userdata($sessArray);

                        //print_r($this->session->all_userdata());
                        //exit();
                        //RECORD IP AND TIME OF LOGIN
                        $this->update($row->ID, array('last_login' => get_date_time(), 'last_IP' => $this->input->ip_address()));
                        //MAKE USER STATUS ONLINE
                        $this->update($row->ID,array('online'=>'Online'));
                        $data = array('status' => 'success', 'msg' => 'Success! Preparing your dashboard, please wait a while. ');
                        echo json_encode($data);
                        exit();
                    } elseif ($row->status == INACTIVE && $row->last_login == '0000-00-00 00:00:00') {


                        //STORING THE EMAIL ADDRESS FOR RESEND VERIFICATION
                        $this->session->set_userdata(array('resend_verification' => true, 'resend_verification_email' => $row->email));

                        $data = array('status' => 'error', 'msg' => 'Your email is still not verified please check your email spam folder. 
                            Make sure you mark our email as "not spam". Or <span class="makeResend" style="font-weight: 700;">resend verification email now.</span>');
                        echo json_encode($data);
                        exit();
                    } else {
                        $data = array('status' => 'error', 'msg' => 'Unable to login. Your account is ' . $row->status . '. Pleaes contact website administrator for further details.');
                        echo json_encode($data);
                        exit();
                    }
                } else {
                    $data = array('status' => 'error', 'msg' => 'Wrong username or password.');
                    echo json_encode($data);
                    exit();
                }
            } else {
                $data = array('status' => 'error', 'msg' => 'Wrong username or password');
                echo json_encode($data);
                exit();
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => 'Unable to loign! Please try again later'
            );
            echo json_encode($data);
            exit();
        }
    }

    //CHEKC USER LOING
    public function userLogin() {
        if (is_user_login()) {
            return true;
        } else {
            redirect('site');
            exit();
        }
    }

    //CHEKC USER TYPE IS STUDENT
    public function userLoginStudent() {
        if (is_user_student()) {
            return true;
        } else {
            redirect('site');
            exit();
        }
    }

    public function get_last_id() {
        $this->db->select_max('ID');
        $result = $this->db->get(SYSTEMUSER)->result_array();
        return $result[0]['ID'];
    }

}
