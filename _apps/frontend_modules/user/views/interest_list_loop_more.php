<?php
//INTERSTED USERS LIST WHILE POP UP LIST MESSAGE
if ($userRowi) {
    if (is_user_login()) {
        if ($userRowi->ID == $this->session->userdata('userID')) {
            ?>
            <li>
                <div class="row">
                    <div class="col-md-2" style="padding-right: 5px;">
                        <a href="<?php echo profile_url($userRowi->ID) ?>" title="<?php echo $userRowi->name; ?>">
                            <?php
                            $profile_image = Modules::run('imagehub/get_profile_image_real', $userRowi->profile_picture);
                            echo $profile_image;
                            ?>
                        </a>
                    </div>
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-md-12"><strong><a href="<?php echo profile_url($userRowi->ID) ?>" title="View Profile">Me <small><i class="fa fa-edit"></i></small></a></strong></div>
                            <div class="col-md-12"><small class="dim"><i class="fa fa-clock-o fa-fw"></i> <time class="timeago"  data-toggle="tooltip" data-placement="top"   title="<?php echo get_full_date($intRow->date_time); ?>" datetime="<?php echo $intRow->date_time;?>"></time></small></div>
                        </div>
                    </div>
                </div>
            </li>
            <?php
        } else {
            ?>
            <li>
                <div class="row">
                    <div class="col-md-2" style="padding-right: 5px;">
                        <a href="<?php echo profile_url($userRowi->ID) ?>" title="<?php echo $userRowi->name; ?>">
                            <?php
                            $profile_image = Modules::run('imagehub/get_profile_image_real', $userRowi->profile_picture);
                            echo $profile_image;
                            ?>
                        </a>
                    </div>
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-md-12"><strong><a href="<?php echo profile_url($userRowi->ID) ?>" title="<?php echo $userRowi->name; ?>"><?php echo $userRowi->name; ?></a></strong></div>
                            <div class="col-md-12"><small><?php echo Modules::run('user/get_full_address',$userRowi->ID); ?></small></div>
                            <div class="col-md-12"><small class="dim"><i class="fa fa-clock-o fa-fw"></i> <time class="timeago" title="<?php echo get_full_date($intRow->date_time); ?>" data-toggle="tooltip" data-placement="top"  datetime="<?php echo $intRow->date_time;?>"></time></small></div>
                        </div>
                    </div>
                </div>
            </li>
            <?php
        }
    } else {
        ?>
        <li>
            <div class="row">
                <div class="col-md-2" style="padding-right: 5px;">
                    <a href="<?php echo profile_url($userRowi->ID) ?>" title="<?php echo $userRowi->name; ?>">
                        <?php
                        $profile_image = Modules::run('imagehub/get_profile_image_real', $userRowi->profile_picture);
                        echo $profile_image;
                        ?>
                    </a>
                </div>
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-md-12"><strong><a href="<?php echo profile_url($userRowi->ID) ?>" title="<?php echo $userRowi->name; ?>"><?php echo $userRowi->name; ?></a></strong></div>
                        <div class="col-md-12"><small><?php echo Modules::run('user/get_full_address',$userRowi->ID); ?></small></div>
                        <div class="col-md-12"><small class="dim"><i class="fa fa-clock-o fa-fw"></i> <time class="timeago"  data-toggle="tooltip" data-placement="top" title="<?php echo get_full_date($intRow->date_time); ?>" datetime="<?php echo $intRow->date_time;?>"></time></small></div>
                    </div>
                </div>
            </div>
        </li>
        <?php
    }
}
?>