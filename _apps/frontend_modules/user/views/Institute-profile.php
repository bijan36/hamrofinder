<?php
if (isset($userRow) && !empty($userRow)) {
    //STUDENT META
    $meta = Modules::run('institute/get_row_by_parent', $userRow->ID);
    ?>
    <h1><span class="blueHead"><span class="uppercaseTextYellow"><?php echo $userRow->type; ?>: </span> <?php echo $userRow->name; ?></span>
        <?php
        //show edit link if the login user is same
        if (is_user_login() && $this->session->userdata('userID') == $userRow->ID) {
            ?>
            <span class="pull-right">
                <a href="<?php echo site_url('dashboard') ?>" class="btn yelloButton pull-right" title="View/Edit Your Profile">
                    <i class="fa fa-edit fa-fw"></i> Edit Profile
                </a>
            </span>
            <?php
        }
        ?>
    </h1>


    <!--PROFILE INFO BASIC-->
    <div class="grey-bg standard-padding round-border" style="margin-bottom: 15px;">


        <?php
        if ($this->session->userdata('userType') == 'Student') {
            //FIND CURRENT PROFILE IS ONLINE OR NOT
            if ($onlineStatus = Modules::run('user/find_online_status', $userRow->ID)) {
                if ($onlineStatus == 'Online') {
                    ?>
                    <div class="onlineChatIcon">
                        <span class="onlineClicker" onclick="javascript:chatWith('<?php echo $userRow->ID ?>', '<?php echo $userRow->name ?>')">
                            <img src="<?php echo base_url() ?>_public/front/assets/img/online.png">
                        </span>
                    </div>
                    <?php
                } else {
                    ?>
                    <div class="offlineChatIcon">
                        <img src="<?php echo base_url() ?>_public/front/assets/img/offline.png">
                    </div>
                    <?php
                }
            }
        }
        ?>
        <!--USER ONLINE TABS-->



        <div class="row">
            <div class="col-md-4">
                <div class="profile-pic">
                    <?php
                    $proImgID = $userRow->profile_picture ? $userRow->profile_picture : '0';
                    $profile_image = Modules::run('imagehub/get_profile_image_real', $proImgID);
                    echo $profile_image;
                    ?>
                </div>
                <span class="bold-uppercase-highlight center-block" style="margin-top: 10px;"><?php echo $userRow->type; ?></span>

            </div>

            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12">
                        <h2><?php echo $userRow->name; ?></h2>
                    </div>
                    <div class="col-md-12">
                        <table class="table">

                            <?php if (!empty($userRow->contact)) { ?>
                                <tr>
                                    <td style="width: 120px;">Mobile No.:</td>
                                    <td><?php echo $userRow->contact; ?></td>
                                </tr>
                            <?php } ?>

                            <?php if (!empty($meta->phone_no)) { ?>
                                <tr>
                                    <td>Phone No.:</td>
                                    <td><?php echo $meta->phone_no; ?></td>
                                </tr>
                            <?php } ?>
                            <?php if (!empty($userRow->email)) { ?>
                                <tr>
                                    <td>Email:</td>
                                    <td><?php echo $userRow->email; ?></td>
                                </tr>
                            <?php } ?>


                            <?php if (!empty($userRow->address)) { ?>
                                <tr>
                                    <td>Address:</td>
                                    <td><?php echo Modules::run('user/get_full_address', $userRow->ID); ?></td>
                                </tr>
                            <?php } ?>

                            <?php if (!empty($meta->website)) { ?>
                                <tr>
                                    <td>Website:</td>
                                    <td><?php echo $meta->website; ?></td>
                                </tr>
                            <?php } ?>

                            <?php if (!empty($meta->year_established)) { ?>
                                <tr>
                                    <td>ESTD:</td>
                                    <td><?php echo $meta->year_established; ?></td>
                                </tr>
                            <?php } ?>
                            <?php if (!empty($meta->area_occupied)) { ?>
                                <tr>
                                    <td>Area:</td>
                                    <td><?php echo $meta->area_occupied; ?></td>
                                </tr>
                            <?php } ?>
                            <?php if (!empty($meta->average_student_per_class)) { ?>
                                <tr>
                                    <td>Average Student/Class:</td>
                                    <td><?php echo $meta->average_student_per_class; ?></td>
                                </tr>
                            <?php } ?>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--PROFILE INFO BASIC-->





    <!--SOME PROGRAME OFFERED-->

    <?php
    $currentProgramms = Modules::run('institute_programe/get_by_parent_level', $userRow->ID);
    if ($currentProgramms) {
        $ids = array();
        foreach ($currentProgramms as $cp):
            $ids[] = $cp->ID;
        endforeach;
        $idArray['idArray'] = $ids;
        $idArray['no_del'] = true;
        ?>
        <div class="grey-bg standard-padding round-border" style="margin-top: 15px;">
            <div class="row">
                <div class="col-md-12">
                    <h4>Program Offered:</h4>
                    <?php $this->load->view('institute_programe/institute_program_loop', $idArray); ?>
                </div>
            </div>
        </div>
        <?php
    }
    ?>

    <!--SOME PROGRAME OFFERED-->



    <!--SOME AFFILIATIONS-->
    <div class="grey-bg standard-padding round-border" style="margin-top: 15px;">
        <div class="row">
            <?php
            if (!empty($meta->facilities_in_college)) {
                ?>
                <div class="col-md-6">

                    <div class="row">
                        <div class="col-md-12">
                            <h4>Facilities in institute:</h4>
                        </div>
                        <div class="col-md-12">
                            <table class="table">
                                <?php
                                $params = explode(',', $meta->facilities_in_college);
                                foreach ($params as $prm):
                                    ?>
                                    <tr>
                                        <td><i class="fa fa-check-square-o fa-fw"></i> <?php echo $prm; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        </div>
                    </div>

                </div>
                <?php
            }
            ?>
            <?php
            if (!empty($meta->secuirty_issues)) {
                ?>
                <div class="col-md-6">

                    <div class="row">
                        <div class="col-md-12">
                            <h4>Security Issue:</h4>
                        </div>
                        <div class="col-md-12">
                            <table class="table">
                                <?php
                                $params = explode(',', $meta->secuirty_issues);
                                foreach ($params as $prm):
                                    ?>
                                    <tr>
                                        <td><i class="fa fa-check-square-o fa-fw"></i> <?php echo $prm; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        </div>
                    </div>

                </div>
                <?php
            }
            ?>
            <div class="clearfix"></div>
            <?php
            if (!empty($meta->additional_features)) {
                ?>
                <div class="col-md-6">

                    <div class="row">
                        <div class="col-md-12">
                            <h4>Additional Features:</h4>
                        </div>
                        <div class="col-md-12">
                            <table class="table">
                                <?php
                                $params = explode(',', $meta->additional_features);
                                foreach ($params as $prm):
                                    ?>
                                    <tr>
                                        <td><i class="fa fa-check-square-o fa-fw"></i> <?php echo $prm; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        </div>
                    </div>

                </div>
                <?php
            }
            ?>
        </div>
    </div>

    <?php
    //DOCUMENTS
    $allDocs = Modules::run('documents/get_docs_by', $userRow->ID, INSTITUTE);
    if ($allDocs) {
        $totalDoc = count($allDocs);
        ?>
        <div class="ad-box-round-corner standard-padding round-border normal-list" style="margin-top: 25px;">
            <h4>Documents of <?php echo $userRow->name; ?></h4>
            <div class="row no-gutter">
                <?php
                foreach ($allDocs as $doc):
                    $docData['doc'] = $doc;
                    $docData['total'] = $totalDoc;
                    $this->load->view('documents/loop', $docData);
                endforeach;
                ?>
            </div>
        </div>
        <?php
    }
    ?>

    <?php
    //PHOTO GALLERY
    if ($userRow->gallery_picture) {
        ?>
        <div class="ad-box-round-corner standard-padding round-border normal-list" style="margin-top: 25px;">
            <h4>Gallery Images</h4>
            <div class="row">
                <?php
                if (strpos($userRow->gallery_picture, ',') !== false) {
                    //yes comma exist
                    $galImages = explode(',', $userRow->gallery_picture);
                    foreach ($galImages as $gim):
                        $this->load->view('imagehub/gal_img_big', array('imgID' => $gim, 'college_name' => $userRow->name));
                    endforeach;
                } else {
                    //no comma that means just one image exist
                    $this->load->view('imagehub/gal_img_big', array('imgID' => $userRow->gallery_picture, 'college_name' => $userRow->name));
                }
                ?>
            </div>
        </div>
        <?php
    }
    ?>

    <?php
}
?>
    


