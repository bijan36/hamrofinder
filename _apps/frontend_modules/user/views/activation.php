<h1><i class="fa fa-sign-in fa-fw"></i> User Activation</h1>
<div class="row">
	<div class="col-md-12">
		<div class="grey-bg standard-padding round-border">
			Congratulations! Welcome to hamrofinder. Please Login and complete your profile to enjoy all website features.
		</div>
	</div>
</div>