<div id="loginBox">
    <h1><i class="fa fa-sign-in fa-fw"></i> Login</h1>
    <div class="row">

        <div class="col-md-12">
            <div class="grey-bg standard-padding round-border">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="emailAddress">Email Address</label>
                            <input type="email" class="form-control" value="">
                        </div>
                        <div class="form-group">
                            <label for="passCode">Password</label>
                            <input type="password" class="form-control" value="">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="blueButton"><i class="fa fa-sign-in fa-fw"></i> Sign In</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="standard-padding">
                <div class="sub-heading">Not a member yet?</div>
                <br/>
                <a href="<?php echo site_url('register') ?>" class="btn yelloButton">Register For Free</a>

                <br/>
                <br/>
                <p>
                    When you registered, you will gain access to
                </p>
                <i class="fa fa-check-square-o fa-fw"></i> Place you ad<br/>
                <i class="fa fa-check-square-o fa-fw"></i> Save you ad<br/>
                <i class="fa fa-check-square-o fa-fw"></i> Contact Advertise<br/>
                <i class="fa fa-check-square-o fa-fw"></i> Email Address<br/>

            </div>
        </div>
    </div>
</div>