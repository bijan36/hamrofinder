<div class="modal fade modal-wide" id="makeapply-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><span class="blueHead"><span class="uppercaseTextYellow">Applying to: </span><span class="toApply"></span></span></h4>
            </div>
            <div class="modal-body">
                <?php echo form_open('studentapply/make_apply', array('id' => 'doMakeApply')) ?>
                <div class="form-box">
                    <div class="form-group">
                        <label for="message">You are applying to: <strong><span class="toApply"></span></strong></label><br/>
                        Type your message in box below:
                        <textarea name="message" class="form-control apply-msg"></textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary submitApply">Submit</button>
                    </div>
                    <input type="hidden" name="adID" id="adIDApplyField" value="">
                    <input type="hidden" name="adType" id="adIDType" value="">
                    <input type="hidden" name="url" id="modalUrlApply" value="<?php echo base_url(); ?>">
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
