<!--intersted person modal-->
<div class="modal fade modal-wide" id="make-offer-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-envelope-o fa-fw"></i> Make an offer to <span class="bold-uppercase-highlight"><span class="toInterest"></span></span></h4>
            </div>
            <div class="modal-body">

                <?php echo form_open('offer/make_offer', array('id' => 'doMakeOffer')) ?>
                <div class="form-box">
                    <div class="form-group">
                        <label for="message">Offer Message to <span class="toInterest"></span></label>
                        <textarea name="message" class="form-control offer-msg"></textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary submitOffer">Submit</button>
                    </div>
                    <input type="hidden" name="adID" id="adIDModalField" value="">
                    <input type="hidden" name="url" id="modalUrl" value="<?php echo base_url();?>">
                </div>
                
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

