<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends Frontendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('User_m', 'fuser');
    }

    //USER DASHBOARD
    public function index() {
        redirect('site');
        exit();
    }

    //GET SAFE DATA BY USER ID
    public function get_row($ID) {
        $userRow = $this->fuser->get($ID);
        if ($userRow) {
            unset($userRow->passcode);
            unset($userRow->code);
            return $userRow;
        } else {
            return false;
        }
    }

    //FOR CAN SHOW AD HELPER FUNCTION 
    public function get_active_row($ID) {
        $userRow = $this->fuser->get($ID);
        if ($userRow) {
            if ($userRow->status == ACTIVE) {
                unset($userRow->passcode);
                unset($userRow->code);
                return $userRow;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    //GET THE CURRENT PLAN
    public function get_current_plan() {
        $ID = $this->session->userdata('userID');
        if ($ID) {
            $urow = $this->fuser->get($ID);
            if ($urow) {
                return $urow->level;
            } else {
                return 'Unknown';
            }
        } else {
            return 'Unknown';
        }
    }

    //WITH CODE
    public function get_row_code($ID) {
        $userRow = $this->fuser->get($ID);
        if ($userRow) {
            unset($userRow->passcode);
            return $userRow;
        } else {
            return false;
        }
    }

    //GET SAFE DATA BY USER ID
    public function get_row_where($where) {
        $userRow = $this->fuser->get_by($where);
        if ($userRow) {
            unset($userRow->passcode);
            unset($userRow->code);
            return $userRow;
        } else {
            return false;
        }
    }

    //GET FULL ADDRESS
    public function get_full_address($ID) {
        $address = '';
        $row = $this->fuser->get($ID);
        if ($row) {
            $country = !empty($row->country) ? ', ' . $row->country : '';
            $province = !empty($row->province) ? ', ' . $row->province : '';
            $district = !empty($row->district) ? ', ' . $row->district : '';
            $local_level = !empty($row->local_level) ? ', ' . $row->local_level : '';
            $ward_no = !empty($row->ward_no) ? ' - ' . $row->ward_no : '';
            $address = !empty($row->address) ? $row->address : '';

            $address = $address . $local_level . $ward_no . $district . $province . $country;
        }
        return $address;
    }

    //GET SAFE DATA BY STUDENTS ROW
    public function get_rows_order($field1, $field2, $where) {
        $userRows = $this->fuser->order_by($field1, $field2)->get_many_by($where);
        $res = array();
        if ($userRows) {
            foreach ($userRows as $row):
                unset($row->passcode);
                unset($row->code);
                $res[] = $row;
            endforeach;
            return $res;
        } else {
            return false;
        }
    }

    //GET COLLEGE NAME
    public function get_name($ID) {
        $userRow = $this->fuser->get($ID);
        return $userRow ? $userRow->name : 'Unknown';
    }

    //SHOWING USER PROFILE
    public function show($userID) {
        if (is_user_login()) {
            if (isset($userID)) {


                $userType = $this->session->userdata('userType');
                $userLevel = $this->session->userdata('userLevel');

                //LEVEL ATHONTICATION FOR THE SHOW AN INTEREST ACTION FOR USERS
                if ($userType == 'College') {
                    $interestAuth = Modules::run('college_membership_plans/find_authorise', $userLevel, 'profile');
                    if ($interestAuth == 'No') {
                        //THE RULE IS ONLY VALID FOR 
                        //NOT THEIR OWN PROFILE
                        //BUT OTHERS PROFILE WANT TO VIEW
                        if (get_college_ID() != $userID) {
                            redirect('notfound/invalid');
                            exit();
                        }
                    }
                }

                if ($userType == 'Institute') {
                    $interestAuth = Modules::run('institute_membership_plans/find_authorise', $userLevel, 'profile');
                    if ($interestAuth == 'No') {
                        //THE RULE IS ONLY VALID FOR 
                        //NOT THEIR OWN PROFILE
                        //BUT OTHERS PROFILE WANT TO VIEW
                        if (get_institute_ID() != $userID) {
                            redirect('notfound/invalid');
                            exit();
                        }
                    }
                }

                //LEVEL ATHONTICATION FOR THE SHOW AN INTEREST ACTION FOR USERS
                //IS USER VALID ?
                if (is_valid_profile($userID)) {
                    $userRow = $this->get_row($userID);
                    if ($userRow) {
                        if ($userRow->type != $this->session->userdata('userType')) {

                            //DONT ALLOW COLLEGE TO VIEW INSTITUTE PROFILE
                            if ($this->session->userdata('userType') == 'College' && $userRow->type == 'Institute') {
                                redirect('notfound/invalid');
                                exit();
                            }
                            //DONT ALLOW INSTITUTE TO VIEW COLLEGE PROFILE
                            if ($this->session->userdata('userType') == 'Institute' && $userRow->type == 'College') {
                                redirect('notfound/invalid');
                                exit();
                            }
                            //TO LET OTHER TO VEIW PROFILE
                            $data['userRow'] = $userRow;
                            $this->template->load(get_template($userRow->type . '-profile'), $userRow->type . '-profile', $data);
                        } else {
                            //JUST FOR SELF VEIW PROFILE
                            if ($userRow->ID == $this->session->userdata('userID')) {
                                $data['userRow'] = $userRow;
                                $this->template->load(get_template($userRow->type . '-profile'), $userRow->type . '-profile', $data);
                            } else {
                                redirect('notfound/invalid');
                                exit();
                            }
                        }
                    } else {
                        redirect('notfound');
                        exit();
                    }
                } else {
                    redirect('notfound');
                    exit();
                }
            } else {
                redirect('login');
                exit();
            }
        } else {
            redirect('login');
            exit();
        }
    }

    //VALIDATE MORE INFO BTN
    public function validate_more_info() {
        $dataPack = $this->input->post('id');
        $parts = explode('-', $dataPack);
        $adID = $parts[0];
        $userID = $parts[1];
        $adType = $parts[2];

        if (is_user_login()) {

            //IF COLLEGE JUST ALLOW STUDENT TO VIEW, IF STUDENT JUST ALLOW COLLEGE TO VIEW
            $CurrentUserType = $this->session->userdata('userType');
            $CurrentUserLevel = $this->session->userdata('userLevel');


            //LEVEL ATHONTICATION FOR THE SHOW AN INTEREST ACTION FOR USERS
            if ($CurrentUserType == 'College') {
                $interestAuth = Modules::run('college_membership_plans/find_authorise', $CurrentUserLevel, 'profile');
                if ($interestAuth == 'No') {
                    $data = array(
                        'status' => 'error',
                        'msg' => INVALIDMSG,
                        'do' => 'sametype'
                    );
                    echo json_encode($data);
                    exit();
                }
            }

            if ($CurrentUserType == 'Institute') {
                $interestAuth = Modules::run('institute_membership_plans/find_authorise', $CurrentUserLevel, 'profile');
                if ($interestAuth == 'No') {
                    $data = array(
                        'status' => 'error',
                        'msg' => INVALIDMSG,
                        'do' => 'sametype'
                    );
                    echo json_encode($data);
                    exit();
                }
            }
            //LEVEL ATHONTICATION FOR THE SHOW AN INTEREST ACTION FOR USERS

            if ($adType == 'Student') {
                if (Modules::run('studentad/is_belongs_to', $adID, $userID)) {
                    if ($CurrentUserType != $adType) {
                        $data = array(
                            'status' => 'success',
                            'controller' => 'studentad',
                            'adID' => $adID
                        );
                        echo json_encode($data);
                        exit();
                    } else {
                        //FOR SELF OPEN AD
                        if ($userID == $this->session->userdata('userID')) {
                            $data = array(
                                'status' => 'success',
                                'controller' => 'studentad',
                                'adID' => $adID
                            );
                            echo json_encode($data);
                            exit();
                        } else {
                            $data = array(
                                'status' => 'error',
                                'con' => 'nochange',
                                'msg' => INVALIDMSG
                            );
                            echo json_encode($data);
                            exit();
                        }
                    }
                } else {
                    $data = array(
                        'status' => 'error',
                        'con' => 'nochange',
                        'msg' => INVALIDMSG
                    );
                    echo json_encode($data);
                    exit();
                }
            } elseif ($adType == 'College') {
                if (Modules::run('collegead/is_belongs_to', $adID, $userID)) {

                    if ($CurrentUserType != $adType) {
                        $data = array(
                            'status' => 'success',
                            'controller' => 'collegead',
                            'adID' => $adID
                        );
                        echo json_encode($data);
                        exit();
                    } else {
                        //FOR SELF OPEN AD
                        if ($userID == $this->session->userdata('userID')) {
                            $data = array(
                                'status' => 'success',
                                'controller' => 'collegead',
                                'adID' => $adID
                            );
                            echo json_encode($data);
                            exit();
                        } else {
                            $data = array(
                                'status' => 'error',
                                'con' => 'nochange',
                                'msg' => INVALIDMSG
                            );
                            echo json_encode($data);
                            exit();
                        }
                    }
                } else {
                    $data = array(
                        'status' => 'error',
                        'con' => 'nochange',
                        'msg' => INVALIDMSG
                    );
                    echo json_encode($data);
                    exit();
                }
            } elseif ($adType == 'Institute') {
                if (Modules::run('institutead/is_belongs_to', $adID, $userID)) {

                    if ($CurrentUserType != $adType) {
                        $data = array(
                            'status' => 'success',
                            'controller' => 'institutead',
                            'adID' => $adID
                        );
                        echo json_encode($data);
                        exit();
                    } else {
                        //FOR SELF OPEN AD
                        if ($userID == $this->session->userdata('userID')) {
                            $data = array(
                                'status' => 'success',
                                'controller' => 'institutead',
                                'adID' => $adID
                            );
                            echo json_encode($data);
                            exit();
                        } else {
                            $data = array(
                                'status' => 'error',
                                'con' => 'nochange',
                                'msg' => INVALIDMSG
                            );
                            echo json_encode($data);
                            exit();
                        }
                    }
                } else {
                    $data = array(
                        'status' => 'error',
                        'con' => 'nochange',
                        'msg' => INVALIDMSG
                    );
                    echo json_encode($data);
                    exit();
                }
            } else {
                $data = array(
                    'status' => 'error',
                    'con' => 'nochange',
                    'msg' => INVALIDMSG
                );
                echo json_encode($data);
                exit();
            }
        } else {
            $data = array(
                'status' => 'error',
                'con' => 'login',
                'msg' => 'For more info, please register or login.'
            );
            echo json_encode($data);
            exit();
        }
    }

    //VALIDATE MAKE OFFER
    public function validate_make_offer() {
        $dataPack = $this->input->post('id');
        $parts = explode('-', $dataPack);
        $adID = $parts[0];
        $userID = $parts[1];
        $adType = $parts[2];

        if (is_user_login()) {

            //IF COLLEGE JUST ALLOW STUDENT TO VIEW, IF STUDENT JUST ALLOW COLLEGE TO VIEW
            $CurrentUserType = $this->session->userdata('userType');

            if ($adType == 'Student') {
                if (Modules::run('studentad/is_belongs_to', $adID, $userID)) {
                    if ($CurrentUserType != $adType) {
                        $data = array(
                            'status' => 'success',
                            'controller' => 'studentad',
                            'adID' => $adID
                        );
                        echo json_encode($data);
                        exit();
                    } else {
                        //FOR SELF OPEN AD
                        if ($userID == $this->session->userdata('userID')) {
                            $data = array(
                                'status' => 'success',
                                'controller' => 'studentad',
                                'adID' => $adID
                            );
                            echo json_encode($data);
                            exit();
                        } else {
                            $data = array(
                                'status' => 'error',
                                'con' => 'nochange',
                                'msg' => INVALIDMSG
                            );
                            echo json_encode($data);
                            exit();
                        }
                    }
                } else {
                    $data = array(
                        'status' => 'error',
                        'con' => 'nochange',
                        'msg' => INVALIDMSG
                    );
                    echo json_encode($data);
                    exit();
                }
            } elseif ($adType == 'College') {
                if (Modules::run('collegead/is_belongs_to', $adID, $userID)) {

                    if ($CurrentUserType != $adType) {
                        $data = array(
                            'status' => 'success',
                            'controller' => 'collegead',
                            'adID' => $adID
                        );
                        echo json_encode($data);
                        exit();
                    } else {
                        //FOR SELF OPEN AD
                        if ($userID == $this->session->userdata('userID')) {
                            $data = array(
                                'status' => 'success',
                                'controller' => 'collegead',
                                'adID' => $adID
                            );
                            echo json_encode($data);
                            exit();
                        } else {
                            $data = array(
                                'status' => 'error',
                                'con' => 'nochange',
                                'msg' => INVALIDMSG
                            );
                            echo json_encode($data);
                            exit();
                        }
                    }
                } else {
                    $data = array(
                        'status' => 'error',
                        'con' => 'nochange',
                        'msg' => INVALIDMSG
                    );
                    echo json_encode($data);
                    exit();
                }
            } else {
                $data = array(
                    'status' => 'error',
                    'con' => 'nochange',
                    'msg' => INVALIDMSG
                );
                echo json_encode($data);
                exit();
            }
        } else {
            $data = array(
                'status' => 'error',
                'con' => 'login',
                'msg' => 'To make offer,  please register or login.'
            );
            echo json_encode($data);
            exit();
        }
    }

    //USER LOGIN HERE
    public function logintry() {
        $this->form_validation->set_rules('emailAddress', 'Email Address', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required|trim');

        if ($this->form_validation->run() == TRUE) {
            $queryData = array(
                'email' => $this->input->post('emailAddress'),
                'passcode' => $this->password_encrypt($this->input->post('password'))
            );

            //VALIDATE HERE
            $this->fuser->logincheck($queryData);
        } else {
            $data = array(
                'status' => 'error',
                'msg' => validation_errors()
            );
        }
        echo json_encode($data);
        exit();
    }

    //GET REGISTED
    public function get_registered($data) {
        if ($this->fuser->insert($data)) {
            $currentID = $this->fuser->get_last_id();
            return $currentID;
        } else {
            return false;
        }
    }

    //RESEND VERIFICATION
    public function resendverification() {

        if ($this->session->userdata('resend_verification') == true) {

            $resendEmail = $this->session->userdata('resend_verification_email');

            if ($resendEmail) {

                $userRow = $this->fuser->get_by(array('email' => $resendEmail));
                if ($userRow) {

                    if ($userRow->status == INACTIVE && $userRow->last_login == '0000-00-00 00:00:00') {


                        //making new password
                        $rowPassword = get_hash();
                        $updateData = array(
                            'passcode' => $this->makepassword($rowPassword)
                        );

                        if ($this->fuser->update($userRow->ID, $updateData)) {

                            //LETS SEND VERIFICATION AGAIN
                            $data['to'] = $userRow->email;
                            $data['toName'] = $userRow->name;
                            $data['subject'] = 'Please verify your account.';
                            $data['template'] = 'user_registration_activation_email';
                            $data['others'] = array(
                                'userfullname' => $userRow->name,
                                'emailaddress' => $userRow->email,
                                'password' => $rowPassword,
                                'activation_url' => site_url('user/activate/' . $userRow->ID . '/' . $userRow->code)
                            );
                            Modules::run('emailsystem/doEmail', $data);

                            $this->session->unset_userdata('resend_verification');
                            $this->session->unset_userdata('resend_verification_email');

                            $data = array(
                                'status' => 'success',
                                'msg' => 'A verification email has been sent to your email. Please check your email and verify it.'
                            );
                            echo json_encode($data);
                            exit();
                        } else {

                            $data = array(
                                'status' => 'error',
                                'msg' => 'Your session is expired, please try again later.'
                            );
                            echo json_encode($data);
                            exit();
                        }
                    } else {
                        $data = array(
                            'status' => 'error',
                            'msg' => 'Your session is expired, please try again later.'
                        );
                        echo json_encode($data);
                        exit();
                    }
                } else {
                    $data = array(
                        'status' => 'error',
                        'msg' => 'Your session is expired, please try again later.'
                    );
                    echo json_encode($data);
                    exit();
                }
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => 'Your session is expired, please try again later.'
                );
                echo json_encode($data);
                exit();
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => 'Your session is expired, please try again later.'
            );
            echo json_encode($data);
            exit();
        }
    }

    //USER ACTIVATION FUNCTION
    public function activate($id, $key) {
        if (isset($id) && isset($key)) {
            if (strlen($key) == '32') {
                $row = $this->fuser->get_by(array('ID' => $id, 'code' => $key));
                if ($row) {
                    //LETS ACTIVE USER HERE
                    if ($this->fuser->update($row->ID, array('status' => ACTIVE, 'code' => get_hash()))) {
                        $this->template->load(get_template('default'), 'activation');
                    } else {
                        redirect('notfound');
                        exit();
                    }
                } else {
                    redirect('notfound');
                    exit();
                }
            } else {
                redirect('notfound/invalid');
                exit();
            }
        } else {
            redirect('notfound/invalid');
            exit();
        }
    }

    //CHEKC EMAIL EXIST
    public function is_exist($email) {
        $userRow = $this->fuser->get_by(array('email' => $email));
        if ($userRow) {
            return true;
        } else {
            return false;
        }
    }

    //PASSWORD MAKING
    public function makepassword($stirng) {
        return $this->password_encrypt($stirng);
    }

    private function get_crypt() {
        $st = $this->config->item('encryption_key');
        return substr($st, 5, 14);
    }

    private function password_encrypt($password) {
        $getTo = $password . $this->get_crypt();
        $hash = hash('sha512', $getTo);
        return $hash;
    }

    //USER UPDATE PROFILE
    public function updateprofile() {

        //print_r($_POST);
        //exit();

        if (!is_user_login()) {
            $data = array(
                'status' => 'error',
                'msg' => 'You are not authorised to update profile'
            );
            echo json_encode($data);
            exit();
        }
        if (is_user_student() == 0) {
            $data = array(
                'status' => 'error',
                'msg' => 'You are not authorised to update profile'
            );
            echo json_encode($data);
            exit();
        }
        $ability = is_user_student_able();
        if ($ability['status'] == 'error') {
            echo json_encode($ability);
            exit();
        }

        //SLC AFFILIATION RESETTER IF ANY VALUE SET THEN DO VALIATION
        $slc_affiliation = $this->input->post('slc_affiliation');
        $slc_score = '';
        $slc_school = '';
        $slc_school_location = '';
        if (!empty($slc_affiliation)) {
            $this->form_validation->set_rules('slc_score', 'SLC score', 'required|trim');
            $this->form_validation->set_rules('slc_school', 'SLC school name', 'required|trim');
            $this->form_validation->set_rules('slc_school_location', 'SLC school location', 'required|trim');
            if ($this->form_validation->run() == TRUE) {
                $slc_score = $this->input->post('slc_score');
                $slc_school = $this->input->post('slc_school');
                $slc_school_location = $this->input->post('slc_school_location');
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => validation_errors()
                );
                echo json_encode($data);
                exit();
            }
        }
        //SLC AFFILIATION RESETTER IF ANY VALUE SET THEN DO VALIATION
        //PLUS 2 AFFILIATION RESETTER IF ANY VALUE SET THEN DO VALIATION
        $plus2_affiliation = $this->input->post('plus2_affiliation');
        $plus2_score = '';
        $plus2_school = '';
        $plus2_school_location = '';
        if (!empty($plus2_affiliation)) {
            $this->form_validation->set_rules('plus2_score', 'plus 2 score', 'required|trim');
            $this->form_validation->set_rules('plus2_school', 'plus 2 school name', 'required|trim');
            $this->form_validation->set_rules('plus2_school_location', 'plus 2 school location', 'required|trim');
            if ($this->form_validation->run() == TRUE) {
                $plus2_score = $this->input->post('plus2_score');
                $plus2_school = $this->input->post('plus2_school');
                $plus2_school_location = $this->input->post('plus2_school_location');
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => validation_errors()
                );
                echo json_encode($data);
                exit();
            }
        }
        //PLUS 2 AFFILIATION RESETTER IF ANY VALUE SET THEN DO VALIATION
        //BACHELOR AFFILIATION RESETTER IF ANY VALUE SET THEN DO VALIATION
        $bachelor_affiliation = $this->input->post('bachelor_affiliation');
        $bachelor_score = '';
        $bachelor_school = '';
        $bachelor_school_location = '';
        if (!empty($bachelor_affiliation)) {
            $this->form_validation->set_rules('bachelor_score', 'bachelor score', 'required|trim');
            $this->form_validation->set_rules('bachelor_school', 'bachelor college name', 'required|trim');
            $this->form_validation->set_rules('bachelor_school_location', 'bachelor college location', 'required|trim');
            if ($this->form_validation->run() == TRUE) {
                $bachelor_score = $this->input->post('bachelor_score');
                $bachelor_school = $this->input->post('bachelor_school');
                $bachelor_school_location = $this->input->post('bachelor_school_location');
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => validation_errors()
                );
                echo json_encode($data);
                exit();
            }
        }
        //PLUS 2 AFFILIATION RESETTER IF ANY VALUE SET THEN DO VALIATION
        //SESSION STUDNET USER ID
        $ID = get_student_ID();

        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('country', 'Country', 'required');
        $this->form_validation->set_rules('province', 'Province', 'required');
        $this->form_validation->set_rules('district', 'District', 'required');
        $this->form_validation->set_rules('local_level', 'Local Level', 'required');
        $this->form_validation->set_rules('ward_no', 'Ward No', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('mobile', 'Contact', 'required');
        $this->form_validation->set_rules('country', 'Country', 'required|alpha');
        $this->form_validation->set_rules('gender', 'Gender', 'required');

        if ($this->form_validation->run() == TRUE) {

            $country = $this->input->post('country');
            $province = $this->input->post('province');
            $district = $this->input->post('district');
            $local_level = $this->input->post('local_level');
            $ward_no = $this->input->post('ward_no');
            $name = $this->input->post('name');
            $address = $this->input->post('address');
            $contact = $this->input->post('mobile');

            if (($country === '0') || ($province === '0') || ($district === '0') || ($local_level === '0') || ($ward_no === '0')) {

                $data = array(
                    'status' => 'error',
                    'msg' => 'Please select the address properly and try again'
                );
                echo json_encode($data);
                exit();
            } else {
                //UPDATING BASIC INFO
                $dataBasic = array(
                    'name' => $name,
                    'country' => $country,
                    'province' => $province,
                    'district' => $district,
                    'local_level' => $local_level,
                    'ward_no' => $ward_no,
                    'address' => $address,
                    'contact' => $contact
                );

                //UPDATE HERE
                $this->fuser->update($ID, $dataBasic);

                //LETS MOVE ON TO USERS META SECTION
                $phone_no = $this->input->post('phone_no');
                $intersted_sports = $this->input->post('intersted_sports');
                $intersted_art = $this->input->post('intersted_art');
                $interst = $this->input->post('interest');
                $country = $this->input->post('country');
                $gender = $this->input->post('gender');

                //$slc_affiliation = $this->input->post('slc_affiliation');
                //$slc_score = $this->input->post('slc_score');
                //$slc_school = $this->input->post('slc_school');
                //$slc_school_location = $this->input->post('slc_school_location');
                //$plus2_affiliation = $this->input->post('plus2_affiliation');
                //$plus2_score = $this->input->post('plus2_score');
                //$plus2_school = $this->input->post('plus2_school');
                //$plus2_school_location = $this->input->post('plus2_school_location');
                //$bachelor_affiliation = $this->input->post('bachelor_affiliation');
                //$bachelor_score = $this->input->post('bachelor_score');
                //$bachelor_school = $this->input->post('bachelor_school');
                //$bachelor_school_location = $this->input->post('bachelor_school_location');

                $plus_2_college_entrance = 'Appeared';
                $plus_2_college_entrance_set = $this->input->post('plus_2_college_entrance');
                if (isset($plus_2_college_entrance_set) && $plus_2_college_entrance_set == 'Not Appeared') {
                    $plus_2_college_entrance = 'Not Appeared';
                }

                $plus_2_college_entrance_marks = '';
                $CMAT = '';
                $Engineering = '';
                $KUUMAT = '';
                $Medicine = '';

                if ($plus_2_college_entrance == 'Appeared') {
                    $plus_2_college_entrance_marks = $this->input->post('plus_2_college_entrance_marks');
                    $CMAT = $this->input->post('CMAT');
                    $Engineering = $this->input->post('Engineering');
                    $KUUMAT = $this->input->post('KUUMAT');
                    $Medicine = $this->input->post('Medicine');
                }
                $ip_address = $this->input->ip_address();

                $dataAdvance = array(
                    'parent_ID' => $ID,
                    'phone_no' => $phone_no,
                    'intersted_sports' => $intersted_sports,
                    'intersted_art' => $intersted_art,
                    'interst' => $interst,
                    'country' => $country,
                    'gender' => $gender,
                    'slc_affiliation' => $slc_affiliation,
                    'slc_score' => $slc_score,
                    'slc_school' => $slc_school,
                    'slc_school_location' => $slc_school_location,
                    'plus2_affiliation' => $plus2_affiliation,
                    'plus2_score' => $plus2_score,
                    'plus2_school' => $plus2_school,
                    'plus2_school_location' => $plus2_school_location,
                    'bachelor_affiliation' => $bachelor_affiliation,
                    'bachelor_score' => $bachelor_score,
                    'bachelor_school' => $bachelor_school,
                    'bachelor_school_location' => $bachelor_school_location,
                    'plus_2_college_entrance' => $plus_2_college_entrance,
                    'plus_2_college_entrance_marks' => $plus_2_college_entrance_marks,
                    'CMAT' => $CMAT,
                    'engineering' => $Engineering,
                    'KUUMAT' => $KUUMAT,
                    'medicine' => $Medicine,
                    'ip_address' => $ip_address
                );

                $res = Modules::run('student/update_student_meta', $ID, $dataAdvance);

                echo json_encode($res);
                exit();
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => validation_errors()
            );
            echo json_encode($data);
            exit();
        }
    }

    //UPDATE USER PROFILE IMAGE ONLY
    //WHILE USER UPDATE HIS PROFILE
    public function update_profile_image($imgID) {
        if (is_user_login()) {
            $userID = $this->session->userdata('userID');
            if (is_valid_profile($userID)) {
                $this->fuser->update($userID, array('profile_picture' => $imgID));
            }
        }
    }

    public function update_gallery_image($imgID) {
        if (is_user_login()) {
            $userID = $this->session->userdata('userID');
            $userRow = $this->get_row($userID);
            if ($userRow) {
                if (is_valid_profile($userID)) {
                    $current_gallery = $userRow->gallery_picture;
                    if (!empty($current_gallery)) {
                        //UPDATE HERE
                        $newStr = $current_gallery . ',' . $imgID;
                        $this->fuser->update($userID, array('gallery_picture' => $newStr));
                        return true;
                    } else {
                        //INSERT HERE
                        $this->fuser->update($userID, array('gallery_picture' => $imgID));
                        return false;
                    }
                }
            }
        }
        return false;
    }

    //USER UPDATE PROFILE
    public function updateprofile_college() {

        if (!is_user_login()) {
            $data = array(
                'status' => 'error',
                'msg' => 'You are not authorised to update profile'
            );
            echo json_encode($data);
            exit();
        }
        if (is_user_college() == 0) {
            $data = array(
                'status' => 'error',
                'msg' => 'You are not authorised to update profile'
            );
            echo json_encode($data);
            exit();
        }

        $ability = is_user_college_able();
        if ($ability['status'] == 'error') {
            echo json_encode($ability);
            exit();
        }

        $ID = get_college_ID();

        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('country', 'Country', 'required');
        $this->form_validation->set_rules('province', 'Province', 'required');
        $this->form_validation->set_rules('district', 'District', 'required');
        $this->form_validation->set_rules('local_level', 'Local Level', 'required');
        $this->form_validation->set_rules('ward_no', 'Ward No', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('mobile', 'Contact', 'required');
        $this->form_validation->set_rules('country', 'Country', 'required|alpha');
        $this->form_validation->set_rules('year_established', 'Year establish', 'required');

        if ($this->form_validation->run() == TRUE) {

            $country = $this->input->post('country');
            $province = $this->input->post('province');
            $district = $this->input->post('district');
            $local_level = $this->input->post('local_level');
            $ward_no = $this->input->post('ward_no');
            $name = $this->input->post('name');
            $address = $this->input->post('address');
            $contact = $this->input->post('mobile');

            if (($country === '0') || ($province === '0') || ($district === '0') || ($local_level === '0') || ($ward_no === '0')) {

                $data = array(
                    'status' => 'error',
                    'msg' => 'Please select the address properly and try again'
                );
                echo json_encode($data);
                exit();
            } else {

                //UPDATING BASIC INFO
                $dataBasic = array(
                    'name' => $name,
                    'country' => $country,
                    'province' => $province,
                    'district' => $district,
                    'local_level' => $local_level,
                    'ward_no' => $ward_no,
                    'address' => $address,
                    'contact' => $contact
                );

                //UPDATE HERE
                $this->fuser->update($ID, $dataBasic);

                //LETS MOVE ON TO USERS META SECTION
                $year_established = $this->input->post('year_established');
                $average_student_per_class = $this->input->post('average_student_per_class');
                $website = $this->input->post('website');
                $area_occupied = $this->input->post('area_occupied');
                $country = $this->input->post('country');


                $facilities_in_college = '';
                $facilities_in_college_field = $this->input->post('facilities_in_college[]');
                if (!empty($facilities_in_college_field)) {
                    $sepa = '';
                    foreach ($facilities_in_college_field as $fcf):
                        $facilities_in_college .= $sepa . $fcf;
                        $sepa = ',';
                    endforeach;
                }

                $secuirty_issues = '';
                $secuirty_issues_field = $this->input->post('secuirty_issues[]');
                if (!empty($secuirty_issues_field)) {
                    $sepa = '';
                    foreach ($secuirty_issues_field as $sif):
                        $secuirty_issues .= $sepa . $sif;
                        $sepa = ',';
                    endforeach;
                }

                $additional_features = '';
                $additional_features_field = $this->input->post('additional_features[]');
                if (!empty($additional_features_field)) {
                    $sepa = '';
                    foreach ($additional_features_field as $aff):
                        $additional_features .= $sepa . $aff;
                        $sepa = ',';
                    endforeach;
                }


                $ip_address = $this->input->ip_address();

                $dataAdvance = array(
                    'parent_ID' => $ID,
                    'year_established' => $year_established,
                    'average_student_per_class' => $average_student_per_class,
                    'website' => $website,
                    'area_occupied' => $area_occupied,
                    'country' => $country,
                    'facilities_in_college' => $facilities_in_college,
                    'secuirty_issues' => $secuirty_issues,
                    'additional_features' => $additional_features,
                    'ip_address' => $ip_address
                );

                $res = Modules::run('college/update_student_meta', $ID, $dataAdvance);

                echo json_encode($res);
                exit();
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => validation_errors()
            );
            echo json_encode($data);
            exit();
        }
    }

//INSTITUTE UPDATE PROFILE
    public function updateprofile_institute() {

        if (!is_user_login()) {
            $data = array(
                'status' => 'error',
                'msg' => 'You are not authorised to update profile'
            );
            echo json_encode($data);
            exit();
        }
        if (is_user_institute() == 0) {
            $data = array(
                'status' => 'error',
                'msg' => 'You are not authorised to update profile'
            );
            echo json_encode($data);
            exit();
        }

        $ability = is_user_institute_able();
        if ($ability['status'] == 'error') {
            echo json_encode($ability);
            exit();
        }

        $ID = get_institute_ID();

        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('country', 'Country', 'required');
        $this->form_validation->set_rules('province', 'Province', 'required');
        $this->form_validation->set_rules('district', 'District', 'required');
        $this->form_validation->set_rules('local_level', 'Local Level', 'required');
        $this->form_validation->set_rules('ward_no', 'Ward No', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('mobile', 'Contact', 'required');
        $this->form_validation->set_rules('country', 'Country', 'required|alpha');
        $this->form_validation->set_rules('year_established', 'Year establish', 'required');

        if ($this->form_validation->run() == TRUE) {

            $country = $this->input->post('country');
            $province = $this->input->post('province');
            $district = $this->input->post('district');
            $local_level = $this->input->post('local_level');
            $ward_no = $this->input->post('ward_no');
            $name = $this->input->post('name');
            $address = $this->input->post('address');
            $contact = $this->input->post('mobile');

            if (($country === '0') || ($province === '0') || ($district === '0') || ($local_level === '0') || ($ward_no === '0')) {

                $data = array(
                    'status' => 'error',
                    'msg' => 'Please select the address properly and try again'
                );
                echo json_encode($data);
                exit();
            } else {

                //UPDATING BASIC INFO
                $dataBasic = array(
                    'name' => $name,
                    'country' => $country,
                    'province' => $province,
                    'district' => $district,
                    'local_level' => $local_level,
                    'ward_no' => $ward_no,
                    'address' => $address,
                    'contact' => $contact
                );

                //UPDATE HERE
                $this->fuser->update($ID, $dataBasic);

                //LETS MOVE ON TO USERS META SECTION
                $year_established = $this->input->post('year_established');
                $average_student_per_class = $this->input->post('average_student_per_class');
                $website = $this->input->post('website');
                $area_occupied = $this->input->post('area_occupied');
                $country = $this->input->post('country');


                $facilities_in_college = '';
                $facilities_in_college_field = $this->input->post('facilities_in_college[]');
                if (!empty($facilities_in_college_field)) {
                    $sepa = '';
                    foreach ($facilities_in_college_field as $fcf):
                        $facilities_in_college .= $sepa . $fcf;
                        $sepa = ',';
                    endforeach;
                }

                $secuirty_issues = '';
                $secuirty_issues_field = $this->input->post('secuirty_issues[]');
                if (!empty($secuirty_issues_field)) {
                    $sepa = '';
                    foreach ($secuirty_issues_field as $sif):
                        $secuirty_issues .= $sepa . $sif;
                        $sepa = ',';
                    endforeach;
                }

                $additional_features = '';
                $additional_features_field = $this->input->post('additional_features[]');
                if (!empty($additional_features_field)) {
                    $sepa = '';
                    foreach ($additional_features_field as $aff):
                        $additional_features .= $sepa . $aff;
                        $sepa = ',';
                    endforeach;
                }


                $ip_address = $this->input->ip_address();

                $dataAdvance = array(
                    'parent_ID' => $ID,
                    'year_established' => $year_established,
                    'average_student_per_class' => $average_student_per_class,
                    'website' => $website,
                    'area_occupied' => $area_occupied,
                    'country' => $country,
                    'facilities_in_college' => $facilities_in_college,
                    'secuirty_issues' => $secuirty_issues,
                    'additional_features' => $additional_features,
                    'ip_address' => $ip_address
                );

                $res = Modules::run('institute/update_institute_meta', $ID, $dataAdvance);

                echo json_encode($res);
                exit();
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => validation_errors()
            );
            echo json_encode($data);
            exit();
        }
    }

    public function get_student_address_select($selected = null) {
        $allStudents = $this->fuser->get_many_by(array('type' => 'Student', 'status' => 'Active'));
        $output = '<option value="">Select One</option>';
        if ($allStudents) {
            $ads = array();
            foreach ($allStudents as $st):
                $ads[] = strtolower($st->address);
            endforeach;
            $ads = array_unique($ads);
            sort($ads);
            foreach ($ads as $ro):
                if ($selected) {
                    $sel = strtolower($selected) == $ro ? 'selected' : '';
                    $output .= '<option value="' . ucwords(trim($ro)) . '" ' . $sel . '>' . ucwords($ro) . '</option>';
                } else {
                    $output .= '<option value="' . ucwords(trim($ro)) . '">' . ucwords($ro) . '</option>';
                }
            endforeach;
        } else {
            $output .= '<option value="0">Not Found!</option>';
        }
        return $output;
    }

    public function get_college_address_select($selected = null) {
        $allCollege = $this->fuser->get_many_by(array('type' => 'College', 'status' => 'Active'));
        $output = '<option value="">Select One</option>';
        if ($allCollege) {
            $ads = array();
            foreach ($allCollege as $st):
                $ads[] = strtolower($st->address);
            endforeach;
            $ads = array_unique($ads);
            sort($ads);
            foreach ($ads as $ro):
                if ($selected) {
                    $sel = strtolower($selected) == $ro ? 'selected' : '';
                    $output .= '<option value="' . ucwords(trim($ro)) . '" ' . $sel . '>' . ucwords($ro) . '</option>';
                } else {
                    $output .= '<option value="' . ucwords(trim($ro)) . '">' . ucwords($ro) . '</option>';
                }
            endforeach;
        } else {
            $output .= '<option value="0">Not Found!</option>';
        }
        return $output;
    }

    //UPDATE HASH
    public function updateHash($ID, $hash) {
        $this->fuser->update($ID, array('code' => $hash));
    }

    public function updateHashPass($ID, $hash, $password) {
        $this->fuser->update($ID, array('code' => $hash, 'passcode' => $password));
    }

    //REMOVE GALLEY IMAGE
    public function deletegalleyimage($ids) {
        $userID = $this->session->userdata('userID');
        if ($this->fuser->update($userID, array('gallery_picture' => $ids))) {
            return true;
        } else {
            return false;
        }
    }

    //get confirm by password
    public function confirmByPass() {
        $CurrentID = $this->session->userdata('userID');
        $CurrentRow = $this->fuser->get($CurrentID);
        $pass = $this->password_encrypt($this->input->post('conPass'));
        $pass1 = $CurrentRow->passcode;

        if ($pass == $pass1) {
            $data = array('status' => 'success', 'msg' => '');
            echo json_encode($data);
            exit();
        } else {
            $data = array('status' => 'error', 'msg' => 'Sorry unable to post ad, wrong password.');
            echo json_encode($data);
            exit();
        }
    }

    //UPDATE MEMBERSHIP ONLY BY THE CURRENT LOGGED IN USER VIA ESEWA
    public function esewa_memeber_upgrade_instant($ID, $updateTo) {
        $currentID = $this->session->userdata('userID');
        if ($currentID == $ID) {
            $this->fuser->update($ID, array('level' => $updateTo));
        }
    }

    //GET THE STUDENT ID ARRAY BY ADDRESS 
    public function get_student_by_address($address) {
        $array = array();
        if ($address) {
            $rows = $this->fuser->get_many_by(array('address' => $address, 'type' => 'Student', 'status' => ACTIVE));
            if ($rows) {
                foreach ($rows as $row):
                    $array[] = $row->ID;
                endforeach;
            }
            return $array;
        }else {
            return $array;
        }
    }
    //EXTENDED AFTER MAKING NEW ADDRESS SYSTEM
    public function get_student_by_address_extended($addressArray) {
        $array = array();
        
        $searchAddParameters = array();
        if($addressArray['country'] !='0'){
            $searchAddParameters['country'] = $addressArray['country'];
        }
        if($addressArray['province'] !='0'){
            $searchAddParameters['province'] = $addressArray['province'];
        }
        if($addressArray['district'] !='0'){
            $searchAddParameters['district'] = $addressArray['district'];
        }
        if($addressArray['local_level'] !='0'){
            $searchAddParameters['local_level'] = $addressArray['local_level'];
        }
        if($addressArray['ward_no'] !='0'){
            $searchAddParameters['ward_no'] = $addressArray['ward_no'];
        }
        if($addressArray['address'] !='0'){
            $searchAddParameters['address'] = $addressArray['address'];
        }
        
        if ($searchAddParameters) {
            $searchAddParameters['status'] = ACTIVE;
            $searchAddParameters['type'] = 'Student';
            $rows = $this->fuser->get_many_by($searchAddParameters);
            if ($rows) {
                foreach ($rows as $row):
                    $array[] = $row->ID;
                endforeach;
            }
            return $array;
        }else {
            return $array;
        }
    }
    
    public function get_college_by_address_extended($addressArray) {
        $array = array();
        
        $searchAddParameters = array();
        if($addressArray['country'] !='0'){
            $searchAddParameters['country'] = $addressArray['country'];
        }
        if($addressArray['province'] !='0'){
            $searchAddParameters['province'] = $addressArray['province'];
        }
        if($addressArray['district'] !='0'){
            $searchAddParameters['district'] = $addressArray['district'];
        }
        if($addressArray['local_level'] !='0'){
            $searchAddParameters['local_level'] = $addressArray['local_level'];
        }
        if($addressArray['ward_no'] !='0'){
            $searchAddParameters['ward_no'] = $addressArray['ward_no'];
        }
        if($addressArray['address'] !='0'){
            $searchAddParameters['address'] = $addressArray['address'];
        }
        
        if ($searchAddParameters) {
            $searchAddParameters['status'] = ACTIVE;
            $searchAddParameters['type'] = 'College';
            $rows = $this->fuser->get_many_by($searchAddParameters);
            if ($rows) {
                foreach ($rows as $row):
                    $array[] = $row->ID;
                endforeach;
            }
            return $array;
        }else {
            return $array;
        }
    }
    public function get_institute_by_address_extended($addressArray) {
        $array = array();
        
        $searchAddParameters = array();
        if($addressArray['country'] !='0'){
            $searchAddParameters['country'] = $addressArray['country'];
        }
        if($addressArray['province'] !='0'){
            $searchAddParameters['province'] = $addressArray['province'];
        }
        if($addressArray['district'] !='0'){
            $searchAddParameters['district'] = $addressArray['district'];
        }
        if($addressArray['local_level'] !='0'){
            $searchAddParameters['local_level'] = $addressArray['local_level'];
        }
        if($addressArray['ward_no'] !='0'){
            $searchAddParameters['ward_no'] = $addressArray['ward_no'];
        }
        if($addressArray['address'] !='0'){
            $searchAddParameters['address'] = $addressArray['address'];
        }
        
        if ($searchAddParameters) {
            $searchAddParameters['status'] = ACTIVE;
            $searchAddParameters['type'] = 'Institute';
            $rows = $this->fuser->get_many_by($searchAddParameters);
            if ($rows) {
                foreach ($rows as $row):
                    $array[] = $row->ID;
                endforeach;
            }
            return $array;
        }else {
            return $array;
        }
    }

    //GET THE ADDRESS BY ITS ID
    public function get_address_by_id($ID) {
        $row = $this->fuser->get($ID);
        if ($row) {
            return $row->address;
        } else {
            return false;
        }
    }

    //LETS MARK USER AS LOGOUT
    public function make_offline() {
        if ($this->session->userdata('userLogin')) {
            $ID = $this->session->userdata('userID');
            $this->fuser->update($ID, array('online' => 'Offline'));
        }
    }

    //find the current online status
    public function find_online_status($ID) {
        if (isset($ID)) {
            $row = $this->fuser->get($ID);
            if ($row) {
                return $row->online;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    //get small onilnle status
    public function get_online_status($ID) {
        $statusIcon = '';
        if (isset($ID)) {
            $row = $this->fuser->get($ID);
            if ($row) {
                if ($row->online == 'Online') {
                    $statusIcon = '<div class="currentStatus" data-toggle="tooltip" data-placement="top" title="Online"></div>';
                }
            }
        }
        return $statusIcon;
    }

    //GET ADDRESS SELECT BOX BY DATA RECEIVE
    public function get_browse_address_by_filter() {

        $country = $this->input->post('country');
        $province = $this->input->post('province');
        $district = $this->input->post('district');
        $local_level = $this->input->post('local_level');
        $ward_no = $this->input->post('ward_no');
        $type = $this->input->post('type');

        $filterData = array(
            'country' => $country,
            'province' => $province,
            'district' => $district,
            'local_level' => $local_level,
            'ward_no' => $ward_no,
            'status' => ACTIVE
        );


        $rows = $this->fuser->order_by('address', 'ASC')->get_many_by($filterData);
        if (!empty($rows)) {
            $output = '<label>Address</label><div class="drop_list"><select class="form-control" name="browseaddress" id="browseaddress-' . $type . '" data-type="' . $type . '">';
            $output .= '<option selected value="0">Select One</option>';
            foreach ($rows as $row):
                $output .= '<option value="' . $row->address . '">' . $row->address . '</option>';
            endforeach;
            $output .= '</select></div>';
        } else {
            $output = '<label>Address</label><div class="drop_list"><select class="form-control" name="browseaddress" id="browseaddress-' . $type . '" data-type="' . $type . '">';
            $output .= '<option selected value="0">Select One</option>';
            $output .= '</select></div>';
        }

        $datas = array(
            'status' => 'success',
            'selectAddress' => $output
        );
        echo json_encode($datas);
        exit();
    }

    //ADVANCE SEARCH 
    public function get_adv_address_by_filter() {

        $country = $this->input->post('country');
        $province = $this->input->post('province');
        $district = $this->input->post('district');
        $local_level = $this->input->post('local_level');
        $ward_no = $this->input->post('ward_no');
        $type = $this->input->post('type');

        $filterData = array(
            'country' => $country,
            'province' => $province,
            'district' => $district,
            'local_level' => $local_level,
            'ward_no' => $ward_no,
            'status' => ACTIVE
        );


        $rows = $this->fuser->order_by('address', 'ASC')->get_many_by($filterData);
        if (!empty($rows)) {
            $nameID = 'advaddress-' . $type;
            $output = '<label>Address</label><div class="drop_list"><select class="form-control" name="' . $nameID . '" id="' . $nameID . '" data-type="' . $type . '">';
            $output .= '<option selected value="0">Select One</option>';
            foreach ($rows as $row):
                $output .= '<option value="' . $row->address . '">' . $row->address . '</option>';
            endforeach;
            $output .= '</select></div>';
        } else {
            $nameID = 'advaddress-' . $type;
            $output = '<label>Address</label><div class="drop_list"><select class="form-control" name="' . $nameID . '" id="' . $nameID . '" data-type="' . $type . '">';
            $output .= '<option selected value="0">Select One</option>';
            $output .= '</select></div>';
        }

        $datas = array(
            'status' => 'success',
            'selectAddress' => $output
        );
        echo json_encode($datas);
        exit();
    }

    //BROWSE BY COUNTRY
    public function users_by_country() {
        $country = $this->input->post('country');
        $type = $this->input->post('type');

        $adTable = $type . '_ad';

        $query_string = "SELECT a.ID FROM $adTable a JOIN $type b ON b.parent_ID = a.parent_ID JOIN users c ON a.parent_ID = c.ID WHERE c.status = 'Active' AND a.status = 'Active' AND c.country LIKE '%$country%'";
        $query = $this->db->query($query_string);
        $ads = makeArrayID($query->result());
        $data['method'] = ucfirst($type);
        $data['type'] = 'location';
        $data['allAds'] = $ads;
        $newView = $this->load->view('search/browse/browse', $data, true);
        $data = array(
            'status' => 'success',
            'msg' => 'Please wait collecting reasults',
            'newView' => $newView,
        );
        echo json_encode($data);
        exit();
    }

    //BROWSE BY COUNTRY PROVINCE
    public function users_by_country_province() {
        $country = $this->input->post('country');
        $province = $this->input->post('province');
        $type = $this->input->post('type');

        $adTable = $type . '_ad';

        $query_string = "SELECT a.ID FROM $adTable a JOIN $type b ON b.parent_ID = a.parent_ID JOIN users c ON a.parent_ID = c.ID WHERE c.status = 'Active' AND a.status = 'Active' AND c.country LIKE '%$country%' AND c.province LIKE '%$province%'";
        $query = $this->db->query($query_string);
        $ads = makeArrayID($query->result());

        $data['method'] = ucfirst($type);
        $data['type'] = 'location';
        $data['allAds'] = $ads;
        $newView = $this->load->view('search/browse/browse', $data, true);
        $newView = !empty($newView) ? $newView : 'No data found';
        $data = array(
            'status' => 'success',
            'msg' => 'Please wait collecting reasults',
            'newView' => $newView,
        );
        echo json_encode($data);
        exit();
    }

    //BROWSE BY COUNTRY PROVINCE DISTIRCT
    public function users_by_country_province_dis() {
        $country = $this->input->post('country');
        $province = $this->input->post('province');
        $district = $this->input->post('district');
        $type = $this->input->post('type');

        $adTable = $type . '_ad';

        $query_string = "SELECT a.ID FROM $adTable a JOIN $type b ON b.parent_ID = a.parent_ID JOIN users c ON a.parent_ID = c.ID WHERE c.status = 'Active' AND a.status = 'Active' AND c.country LIKE '%$country%' AND c.province LIKE '%$province%' AND c.district LIKE '%$district%'";
        $query = $this->db->query($query_string);
        $ads = makeArrayID($query->result());
        $data['method'] = ucfirst($type);
        $data['type'] = 'location';
        $data['allAds'] = $ads;
        $newView = $this->load->view('search/browse/browse', $data, true);
        $data = array(
            'status' => 'success',
            'msg' => 'Please wait collecting reasults',
            'newView' => $newView,
        );
        echo json_encode($data);
        exit();
    }

    //BROWSE BY COUNTRY PROVINCE DISTIRCT LOCAL LEVEL
    public function users_by_country_province_dis_lc() {
        $country = $this->input->post('country');
        $province = $this->input->post('province');
        $district = $this->input->post('district');
        $local_level = $this->input->post('local_level');
        $type = $this->input->post('type');

        $adTable = $type . '_ad';

        $query_string = "SELECT a.ID FROM $adTable a JOIN $type b ON b.parent_ID = a.parent_ID JOIN users c ON a.parent_ID = c.ID WHERE c.status = 'Active' AND a.status = 'Active' AND c.country LIKE '%$country%' AND c.province LIKE '%$province%' AND c.district LIKE '%$district%' AND c.local_level LIKE '%$local_level%'";
        $query = $this->db->query($query_string);
        $ads = makeArrayID($query->result());
        $data['method'] = ucfirst($type);
        $data['type'] = 'location';
        $data['allAds'] = $ads;
        $newView = $this->load->view('search/browse/browse', $data, true);
        $data = array(
            'status' => 'success',
            'msg' => 'Please wait collecting reasults',
            'newView' => $newView,
        );
        echo json_encode($data);
        exit();
    }

    //BROWSE BY COUNTRY PROVINCE DISTIRCT LOCAL LEVEL
    public function users_by_country_province_dis_lc_wn() {
        $country = $this->input->post('country');
        $province = $this->input->post('province');
        $district = $this->input->post('district');
        $local_level = $this->input->post('local_level');
        $ward_no = $this->input->post('ward_no');
        $type = $this->input->post('type');

        $adTable = $type . '_ad';

        $query_string = "SELECT a.ID FROM $adTable a JOIN $type b ON b.parent_ID = a.parent_ID JOIN users c ON a.parent_ID = c.ID WHERE c.status = 'Active' AND a.status = 'Active' AND c.country LIKE '%$country%' AND c.province LIKE '%$province%' AND c.district LIKE '%$district%' AND c.local_level LIKE '%$local_level%' AND c.ward_no LIKE '%$ward_no%'";
        $query = $this->db->query($query_string);
        $ads = makeArrayID($query->result());

        $data['method'] = ucfirst($type);
        $data['type'] = 'location';
        $data['allAds'] = $ads;
        $newView = $this->load->view('search/browse/browse', $data, true);
        $data = array(
            'status' => 'success',
            'msg' => 'Please wait collecting reasults',
            'newView' => $newView,
        );
        echo json_encode($data);
        exit();
    }

    //BROWSE BY COUNTRY PROVINCE DISTIRCT LOCAL LEVEL ADDRESS
    public function users_by_country_province_dis_lc_wn_address() {
        $country = $this->input->post('country');
        $province = $this->input->post('province');
        $district = $this->input->post('district');
        $local_level = $this->input->post('local_level');
        $ward_no = $this->input->post('ward_no');
        $address = $this->input->post('address');
        $type = $this->input->post('type');

        $adTable = $type . '_ad';

        $query_string = "SELECT a.ID FROM $adTable a JOIN $type b ON b.parent_ID = a.parent_ID JOIN users c ON a.parent_ID = c.ID WHERE c.status = 'Active' AND a.status = 'Active' AND c.country LIKE '%$country%' AND c.province LIKE '%$province%' AND c.district LIKE '%$district%' AND c.local_level LIKE '%$local_level%' AND c.ward_no LIKE '%$ward_no%' AND c.address LIKE '%$address%'";
        $query = $this->db->query($query_string);
        $ads = makeArrayID($query->result());
        $data['method'] = ucfirst($type);
        $data['type'] = 'location';
        $data['allAds'] = $ads;
        $newView = $this->load->view('search/browse/browse', $data, true);
        $data = array(
            'status' => 'success',
            'msg' => 'Please wait collecting reasults',
            'newView' => $newView,
        );
        echo json_encode($data);
        exit();
    }

    //ADMIN LOGIN WITH
    public function admlogin($key = null, $type = null, $id = null) {

        //CHEKC BROWSER IS ADMIN LOGIN OR NOT
        if ($this->session->userdata('adminLogged') && $this->session->userdata('adminName') && $this->session->userdata('adminEmail')) {
            //CONFIRM ADMIN IS LOGGED IN NOW
            if (isset($key) && !empty($key) && isset($type) && !empty($type) && isset($id) && !empty($id)) {
                //CONFIRM THE PARAMETERS
                if ($type == 'Student' || $type == 'College' || $type == 'Institute') {

                    $userRow = $this->fuser->get_by(array('code' => $key, 'type' => $type, 'ID' => $id));
                    if ($userRow) {
                        if ($userRow->status == ACTIVE) {
                            $sessArray = array(
                                'userID' => $userRow->ID,
                                'username' => $userRow->ID,
                                'userName' => $userRow->name,
                                'chatusername' => $userRow->name,
                                'userEmail' => $userRow->email,
                                'userType' => $userRow->type,
                                'userLevel' => $userRow->level,
                                'userLogin' => TRUE
                            );
                            $this->session->set_userdata($sessArray);
                            //RECORD IP AND TIME OF LOGIN
                            $this->fuser->update($userRow->ID, array('last_login' => get_date_time(), 'last_IP' => $this->input->ip_address()));
                            //MAKE USER STATUS ONLINE
                            $this->fuser->update($userRow->ID, array('online' => 'Online'));
                            //redirect to respective dashbaord of user
                            redirect('dashboard');
                            exit();
                        } else {
                            redirect('site');
                            exit();
                        }
                    } else {
                        redirect('site');
                        exit();
                    }
                } else {
                    redirect('site');
                    exit();
                }
            } else {
                redirect('site');
                exit();
            }
        } else {
            redirect('site');
            exit();
        }
    }

}
