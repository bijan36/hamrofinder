<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends Frontendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('Settings_m', 'fsettings');
    }

    //USER DASHBOARD
    public function index() {
        redirect('site');
        exit();
    }

    //GETTING VALUES
    public function get_value($slug) {
        if ($slug) {
            $row = $this->fsettings->get_by(array('slug' => $slug));
            if ($row) {
                return $row->val;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    //SELECT BOX
    public function get_selectbox() {
        $searchfor = $this->input->post('searchingfor');
        $output = ''; //PROGRAM OR COURSE
        $output1 = ''; //AFFILIATION OR CERTFIED
        if ($searchfor == COLLEGE) {
            $slug = 'college-ad-course-looking-for';
            $seloptions = get_select_box($slug);
            $output = '<div class="form-group"><label for="looking_for">Looking For Program</label><div class="drop_list"><select class="form-control" name="looking_for">';
            $output .= $seloptions;
            $output .= '</select></div></div>';

            // FOR AFFILICATOIN
            $slug1 = 'student-ad-affiliation';
            $seloptions1 = get_select_box($slug1);
            $output1 = '<div class="form-group"><label for="affiliation">Affiliation</label><div class="drop_list"><select class="form-control" name="affiliation">';
            $output1 .= $seloptions1;
            $output1 .= '</select></div></div>';
        } elseif ($searchfor == INSTITUTE) {
            $slug = 'institute-course-options';
            $seloptions = get_select_box($slug);
            $output = '<div class="form-group"><label for="looking_for">Looking For Course</label><div class="drop_list"><select class="form-control" name="looking_for">';
            $output .= $seloptions;
            $output .= '</select></div></div>';

            // FOR CERTIFIED BY
            $slug1 = 'institute-certified-by-options';
            $seloptions1 = get_select_box($slug1);
            $output1 = '<div class="form-group"><label for="affiliation">Certified By</label><div class="drop_list"><select class="form-control" name="affiliation">';
            $output1 .= $seloptions1;
            $output1 .= '</select></div></div>';
        } else {
            //for blank box
            $slug = 'nothing';
            $slug1 = 'nothing';
            $seloptions = get_select_box($slug);
            $output = '<div class="form-group"><label for="looking_for">Looking For Course</label><div class="drop_list"><select class="form-control" name="">';
            $output .= $seloptions;
            $output .= '</select></div></div>';

            // FOR CERTIFIED BY
            $slug1 = 'institute-certified-by-options';
            $seloptions1 = get_select_box($slug1);
            $output1 = '<div class="form-group"><label for="affiliation">Certified By</label><div class="drop_list"><select class="form-control" name="">';
            $output1 .= $seloptions1;
            $output1 .= '</select></div></div>';
        }

        $data = array(
            'status' => 'success',
            'resultSelect' => $output,
            'resultSelectAff' => $output1
        );
        echo json_encode($data);
        exit();
    }

}
