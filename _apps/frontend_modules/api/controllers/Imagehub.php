<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Imagehub extends Apicontroller {

    /**
     * @ Author: Suraj Bajracharya
     * @ Author URL : www.indesignmedia.net
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     *
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('imagehub_m', 'imagehub');
    }

    /*
     * **************************************************************
     * @ IMAGE CONTROLLERS
     * ************************************************************* 
     */

    public function studentpicupdate() {
        //var_dump('expression');die();
        if (is_user_login()) {
            if ($this->input->post('addtolib') == 'yes') {
                $imgName = $_FILES['student_profile_pic']['name'];
                if (!empty($imgName)) {
                    $class = $this->input->post('classname');
                    $datas = $this->imagehub->do_upload_ajax('student_profile_pic', $class);
                } else {
                    $datas = array(
                        'status' => 'error',
                        'msg' => 'Not uploaded'
                    );
                }
            } else {
                $datas = array(
                    'status' => 'error',
                    'msg' => 'Not allowed'
                );
            }
        }else{
            $datas = array(
                'status'    => 'error',
                'msg'   => 'Please Login to Continue'
            );
        }
        echo json_encode($datas);
        exit();
    }

}
