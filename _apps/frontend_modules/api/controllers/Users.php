<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends Apicontroller {

    public function __construct() {
        parent::__construct();
        $this->load->model('User_m', 'fuser');
    }

   public function index() {
    	$cars = array("Volvo", "BMW", "Toyota");
		echo json_encode($cars);    
	}

	public function register(){
		//var_dump($_POST);
		 //FORM VALIDATION
        $this->form_validation->set_rules('searchtype', 'Search type', 'required|in_list[student,college,institute]');
        $this->form_validation->set_rules('name', 'Name', 'required|trim|xss_clean');
        $this->form_validation->set_rules('contact_no', 'Contact No', 'required|trim|xss_clean');
        $this->form_validation->set_rules('emailAddress', 'Email Address', 'required|valid_email');
        //$this->form_validation->set_rules('password', 'Password', 'required|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|max_length[30]', array('required' => 'You must provide a %s.')
        );
        $this->form_validation->set_rules('passCodeConfirm', 'Confirm Password', 'required|trim|matches[password]', array(
            'required' => 'Confirm password is required',
            'matches' => 'Confirm password is not matched'
                )
        );

        if ($this->form_validation->run() == TRUE) {
        	$user = Modules::run('user/is_exist', $this->input->post('emailAddress'));
            if ($user) {
                $data = array(
                    'status' => 'error',
                    'msg' => 'You are already registered with us. Please login to continue.'
                );

                echo json_encode($data);
            } else {

                $searchType = $this->input->post('searchtype');
                //$type = $searchType == 'student' ? 'College' : 'Student';
                //$type = $searchType;
                //DEFAULT LEVEL
                $level = $searchType == 'student' ? 'Registered' : 'Registered'; //start user level as Registered in both way

                $rowPassword = $this->input->post('password');
                $code = get_hash();

                $name = $this->input->post('name');
                $email = $this->input->post('emailAddress');


                $dataRegisted = array(
                    'type' => $searchType,
                    'name' => $name,
                    'contact' => $this->input->post('contact_no'),
                    'email' => $this->input->post('emailAddress'),
                    'level' => $level,
                    'join_date' => get_date_time(),
                    'last_IP' => $this->input->ip_address(),
                    'passcode' => Modules::run('user/makepassword', $rowPassword),
                    'code' => $code
                );

                $reg = Modules::run('user/get_registered', $dataRegisted);

                if ($reg) {
                    //LETS DO EMAIL TO USER
                    $data['to'] = $email;
                    $data['toName'] = $name;
                    $data['subject'] = 'hamrofinder.com user verification';
                    $data['template'] = 'user_registration_activation_email';
                    $data['others'] = array(
                        'userfullname' => $name,
                        'emailaddress' => $email,
                        'password' => $rowPassword,
                        'activation_url' => site_url('user/activate/' . $reg . '/' . $code)
                    );
                    Modules::run('emailsystem/doEmail', $data);
                    //LETS DO EMAIL TO USER

                    $data = array(
                        'status' => 'success',
                        'msg' => 'Congratulations! ' . $name . '<br/>Please verify our verification email soon. Please do check Junk/Spam folder. First emails sometimes hide there.'
                    );

                    echo json_encode($data);
                } else {
                    $data = array(
                        'status' => 'error',
                        'msg' => 'Unable to register now, Please try again later'
                    );
                    echo json_encode($data);
                }
            }        }else {
            //FORM VALIDAITON ERROR
            $data = array(
                'status' => 'error',
                'msg' => validation_errors()
            );

            echo json_encode($data);	
        }
	}

	public function login(){
		 $this->form_validation->set_rules('emailAddress', 'Email Address', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required|trim');

        if ($this->form_validation->run() == TRUE) {
            $queryData = array(
                'email' => $this->input->post('emailAddress'),
                'passcode' => $this->password_encrypt($this->input->post('password'))
            );

            //VALIDATE HERE
            $this->fuser->logincheck($queryData);
        } else {
            $data = array(
                'status' => 'error',
                'msg' => validation_errors()
            );
        }
        echo json_encode($data);
        exit();
	}

	private function password_encrypt($password) {
        $getTo = $password . $this->get_crypt();
        $hash = hash('sha512', $getTo);
        return $hash;
    }

    private function get_crypt() {
        $st = $this->config->item('encryption_key');
        return substr($st, 5, 14);
    }

     //SHOWING USER PROFILE
    public function show() {
        $userID = $this->input->post('userID');
        if (is_user_login()) {
            if (isset($userID)) {
                $userType = $this->session->userdata('userType');
                $userLevel = $this->session->userdata('userLevel');

                //LEVEL ATHONTICATION FOR THE SHOW AN INTEREST ACTION FOR USERS
                if ($userType == 'College') {
                    $interestAuth = Modules::run('college_membership_plans/find_authorise', $userLevel, 'profile');
                    if ($interestAuth == 'No') {
                        //THE RULE IS ONLY VALID FOR 
                        //NOT THEIR OWN PROFILE
                        //BUT OTHERS PROFILE WANT TO VIEW
                        if (get_college_ID() != $userID) {
                            $data = array(
                                'status' => 'error',
                                'msg' => 'User not found'
                            );
                            echo json_encode($data);
                            exit();
                        }
                    }
                }

                if ($userType == 'Institute') {
                    $interestAuth = Modules::run('institute_membership_plans/find_authorise', $userLevel, 'profile');
                    if ($interestAuth == 'No') {
                        //THE RULE IS ONLY VALID FOR 
                        //NOT THEIR OWN PROFILE
                        //BUT OTHERS PROFILE WANT TO VIEW
                        if (get_institute_ID() != $userID) {
                            $data = array(
                                'status' => 'error',
                                'msg' => 'User not found'
                            );
                            echo json_encode($data);
                            exit();
                        }
                    }
                }

                //LEVEL ATHONTICATION FOR THE SHOW AN INTEREST ACTION FOR USERS
                //IS USER VALID ?
                if (is_valid_profile($userID)) {
                    $userRow = $this->get_row($userID);
                    if ($userRow) {
                        if ($userRow->type != $this->session->userdata('userType')) {

                            //DONT ALLOW COLLEGE TO VIEW INSTITUTE PROFILE
                            if ($this->session->userdata('userType') == 'College' && $userRow->type == 'Institute') {
                                $data = array(
                                'status' => 'error',
                                'msg' => 'User not found'
                            );
                            echo json_encode($data);
                                exit();
                            }
                            //DONT ALLOW INSTITUTE TO VIEW COLLEGE PROFILE
                            if ($this->session->userdata('userType') == 'Institute' && $userRow->type == 'College') {
                                $data = array(
                                'status' => 'error',
                                'msg' => 'User not found'
                            );
                            echo json_encode($data);
                                exit();
                            }
                            //TO LET OTHER TO VEIW PROFILE
                            $data = array(
                                    'status'  => 'success',
                                );
                                $data['userRow'] = $userRow;
                                echo json_encode($data);
                           // $this->template->load(get_template($userRow->type . '-profile'), $userRow->type . '-profile', $data);
                        } else {
                            //JUST FOR SELF VEIW PROFILE
                            //var_dump('sadsa');die();
                            if ($userRow->ID == $this->session->userdata('userID')) {
                                $data['userRow'] = $userRow;
                                $data = array(
                                    'status'  => 'success',
                                    'userRow'   => $userRow
                                );
                                echo json_encode($data);
                                //var_dump($userRow);die();
                                //$this->template->load(get_template($userRow->type . '-profile'), $userRow->type . '-profile', $data);
                            } else {
                                $data = array(
                                'status' => 'error',
                                'msg' => 'User not found'
                            );
                            echo json_encode($data);
                                exit();
                            }
                        }
                    } else {
                        $data = array(
                                'status' => 'error',
                                'msg' => 'User not found'
                            );
                            echo json_encode($data);
                        exit();
                    }
                } else {
                    $data = array(
                                'status' => 'error',
                                'msg' => 'User not Valid'
                            );
                            echo json_encode($data);
                    exit();
                }
            } else {
                $data = array(
                'status' => 'error',
                'msg' => 'User id not set'
            );
                echo json_encode($data);
                exit();
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => 'Please Login to process',
            );
            echo json_encode($data);
        }
    }


    public function get_row($ID) {
        $userRow = $this->fuser->get($ID);
        if ($userRow) {
            unset($userRow->passcode);
            unset($userRow->code);
            return $userRow;
        } else {
            return false;
        }
    }

}
