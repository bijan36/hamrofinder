<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends Apicontroller {

    public function __construct() {
        parent::__construct();
        if (is_user_login()) {
            //MAKE USER OFFLINE BEFORE LOGOUT
            Modules::run('user/make_offline');
            $this->clearCache();
            //lets logout users
            $dataToUnset = array('userID', 'userName', 'userEmail', 'userLogin','username','chatusername','userType','userLevel','chatHistory','openChatBoxes','tsChatBoxes');           
            $this->session->unset_userdata($dataToUnset);
            //exit();
        } else {
            //MAKE USER OFFLINE BEFORE LOGOUT
            Modules::run('user/make_offline');
            $this->clearCache();
            $dataToUnset = array('userID', 'userName', 'userEmail', 'userLogin');
            $this->session->unset_userdata($dataToUnset);
        }
    }

    //DEFAULT
    public function index() {
        $data['logout'] = true;
        $datas = array(
                'status' => 'success',
                'msg' => 'Logout successfully'
            );
        echo json_encode($datas);
       //$this->template->load(get_template('default'), 'login/login', $data);
        //$this->template->load(get_template('fullwidth'), 'logout');
    }

    protected function clearCache() {
        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
        $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    }

}