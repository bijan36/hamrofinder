<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class College_membership_plans extends Frontendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('College_membership_plans_m', 'fcolmem');
    }

    //STUDENT LIST
    public function index() {
        redirect('site');
        exit();
    }

    public function get_all_plans() {
        $rows = $this->fcolmem->order_by('position', 'ASC')->get_all();
        return $rows ? $rows : false;
    }

    public function find_authorise($levelName, $index) {
        $row = $this->fcolmem->get_by(array('title' => $levelName));
        if ($row) {
            if (!empty($row->privileged)) {

                if (is_serialized($row->privileged)) {
                    $packs = unserialize($row->privileged);
                    if (is_array($packs)) {
                        if ($packs[$index]) {
                            if ($packs[$index] == 'Yes') {
                                return $packs[$index];
                            } else {
                                return 'No';
                            }
                        } else {
                            return 'No';
                        }
                    } else {
                        return 'No';
                    }
                } else {
                    return 'No';
                }
            } else {
                return 'No';
            }
        } else {
            return 'No';
        }
    }

    public function get_plan_by_name($planName) {
        //THIS FUNCTION ONLY AVAILABLE FOR COLLEGE LOGIN UESR
        if (!is_user_login() && is_user_college()) {
            redirect('login');
            exit();
        }

        $row = $this->fcolmem->get_by(array('title' => $planName));
        if ($row) {
            return $row;
        } else {
            return false;
        }
    }

    public function get_plan_by_ID($ID) {
        if (!is_user_login() && is_user_college()) {
            redirect('login');
            exit();
        }
        $row = $this->fcolmem->get_by(array('ID'=>$ID, 'Active'));
        if ($row) {
            return $row;
        } else {
            return false;
        }
    }

}
