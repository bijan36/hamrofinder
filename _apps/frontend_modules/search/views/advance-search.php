<?php
if (is_array($allAds) && !empty($allAds)) {
    ?>
    <?php echo count($allAds) == 1 ? '<div class="row no-gutter-single-item">' : '<div class="row equal-item-holder no-gutter">'; ?>

    <?php if ($addType == 'Student') { ?>
        <div class="searchParameters">
            <?php
            if ($searchParam) {
                echo '<div class="row">';
                echo '<div class="col-md-2"><span class="advSearchHeading">Search Results for: </span></div><div class="col-md-10" id="searchFilters">';
                $allParam = '';
                $allSepa = '';
                foreach ($searchParam as $spk => $spv):
                    if ($spk == 'student_gender') {
                        echo '<span class="advSearchLebel-no" data-kee="' . $spk . '">' . ucfirst(str_replace('_', ' ', $spk)) . ': ' . $spv . '</span>';
                        /* if ($spv == 'Male') {
                          echo '<span class="advSearchLebel-no" data-kee="' . $spk . '">' . ucfirst(str_replace('_', ' ', $spk)) . ': ' . $spv . '</span>';
                          } elseif ($spv == 'Female') {
                          echo '<span class="advSearchLebel-no" data-kee="' . $spk . '">' . ucfirst(str_replace('_', ' ', $spk)) . ': ' . $spv . '</span>';
                          } else {
                          echo '<span class="advSearchLebel" data-kee="Male">Male</span>';
                          echo '<span class="advSearchLebel" data-kee="Female">Female</span>';
                          } */
                    } else {
                        if ($spk != 'searchTypeSearch') {
                            if ($spk == 'advcountry-student') {
                                echo '<span class="inactiveBtn" data-kee="' . $spk . '">Country: ' . $spv . '</span>';
                            } elseif ($spk == 'advprovince-student') {
                                echo '<span class="inactiveBtn" data-kee="' . $spk . '">Province: ' . $spv . '</span>';
                            } elseif ($spk == 'advdistrict-student') {
                                echo '<span class="inactiveBtn" data-kee="' . $spk . '">District: ' . $spv . '</span>';
                            } elseif ($spk == 'advlocal_level-student') {
                                echo '<span class="inactiveBtn" data-kee="' . $spk . '">Local Level: ' . $spv . '</span>';
                            } elseif ($spk == 'advward_no-student') {
                                echo '<span class="inactiveBtn" data-kee="' . $spk . '">Ward No: ' . $spv . '</span>';
                            } elseif ($spk == 'advaddress-student') {
                                echo '<span class="inactiveBtn" data-kee="' . $spk . '">Address: ' . $spv . '</span>';
                            } else {
                                echo '<span class="inactiveBtn" data-kee="' . $spk . '">' . ucfirst(str_replace('_', ' ', $spk)) . ': ' . $spv . '</span>';
                            }

                            $allParam .= $allSepa . $spk;
                            $allSepa = ',';
                        }
                    }
                endforeach;
                echo '</div></div>';
            }
            ?>
        </div>
    <?php } elseif ($addType == 'College') { ?>
        <div class="searchParameters">
            <?php
            if ($searchParam) {
                //print_r($searchParam);
                echo '<div class="row">';
                echo '<div class="col-md-2"><span class="advSearchHeading">Search Results for: </span></div><div class="col-md-10" id="searchFilters">';
                $allParam = '';
                $allSepa = '';
                foreach ($searchParam as $spk => $spv):
                    //if ($spk == 'entrance_required' || $spk == 'college_bus') {
                    if ($spk == 'entrance_required') {
                        echo '<span class="advSearchLebel-no" data-kee="' . $spk . '">' . ucfirst(str_replace('_', ' ', $spk)) . ': ' . $spv . '</span>';
                        /* if ($spv == 'Male') {
                          echo '<span class="advSearchLebel-no" data-kee="' . $spk . '">' . ucfirst(str_replace('_', ' ', $spk)) . ': ' . $spv . '</span>';
                          } elseif ($spv == 'Female') {
                          echo '<span class="advSearchLebel-no" data-kee="' . $spk . '">' . ucfirst(str_replace('_', ' ', $spk)) . ': ' . $spv . '</span>';
                          } else {
                          echo '<span class="advSearchLebel" data-kee="Male">Male</span>';
                          echo '<span class="advSearchLebel" data-kee="Female">Female</span>';
                          } */
                    } elseif ($spk == 'college_facilities') {
                        //KEYS FOR THE FACILITIES
                        if (!empty($spv)) {
                            ?>
                            <div class="advSearchBoarder">
                                <h4>College Facilities:</h4>
                                <?php foreach ($spv as $ssppvv): ?>
                                    <span class="inactiveBtn" data-kee="<?php echo strtolower(str_replace(' ', '_', $ssppvv)); ?>"><?php echo $ssppvv; ?></span>
                                <?php endforeach; ?>
                            </div>
                            <?php
                        }
                    } elseif ($spk == 'college_security') {
                        //KEYS FOR THE SECIRITIES
                        if (!empty($spv)) {
                            ?>
                            <div class="advSearchBoarder">
                                <h4>College Securities:</h4>
                                <?php foreach ($spv as $ssppvv): ?>
                                    <span class="inactiveBtn" data-kee="<?php echo strtolower(str_replace(' ', '_', $ssppvv)); ?>"><?php echo $ssppvv; ?></span>
                                <?php endforeach; ?>
                            </div>
                            <?php
                        }
                    } elseif ($spk == 'college_additionals') {
                        //KEYS FOR THE SECIRITIES
                        if (!empty($spv)) {
                            ?>
                            <div class="advSearchBoarder">
                                <h4>Additional:</h4>
                                <?php foreach ($spv as $ssppvv): ?>
                                    <span class="inactiveBtn" data-kee="<?php echo strtolower(str_replace(' ', '_', $ssppvv)); ?>"><?php echo $ssppvv; ?></span>
                                <?php endforeach; ?>
                            </div>
                            <?php
                        }
                    } else {
                        if ($spk != 'searchTypeSearch') {

                            if ($spk == 'advcountry-college') {
                                echo '<span class="inactiveBtn" data-kee="' . $spk . '">Country: ' . $spv . '</span>';
                            } elseif ($spk == 'advprovince-college') {
                                echo '<span class="inactiveBtn" data-kee="' . $spk . '">Province: ' . $spv . '</span>';
                            } elseif ($spk == 'advdistrict-college') {
                                echo '<span class="inactiveBtn" data-kee="' . $spk . '">District: ' . $spv . '</span>';
                            } elseif ($spk == 'advlocal_level-college') {
                                echo '<span class="inactiveBtn" data-kee="' . $spk . '">Local Level: ' . $spv . '</span>';
                            } elseif ($spk == 'advward_no-college') {
                                echo '<span class="inactiveBtn" data-kee="' . $spk . '">Ward No: ' . $spv . '</span>';
                            } elseif ($spk == 'advaddress-college') {
                                echo '<span class="inactiveBtn" data-kee="' . $spk . '">Address: ' . $spv . '</span>';
                            } else {
                                echo '<span class="inactiveBtn" data-kee="' . $spk . '">' . ucfirst(str_replace('_', ' ', $spk)) . ': ' . $spv . '</span>';
                            }

                            $allParam .= $allSepa . $spk;
                            $allSepa = ',';
                        }
                    }
                endforeach;
                echo '</div></div>';
            }
            ?>
        </div>
    <?php } else { ?>
        <div class="searchParameters">
            <?php
            if ($searchParam) {
                //print_r($searchParam);
                echo '<div class="row">';
                echo '<div class="col-md-2"><span class="advSearchHeading">Search Results for: </span></div><div class="col-md-10" id="searchFilters">';
                $allParam = '';
                $allSepa = '';
                foreach ($searchParam as $spk => $spv):
                    //if ($spk == 'entrance_required' || $spk == 'college_bus') {
                    if ($spk == 'entrance_required') {
                        echo '<span class="advSearchLebel-no" data-kee="' . $spk . '">' . ucfirst(str_replace('_', ' ', $spk)) . ': ' . $spv . '</span>';
                        /* if ($spv == 'Male') {
                          echo '<span class="advSearchLebel-no" data-kee="' . $spk . '">' . ucfirst(str_replace('_', ' ', $spk)) . ': ' . $spv . '</span>';
                          } elseif ($spv == 'Female') {
                          echo '<span class="advSearchLebel-no" data-kee="' . $spk . '">' . ucfirst(str_replace('_', ' ', $spk)) . ': ' . $spv . '</span>';
                          } else {
                          echo '<span class="advSearchLebel" data-kee="Male">Male</span>';
                          echo '<span class="advSearchLebel" data-kee="Female">Female</span>';
                          } */
                    } elseif ($spk == 'college_facilities') {
                        //KEYS FOR THE FACILITIES
                        if (!empty($spv)) {
                            ?>
                            <div class="advSearchBoarder">
                                <h4>College Facilities:</h4>
                                <?php foreach ($spv as $ssppvv): ?>
                                    <span class="inactiveBtn" data-kee="<?php echo strtolower(str_replace(' ', '_', $ssppvv)); ?>"><?php echo $ssppvv; ?></span>
                                <?php endforeach; ?>
                            </div>
                            <?php
                        }
                    } elseif ($spk == 'college_security') {
                        //KEYS FOR THE SECIRITIES
                        if (!empty($spv)) {
                            ?>
                            <div class="advSearchBoarder">
                                <h4>College Securities:</h4>
                                <?php foreach ($spv as $ssppvv): ?>
                                    <span class="inactiveBtn" data-kee="<?php echo strtolower(str_replace(' ', '_', $ssppvv)); ?>"><?php echo $ssppvv; ?></span>
                                <?php endforeach; ?>
                            </div>
                            <?php
                        }
                    } elseif ($spk == 'college_additionals') {
                        //KEYS FOR THE SECIRITIES
                        if (!empty($spv)) {
                            ?>
                            <div class="advSearchBoarder">
                                <h4>Additional:</h4>
                                <?php foreach ($spv as $ssppvv): ?>
                                    <span class="inactiveBtn" data-kee="<?php echo strtolower(str_replace(' ', '_', $ssppvv)); ?>"><?php echo $ssppvv; ?></span>
                                <?php endforeach; ?>
                            </div>
                            <?php
                        }
                    } else {
                        if ($spk != 'searchTypeSearch') {
                            if ($spk == 'advcountry-institute') {
                                echo '<span class="inactiveBtn" data-kee="' . $spk . '">Country: ' . $spv . '</span>';
                            } elseif ($spk == 'advprovince-institute') {
                                echo '<span class="inactiveBtn" data-kee="' . $spk . '">Province: ' . $spv . '</span>';
                            } elseif ($spk == 'advdistrict-institute') {
                                echo '<span class="inactiveBtn" data-kee="' . $spk . '">District: ' . $spv . '</span>';
                            } elseif ($spk == 'advlocal_level-institute') {
                                echo '<span class="inactiveBtn" data-kee="' . $spk . '">Local Level: ' . $spv . '</span>';
                            } elseif ($spk == 'advward_no-institute') {
                                echo '<span class="inactiveBtn" data-kee="' . $spk . '">Ward No: ' . $spv . '</span>';
                            } elseif ($spk == 'advaddress-institute') {
                                echo '<span class="inactiveBtn" data-kee="' . $spk . '">Address: ' . $spv . '</span>';
                            } else {
                                echo '<span class="inactiveBtn" data-kee="' . $spk . '">' . ucfirst(str_replace('_', ' ', $spk)) . ': ' . $spv . '</span>';
                            }
                            $allParam .= $allSepa . $spk;
                            $allSepa = ',';
                        }
                    }
                endforeach;
                //print_r($dynamicTag);
                //exit();
                echo '</div></div>';
            }
            ?>
        </div>
    <?php } ?>

    <?php
    foreach ($allAds as $ad):
        if ($addType == 'Student') {
            $adRow = Modules::run('studentad/get_row', $ad);
            if (!empty($adRow)) {
                $data['row'] = $adRow;
                $data['dynamicTag'] = $dynamicTag;
                $data['addressValues'] = $addressValues;
                $this->load->view('studentad/loop_student_common_search', $data);
            }
        } elseif ($addType == 'College') {
            $adRow = Modules::run('collegead/get_row', $ad);
            if (!empty($adRow)) {
                $data['row'] = $adRow;
                $data['addressValues'] = $addressValues;
                $this->load->view('collegead/loop_college_common_search', $data);
            }
        } elseif ($addType == 'Institute') {
            $adRow = Modules::run('institutead/get_row', $ad);
            if (!empty($adRow)) {
                $data['row'] = $adRow;
                $data['addressValues'] = $addressValues;
                $this->load->view('institutead/loop_institute_common_search', $data);
            }
        } else {
            echo 'Sorry! can not make request completed.';
        }

    endforeach;
    ?>
    </div>
    <?php
} else {

    echo "Sorry no result found :)";
}
?>