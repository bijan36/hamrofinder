<table class="table table-bordered">
    <?php
    if (is_array($allAds)) {
        if (!empty($allAds)) {
            
            $sn = 1;
            $snc = 1;
            foreach ($allAds as $ad):
                if ($method == 'Student') {
                    $adRow = Modules::run('studentad/get_row', $ad);
                    if (!empty($adRow)) {
                        if (can_show_ad($adRow->ID, $adRow->parent_ID)) {
                            $data['row'] = $adRow;
                            $data['sn'] = $sn;
                            $this->load->view('studentad/browse/' . $type, $data);
                            $sn++;
                        }
                    }
                } elseif ($method == 'College') {
                    $adRow = Modules::run('collegead/get_row', $ad);
                    if (!empty($adRow)) {
                        if (can_show_ad($adRow->ID, $adRow->parent_ID)) {
                            $data['row'] = $adRow;
                            $data['snc'] = $snc;
                            $this->load->view('collegead/browse/' . $type, $data);
                            $snc++;
                        }
                    }
                } else {
                    $adRow = Modules::run('institutead/get_row', $ad);
                    if (!empty($adRow)) {
                        if (can_show_ad($adRow->ID, $adRow->parent_ID)) {
                            $data['row'] = $adRow;
                            $data['snc'] = $snc;
                            $this->load->view('institutead/browse/' . $type, $data);
                            $snc++;
                        }
                    }
                }
            endforeach;
        } else {
            echo "Sorry no result found :)";
        }
    } else {
        echo "Sorry no result found :)";
    }
    ?>
</table>


