<section>
    <div class="container">
        <div class="main white-bg standard-padding">
            <h1>Search Results</h1>
            <div class="row no-gutter equal-item-holder">
                <?php
                if (is_array($allAds) && !empty($allAds)) {
                    foreach ($allAds as $ad):
                        if ($addType == 'Student') {
                            $adRow = Modules::run('studentad/get_row', $ad);
                            if (!empty($adRow)) {
                                $data['row'] = $adRow;
                                $this->load->view('studentad/loop_student_common_search', $data);
                            }
                        } elseif ($addType == 'College') {
                            $adRow = Modules::run('collegead/get_row', $ad);
                            if (!empty($adRow)) {
                                $data['row'] = $adRow;
                                $this->load->view('collegead/loop_college_common_search', $data);
                            }
                        } elseif ($addType == 'Institute') {
                            $adRow = Modules::run('institutead/get_row', $ad);
                            if (!empty($adRow)) {
                                $data['row'] = $adRow;
                                $this->load->view('institutead/loop_institute_common_search', $data);
                            }
                        } else {
                            echo 'Sorry nothing to display for now';
                        }
                    endforeach;
                } else {

                    echo "Sorry no result found!";
                }
                ?>



                <div class="col-md-12" style="margin-top: 50px;">
                    <div class="bigBtn">
                        <a href="<?php echo site_url("search"); ?>">Click here for Advance Search</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>