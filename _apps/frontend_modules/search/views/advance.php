<div class="adv-search">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li id="search1" role="presentation" class="active"><a href="#advanceStudent" aria-controls="advanceStudent" role="tab" data-toggle="tab"><strong>Student Search</strong></a></li>
        <li id="search2" role="presentation"><a href="#advanceCollege" aria-controls="advanceStudent" role="tab" data-toggle="tab"><strong>College Search</strong></a></li>
        <li id="search3" role="presentation"><a href="#advanceInstitute" aria-controls="advanceInstitute" role="tab" data-toggle="tab"><strong>Institute Search</strong></a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">

        <div role="tabpanel" class="tab-pane active" id="advanceStudent">

            <div class="grey-bg standard-padding" id="student-box">
                <?php echo form_open('', array('class' => 'search_request')); ?>

                <h4>Search By:</h4>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="fee_range">Fee Range(Lakhs)</label>
                            <div class="drop_list">
                                <select class="form-control" name="fee_range">
                                    <?php echo get_select_box('student-ad-tuition-fee-range'); ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="course">Course</label>
                            <div class="drop_list">
                                <select class="form-control" name="course">
                                    <?php echo get_select_box('student-ad-course-looking-for'); ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="affiliation">Affiliation</label>
                            <div class="drop_list">
                                <select class="form-control" name="affiliation">
                                    <?php echo get_select_box('student-ad-affiliation'); ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="admission_within">Can admit in (Days)</label>
                            <div class="drop_list">
                                <select class="form-control" name="admission_within">
                                    <?php echo get_select_box('student-ad-admission-within-days') ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="achieved_grade">Recent CGPA/Percentage</label>
                            <div class="drop_list">
                                <select class="form-control" name="achieved_grade">
                                    <?php echo get_select_box('student-ad-achieved-grade') ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12"><div class="line-separator"></div></div>
                </div>
                <h4>Address:</h4>
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <?php
                            $country = 'Select One';
                            echo Modules::run('address/adv_get_select_box', 0, 'Country', 'name', 'advcountry', STUDENT);
                            ?>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group" id="provienceDiv-<?php echo STUDENT ?>">
                            <?php
                            $province = 'Select One';
                            ?>
                            <?php echo Modules::run('address/adv_get_select_box_find_parent', $country, 'Province', 'name', 'advprovince', STUDENT); ?>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group" id="districtDiv-<?php echo STUDENT ?>">
                            <?php
                            $district = 'Select One';
                            ?>
                            <?php echo Modules::run('address/adv_get_select_box_find_parent', $province, 'District', 'name', 'advdistrict', STUDENT); ?>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group" id="localLevelDiv-<?php echo STUDENT ?>">
                            <?php
                            $local_level = 'Select One';
                            ?>
                            <?php echo Modules::run('address/adv_get_select_box_find_parent', $district, 'Local Level', 'name', 'advlocal_level', STUDENT); ?>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group" id="wardnoDiv-<?php echo STUDENT ?>">
                            <?php
                            $ward_no = 'Select One';
                            ?>
                            <?php echo Modules::run('address/adv_get_select_box_find_parent', $local_level, 'Ward No', 'name', 'advward_no', STUDENT); ?>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group" id="addressDiv-<?php echo STUDENT ?>">
                            <?php
                            $address = 'Select One';
                            ?>
                            <?php echo Modules::run('address/adv_get_select_box_find_parent', $ward_no, 'Address', 'name', 'advaddress', STUDENT); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12"><div class="line-separator"></div></div>
                </div>
                <h4>Gender:</h4>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="student_gender">Student Gender</label><br/>
                            <input type="radio" name="student_gender" value="Both" checked> Both
                            <input type="radio" name="student_gender" value="Male"> Male
                            <input type="radio" name="student_gender" value="Female"> Female
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button type="submit" class="blueButton">Search</button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>



        <div role="tabpanel" class="tab-pane" id="advanceCollege">
            <div  class="grey-bg standard-padding" id="college-box">
                <?php echo form_open('', array('class' => 'search_request')); ?>

                <h4>Search By:</h4>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="fee_range">Course Fee Range(Lakhs)</label>
                            <div class="drop_list">
                                <select class="form-control" name="fee_range">
                                    <?php echo get_select_box('college-ad-tuition-fee-range'); ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="course">Course</label>
                            <div class="drop_list">
                                <select class="form-control" name="course">
                                    <?php echo get_select_box('college-ad-course-looking-for'); ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="affiliation">Affiliated to</label>
                            <div class="drop_list">
                                <select class="form-control" name="affiliation">
                                    <?php echo get_select_box('college-programe-affiliation-select-options'); ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="admission_within">Can Admit in (Days)</label>
                            <div class="drop_list">
                                <select class="form-control" name="admission_within">
                                    <?php echo get_select_box('college-ad-admission-within-days') ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="accepted_grade">Eligible CGPA/Percentage</label>
                            <div class="drop_list">
                                <select class="form-control" name="accepted_grade">
                                    <?php echo get_select_box('college-ad-grade-accepted') ?>
                                </select>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="row">
                    <div class="col-md-12"><div class="line-separator"></div></div>
                </div>


                <h4>Address:</h4>
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <?php
                            $country = 'Select One';
                            echo Modules::run('address/adv_get_select_box', 0, 'Country', 'name', 'advcountry', COLLEGE);
                            ?>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group" id="provienceDiv-<?php echo COLLEGE ?>">
                            <?php
                            $province = 'Select One';
                            ?>
                            <?php echo Modules::run('address/adv_get_select_box_find_parent', $country, 'Province', 'name', 'advprovince', COLLEGE); ?>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group" id="districtDiv-<?php echo COLLEGE ?>">
                            <?php
                            $district = 'Select One';
                            ?>
                            <?php echo Modules::run('address/adv_get_select_box_find_parent', $province, 'District', 'name', 'advdistrict', COLLEGE); ?>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group" id="localLevelDiv-<?php echo COLLEGE ?>">
                            <?php
                            $local_level = 'Select One';
                            ?>
                            <?php echo Modules::run('address/adv_get_select_box_find_parent', $district, 'Local Level', 'name', 'advlocal_level', COLLEGE); ?>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group" id="wardnoDiv-<?php echo COLLEGE ?>">
                            <?php
                            $ward_no = 'Select One';
                            ?>
                            <?php echo Modules::run('address/adv_get_select_box_find_parent', $local_level, 'Ward No', 'name', 'advward_no', COLLEGE); ?>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group" id="addressDiv-<?php echo COLLEGE ?>">
                            <?php
                            $address = 'Select One';
                            ?>
                            <?php echo Modules::run('address/adv_get_select_box_find_parent', $ward_no, 'Address', 'name', 'advaddress', COLLEGE); ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12"><div class="line-separator"></div></div>
                </div>


                <h4>College Description:</h4>
                <div class="row">

                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="shift">Shift</label>
                            <select class="form-control" name="shift">
                                <?php echo get_select_box('college-ad-shift') ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="starting_date">Starting Date</label>
                            <input type="text" class="form-control datepicker" name="starting_date">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="entrance_required">Entrance Required</label><br/>
                            <input type="radio" name="entrance_required" value="Both" checked> Both
                            <input type="radio" name="entrance_required" value="Yes"> Required
                            <input type="radio" name="entrance_required" value="No"> Not Required
                        </div>
                    </div>
                    <!---
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="college_bus">College Bus</label><br/>
                            <input type="radio" name="college_bus" value="Both" checked> Both
                            <input type="radio" name="college_bus" value="Yes"> Yes
                            <input type="radio" name="college_bus" value="No"> No
                        </div>
                    </div>-->
                    <div class="clearfix"></div>
                </div>
                <div class="row">
                    <div class="col-md-12"><div class="line-separator"></div></div>
                </div>
                <h4>College Facilities:</h4>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <?php echo get_check_box('college-facilities-checked-options', 'college_facilities'); ?>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12"><div class="line-separator"></div></div>
                </div>
                <h4>Security Issue:</h4>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <?php echo get_check_box('college-security-checked-options', 'college_security'); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12"><div class="line-separator"></div></div>
                </div>
                <h4>Additional Features:</h4>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <?php echo get_check_box('college-additional-features-checked-options', 'college_additionals'); ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">

                            <button type="submit" class="blueButton">Search</button>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="searchTypeSearch" value="College">
                <?php echo form_close(); ?>
            </div>
        </div>









        <div role="tabpanel" class="tab-pane" id="advanceInstitute">
            <div  class="grey-bg standard-padding" id="college-box">
                <?php echo form_open('', array('class' => 'search_request')); ?>

                <h4>Search By:</h4>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="fee_range">Total Fee (NRs. in 000)</label>
                            <div class="drop_list">
                                <select class="form-control" name="fee_range">
                                    <?php echo get_select_box('institute-ad-tuition-fee-range'); ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="course">Course</label>
                            <div class="drop_list">
                                <select class="form-control" name="course">
                                    <?php echo get_select_box('institute-course-options'); ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="affiliation">Certified By</label>
                            <div class="drop_list">
                                <select class="form-control" name="affiliation">
                                    <?php echo get_select_box('institute-certified-by-options'); ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="group_discount">Group Discount</label>
                            <div class="drop_list">
                                <select class="form-control" name="group_discount">
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="audio_visual_class">Audio-Visual Classes</label>
                            <div class="drop_list">
                                <select class="form-control" name="audio_visual_class">
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="class_duration">Class Duration</label>
                            <div class="drop_list">
                                <select class="form-control" name="class_duration">
                                    <?php echo get_select_box('institute-class-duration-options') ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="accepted_grade">Eligible CGPA/Percentage</label>
                            <div class="drop_list">
                                <select class="form-control" name="accepted_grade">
                                    <?php echo get_select_box('institute-ad-grade-accepted') ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12"><div class="line-separator"></div></div>
                </div>
                <h4>Address:</h4>
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <?php
                            $country = 'Select One';
                            echo Modules::run('address/adv_get_select_box', 0, 'Country', 'name', 'advcountry', INSTITUTE);
                            ?>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group" id="provienceDiv-<?php echo INSTITUTE ?>">
                            <?php
                            $province = 'Select One';
                            ?>
                            <?php echo Modules::run('address/adv_get_select_box_find_parent', $country, 'Province', 'name', 'advprovince', INSTITUTE); ?>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group" id="districtDiv-<?php echo INSTITUTE ?>">
                            <?php
                            $district = 'Select One';
                            ?>
                            <?php echo Modules::run('address/adv_get_select_box_find_parent', $province, 'District', 'name', 'advdistrict', INSTITUTE); ?>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group" id="localLevelDiv-<?php echo INSTITUTE ?>">
                            <?php
                            $local_level = 'Select One';
                            ?>
                            <?php echo Modules::run('address/adv_get_select_box_find_parent', $district, 'Local Level', 'name', 'advlocal_level', INSTITUTE); ?>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group" id="wardnoDiv-<?php echo INSTITUTE ?>">
                            <?php
                            $ward_no = 'Select One';
                            ?>
                            <?php echo Modules::run('address/adv_get_select_box_find_parent', $local_level, 'Ward No', 'name', 'advward_no', INSTITUTE); ?>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group" id="addressDiv-<?php echo INSTITUTE ?>">
                            <?php
                            $address = 'Select One';
                            ?>
                            <?php echo Modules::run('address/adv_get_select_box_find_parent', $ward_no, 'Address', 'name', 'advaddress', INSTITUTE); ?>
                        </div>
                    </div>
                </div>



                <div class="row">
                    <div class="col-md-12"><div class="line-separator"></div></div>
                </div>
                <h4>Institute Description:</h4>
                <div class="row">

                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="shift">Shift</label>
                            <select class="form-control" name="shift">
                                <?php echo get_select_box('institute-ad-shift') ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="starting_date">Starting Date</label>
                            <input type="text" class="form-control datepicker" name="starting_date">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="entrance_required">Entrance Required</label><br/>
                            <input type="radio" name="entrance_required" value="Both" checked> Both
                            <input type="radio" name="entrance_required" value="Yes"> Required
                            <input type="radio" name="entrance_required" value="No"> Not Required
                        </div>
                    </div>
                    <!--<div class="col-md-3">
                        <div class="form-group">
                            <label for="college_bus">Institute Bus</label><br/>
                            <input type="radio" name="college_bus" value="Both" checked> Both
                            <input type="radio" name="college_bus" value="Yes"> Yes
                            <input type="radio" name="college_bus" value="No"> No
                        </div>
                    </div>-->
                    <div class="clearfix"></div>
                </div>
                <div class="row">
                    <div class="col-md-12"><div class="line-separator"></div></div>
                </div>
                <h4>Facilities:</h4>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <?php echo get_check_box('institute-facilities-checked-options', 'institute_facilities'); ?>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12"><div class="line-separator"></div></div>
                </div>
                <h4>Security Issue:</h4>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <?php echo get_check_box('institute-security-checked-options', 'institute_security'); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12"><div class="line-separator"></div></div>
                </div>
                <h4>Additional Features:</h4>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <?php echo get_check_box('institute-additional-features-checked-options', 'institute_additionals'); ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <button type="submit" class="blueButton">Search</button>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="searchTypeSearch" value="Institute">
                <?php echo form_close(); ?>
            </div>
        </div>


    </div>

</div>

