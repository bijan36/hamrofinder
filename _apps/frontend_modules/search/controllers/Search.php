<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends Frontendcontroller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $this->template->load(get_template('advance-search'), 'advance');
    }

    function search_ads() {
        $isSearched = $this->input->post('search_now');
        if ($isSearched == 'YES') {
            $match = $this->input->post("search-key-word");
            $adtype = $this->input->post("adtype");
            $destination = $this->input->post("destination");

            if ($adtype == 'Student') {

                if ($destination == 'nepal') {

                    $query_string = "SELECT a.ID FROM student_ad a JOIN student b ON b.parent_ID = a.parent_ID JOIN users c ON a.parent_ID = c.ID WHERE c.status = 'Active' AND a.status = 'Active' AND b.country = 'NP' AND c.name LIKE '%$match%' OR c.address LIKE '%$match%' OR a.looking_for LIKE '%$match%' OR a.affiliation LIKE '%$match%' OR a.achieved_grade LIKE '%$match%' OR a.tution_fee_range LIKE '%$match%'";
                    $query = $this->db->query($query_string);
                    $ads = makeArrayID($query->result());
                    $data['addType'] = $adtype;
                    $data['allAds'] = $ads;
                    $this->template->load(get_template('site'), 'front_search', $data);

                    //COMBINING ALL THE ARRAYS TOGATHER
                } elseif ($destination == 'abroad') {
                    //SEARCH ONLY FOR BESIDES NEPAL
                    $query_string = "SELECT a.ID FROM student_ad a JOIN student b ON b.parent_ID = a.parent_ID JOIN users c ON a.parent_ID = c.ID WHERE c.status = 'Active' AND a.status = 'Active' AND b.country NOT LIKE 'NP' AND c.name LIKE '%$match%' OR c.address LIKE '%$match%' OR a.looking_for LIKE '%$match%' OR a.affiliation LIKE '%$match%' OR a.achieved_grade LIKE '%$match%' OR a.tution_fee_range LIKE '%$match%'";
                    $query = $this->db->query($query_string);
                    $ads = makeArrayID($query->result());
                    $data['addType'] = $adtype;
                    $data['allAds'] = $ads;
                    $this->template->load(get_template('site'), 'front_search', $data);
                    //BY NAME PROCESS ===============================================
                } else {
                    //DO NOTHING
                    $data['addType'] = $adtype;
                    $data['allAds'] = array();
                    $this->template->load(get_template('site'), 'front_search', $data);
                }
            } elseif ($adtype == 'College') {
                if ($destination == 'nepal') {
                    //SEARCH ONLY FOR NEPAL
                    //BY NAME PROCESS ===============================================
                    $query_string = "SELECT a.ID FROM college_ad a JOIN college b ON b.parent_ID = a.parent_ID JOIN users c ON a.parent_ID = c.ID WHERE c.status = 'Active' AND a.status = 'Active' AND b.country = 'NP' AND c.name LIKE '%$match%' OR c.address LIKE '%$match%' OR a.looking_for LIKE '%$match%' OR a.affiliation LIKE '%$match%' OR a.grade_accepted LIKE '%$match%' OR a.tuition_fee_range LIKE '%$match%'";
                    $query = $this->db->query($query_string);
                    $ads = makeArrayID($query->result());
                    $data['addType'] = $adtype;
                    $data['allAds'] = $ads;
                    $this->template->load(get_template('site'), 'front_search', $data);
                    //BY NAME PROCESS ===============================================
                    //COMBINING ALL THE ARRAYS TOGATHER
                } elseif ($destination == 'abroad') {
                    //SEARCH ONLY FOR BESIDES NEPAL
                    //BY NAME PROCESS ===============================================
                    $query_string = "SELECT a.ID FROM college_ad a JOIN college b ON b.parent_ID = a.parent_ID JOIN users c ON a.parent_ID = c.ID WHERE c.status = 'Active' AND a.status = 'Active' AND b.country NOT LIKE 'NP' AND c.name LIKE '%$match%' OR c.address LIKE '%$match%' OR a.looking_for LIKE '%$match%' OR a.affiliation LIKE '%$match%' OR a.grade_accepted LIKE '%$match%' OR a.tuition_fee_range LIKE '%$match%'";
                    $query = $this->db->query($query_string);
                    $ads = makeArrayID($query->result());
                    $data['addType'] = $adtype;
                    $data['allAds'] = $ads;
                    $this->template->load(get_template('site'), 'front_search', $data);
                    //COMBINING ALL THE ARRAYS TOGATHER
                } else {
                    $data['addType'] = $adtype;
                    $data['allAds'] = array();
                    $this->template->load(get_template('site'), 'front_search', $data);
                }
                //BY NAME PROCESS ===============================================
            } elseif ($adtype == 'Institute') {
                if ($destination == 'nepal') {
                    //SEARCH ONLY FOR NEPAL
                    //BY NAME PROCESS ===============================================
                    $query_string = "SELECT a.ID FROM institute_ad a JOIN institute b ON b.parent_ID = a.parent_ID JOIN users c ON a.parent_ID = c.ID WHERE c.status = 'Active' AND a.status = 'Active' AND b.country = 'NP' AND c.name LIKE '%$match%' OR c.address LIKE '%$match%' OR a.looking_for LIKE '%$match%' OR a.affiliation LIKE '%$match%' OR a.grade_accepted LIKE '%$match%' OR a.tuition_fee_range LIKE '%$match%'";
                    $query = $this->db->query($query_string);
                    $ads = makeArrayID($query->result());
                    $data['addType'] = $adtype;
                    $data['allAds'] = $ads;
                    $this->template->load(get_template('site'), 'front_search', $data);
                    //BY NAME PROCESS ===============================================
                    //COMBINING ALL THE ARRAYS TOGATHER
                } elseif ($destination == 'abroad') {
                    //SEARCH ONLY FOR BESIDES NEPAL
                    //BY NAME PROCESS ===============================================
                    $query_string = "SELECT a.ID FROM institute_ad a JOIN institute b ON b.parent_ID = a.parent_ID JOIN users c ON a.parent_ID = c.ID WHERE c.status = 'Active' AND a.status = 'Active' AND b.country NOT LIKE 'NP' AND c.name LIKE '%$match%' OR c.address LIKE '%$match%' OR a.looking_for LIKE '%$match%' OR a.affiliation LIKE '%$match%' OR a.grade_accepted LIKE '%$match%' OR a.tuition_fee_range LIKE '%$match%'";
                    $query = $this->db->query($query_string);
                    $ads = makeArrayID($query->result());
                    $data['addType'] = $adtype;
                    $data['allAds'] = $ads;
                    $this->template->load(get_template('site'), 'front_search', $data);
                    //COMBINING ALL THE ARRAYS TOGATHER
                } else {
                    $data['addType'] = $adtype;
                    $data['allAds'] = array();
                    $this->template->load(get_template('site'), 'front_search', $data);
                }
                //BY NAME PROCESS ===============================================    
            } else {
                //DO NOTHING
                $data['addType'] = $adtype;
                $data['allAds'] = array();
                $this->template->load(get_template('site'), 'front_search', $data);
            }
        } else {
            redirect('notfound');
            exit();
        }
    }

    //ADVANCE SEARCH FUNCTION
    public function advance_search() {

        $gender = $this->input->post('student_gender');
        //BY GENDER IT PROVE THAT IS IS STUDENT AREA COZ NO GENDER IN COLLEGE
        if (isset($gender)) {

            $dynamicTag = array();

            $fee_range = $this->input->post('fee_range');
            $course = $this->input->post('course');
            $affiliation = $this->input->post('affiliation');
            $address = $this->input->post('address');
            $admission_within = $this->input->post('admission_within');
            $achieved_grade = $this->input->post('achieved_grade');
            //$student_gender = $this->input->post('student_gender');
            //ADDRESS PARAMETERS
            $addressArray = array();
            $addressExist = false;
            $country_raw = $this->input->post('advcountry-student');
            $addressArray['country'] = $country_raw;
            $country = $country_raw == '0' ? false : $country_raw;
            if ($country) {
                $addressExist = true;
            }

            $province_raw = $this->input->post('advprovince-student');
            $addressArray['province'] = $province_raw;
            $province = $province_raw == '0' ? false : $province_raw;
            if ($province) {
                $addressExist = true;
            }

            $district_raw = $this->input->post('advdistrict-student');
            $addressArray['district'] = $district_raw;
            $district = $district_raw == '0' ? false : $district_raw;
            if ($district) {
                $addressExist = true;
            }

            $local_level_raw = $this->input->post('advlocal_level-student');
            $addressArray['local_level'] = $local_level_raw;
            $local_level = $local_level_raw == '0' ? false : $local_level_raw;
            if ($local_level) {
                $addressExist = true;
            }

            $ward_no_raw = $this->input->post('advward_no-student');
            $addressArray['ward_no'] = $ward_no_raw;
            $ward_no = $ward_no_raw == '0' ? false : $ward_no_raw;
            if ($ward_no) {
                $addressExist = true;
            }

            $address_raw = $this->input->post('advaddress-student');
            $addressArray['address'] = $address_raw;
            $address = $address_raw == '0' ? false : $address_raw;
            if ($address) {
                $addressExist = true;
            }

            //GET STUDENT AD IDS BY ADDRESS
            $idByAddress = array();
            $addressValues = array();
            if (!empty($addressExist)) {
                $idByAddress = Modules::run('studentad/search_ad_by_address_extended', $addressArray, $gender);
                if ($country) {
                    $dynamicTag['advcountry-student'] = $idByAddress;
                    $addressValues['country'] = $country_raw;
                }
                if ($province) {
                    $dynamicTag['advprovince-student'] = $idByAddress;
                    $addressValues['province'] = $province_raw;
                }
                if ($district) {
                    $dynamicTag['advdistrict-student'] = $idByAddress;
                    $addressValues['district'] = $district_raw;
                }
                if ($local_level) {
                    $dynamicTag['advlocal_level-student'] = $idByAddress;
                    $addressValues['local_level'] = $local_level_raw;
                }
                if ($ward_no) {
                    $dynamicTag['advward_no-student'] = $idByAddress;
                    $addressValues['ward_no'] = $ward_no_raw;
                }
                if ($address) {
                    $dynamicTag['advaddress-student'] = $idByAddress;
                    $addressValues['address'] = $address_raw;
                }
            }

            //ADDRESS PARAMETERS
            //GET STUDENT AD IDS BY FEE RANGE
            $idByFee = array();
            if ($fee_range != '') {
                $idByFee = Modules::run('studentad/search_ad_by_fee_range', $fee_range, $gender);
                $dynamicTag['fee_range'] = $idByFee;
            }

            //GET STUDENT AD IDS BY COURSE
            $idByCourse = array();
            if ($course != '') {
                $idByCourse = Modules::run('studentad/search_ad_by_course', $course, $gender);
                $dynamicTag['course'] = $idByCourse;
            }

            //GET STUDENT AD IDS BY AFFILIATIONS
            $idByAffiliation = array();
            if ($affiliation != '') {
                $idByAffiliation = Modules::run('studentad/search_ad_by_affiliation', $affiliation, $gender);
                $dynamicTag['affiliation'] = $idByAffiliation;
            }



            //GET STUDENT AD IDS BY ADDMISSION WITHIN
            $idByAdmissionWithin = array();
            if ($admission_within != '') {
                $idByAdmissionWithin = Modules::run('studentad/search_ad_by_admission_within', $admission_within, $gender);
                $dynamicTag['admission_within'] = $idByAdmissionWithin;
            }

            //GET STUDENT AD IDS BY ACHIEVED GRADE
            $idByAchivedGrade = array();
            if ($achieved_grade != '') {
                $idByAchivedGrade = Modules::run('studentad/search_ad_by_achieved_grade', $achieved_grade, $gender);
                $dynamicTag['achieved_grade'] = $idByAchivedGrade;
            }

            //FINAL FILTERED IDS
            $finalIDs = array_unique(array_merge($idByAchivedGrade, $idByAdmissionWithin, $idByAddress, $idByAffiliation, $idByCourse, $idByFee));

            rsort($finalIDs);

            //REAL SEARCH PARAMENTERS
            $searchParam = $_POST;
            $spShow = array();
            foreach ($searchParam as $sp => $spv):
                if (!empty($spv))
                    $spShow[$sp] = $spv;
            endforeach;


            $data['addType'] = 'Student';
            $data['allAds'] = $finalIDs;
            $data['searchParam'] = $spShow;
            $data['dynamicTag'] = $dynamicTag;
            $data['addressValues'] = $addressValues;
            $newView = $this->load->view('advance-search', $data, true);
            $data = array(
                'status' => 'success',
                'msg' => 'Please wait collecting reasults',
                'newView' => $newView,
            );
            echo json_encode($data);
            exit();
        } else {
            //COLLEGE SEARCH AREA
            $searchTypeSearch = $this->input->post("searchTypeSearch");

            if ($searchTypeSearch == "College") {


                $dynamicTag = array();


                //ADVANCE
                $fee_range = $this->input->post('fee_range');
                $course = $this->input->post('course');
                $affiliation = $this->input->post('affiliation');
                $admission_within = $this->input->post('admission_within');
                $accepted_grade = $this->input->post('accepted_grade');
                $shift = $this->input->post('shift');
                //$college_name = $this->input->post('college_name');
                $starting_date = $this->input->post('starting_date');
                $entrance_required = $this->input->post('entrance_required');
                //$college_bus = $this->input->post('college_bus');
                //$address = $this->input->post('address');
                $college_facilities = $this->input->post('college_facilities');
                $college_security = $this->input->post('college_security');
                $college_additionals = $this->input->post('college_additionals');



                //ADDRESS PARAMETERS
                $addressArray = array();
                $addressExist = false;
                $country_raw = $this->input->post('advcountry-college');
                $addressArray['country'] = $country_raw;
                $country = $country_raw == '0' ? false : $country_raw;
                if ($country) {
                    $addressExist = true;
                }

                $province_raw = $this->input->post('advprovince-college');
                $addressArray['province'] = $province_raw;
                $province = $province_raw == '0' ? false : $province_raw;
                if ($province) {
                    $addressExist = true;
                }

                $district_raw = $this->input->post('advdistrict-college');
                $addressArray['district'] = $district_raw;
                $district = $district_raw == '0' ? false : $district_raw;
                if ($district) {
                    $addressExist = true;
                }

                $local_level_raw = $this->input->post('advlocal_level-college');
                $addressArray['local_level'] = $local_level_raw;
                $local_level = $local_level_raw == '0' ? false : $local_level_raw;
                if ($local_level) {
                    $addressExist = true;
                }

                $ward_no_raw = $this->input->post('advward_no-college');
                $addressArray['ward_no'] = $ward_no_raw;
                $ward_no = $ward_no_raw == '0' ? false : $ward_no_raw;
                if ($ward_no) {
                    $addressExist = true;
                }

                $address_raw = $this->input->post('advaddress-college');
                $addressArray['address'] = $address_raw;
                $address = $address_raw == '0' ? false : $address_raw;
                if ($address) {
                    $addressExist = true;
                }
                //ADDRESS PARAMETERS
                //GET COLLEGE AD IDS BY FEE RANGE
                $idByFee = array();
                if ($fee_range != '') {
                    $idByFee = Modules::run('collegead/search_ad_by_fee_range', $fee_range);
                    $dynamicTag['fee_range'] = $idByFee;
                }

                //GET COLLEGE AD IDS BY COURSE
                $idByCourse = array();
                if ($course != '') {
                    $idByCourse = Modules::run('collegead/search_ad_by_course', $course);
                    $dynamicTag['course'] = $idByCourse;
                }

                //GET COLLEGE AD IDS BY AFFILIATIONS
                $idByAffiliation = array();
                if ($affiliation != '') {
                    $idByAffiliation = Modules::run('collegead/search_ad_by_affiliation', $affiliation);
                    $dynamicTag['affiliation'] = $idByAffiliation;
                }

                //GET COLLEGE AD IDS BY ADDMISSION WITHIN
                $idByAdmissionWithin = array();
                if ($admission_within != '') {
                    $idByAdmissionWithin = Modules::run('collegead/search_ad_by_admission_within', $admission_within);
                    $dynamicTag['admission_within'] = $idByAdmissionWithin;
                }

                //GET COLLAGE AD IDS BY ACCEPTED GRADE
                $idByAcceptedGrade = array();
                if ($accepted_grade != '') {
                    $idByAcceptedGrade = Modules::run('collegead/search_ad_by_accepted_grade', $accepted_grade);
                    $dynamicTag['accepted_grade'] = $idByAcceptedGrade;
                }

                //GET COLLAGE AD SHIFT
                $idByShift = array();
                if ($shift != '') {
                    $idByShift = Modules::run('collegead/search_ad_by_shift', $shift);
                    $dynamicTag['shift'] = $idByShift;
                }
                //GET COLLAGE AD CLASS STARTING DATE
                $idByStartDate = array();
                if ($starting_date != '') {
                    $idByStartDate = Modules::run('collegead/search_ad_by_starting_date', $starting_date);
                    $dynamicTag['starting_date'] = $idByStartDate;
                }
                //GET COLLAGE AD ENTRANCE
                $idByIntrance = array();
                if ($entrance_required != '' && $entrance_required != 'Both') {
                    $idByIntrance = Modules::run('collegead/search_ad_by_entrance_required', $entrance_required);
                    $dynamicTag['entrance_required'] = $idByIntrance;
                }
                //GET COLLAGE BUS
                /* $idByBus = array();
                  if ($college_bus != '' && $college_bus != 'Both') {
                  $idByBus = Modules::run('collegead/search_ad_by_bus', $college_bus);
                  $dynamicTag['college_bus'] = $idByBus;
                  } */

                //PRE FINAL IDS COLLECTION OF ADS
                $finalIDs = array_unique(array_merge(
                                $idByFee, $idByCourse, $idByAffiliation, $idByAdmissionWithin, $idByAcceptedGrade, $idByShift, $idByStartDate, $idByIntrance
                ));

                if (!empty($finalIDs)) {
                    //SORT THE IDS BY DESCING LATEST FIRST
                    rsort($finalIDs);
                    //FILTER COLLEGE ADS BY ITS COLLEGE ADDRESS
                    $idByAddress = $finalIDs;

                    //$idByAddress = array();
                    $addressValues = array();
                    if (!empty($addressExist)) {
                        $idByAddressIDs = Modules::run('collegead/search_ad_by_address_extended', $addressArray, $gender);
                        if ($country) {
                            $dynamicTag['advcountry-college'] = $idByAddressIDs;
                            $addressValues['country'] = $country_raw;
                        }
                        if ($province) {
                            $dynamicTag['advprovince-college'] = $idByAddressIDs;
                            $addressValues['province'] = $province_raw;
                        }
                        if ($district) {
                            $dynamicTag['advdistrict-college'] = $idByAddressIDs;
                            $addressValues['district'] = $district_raw;
                        }
                        if ($local_level) {
                            $dynamicTag['advlocal_level-college'] = $idByAddressIDs;
                            $addressValues['local_level'] = $local_level_raw;
                        }
                        if ($ward_no) {
                            $dynamicTag['advward_no-college'] = $idByAddressIDs;
                            $addressValues['ward_no'] = $ward_no_raw;
                        }
                        if ($address) {
                            $dynamicTag['advaddress-college'] = $idByAddressIDs;
                            $addressValues['address'] = $address_raw;
                        }
                    }



                    /* if ($address != '') {
                      foreach ($finalIDs as $fi):
                      $parentID = Modules::run('collegead/get_parent_by_id', $fi);
                      if ($parentID) {
                      //find the address of college is matched or not
                      $dataAddress = Modules::run('user/get_address_by_id', $parentID);
                      if ($dataAddress) {
                      if (strtolower($dataAddress) == strtolower($address)) {
                      $idByAddress[] = $fi;
                      }
                      }
                      }
                      endforeach;
                      $dynamicTag['address'] = $idByAddress;
                      } */

                    //FILTER THE IDS BY FALICITIES
                    $idByCollegeFacilities = $idByAddress;
                    //print_r($idByCollegeFacilities);
                    if (!empty($college_facilities) && !empty($idByAddress)) {
                        foreach ($idByAddress as $coAd):
                            $parentID = Modules::run('collegead/get_parent_by_id', $coAd);
                            if ($parentID) {
                                $dataFacilities = Modules::run('college/get_facility_values', $parentID);
                                if ($dataFacilities) {
                                    $dataArr = explode(',', $dataFacilities);
                                    //print_r($dataArr);
                                    //print_r($college_facilities);
                                    // exit();
                                    foreach ($college_facilities as $cofi):
                                        if (in_array(strtolower($cofi), $dataArr)) {
                                            $idByCollegeFacilities[] = $coAd;
                                            //SEND THE INDIVIDUAL CHECK BOX REFERANCE ID
                                            $dynamicTag['college_facilities_element'][strtolower($cofi) . '-' . $coAd] = $coAd;
                                        }
                                    endforeach;
                                    $idByCollegeFacilities = array_unique($idByCollegeFacilities);
                                }
                            }
                        endforeach;
                        $dynamicTag['college_facilities'] = $idByCollegeFacilities;
                    }


                    //print_r($idByCollegeFacilities);
                    //print_r($dynamicTag);
                    //print_r($college_facilities);
                    //print_r($dataArr);
                    //exit();
                    //FILTER THE IDS BY COLLEGE SECURITIES
                    $idByCollegeSecurity = $idByCollegeFacilities;
                    if (!empty($college_security) && !empty($idByCollegeFacilities)) {
                        foreach ($idByCollegeFacilities as $coFis):
                            $parentID = Modules::run('collegead/get_parent_by_id', $coFis);
                            if ($parentID) {
                                $dataSecurities = Modules::run('college/get_securities_values', $parentID);
                                if ($dataSecurities) {
                                    $dataArrSi = explode(',', $dataSecurities);
                                    foreach ($college_security as $coSi):
                                        if (in_array(strtolower($coSi), $dataArrSi)) {
                                            $idByCollegeSecurity[] = $coFis;
                                            //SEND THE INDIVIDUAL CHECK BOX REFERANCE ID
                                            $dynamicTag['college_securities_element'][strtolower($coSi) . '-' . $coFis] = $coFis;
                                        }
                                    endforeach;
                                    $idByCollegeSecurity = array_unique($idByCollegeSecurity);
                                }
                            }
                        endforeach;
                        $dynamicTag['college_securities'] = $idByCollegeSecurity;
                    }



                    //FILTER THE IDS BY COLLEGE SECURITIES
                    $idByCollegeAdditionals = $idByCollegeSecurity;
                    if (!empty($college_additionals) && !empty($idByCollegeSecurity)) {
                        foreach ($idByCollegeSecurity as $coAddi):
                            $parentID = Modules::run('collegead/get_parent_by_id', $coAddi);
                            if ($parentID) {
                                $dataAdditioals = Modules::run('college/get_additionals_values', $parentID);
                                if ($dataAdditioals) {
                                    $dataArrAdd = explode(',', $dataAdditioals);
                                    foreach ($college_additionals as $coAi):
                                        if (in_array(strtolower($coAi), $dataArrAdd)) {
                                            $idByCollegeAdditionals[] = $coAddi;
                                            //SEND THE INDIVIDUAL CHECK BOX REFERANCE ID
                                            $dynamicTag['additional_features_element'][strtolower($coAi) . '-' . $coAddi] = $coAddi;
                                        }
                                    endforeach;
                                    $idByCollegeAdditionals = array_unique($idByCollegeAdditionals);
                                }
                            }
                        endforeach;
                        $dynamicTag['additional_features'] = $idByCollegeAdditionals;
                    }

                    $theIDs = array_unique($idByCollegeAdditionals);

                    if (!empty($theIDs)) {
                        //REAL SEARCH PARAMENTERS
                        $searchParam = $_POST;
                        $spShow = array();
                        foreach ($searchParam as $sp => $spv):
                            if (!empty($spv))
                                $spShow[$sp] = $spv;
                        endforeach;

                        $data['addType'] = 'College';
                        //$data['allAds'] = rsort($theIDs);
                        $data['allAds'] = $theIDs;
                        $data['searchParam'] = $spShow;
                        $data['dynamicTag'] = $dynamicTag;
                        $data['addressValues'] = $addressValues;
                        $newView = $this->load->view('advance-search', $data, true);
                        $data = array(
                            'status' => 'success',
                            'msg' => 'Please wait collecting reasults',
                            'newView' => $newView,
                        );
                        echo json_encode($data);
                        exit();
                    }else {
                        $data = array(
                            'status' => 'error',
                            'msg' => 'No result found! 2'
                        );
                        echo json_encode($data);
                        exit();
                    }
                } else {
                    //THE FINAL IDES ARE EMPTY NO RESULT TO BE FILTER DEEPLY
                    $data = array(
                        'status' => 'error',
                        'msg' => 'No result found! 1'
                    );
                    echo json_encode($data);
                    exit();
                }



                //FILTER COLLEGE ADS BY ITS COLLEGE ADDRESS
                /* $idByAddress = array();
                  if ($address != '') {
                  $idByAddress = Modules::run('collegead/search_ad_by_address', $address);
                  $dynamicTag['address'] = $idByAddress;
                  }


                  //FILTER COLLEGE ADS BY ITS COLLEGE FACILITIES
                  $idByFacilities = array();
                  if ($college_facilities != '') {
                  $idByFacilities = Modules::run('college/search_ad_by_facilities', $college_facilities);
                  $dynamicTag['college_facilities'] = $idByFacilities;
                  } */


                //REAL SEARCH PARAMENTERS
                $searchParam = $_POST;
                $spShow = array();
                foreach ($searchParam as $sp => $spv):
                    if (!empty($spv))
                        $spShow[$sp] = $spv;
                endforeach;



                $data['addType'] = 'Student';
                $data['allAds'] = $finalIDs;
                $data['searchParam'] = $spShow;
                $data['dynamicTag'] = $dynamicTag;
                $newView = $this->load->view('advance-search', $data, true);
                $data = array(
                    'status' => 'success',
                    'msg' => 'Please wait collecting reasults',
                    'newView' => $newView,
                );
                echo json_encode($data);
                exit();
            } else {

                //INSTITUTE SEARCH AREA ====================================================================
                //ADVANCE

                $dynamicTag = array();

                $fee_range = $this->input->post('fee_range');
                $course = $this->input->post('course');
                $affiliation = $this->input->post('affiliation');
                $group_discount = $this->input->post('group_discount');
                $accepted_grade = $this->input->post('accepted_grade');
                $shift = $this->input->post('shift');
                //$college_name = $this->input->post('college_name');
                $starting_date = $this->input->post('starting_date');
                $entrance_required = $this->input->post('entrance_required');
                $audio_visual_class = $this->input->post('audio_visual_class');
                $class_duration = $this->input->post('class_duration');


                //$address = $this->input->post('address');
                $institute_facilities = $this->input->post('institute_facilities');
                $institute_security = $this->input->post('institute_security');
                $institute_additionals = $this->input->post('institute_additionals');


                //ADDRESS PARAMETERS
                $addressArray = array();
                $addressExist = false;
                $country_raw = $this->input->post('advcountry-institute');
                $addressArray['country'] = $country_raw;
                $country = $country_raw == '0' ? false : $country_raw;
                if ($country) {
                    $addressExist = true;
                }

                $province_raw = $this->input->post('advprovince-institute');
                $addressArray['province'] = $province_raw;
                $province = $province_raw == '0' ? false : $province_raw;
                if ($province) {
                    $addressExist = true;
                }

                $district_raw = $this->input->post('advdistrict-institute');
                $addressArray['district'] = $district_raw;
                $district = $district_raw == '0' ? false : $district_raw;
                if ($district) {
                    $addressExist = true;
                }

                $local_level_raw = $this->input->post('advlocal_level-institute');
                $addressArray['local_level'] = $local_level_raw;
                $local_level = $local_level_raw == '0' ? false : $local_level_raw;
                if ($local_level) {
                    $addressExist = true;
                }

                $ward_no_raw = $this->input->post('advward_no-institute');
                $addressArray['ward_no'] = $ward_no_raw;
                $ward_no = $ward_no_raw == '0' ? false : $ward_no_raw;
                if ($ward_no) {
                    $addressExist = true;
                }

                $address_raw = $this->input->post('advaddress-institute');
                $addressArray['address'] = $address_raw;
                $address = $address_raw == '0' ? false : $address_raw;
                if ($address) {
                    $addressExist = true;
                }
                //ADDRESS PARAMETERS
                //GET COLLEGE AD IDS BY FEE RANGE
                $idByFee = array();
                if ($fee_range != '') {
                    $idByFee = Modules::run('institutead/search_ad_by_fee_range', $fee_range);
                    $dynamicTag['fee_range'] = $idByFee;
                }

                //GET COLLEGE AD IDS BY COURSE
                $idByCourse = array();
                if ($course != '') {
                    $idByCourse = Modules::run('institutead/search_ad_by_course', $course);
                    $dynamicTag['course'] = $idByCourse;
                }

                //GET COLLEGE AD IDS BY AFFILIATIONS
                $idByAffiliation = array();
                if ($affiliation != '') {
                    $idByAffiliation = Modules::run('institutead/search_ad_by_affiliation', $affiliation);
                    $dynamicTag['affiliation'] = $idByAffiliation;
                }

                //GET INSTITUTE AD IDS BY GROUP DISCOUNT
                $idByGroupDiscount = array();
                if ($group_discount != '') {
                    $idByGroupDiscount = Modules::run('institutead/search_ad_group_discount', $group_discount);
                    $dynamicTag['group_discount'] = $idByGroupDiscount;
                }

                //GET COLLAGE AD IDS BY ACCEPTED GRADE
                $idByAcceptedGrade = array();
                if ($accepted_grade != '') {
                    $idByAcceptedGrade = Modules::run('institutead/search_ad_by_accepted_grade', $accepted_grade);
                    $dynamicTag['eligible_CGPA/Percentage'] = $idByAcceptedGrade;
                }

                //GET COLLAGE AD SHIFT
                $idByShift = array();
                if ($shift != '') {
                    $idByShift = Modules::run('institutead/search_ad_by_shift', $shift);
                    $dynamicTag['shift'] = $idByShift;
                }
                //GET COLLAGE AD CLASS STARTING DATE
                $idByStartDate = array();
                if ($starting_date != '') {
                    $idByStartDate = Modules::run('institutead/search_ad_by_starting_date', $starting_date);
                    $dynamicTag['starting_date'] = $idByStartDate;
                }
                //GET COLLAGE AD ENTRANCE
                $idByIntrance = array();
                if ($entrance_required != '' && $entrance_required != 'Both') {
                    $idByIntrance = Modules::run('institutead/search_ad_by_entrance_required', $entrance_required);
                    $dynamicTag['entrance_required'] = $idByIntrance;
                }
                //GET institute audio video claass
                $idByAudioVideoClass = array();
                if ($audio_visual_class != '') {
                    $idByAudioVideoClass = Modules::run('institutead/search_ad_by_audio_video', $audio_visual_class);
                    $dynamicTag['audio_visual_class'] = $idByAudioVideoClass;
                }
                //GET institute by class duration
                $idByClassDuration = array();
                if ($class_duration != '') {
                    $idByClassDuration = Modules::run('institutead/search_ad_by_class_duration', $class_duration);
                    $dynamicTag['class_duration'] = $idByClassDuration;
                }

                //PRE FINAL IDS COLLECTION OF ADS
                $finalIDs = array();
                if (
                        !empty($idByFee) ||
                        !empty($idByCourse) ||
                        !empty($idByAffiliation) ||
                        !empty($idByGroupDiscount) ||
                        !empty($idByAcceptedGrade) ||
                        !empty($idByShift) ||
                        !empty($idByStartDate) ||
                        !empty($idByIntrance) ||
                        !empty($idByAudioVideoClass) ||
                        !empty($idByClassDuration)
                ) {
                    $finalIDs = array_unique(array_merge($idByFee, $idByCourse, $idByAffiliation, $idByGroupDiscount, $idByAcceptedGrade, $idByShift, $idByStartDate, $idByIntrance, $idByAudioVideoClass, $idByClassDuration));
                }

                if (!empty($finalIDs)) {
                    rsort($finalIDs);
                    //FILTER COLLEGE ADS BY ITS INSTITUTE ADDRESS
                    $idByAddress = $finalIDs;
                    $addressValues = array();
                    if (!empty($addressExist)) {
                        $idByAddressIDs = Modules::run('institutead/search_ad_by_address_extended', $addressArray, $gender);
                        if ($country) {
                            $dynamicTag['advcountry-institute'] = $idByAddressIDs;
                            $addressValues['country'] = $country_raw;
                        }
                        if ($province) {
                            $dynamicTag['advprovince-institute'] = $idByAddressIDs;
                            $addressValues['province'] = $province_raw;
                        }
                        if ($district) {
                            $dynamicTag['advdistrict-institute'] = $idByAddressIDs;
                            $addressValues['district'] = $district_raw;
                        }
                        if ($local_level) {
                            $dynamicTag['advlocal_level-institute'] = $idByAddressIDs;
                            $addressValues['local_level'] = $local_level_raw;
                        }
                        if ($ward_no) {
                            $dynamicTag['advward_no-institute'] = $idByAddressIDs;
                            $addressValues['ward_no'] = $ward_no_raw;
                        }
                        if ($address) {
                            $dynamicTag['advaddress-institute'] = $idByAddressIDs;
                            $addressValues['address'] = $address_raw;
                        }
                    }



                    /* if ($address != '') {

                      foreach ($finalIDs as $fi):
                      $parentID = Modules::run('institutead/get_parent_by_id', $fi);
                      if ($parentID) {
                      //find the address of college is matched or not
                      $dataAddress = Modules::run('user/get_address_by_id', $parentID);
                      if ($dataAddress) {
                      if (strtolower($dataAddress) == strtolower($address)) {
                      $idByAddress[] = $fi;
                      }
                      }
                      }
                      endforeach;
                      $dynamicTag['address'] = $idByAddress;
                      } */





                    //FILTER THE IDS BY FALICITIES
                    $idByCollegeFacilities = $idByAddress;
                    //print_r($idByCollegeFacilities);
                    if (!empty($college_facilities) && !empty($idByAddress)) {
                        foreach ($idByAddress as $coAd):
                            $parentID = Modules::run('institutead/get_parent_by_id', $coAd);
                            if ($parentID) {
                                $dataFacilities = Modules::run('institute/get_facility_values', $parentID);
                                if ($dataFacilities) {
                                    $dataArr = explode(',', $dataFacilities);
                                    //print_r($dataArr);
                                    //print_r($college_facilities);
                                    // exit();
                                    foreach ($college_facilities as $cofi):
                                        if (in_array(strtolower($cofi), $dataArr)) {
                                            $idByCollegeFacilities[] = $coAd;
                                            //SEND THE INDIVIDUAL CHECK BOX REFERANCE ID
                                            $dynamicTag['institute_facilities_element'][strtolower($cofi) . '-' . $coAd] = $coAd;
                                        }
                                    endforeach;
                                    $idByCollegeFacilities = array_unique($idByCollegeFacilities);
                                }
                            }
                        endforeach;
                        $dynamicTag['college_facilities'] = $idByCollegeFacilities;
                    }


                    //print_r($idByCollegeFacilities);
                    //print_r($dynamicTag);
                    //print_r($college_facilities);
                    //print_r($dataArr);
                    //exit();
                    //FILTER THE IDS BY COLLEGE SECURITIES
                    $idByCollegeSecurity = $idByCollegeFacilities;
                    if (!empty($college_security) && !empty($idByCollegeFacilities)) {
                        foreach ($idByCollegeFacilities as $coFis):
                            $parentID = Modules::run('institutead/get_parent_by_id', $coFis);
                            if ($parentID) {
                                $dataSecurities = Modules::run('institute/get_securities_values', $parentID);
                                if ($dataSecurities) {
                                    $dataArrSi = explode(',', $dataSecurities);
                                    foreach ($college_security as $coSi):
                                        if (in_array(strtolower($coSi), $dataArrSi)) {
                                            $idByCollegeSecurity[] = $coFis;
                                            //SEND THE INDIVIDUAL CHECK BOX REFERANCE ID
                                            $dynamicTag['institute_securities_element'][strtolower($coSi) . '-' . $coFis] = $coFis;
                                        }
                                    endforeach;
                                    $idByCollegeSecurity = array_unique($idByCollegeSecurity);
                                }
                            }
                        endforeach;
                        $dynamicTag['college_securities'] = $idByCollegeSecurity;
                    }



                    //FILTER THE IDS BY COLLEGE SECURITIES
                    $idByCollegeAdditionals = $idByCollegeSecurity;
                    if (!empty($college_additionals) && !empty($idByCollegeSecurity)) {
                        foreach ($idByCollegeSecurity as $coAddi):
                            $parentID = Modules::run('institutead/get_parent_by_id', $coAddi);
                            if ($parentID) {
                                $dataAdditioals = Modules::run('institute/get_additionals_values', $parentID);
                                if ($dataAdditioals) {
                                    $dataArrAdd = explode(',', $dataAdditioals);
                                    foreach ($college_additionals as $coAi):
                                        if (in_array(strtolower($coAi), $dataArrAdd)) {
                                            $idByCollegeAdditionals[] = $coAddi;
                                            //SEND THE INDIVIDUAL CHECK BOX REFERANCE ID
                                            $dynamicTag['additional_features_element'][strtolower($coAi) . '-' . $coAddi] = $coAddi;
                                        }
                                    endforeach;
                                    $idByCollegeAdditionals = array_unique($idByCollegeAdditionals);
                                }
                            }
                        endforeach;
                        $dynamicTag['additional_features'] = $idByCollegeAdditionals;
                    }

                    $theIDs = array_unique($idByCollegeAdditionals);

                    if (!empty($theIDs)) {

                        //REAL SEARCH PARAMENTERS
                        $searchParam = $_POST;
                        $spShow = array();
                        foreach ($searchParam as $sp => $spv):
                            if (!empty($spv))
                                $spShow[$sp] = $spv;
                        endforeach;

                        $data['addType'] = 'Institute';
                        $data['allAds'] = $theIDs;
                        $data['searchParam'] = $spShow;
                        $data['dynamicTag'] = $dynamicTag;
                        $data['addressValues'] = $addressValues;
                        $newView = $this->load->view('advance-search', $data, true);
                        $data = array(
                            'status' => 'success',
                            'msg' => 'Please wait collecting reasults',
                            'newView' => $newView,
                        );
                        echo json_encode($data);
                        exit();
                    }else {
                        $data = array(
                            'status' => 'error',
                            'msg' => 'No result found!'
                        );
                        echo json_encode($data);
                        exit();
                    }
                } else {
                    //THE FINAL IDES ARE EMPTY NO RESULT TO BE FILTER DEEPLY
                    $data = array(
                        'status' => 'error',
                        'msg' => 'No result found!'
                    );
                    echo json_encode($data);
                    exit();
                }



                //FILTER COLLEGE ADS BY ITS COLLEGE ADDRESS
                /* $idByAddress = array();
                  if ($address != '') {
                  $idByAddress = Modules::run('collegead/search_ad_by_address', $address);
                  $dynamicTag['address'] = $idByAddress;
                  }


                  //FILTER COLLEGE ADS BY ITS COLLEGE FACILITIES
                  $idByFacilities = array();
                  if ($college_facilities != '') {
                  $idByFacilities = Modules::run('college/search_ad_by_facilities', $college_facilities);
                  $dynamicTag['college_facilities'] = $idByFacilities;
                  } */


                //REAL SEARCH PARAMENTERS
                $searchParam = $_POST;
                $spShow = array();
                foreach ($searchParam as $sp => $spv):
                    if (!empty($spv))
                        $spShow[$sp] = $spv;
                endforeach;



                $data['addType'] = 'Student';
                $data['allAds'] = $finalIDs;
                $data['searchParam'] = $spShow;
                $data['dynamicTag'] = $dynamicTag;
                $newView = $this->load->view('advance-search', $data, true);
                $data = array(
                    'status' => 'success',
                    'msg' => 'Please wait collecting reasults',
                    'newView' => $newView,
                );
                echo json_encode($data);
                exit();
            }
        }
    }

    //BROWSE SEARCH REASULT
    public function browse() {
        $type = $this->input->post('type');
        $method = $this->input->post('method');
        $str = $this->input->post('val');

        if (!empty($method)) {
            if ($method == 'Student') {
                //STUDENT RELATED QURIES
                if (!empty($type) && !empty($str)) {

                    if ($type == 'course') {
                        $query_string = "SELECT a.ID FROM student_ad a JOIN student b ON b.parent_ID = a.parent_ID JOIN users c ON a.parent_ID = c.ID WHERE c.status = 'Active' AND a.status = 'Active' AND a.looking_for LIKE '%$str%'";
                        $query = $this->db->query($query_string);
                        $ads = makeArrayID($query->result());
                        $data['method'] = 'Student';
                        $data['type'] = 'course';
                        $data['allAds'] = $ads;
                        $newView = $this->load->view('browse/browse', $data, true);
                        $data = array(
                            'status' => 'success',
                            'msg' => 'Please wait collecting reasults',
                            'newView' => $newView,
                        );
                        echo json_encode($data);
                        exit();
                    } elseif ($type == 'affiliation') {
                        $query_string = "SELECT a.ID FROM student_ad a JOIN student b ON b.parent_ID = a.parent_ID JOIN users c ON a.parent_ID = c.ID WHERE c.status = 'Active' AND a.status = 'Active' AND a.affiliation LIKE '%$str%'";
                        $query = $this->db->query($query_string);
                        $ads = makeArrayID($query->result());
                        $data['method'] = 'Student';
                        $data['type'] = 'affiliation';
                        $data['allAds'] = $ads;
                        $newView = $this->load->view('browse/browse', $data, true);
                        $data = array(
                            'status' => 'success',
                            'msg' => 'Please wait collecting reasults',
                            'newView' => $newView,
                        );
                        echo json_encode($data);
                        exit();
                    } elseif ($type == 'fee') {
                        $query_string = "SELECT a.ID FROM student_ad a JOIN student b ON b.parent_ID = a.parent_ID JOIN users c ON a.parent_ID = c.ID WHERE c.status = 'Active' AND a.status = 'Active' AND a.tution_fee_range LIKE '%$str%'";
                        $query = $this->db->query($query_string);
                        $ads = makeArrayID($query->result());
                        $data['method'] = 'Student';
                        $data['type'] = 'fee';
                        $data['allAds'] = $ads;
                        $newView = $this->load->view('browse/browse', $data, true);
                        $data = array(
                            'status' => 'success',
                            'msg' => 'Please wait collecting reasults',
                            'newView' => $newView,
                        );
                        echo json_encode($data);
                        exit();
                    } elseif ($type == 'location') {
                        $query_string = "SELECT a.ID FROM student_ad a JOIN student b ON b.parent_ID = a.parent_ID JOIN users c ON a.parent_ID = c.ID WHERE c.status = 'Active' AND a.status = 'Active' AND c.address LIKE '%$str%'";
                        $query = $this->db->query($query_string);
                        $ads = makeArrayID($query->result());
                        $data['method'] = 'Student';
                        $data['type'] = 'location';
                        $data['allAds'] = $ads;
                        $newView = $this->load->view('browse/browse', $data, true);
                        $data = array(
                            'status' => 'success',
                            'msg' => 'Please wait collecting reasults',
                            'newView' => $newView,
                        );
                        echo json_encode($data);
                        exit();
                    } else {
                        $data = array(
                            'status' => 'error',
                            'msg' => 'Error Message'
                        );

                        echo json_encode($data);
                        exit();
                    }
                } else {
                    $data = array(
                        'status' => 'error',
                        'msg' => 'Error Message'
                    );

                    echo json_encode($data);
                    exit();
                }
            } elseif ($method == 'College') {
                //COLLEGE RELATED QURIES
                if (!empty($type) && !empty($str)) {

                    if ($type == 'course') {
                        $query_string = "SELECT a.ID FROM college_ad a JOIN college b ON b.parent_ID = a.parent_ID JOIN users c ON a.parent_ID = c.ID WHERE c.status = 'Active' AND a.status = 'Active' AND a.looking_for LIKE '%$str%'";
                        $query = $this->db->query($query_string);
                        $ads = makeArrayID($query->result());
                        $data['method'] = 'College';
                        $data['type'] = 'course';
                        $data['allAds'] = $ads;
                        $newView = $this->load->view('browse/browse', $data, true);
                        $data = array(
                            'status' => 'success',
                            'msg' => 'Please wait collecting reasults',
                            'newView' => $newView,
                        );
                        echo json_encode($data);
                        exit();
                    } elseif ($type == 'affiliation') {
                        $query_string = "SELECT a.ID FROM college_ad a JOIN college b ON b.parent_ID = a.parent_ID JOIN users c ON a.parent_ID = c.ID WHERE c.status = 'Active' AND a.status = 'Active' AND a.affiliation LIKE '%$str%'";
                        $query = $this->db->query($query_string);
                        $ads = makeArrayID($query->result());
                        $data['method'] = 'College';
                        $data['type'] = 'affiliation';
                        $data['allAds'] = $ads;
                        $newView = $this->load->view('browse/browse', $data, true);
                        $data = array(
                            'status' => 'success',
                            'msg' => 'Please wait collecting reasults',
                            'newView' => $newView,
                        );
                        echo json_encode($data);
                        exit();
                    } elseif ($type == 'fee') {
                        $query_string = "SELECT a.ID FROM college_ad a JOIN college b ON b.parent_ID = a.parent_ID JOIN users c ON a.parent_ID = c.ID WHERE c.status = 'Active' AND a.status = 'Active' AND a.tuition_fee_range LIKE '%$str%'";
                        $query = $this->db->query($query_string);
                        $ads = makeArrayID($query->result());
                        $data['method'] = 'College';
                        $data['type'] = 'fee';
                        $data['allAds'] = $ads;
                        $newView = $this->load->view('browse/browse', $data, true);
                        $data = array(
                            'status' => 'success',
                            'msg' => 'Please wait collecting reasults',
                            'newView' => $newView,
                        );
                        echo json_encode($data);
                        exit();
                    } elseif ($type == 'location') {
                        $query_string = "SELECT a.ID FROM college_ad a JOIN college b ON b.parent_ID = a.parent_ID JOIN users c ON a.parent_ID = c.ID WHERE c.status = 'Active' AND a.status = 'Active' AND c.address LIKE '%$str%'";
                        $query = $this->db->query($query_string);
                        $ads = makeArrayID($query->result());
                        $data['method'] = 'College';
                        $data['type'] = 'location';
                        $data['allAds'] = $ads;
                        $newView = $this->load->view('browse/browse', $data, true);
                        $data = array(
                            'status' => 'success',
                            'msg' => 'Please wait collecting reasults',
                            'newView' => $newView,
                        );
                        echo json_encode($data);
                        exit();
                    } else {
                        $data = array(
                            'status' => 'error',
                            'msg' => 'Error Message'
                        );

                        echo json_encode($data);
                        exit();
                    }
                } else {
                    $data = array(
                        'status' => 'error',
                        'msg' => 'Error Message'
                    );

                    echo json_encode($data);
                    exit();
                }
            } elseif ($method == 'Institute') {
                //INSTITUTE RELATED QURIES
                if (!empty($type) && !empty($str)) {

                    if ($type == 'course') {
                        $query_string = "SELECT a.ID FROM institute_ad a JOIN institute b ON b.parent_ID = a.parent_ID JOIN users c ON a.parent_ID = c.ID WHERE c.status = 'Active' AND a.status = 'Active' AND a.looking_for LIKE '%$str%'";
                        $query = $this->db->query($query_string);
                        $ads = makeArrayID($query->result());
                        $data['method'] = 'Institute';
                        $data['type'] = 'course';
                        $data['allAds'] = $ads;
                        $newView = $this->load->view('browse/browse', $data, true);
                        $data = array(
                            'status' => 'success',
                            'msg' => 'Please wait collecting reasults',
                            'newView' => $newView,
                        );
                        echo json_encode($data);
                        exit();
                    } elseif ($type == 'affiliation') {
                        $query_string = "SELECT a.ID FROM institute_ad a JOIN institute b ON b.parent_ID = a.parent_ID JOIN users c ON a.parent_ID = c.ID WHERE c.status = 'Active' AND a.status = 'Active' AND a.affiliation LIKE '%$str%'";
                        $query = $this->db->query($query_string);
                        $ads = makeArrayID($query->result());
                        $data['method'] = 'Institute';
                        $data['type'] = 'affiliation';
                        $data['allAds'] = $ads;
                        $newView = $this->load->view('browse/browse', $data, true);
                        $data = array(
                            'status' => 'success',
                            'msg' => 'Please wait collecting reasults',
                            'newView' => $newView,
                        );
                        echo json_encode($data);
                        exit();
                    } elseif ($type == 'fee') {
                        $query_string = "SELECT a.ID FROM institute_ad a JOIN institute b ON b.parent_ID = a.parent_ID JOIN users c ON a.parent_ID = c.ID WHERE c.status = 'Active' AND a.status = 'Active' AND a.tuition_fee_range LIKE '%$str%'";
                        $query = $this->db->query($query_string);
                        $ads = makeArrayID($query->result());
                        $data['method'] = 'Institute';
                        $data['type'] = 'fee';
                        $data['allAds'] = $ads;
                        $newView = $this->load->view('browse/browse', $data, true);
                        $data = array(
                            'status' => 'success',
                            'msg' => 'Please wait collecting reasults',
                            'newView' => $newView,
                        );
                        echo json_encode($data);
                        exit();
                    } elseif ($type == 'location') {
                        $query_string = "SELECT a.ID FROM institute_ad a JOIN institute b ON b.parent_ID = a.parent_ID JOIN users c ON a.parent_ID = c.ID WHERE c.status = 'Active' AND a.status = 'Active' AND c.address LIKE '%$str%'";
                        $query = $this->db->query($query_string);
                        $ads = makeArrayID($query->result());
                        $data['method'] = 'Institute';
                        $data['type'] = 'location';
                        $data['allAds'] = $ads;
                        $newView = $this->load->view('browse/browse', $data, true);
                        $data = array(
                            'status' => 'success',
                            'msg' => 'Please wait collecting reasults',
                            'newView' => $newView,
                        );
                        echo json_encode($data);
                        exit();
                    } else {
                        $data = array(
                            'status' => 'error',
                            'msg' => 'Error Message'
                        );

                        echo json_encode($data);
                        exit();
                    }
                } else {
                    $data = array(
                        'status' => 'error',
                        'msg' => 'Error Message'
                    );

                    echo json_encode($data);
                    exit();
                }
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => 'Error Message'
                );

                echo json_encode($data);
                exit();
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => 'Error Message'
            );

            echo json_encode($data);
            exit();
        }
    }

    //BROSER SEARCH FOR LOCATION
    public function browse_location() {
        $type = $this->input->post('type');
        $method = $this->input->post('method');
        $str = $this->input->post('val');

        if (!empty($method)) {
            if ($method == 'Student') {
                //STUDENT RELATED QURIES
                if (!empty($type) && !empty($str)) {

                    if ($type == 'course') {
                        $query_string = "SELECT a.ID FROM student_ad a JOIN student b ON b.parent_ID = a.parent_ID JOIN users c ON a.parent_ID = c.ID WHERE c.status = 'Active' AND a.status = 'Active' AND a.looking_for LIKE '%$str%'";
                        $query = $this->db->query($query_string);
                        $ads = makeArrayID($query->result());
                        $data['method'] = 'Student';
                        $data['type'] = 'course';
                        $data['allAds'] = $ads;
                        $newView = $this->load->view('browse/browse', $data, true);
                        $data = array(
                            'status' => 'success',
                            'msg' => 'Please wait collecting reasults',
                            'newView' => $newView,
                        );
                        echo json_encode($data);
                        exit();
                    } elseif ($type == 'affiliation') {
                        $query_string = "SELECT a.ID FROM student_ad a JOIN student b ON b.parent_ID = a.parent_ID JOIN users c ON a.parent_ID = c.ID WHERE c.status = 'Active' AND a.status = 'Active' AND a.affiliation LIKE '%$str%'";
                        $query = $this->db->query($query_string);
                        $ads = makeArrayID($query->result());
                        $data['method'] = 'Student';
                        $data['type'] = 'affiliation';
                        $data['allAds'] = $ads;
                        $newView = $this->load->view('browse/browse', $data, true);
                        $data = array(
                            'status' => 'success',
                            'msg' => 'Please wait collecting reasults',
                            'newView' => $newView,
                        );
                        echo json_encode($data);
                        exit();
                    } elseif ($type == 'fee') {
                        $query_string = "SELECT a.ID FROM student_ad a JOIN student b ON b.parent_ID = a.parent_ID JOIN users c ON a.parent_ID = c.ID WHERE c.status = 'Active' AND a.status = 'Active' AND a.tution_fee_range LIKE '%$str%'";
                        $query = $this->db->query($query_string);
                        $ads = makeArrayID($query->result());
                        $data['method'] = 'Student';
                        $data['type'] = 'fee';
                        $data['allAds'] = $ads;
                        $newView = $this->load->view('browse/browse', $data, true);
                        $data = array(
                            'status' => 'success',
                            'msg' => 'Please wait collecting reasults',
                            'newView' => $newView,
                        );
                        echo json_encode($data);
                        exit();
                    } elseif ($type == 'location') {
                        $query_string = "SELECT a.ID FROM student_ad a JOIN student b ON b.parent_ID = a.parent_ID JOIN users c ON a.parent_ID = c.ID WHERE c.status = 'Active' AND a.status = 'Active' AND c.address LIKE '%$str%'";
                        $query = $this->db->query($query_string);
                        $ads = makeArrayID($query->result());
                        $data['method'] = 'Student';
                        $data['type'] = 'location';
                        $data['allAds'] = $ads;
                        $newView = $this->load->view('browse/browse', $data, true);
                        $data = array(
                            'status' => 'success',
                            'msg' => 'Please wait collecting reasults',
                            'newView' => $newView,
                        );
                        echo json_encode($data);
                        exit();
                    } else {
                        $data = array(
                            'status' => 'error',
                            'msg' => 'Error Message'
                        );

                        echo json_encode($data);
                        exit();
                    }
                } else {
                    $data = array(
                        'status' => 'error',
                        'msg' => 'Error Message'
                    );

                    echo json_encode($data);
                    exit();
                }
            } elseif ($method == 'College') {
                //COLLEGE RELATED QURIES
                if (!empty($type) && !empty($str)) {

                    if ($type == 'course') {
                        $query_string = "SELECT a.ID FROM college_ad a JOIN college b ON b.parent_ID = a.parent_ID JOIN users c ON a.parent_ID = c.ID WHERE c.status = 'Active' AND a.status = 'Active' AND a.looking_for LIKE '%$str%'";
                        $query = $this->db->query($query_string);
                        $ads = makeArrayID($query->result());
                        $data['method'] = 'College';
                        $data['type'] = 'course';
                        $data['allAds'] = $ads;
                        $newView = $this->load->view('browse/browse', $data, true);
                        $data = array(
                            'status' => 'success',
                            'msg' => 'Please wait collecting reasults',
                            'newView' => $newView,
                        );
                        echo json_encode($data);
                        exit();
                    } elseif ($type == 'affiliation') {
                        $query_string = "SELECT a.ID FROM college_ad a JOIN college b ON b.parent_ID = a.parent_ID JOIN users c ON a.parent_ID = c.ID WHERE c.status = 'Active' AND a.status = 'Active' AND a.affiliation LIKE '%$str%'";
                        $query = $this->db->query($query_string);
                        $ads = makeArrayID($query->result());
                        $data['method'] = 'College';
                        $data['type'] = 'affiliation';
                        $data['allAds'] = $ads;
                        $newView = $this->load->view('browse/browse', $data, true);
                        $data = array(
                            'status' => 'success',
                            'msg' => 'Please wait collecting reasults',
                            'newView' => $newView,
                        );
                        echo json_encode($data);
                        exit();
                    } elseif ($type == 'fee') {
                        $query_string = "SELECT a.ID FROM college_ad a JOIN college b ON b.parent_ID = a.parent_ID JOIN users c ON a.parent_ID = c.ID WHERE c.status = 'Active' AND a.status = 'Active' AND a.tuition_fee_range LIKE '%$str%'";
                        $query = $this->db->query($query_string);
                        $ads = makeArrayID($query->result());
                        $data['method'] = 'College';
                        $data['type'] = 'fee';
                        $data['allAds'] = $ads;
                        $newView = $this->load->view('browse/browse', $data, true);
                        $data = array(
                            'status' => 'success',
                            'msg' => 'Please wait collecting reasults',
                            'newView' => $newView,
                        );
                        echo json_encode($data);
                        exit();
                    } elseif ($type == 'location') {
                        $query_string = "SELECT a.ID FROM college_ad a JOIN college b ON b.parent_ID = a.parent_ID JOIN users c ON a.parent_ID = c.ID WHERE c.status = 'Active' AND a.status = 'Active' AND c.address LIKE '%$str%'";
                        $query = $this->db->query($query_string);
                        $ads = makeArrayID($query->result());
                        $data['method'] = 'College';
                        $data['type'] = 'location';
                        $data['allAds'] = $ads;
                        $newView = $this->load->view('browse/browse', $data, true);
                        $data = array(
                            'status' => 'success',
                            'msg' => 'Please wait collecting reasults',
                            'newView' => $newView,
                        );
                        echo json_encode($data);
                        exit();
                    } else {
                        $data = array(
                            'status' => 'error',
                            'msg' => 'Error Message'
                        );

                        echo json_encode($data);
                        exit();
                    }
                } else {
                    $data = array(
                        'status' => 'error',
                        'msg' => 'Error Message'
                    );

                    echo json_encode($data);
                    exit();
                }
            } elseif ($method == 'Institute') {
                //INSTITUTE RELATED QURIES
                if (!empty($type) && !empty($str)) {

                    if ($type == 'course') {
                        $query_string = "SELECT a.ID FROM institute_ad a JOIN institute b ON b.parent_ID = a.parent_ID JOIN users c ON a.parent_ID = c.ID WHERE c.status = 'Active' AND a.status = 'Active' AND a.looking_for LIKE '%$str%'";
                        $query = $this->db->query($query_string);
                        $ads = makeArrayID($query->result());
                        $data['method'] = 'Institute';
                        $data['type'] = 'course';
                        $data['allAds'] = $ads;
                        $newView = $this->load->view('browse/browse', $data, true);
                        $data = array(
                            'status' => 'success',
                            'msg' => 'Please wait collecting reasults',
                            'newView' => $newView,
                        );
                        echo json_encode($data);
                        exit();
                    } elseif ($type == 'affiliation') {
                        $query_string = "SELECT a.ID FROM institute_ad a JOIN institute b ON b.parent_ID = a.parent_ID JOIN users c ON a.parent_ID = c.ID WHERE c.status = 'Active' AND a.status = 'Active' AND a.affiliation LIKE '%$str%'";
                        $query = $this->db->query($query_string);
                        $ads = makeArrayID($query->result());
                        $data['method'] = 'Institute';
                        $data['type'] = 'affiliation';
                        $data['allAds'] = $ads;
                        $newView = $this->load->view('browse/browse', $data, true);
                        $data = array(
                            'status' => 'success',
                            'msg' => 'Please wait collecting reasults',
                            'newView' => $newView,
                        );
                        echo json_encode($data);
                        exit();
                    } elseif ($type == 'fee') {
                        $query_string = "SELECT a.ID FROM institute_ad a JOIN institute b ON b.parent_ID = a.parent_ID JOIN users c ON a.parent_ID = c.ID WHERE c.status = 'Active' AND a.status = 'Active' AND a.tuition_fee_range LIKE '%$str%'";
                        $query = $this->db->query($query_string);
                        $ads = makeArrayID($query->result());
                        $data['method'] = 'Institute';
                        $data['type'] = 'fee';
                        $data['allAds'] = $ads;
                        $newView = $this->load->view('browse/browse', $data, true);
                        $data = array(
                            'status' => 'success',
                            'msg' => 'Please wait collecting reasults',
                            'newView' => $newView,
                        );
                        echo json_encode($data);
                        exit();
                    } elseif ($type == 'location') {
                        $query_string = "SELECT a.ID FROM institute_ad a JOIN institute b ON b.parent_ID = a.parent_ID JOIN users c ON a.parent_ID = c.ID WHERE c.status = 'Active' AND a.status = 'Active' AND c.address LIKE '%$str%'";
                        $query = $this->db->query($query_string);
                        $ads = makeArrayID($query->result());
                        $data['method'] = 'Institute';
                        $data['type'] = 'location';
                        $data['allAds'] = $ads;
                        $newView = $this->load->view('browse/browse', $data, true);
                        $data = array(
                            'status' => 'success',
                            'msg' => 'Please wait collecting reasults',
                            'newView' => $newView,
                        );
                        echo json_encode($data);
                        exit();
                    } else {
                        $data = array(
                            'status' => 'error',
                            'msg' => 'Error Message'
                        );

                        echo json_encode($data);
                        exit();
                    }
                } else {
                    $data = array(
                        'status' => 'error',
                        'msg' => 'Error Message'
                    );

                    echo json_encode($data);
                    exit();
                }
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => 'Error Message'
                );

                echo json_encode($data);
                exit();
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => 'Error Message'
            );

            echo json_encode($data);
            exit();
        }
    }

}
