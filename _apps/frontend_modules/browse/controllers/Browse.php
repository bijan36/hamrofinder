<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Browse extends Frontendcontroller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $this->template->load(get_template('common-right-bar-browse'), 'browse');
    }

}
