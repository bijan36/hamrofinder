<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li  id="search1" role="presentation" class="active"><a href="#studentBrowse" aria-controls="studentBrowse" role="tab" data-toggle="tab"><strong>Browse Student</strong></a></li>
    <li  id="search2" role="presentation"><a href="#collegeBrowse" aria-controls="collegeBrowse" role="tab" data-toggle="tab"><strong>Browse College</strong></a></li>
    <li  id="search3" role="presentation"><a href="#instituteBrowse" aria-controls="instituteBrowse" role="tab" data-toggle="tab"><strong>Browse Institute</strong></a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="studentBrowse">
        <div class="grey-bg standard-padding-little-wide browseBox">
            <!--PANNEL START-->
            <div class="panel-group" id="inneraccordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingAlphabet">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#inneraccordion" href="#byalphabet" aria-expanded="true" aria-controls="byalphabet">
                                All Advertise
                            </a>
                        </h4>
                    </div>
                    <div id="byalphabet" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingAlphabet">
                        <div class="panel-body">
                            <?php
                            //GETTING ALL STUDENTS BY NAME ALPHABET
                            $adsss = Modules::run('studentad/get_rows_order', 'looking_for', 'ASC', array('status' => ACTIVE));
                            if ($adsss):
                                ?>
                                <table id="dt1" class="table table-condensed table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Student Name</th>
                                            <th>Looking For</th>
                                            <th>Affiliation</th>
                                            <th>Admission</th>
                                            <th>Achieved Grade(%)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $sn = 1;
                                        foreach ($adsss as $ads):
                                            //check the user status before showing it
                                            $urow = Modules::run('user/get_row', $ads->parent_ID);
                                            if (can_show_ad($ads->ID, $urow->ID)) {
                                                if ($urow) {
                                                    if ($urow->status == ACTIVE) {
                                                        ?>
                                                        <tr>
                                                            <td><a href="<?php echo profile_url($urow->ID) ?>"><?php echo $urow->name; ?></a></td>
                                                            <td><a href="<?php echo get_ads_permalink($ads->ID, $urow->ID) ?>"><?php echo $ads->looking_for; ?></a></td>
                                                            <td><a href="<?php echo get_ads_permalink($ads->ID, $urow->ID) ?>"><?php echo $ads->affiliation; ?></a></td>
                                                            <td><a href="<?php echo get_ads_permalink($ads->ID, $urow->ID) ?>"><?php echo $ads->admission_within; ?> Days</a></td>
                                                            <td><?php echo $ads->achieved_grade; ?></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                            }
                                        endforeach;
                                        ?>
                                    </tbody>
                                </table>

                                <?php
                            endif;
                            ?>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingCourse">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#inneraccordion" href="#bycourse" aria-expanded="true" aria-controls="bycourse">
                                By Course
                            </a>
                        </h4>
                    </div>
                    <div id="bycourse" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingCourse">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select class="form-control browseRequest" data-type="course" data-method="Student" data-url="<?php echo base_url(); ?>" name="course">
                                            <?php echo Modules::run('studentad/get_course_list'); ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="area-course">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingAffiliation">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#inneraccordion" href="#byaffiliation" aria-expanded="false" aria-controls="byaffiliation">
                                By Affiliation
                            </a>
                        </h4>
                    </div>
                    <div id="byaffiliation" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingAffiliation">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select class="form-control browseRequest" data-type="affiliation" data-method="Student" data-url="<?php echo base_url(); ?>" name="affiliation">
                                            <?php echo Modules::run('studentad/get_affiliation_list'); ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="area-affiliation"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFee">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#inneraccordion" href="#byfee" aria-expanded="false" aria-controls="byfee">
                                By Fee (in Lakhs)
                            </a>
                        </h4>
                    </div>
                    <div id="byfee" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFee">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select class="form-control browseRequest" data-type="fee" data-method="Student" data-url="<?php echo base_url(); ?>" name="fee">
                                            <?php echo Modules::run('studentad/get_fee_list'); ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="area-fee"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingLocation">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#inneraccordion" href="#bylocation" aria-expanded="false" aria-controls="bylocation">
                                By Location
                            </a>
                        </h4>
                    </div>
                    <div id="bylocation" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingLocation">
                        <div class="panel-body">
                            <div class="row">



                                <div class="col-md-2">
                                    <div class="form-group">
                                        <?php
                                        $country = 'Select One';
                                        echo Modules::run('address/browse_get_select_box', 0, 'Country', 'name', 'browsecountry', STUDENT);
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group" id="provienceDiv-<?php echo STUDENT ?>">
                                        <?php
                                        $province = 'Select One';
                                        ?>
                                        <?php echo Modules::run('address/browse_get_select_box_find_parent', $country, 'Province', 'name', 'browseprovince', STUDENT); ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group" id="districtDiv-<?php echo STUDENT ?>">
                                        <?php
                                        $district = 'Select One';
                                        ?>
                                        <?php echo Modules::run('address/browse_get_select_box_find_parent', $province, 'District', 'name', 'browsedistrict', STUDENT); ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group" id="localLevelDiv-<?php echo STUDENT ?>">
                                        <?php
                                        $local_level = 'Select One';
                                        ?>
                                        <?php echo Modules::run('address/browse_get_select_box_find_parent', $district, 'Local Level', 'name', 'browselocal_level', STUDENT); ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group" id="wardnoDiv-<?php echo STUDENT ?>">
                                        <?php
                                        $ward_no = 'Select One';
                                        ?>
                                        <?php echo Modules::run('address/browse_get_select_box_find_parent', $local_level, 'Ward No', 'name', 'browseward_no', STUDENT); ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group" id="addressDiv-<?php echo STUDENT ?>">
                                        <?php
                                        $address = 'Select One';
                                        ?>
                                        <?php echo Modules::run('address/browse_get_select_box_find_parent', $ward_no, 'Address', 'name', 'browseaddress', STUDENT); ?>
                                    </div>
                                </div>
                                <!--<div class="col-md-12">
                                    <div class="form-group">
                                        <select class="form-control browseRequest" data-type="location" data-method="Student" data-url="<?php //echo base_url();            ?>" name="location">
                                <?php //echo Modules::run('user/get_student_address_select'); ?>
                                        </select>
                                    </div>
                                </div>-->

                                <div class="col-md-12">
                                    <div class="locations-<?php echo STUDENT ?>"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--PANNEL START-->
        </div>
    </div>








    <div role="tabpanel" class="tab-pane" id="collegeBrowse">
        <div class="grey-bg standard-padding-little-wide browseBox">


            <!--PANNEL START-->
            <div class="panel-group" id="inneraccordioncollege" role="tablist" aria-multiselectable="true">


                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingAlphabetCol">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#inneraccordioncollege" href="#byalphabetCol" aria-expanded="true" aria-controls="byalphabetCol">
                                All Advertise
                            </a>
                        </h4>
                    </div>
                    <div id="byalphabetCol" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingAlphabetCol">
                        <div class="panel-body">
                            <?php
                            //GETTING ALL STUDENTS BY NAME ALPHABET
                            $adsss = Modules::run('collegead/get_rows_order', 'looking_for', 'ASC', array('status' => ACTIVE));
                            if ($adsss):
                                ?>
                                <table id="dt2" class="table table-condensed table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>College Name</th>
                                            <th>Looking For</th>
                                            <th>Affiliation</th>
                                            <th>Admission</th>
                                            <th>Eligible Grade/GPA (%)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $sn = 1;
                                        foreach ($adsss as $ads):
                                            //check the user status before showing it
                                            $urow = Modules::run('user/get_row', $ads->parent_ID);
                                            if (can_show_ad($ads->ID, $urow->ID)) {
                                                if ($urow) {
                                                    if ($urow->status == ACTIVE) {
                                                        ?>
                                                        <tr>
                                                            <td><a href="<?php echo profile_url($urow->ID) ?>"><?php echo $urow->name; ?></a></td>
                                                            <td><a href="<?php echo get_ads_permalink($ads->ID, $urow->ID) ?>"><?php echo $ads->looking_for; ?></a></td>
                                                            <td><a href="<?php echo get_ads_permalink($ads->ID, $urow->ID) ?>"><?php echo $ads->affiliation; ?></a></td>
                                                            <td><a href="<?php echo get_ads_permalink($ads->ID, $urow->ID) ?>"><?php echo $ads->admission_within; ?> Days</a></td>
                                                            <td><?php echo $ads->grade_accepted; ?></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                            }
                                        endforeach;
                                        ?>
                                    </tbody>
                                </table>

                                <?php
                            endif;
                            ?>
                        </div>
                    </div>
                </div>




                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingCourseCol">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#inneraccordioncollege" href="#bycoursecolCol" aria-expanded="true" aria-controls="bycoursecolCol">
                                By Course
                            </a>
                        </h4>
                    </div>
                    <div id="bycoursecolCol" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingCourseCol">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select class="form-control browseRequest" data-type="course" data-method="College" data-url="<?php echo base_url(); ?>" name="course">
                                            <?php echo Modules::run('collegead/get_course_list'); ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="area-course-col">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingAffiliationCol">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#inneraccordioncollege" href="#byaffiliationcol" aria-expanded="false" aria-controls="byaffiliationcol">
                                By Affiliation
                            </a>
                        </h4>
                    </div>
                    <div id="byaffiliationcol" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingAffiliationCol">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select class="form-control browseRequest" data-type="affiliation" data-method="College" data-url="<?php echo base_url(); ?>" name="affiliation">
                                            <?php echo Modules::run('collegead/get_affiliation_list'); ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="area-affiliation-col"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFeeCol">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#inneraccordioncollege" href="#byfeecol" aria-expanded="false" aria-controls="byfeecol">
                                By Fee (in Lakhs)
                            </a>
                        </h4>
                    </div>
                    <div id="byfeecol" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFeeCol">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select class="form-control browseRequest" data-type="fee" data-method="College" data-url="<?php echo base_url(); ?>" name="fee">
                                            <?php echo Modules::run('collegead/get_fee_list'); ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="area-fee-col"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingLocationcol">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#inneraccordioncollege" href="#bylocationcol" aria-expanded="false" aria-controls="bylocationcol">
                                By Location
                            </a>
                        </h4>
                    </div>
                    <div id="bylocationcol" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingLocationcol">
                        <div class="panel-body">
                            <div class="row">

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <?php
                                        $country = 'Select One';
                                        echo Modules::run('address/browse_get_select_box', 0, 'Country', 'name', 'browsecountry', COLLEGE);
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group" id="provienceDiv-<?php echo COLLEGE ?>">
                                        <?php
                                        $province = 'Select One';
                                        ?>
                                        <?php echo Modules::run('address/browse_get_select_box_find_parent', $country, 'Province', 'name', 'browseprovince', COLLEGE); ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group" id="districtDiv-<?php echo COLLEGE ?>">
                                        <?php
                                        $district = 'Select One';
                                        ?>
                                        <?php echo Modules::run('address/browse_get_select_box_find_parent', $province, 'District', 'name', 'browsedistrict', COLLEGE); ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group" id="localLevelDiv-<?php echo COLLEGE ?>">
                                        <?php
                                        $local_level = 'Select One';
                                        ?>
                                        <?php echo Modules::run('address/browse_get_select_box_find_parent', $district, 'Local Level', 'name', 'browselocal_level', COLLEGE); ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group" id="wardnoDiv-<?php echo COLLEGE ?>">
                                        <?php
                                        $ward_no = 'Select One';
                                        ?>
                                        <?php echo Modules::run('address/browse_get_select_box_find_parent', $local_level, 'Ward No', 'name', 'browseward_no', COLLEGE); ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group" id="addressDiv-<?php echo COLLEGE ?>">
                                        <?php
                                        $address = 'Select One';
                                        ?>
                                        <?php echo Modules::run('address/browse_get_select_box_find_parent', $ward_no, 'Address', 'name', 'browseaddress', COLLEGE); ?>
                                    </div>
                                </div>

                                <!--<div class="col-md-12">
                                    <div class="form-group">
                                        <select class="form-control browseRequest" data-type="location" data-method="College" data-url="<?php //echo base_url();        ?>" name="location">
                                <?php //echo Modules::run('user/get_college_address_select'); ?>
                                        </select>
                                    </div>
                                </div>-->


                                <div class="col-md-12">
                                    <div class="locations-<?php echo COLLEGE ?>"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <!--PANNEL START-->



        </div>
    </div>





    <div role="tabpanel" class="tab-pane" id="instituteBrowse">
        <div class="grey-bg standard-padding-little-wide browseBox">


            <!--PANNEL START-->
            <div class="panel-group" id="inneraccordioninstitute" role="tablist" aria-multiselectable="true">


                <div class="panel panel-default">

                    <div class="panel-heading" role="tab" id="headingAlphabetCol">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#inneraccordioninstitute" href="#byalphabetColIns" aria-expanded="true" aria-controls="byalphabetColIns">
                                All Advertise
                            </a>
                        </h4>
                    </div>

                    <div id="byalphabetColIns" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingAlphabetCol">
                        <div class="panel-body">
                            <?php
                            //GETTING ALL STUDENTS BY NAME ALPHABET
                            $adsss = Modules::run('institutead/get_rows_order', 'looking_for', 'ASC', array('status' => ACTIVE));
                            if ($adsss):
                                ?>
                                <table id="dt2" class="table table-condensed table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Institute Name</th>
                                            <th>Looking For</th>
                                            <th>Affiliation</th>
                                            <th>Admission</th>
                                            <th>Eligible Grade/GPA (%)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $sn = 1;
                                        foreach ($adsss as $ads):
                                            //check the user status before showing it
                                            $urow = Modules::run('user/get_row', $ads->parent_ID);
                                            if (can_show_ad($ads->ID, $urow->ID)) {
                                                if ($urow) {
                                                    if ($urow->status == ACTIVE) {
                                                        ?>
                                                        <tr>
                                                            <td><a href="<?php echo profile_url($urow->ID) ?>"><?php echo $urow->name; ?></a></td>
                                                            <td><a href="<?php echo get_ads_permalink($ads->ID, $urow->ID) ?>"><?php echo $ads->looking_for; ?></a></td>
                                                            <td><a href="<?php echo get_ads_permalink($ads->ID, $urow->ID) ?>"><?php echo $ads->affiliation; ?></a></td>
                                                            <td><a href="<?php echo get_ads_permalink($ads->ID, $urow->ID) ?>"><?php echo $ads->admission_within; ?> Days</a></td>
                                                            <td><?php echo $ads->grade_accepted; ?></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                            }
                                        endforeach;
                                        ?>
                                    </tbody>
                                </table>

                                <?php
                            endif;
                            ?>
                        </div>
                    </div>
                </div>




                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingCourseColIns">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#inneraccordioncollege" href="#bycoursecolColIns" aria-expanded="true" aria-controls="bycoursecolCol">
                                By Course
                            </a>
                        </h4>
                    </div>
                    <div id="bycoursecolColIns" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingCourseColIns">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select class="form-control browseRequest" data-type="course" data-method="Institute" data-url="<?php echo base_url(); ?>" name="course">
                                            <?php echo Modules::run('institutead/get_course_list'); ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="area-course-ins">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingAffiliationColIns">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#inneraccordioncollege" href="#byaffiliationcolIns" aria-expanded="false" aria-controls="byaffiliationcol">
                                By Affiliation
                            </a>
                        </h4>
                    </div>
                    <div id="byaffiliationcolIns" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingAffiliationColIns">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select class="form-control browseRequest" data-type="affiliation" data-method="Institute" data-url="<?php echo base_url(); ?>" name="affiliation">
                                            <?php echo Modules::run('institutead/get_affiliation_list'); ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="area-affiliation-ins"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFeeColIns">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#inneraccordioncollege" href="#byfeecolIns" aria-expanded="false" aria-controls="byfeecol">
                                By Fee (in Lakhs)
                            </a>
                        </h4>
                    </div>
                    <div id="byfeecolIns" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFeeColIns">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select class="form-control browseRequest" data-type="fee" data-method="Institute" data-url="<?php echo base_url(); ?>" name="fee">
                                            <?php echo Modules::run('institutead/get_fee_list'); ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="area-fee-ins"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingLocationcolIns">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#inneraccordioncollege" href="#bylocationcolIns" aria-expanded="false" aria-controls="bylocationcol">
                                By Location
                            </a>
                        </h4>
                    </div>
                    <div id="bylocationcolIns" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingLocationcolIns">
                        <div class="panel-body">
                            <div class="row">



                                <div class="col-md-2">
                                    <div class="form-group">
                                        <?php
                                        $country = 'Select One';
                                        echo Modules::run('address/browse_get_select_box', 0, 'Country', 'name', 'browsecountry', INSTITUTE);
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group" id="provienceDiv-<?php echo INSTITUTE ?>">
                                        <?php
                                        $province = 'Select One';
                                        ?>
                                        <?php echo Modules::run('address/browse_get_select_box_find_parent', $country, 'Province', 'name', 'browseprovince', INSTITUTE); ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group" id="districtDiv-<?php echo INSTITUTE ?>">
                                        <?php
                                        $district = 'Select One';
                                        ?>
                                        <?php echo Modules::run('address/browse_get_select_box_find_parent', $province, 'District', 'name', 'browsedistrict', INSTITUTE); ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group" id="localLevelDiv-<?php echo INSTITUTE ?>">
                                        <?php
                                        $local_level = 'Select One';
                                        ?>
                                        <?php echo Modules::run('address/browse_get_select_box_find_parent', $district, 'Local Level', 'name', 'browselocal_level', INSTITUTE); ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group" id="wardnoDiv-<?php echo INSTITUTE ?>">
                                        <?php
                                        $ward_no = 'Select One';
                                        ?>
                                        <?php echo Modules::run('address/browse_get_select_box_find_parent', $local_level, 'Ward No', 'name', 'browseward_no', INSTITUTE); ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group" id="addressDiv-<?php echo INSTITUTE ?>">
                                        <?php
                                        $address = 'Select One';
                                        ?>
                                        <?php echo Modules::run('address/browse_get_select_box_find_parent', $ward_no, 'Address', 'name', 'browseaddress', INSTITUTE); ?>
                                    </div>
                                </div>



                                <div class="col-md-12">
                                    <div class="locations-<?php echo INSTITUTE; ?>"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <!--PANNEL START-->



        </div>
    </div>

</div>