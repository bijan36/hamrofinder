<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class College extends Frontendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('College_m', 'fcollege');
    }

    //USER DASHBOARD
    public function index() {
        redirect('site');
        exit();
    }

    //GET SAFE DATA BY USER ID
    public function get_row($ID) {
        $userRow = $this->fcollege->get($ID);
        return $userRow ? $userRow : false;
    }

    //GET SAFE DATA BY USER PARENT
    public function get_row_by_parent($parent_ID) {
        $userRow = $this->fcollege->get_by(array('parent_ID' => $parent_ID));
        return $userRow ? $userRow : false;
    }

    //UPDAET META
    public function update_student_meta($ID, $data) {
        $row = $this->fcollege->get_by(array('parent_ID' => $ID));
        if ($row) {
            //FOND ROW UPDATE HERE
            if ($this->fcollege->update($row->ID, $data)) {
                $data = array('status' => 'success', 'msg' => 'Profile updated successfully');
                return $data;
            } else {
                $data = array('status' => 'error', 'msg' => 'Unable to update profile.');
                return $data;
            }
        } else {
            //NOT FOUND ROW INSERT HERE
            if ($this->fcollege->insert($data)) {
                $data = array('status' => 'success', 'msg' => 'Profile updated successfully');
                return $data;
            } else {
                $data = array('status' => 'error', 'msg' => 'Unable to update profile.');
                return $data;
            }
        }
    }

    //ADVANCE SERCH FOR THE ID
    public function search_ad_by_facilities($facilities_array) {

        $resArray = array();
        if (!empty($facilities_array)) {

            foreach ($facilities_array as $fa):

                $search_term = "%" . $fa . "%";
                $sql = "SELECT * FROM table WHERE field LIKE ? ";
                $query = $this->db->query($sql, array($search_term));
                $res = $query->result();

            endforeach;


            $arg = array(
                'college_bus' => $college_bus,
                'status' => ACTIVE
            );
            $ads = $this->fcollege->get_many_by($arg);
            if ($ads) {
                foreach ($ads as $ad):
                    if (can_show_ad($ad->ID, $ad->parent_ID)) {
                        $resArray[] = $ad->ID;
                    }
                endforeach;
            }
        }
        return $resArray;
    }

    //GET THE FACILITY VALUES BY ID
    public function get_facility_values($parent_ID) {
        if (!empty($parent_ID)) {
            $row = $this->fcollege->get_by(array('parent_ID' => $parent_ID));
            if ($row) {
                if (!empty($row->facilities_in_college)) {
                    return strtolower($row->facilities_in_college);
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    //GET THE SECURITY VALUES BY ID
    public function get_securities_values($parent_ID) {
        if (!empty($parent_ID)) {
            $row = $this->fcollege->get_by(array('parent_ID' => $parent_ID));
            if ($row) {
                return strtolower($row->security_issues);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    //GET THE ADDITIONALS VALUES BY ID
    public function get_additionals_values($parent_ID) {
        if (!empty($parent_ID)) {
            $row = $this->fcollege->get_by(array('parent_ID' => $parent_ID));
            if ($row) {
                return strtolower($row->additional_features);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}
