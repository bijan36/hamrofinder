<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Institute_programe extends Frontendcontroller {
    /*
     * @AUTHOR: COCONUTCREATION.COM
     * @PROGRAMMER: SURAJ BAJRACHARYA
     * @PARAM: USER CONTROLLER
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('Institute_programe_m', 'finstpro');
    }

    //USER DASHBOARD
    public function index() {
        redirect('site');
        exit();
    }

    //ADDING THE MARKS
    public function add() {

        //IF NOT LOGIN
        if (!is_user_login()) {
            $data = array(
                'status' => 'error',
                'msg' => 'Unable to process, please contact site administrator.'
            );
            echo json_encode($data);
            exit();
        }

        //IF NOT COLLEGE USER
        if (!is_user_institute()) {
            $data = array(
                'status' => 'error',
                'msg' => 'Unable to process, please contact site administrator.'
            );
            echo json_encode($data);
            exit();
        }

        $id = $this->session->userdata('userID');
        //SOME FORM VALIDATION

        $program_title = $this->input->post('program_title');
        $affiliation = $this->input->post('affiliation');

        if (empty($program_title)) {
            $this->form_validation->set_rules('program_title_alt', 'Course title', 'required|trim');
            if ($this->form_validation->run() == TRUE) {
                $program_title = $this->input->post('program_title_alt');
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => validation_errors()
                );
                echo json_encode($data);
                exit();
            }
        }

        if (empty($affiliation)) {
            $this->form_validation->set_rules('affiliation_alt', 'Course certified by', 'required|trim');
            if ($this->form_validation->run() == TRUE) {
                $affiliation = $this->input->post('affiliation_alt');
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => validation_errors()
                );
                echo json_encode($data);
                exit();
            }
        }
       

        //other medetory fields 
        $this->form_validation->set_rules('program_duration', 'Course duration', 'required|trim');
        $this->form_validation->set_rules('class_held_at', 'Class held at', 'required|trim');
        //$this->form_validation->set_rules('admission_fee', 'Registration fee', 'required|trim');
        $this->form_validation->set_rules('total_course_fee', 'Course fee', 'required|trim');
        $this->form_validation->set_rules('fee_collect', 'Fee collect method', 'required|trim');

        if ($this->form_validation->run() == TRUE) {

            $dataInsert = array(
                'parent_ID' => $id,
                'offered_programe' => $this->input->post('offered_programe'),
                'program_title' => $program_title,
                'affiliation' => $affiliation,
                'program_duration' => $this->input->post('program_duration'),
                'class_held_at' => $this->input->post('class_held_at'),
                'admission_fee' => $this->input->post('admission_fee'),
                'total_course_fee' => $this->input->post('total_course_fee'),
                'other_fee' => $this->input->post('other_fee'),
                'fee_collect' => $this->input->post('fee_collect'),
            );

            if ($this->finstpro->insert($dataInsert)) {
                $lastID = $this->finstpro->get_last_id();

                $idArray['idArray'] = array($lastID);

                $newView = $this->load->view('institute_program_loop', $idArray, true);

                $data = array(
                    'status' => 'success',
                    'newView' => $newView,
                    'msg' => 'Program added successfully',
                );
                echo json_encode($data);
                exit();
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => 'Unable to add subject and marks please try again'
                );
                echo json_encode($data);
                exit();
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => validation_errors()
            );
            echo json_encode($data);
            exit();
        }
    }

    public function remove() {
        $id = $this->input->post('id');
        $parent_id = $this->input->post('parent');

        if (!empty($id)) {
            $row = $this->finstpro->get_by(array('ID' => $id, 'parent_ID' => $parent_id));
            if ($row) {
                if ($this->finstpro->delete($id)) {
                    $data = array(
                        'status' => 'success',
                        'msg' => 'Program offered has been removed permanently.'
                    );
                    echo json_encode($data);
                    exit();
                } else {
                    $data = array(
                        'status' => 'error',
                        'msg' => 'Unable to remove it!'
                    );
                    echo json_encode($data);
                    exit();
                }
            } else {
                $data = array(
                    'status' => 'error',
                    'msg' => 'Nothing found to remove!!'
                );
                echo json_encode($data);
                exit();
            }
        } else {
            $data = array(
                'status' => 'error',
                'msg' => 'Unable to remove it'
            );
            echo json_encode($data);
            exit();
        }
    }

    //GET SAFE DATA BY USER ID
    public function get_row($ID) {
        $userRow = $this->finstpro->get($ID);
        return $userRow ? $userRow : false;
    }

    //GET SAFE DATA BY USER PARENT
    public function get_by_parent_level($parent_ID) {
        $userRows = $this->finstpro->get_many_by(array('parent_ID' => $parent_ID));
        return $userRows ? $userRows : false;
    }

}
