<?php
$allAdPrices = Modules::run('college_ad_pricing/get_price_list');

//IF THERE ARE ANY AD PRICE SETS
if (!empty($allAdPrices)) {
    ?>

    <div class="col-md-12" style="margin-top: 50px; padding-top: 50px; border-top: 1px solid #e2e1e1;">
        <span class="bigTextLightBlue">
            Better Your <strong>Search.</strong> Be registered.<br/>
            Choose <strong>Better Ad Plan for College.</strong>
            <p>To know more about the Ad plans and tariff please go through our FAQ section or simply contact us.</p>
        </span>
    </div>

    <div class="col-md-12">
        <div class="row">
            <?php foreach ($allAdPrices as $adPrice): ?>
                <div class="col-md-3">
                    <div class="planBox">
                        <div class="planBox-header"><span>Buy</span> <?php echo $adPrice->number_ads; ?> Ad</div>
                        <div><strong>NPR <?php echo $adPrice->per_year_fee; ?></strong><span class="small">/per year</span></div>
                        <div style="margin-top: 15px;">
                            <?php
                            //ONLY SHOW THE BUTTONS IF USER NOT IN "REGISTERED" PLAN
                            if ($this->session->userdata('userLevel') != 'Registered') {
                                ?>
                                <form action = "<?php echo $this->config->item('esewa')['esewa_url'] ?>" method="POST">
                                    <input value="<?php echo $adPrice->per_year_fee; ?>" name="tAmt" type="hidden">
                                    <input value="<?php echo $adPrice->per_year_fee; ?>" name="amt" type="hidden">
                                    <input value="0" name="txAmt" type="hidden">
                                    <input value="0" name="psc" type="hidden">
                                    <input value="0" name="pdc" type="hidden">
                                    <input value="<?php echo $this->config->item('esewa')['merchant_id'] ?>" name="scd" type="hidden">
                                    <input value="<?php echo unique_hash($this->session->userdata('userType'), $adPrice->ID); ?>" name="pid" type="hidden">
                                    <input value="<?php echo site_url('ipn/subs_success'); ?>" type="hidden" name="su">
                                    <input value="<?php echo site_url('ipn/subs_fail'); ?>" type="hidden" name="fu">
                                    <input  class="btn blueButton" value="Buy Now" type="submit">
                                </form>    
                                <?php
                            } else {
                                ?>
                                <a href="#" class="btn blueButton buyit">Buy Now</a>
                                <?php
                            }
                            ?>
                        </div>
                    </div>        
                </div>        
            <?php endforeach; ?>
        </div>
    </div>

    <?php
}
?>
<script>
    $('.buyit').click(function (e) {
        e.preventDefault();
        swal({
            title: "Sorry!",
            text: 'Please upgrade you plan before purchase the ads.',
            type: "error",
            timer: 5000,
            confirmButtonColor: "#fdb316",
            showConfirmButton: true
        });
    });
</script>